/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   SongGuangHui
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
{
    if(total<=firstRow)
    {
        $(".pullUpIcon").hide();
        $(".pullUpLabel").html("没有更多啦");
    }
});
var num = 1; //默认页面显示4条数据              num可变

function pullUpAction (){
    var type = $('.search_type').find('.active').data('type');
    var result_div = $('.'+type);
    var kw = $('[name=kw]').val();
    var sourceNode = result_div.find('ul'); // 获得被克隆的节点对象
    /* console.log(total+'----'+num*firstRow);*/
    if(total > num*firstRow){
        $.get('/FrontIndex/search', {"firstRow":num*firstRow, type:type, kw:kw}, function(r, textStatus)
        {
            // console.log(data);
            var data = r.data
            if (data != null)
            {
                var len = data.length;
                var html = '';
                if(type == 'tiezi'){
                
                for (var i = 0; i < data.length; i++) {
                    html += '<li>\
                        <a href="/FrontChannel/post_detail/post_id/'+data[i].post_id+'">\
                          <div class="user">\
                            <img src="'+data[i].post_user.headimgurl+'" alt=""><span>'+data[i].post_user.nickname+'</span>\
                          </div>\
                          <div class="text">\
                            <h3>'+data[i].title+'</h3>\
                            <p>'+data[i].content+'</p>\
                          </div>\
                        </a>\
                      </li>'
                }
            }else if(type == 'pindao'){
                  for (var i = 0; i < data.length; i++) {
                    var rank_name = '';
                    if(data[i].user_channel_rank != ''){
                      rank_name += '<i class="level">'+data[i].user_channel_rank+'</i>'
                    }
                    html += '<li>\
                        <a href="/FrontChannel/post_list/channel_id/'+data[i].channel_id+'">\
                          <img src="'+data[i].icon+'" alt="">\
                          <span class="channel_name">'+data[i].channel_name+'</span>'
                          +rank_name+
                          '<span class="new_num">新帖'+data[i].new_tie+'</span>\
                        </a>\
                      </li>'
                }
            }else if(type == 'people'){
                  for (var i = 0; i < data.length; i++) {
                    html += '<li>\
                        <a href="/FrontUserCenter/others_space/user_id/'+data[i].user_id+'">\
                          <img src="'+data[i].headimgurl+'" alt=""><span>'+data[i].nickname+'</span>\
                        </a>\
                      </li>'
                }
            }
            
            $('.no-cout').hide();
            $('#load_wrapper').show();
            // $('#loading').hide();
            result_div.show().find('ul').append(html);
                // sourceNode.append(html);
                setTimeout(function(){
                    myScroll.refresh();
                },500);
                num ++;
            }
        }, 'json');
    }
    else
    {   myScroll.refresh();
        $('.pullUpIcon').hide();
        $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  pullUpEl = document.getElementById('pullUp');
  pullUpOffset = pullUpEl.offsetHeight;
//	pullUpAction();
  myScroll = new iScroll('load_wrapper', {
//     useTransition: true,
    topOffset: pullDownOffset,
    onRefresh: function () {
      if (pullUpEl.className.match('loading')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
      }
    },
    onScrollMove: function () {
       if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
        pullUpEl.className = 'flip';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
        this.maxScrollY = this.maxScrollY;
      } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
        this.maxScrollY = pullUpOffset;
      }
    },
    onScrollEnd: function () {
      if (pullUpEl.className.match('flip')) {
        pullUpEl.className = 'loading';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
        pullUpAction(); // ajax call?
      }
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
