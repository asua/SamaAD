$('#post').on('click', function(){
    if(posting){
        return;
    }
    posting = true;

    var content = $('#content').val();

    var imgs = $('#img_url').val();

    if(content == "" && imgs == ""){
        toast('内容处内容、图片必须填写一项', 'fail');
        posting = false;
        return;
    }
    
    $.ajax({
        url:'/FrontChannel/my_answer',
        type:'post',
        data:{content:content, imgs:imgs, post_id:post_id},
        success:function(r){
            if(r.code == 0){
                toast(r.msg, 'succ');
                setTimeout(function(){
                    if(!is_wechat()){
                        native_listen('back_reload');
                    }else{
                        history.go(-1);
                    }  
                },2000);
                
            }else{
                toast(r.msg, 'fail');
                posting = false;
            }
        }
    })
})