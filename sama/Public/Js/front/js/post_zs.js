

//七牛多图上传
 var uploader2 = new plupload.Uploader({
        runtimes : 'html5',
        browse_button : 'upload_btn2', //触发文件选择对话框的按钮，为那个元素id
        url : 'http://up.qiniu.com/', //服务器端的上传页面地址
        flash_swf_url : '/Public/Js/plupload_js/Moxie.swf',//swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url : '/Public/Js/plupload_js/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        filters : {
            // 设置大小
            max_file_size : '10mb',
            // 允许上传的类型
           // mime_types: [
           //   {title : "Image files", extensions : "png,jpeg,gif,bmp,jpg"},
           // ]
        },
        multipart: true,
          //设置post传给七牛的token
          multipart_params: {
           'token': uptoken, 
          },
          //修改post字段为七牛的file
          file_data_name: 'file',
          init: {
                PostInit: function() {

                    console.log("upload init");
                    console.log($('.moxie-shim').children())
                    if(!is_ios()){
                        $("input[type=file]").attr({
                            'capture':'camera',
                            'accept':'image/*'
                        });
                    }
                },
                FilesAdded: function(up, files) {

                    //选择文件后直接上传， 或可改成点击按钮上传
                    uploader2.start();
                },
                UploadProgress: function(up, file) {

                },
                FileUploaded: function(up, file, info) {
                   console.log(info)
                    // var res = $.parseJSON(info);  
                    // var res = eval('"'+info+'"');  
                    var res = info.response;
                    console.log(res)
                    var r = eval('('+res+')');
                    console.log(r);
                    var sourceLink = image_domain + r.key; //获取上传成功后的文件的Url  
                    var  html = '<li><img src="'+sourceLink+'?imageView2/1/w/200/h/200/imageslim" alt="" data-link="'+sourceLink+'"><span class="del_img" onclick="del_img(this)"></span></li>';
                            
                            var container = $('#img_list2');

                            container.append(html);
                            console.log('finish');
                            var imgs = $('#upload_btn2').parent().siblings('.img_url').val();
                            imgs += ','+sourceLink;
                            $('#upload_btn2').parent().siblings('.img_url').val(imgs);
                },
                UploadComplete: function(up, files) {
                    // Called when all files are either uploaded or failed
                    console.log('[完成]');
                },
                Error: function(up, err) {
                    // alert(err.response);
                }
            } 
    }); 
 uploader2.init();
function del_img2(obj){
    var obj_init = $(obj);
    obj_init.parent().remove();
    var imgs = '';
    $('#img_list2 li').each(function(){
        imgs += ','+$(this).find('img').data('link');
    })
    $('#img_url2').val(imgs)
}

    $('.money').on('click',function(){
        $(this).addClass('active').siblings().removeClass('active')
    })
    $('.select_friend').on('click',function(){
        $('.my_follow').show();
    })

$('.customize').blur(function(){
    var price = $(this).val();
    if(price != ''){
        $('.zdy_money').html(price+'元');
        $('.zdy_money').attr('data-money', price);
        $('.money').removeClass('active');
        $('.customize').hide();
        $('.zdy_money').addClass('active').show();

    }else{
        $('.money').eq(0).addClass('active');
        
    }
})

$('.zdy_money').on('click', function(){
    $(this).hide();
    $('.customize').show().focus();
})

$('#post').on('click',function(){
            $('.confirm_box').show();

        })
$('#confirmCancel').on('click',function(){
            $('.confirm_box').hide();
        })

$('#confirmOk').on('click', function(){
    $('.confirm_box').hide();
    if(posting){
        return;
    }
    posting = true;

    var title = $('#title').val();

    var content = $('#content').val();

    var imgs = $('#img_url').val();

    if(title == "" && content == "" && imgs == ""){
        toast('展示内容处标题、内容、图片必须填写一项', 'fail');
        posting = false;
        return;
    }

    var hide_content = $('#hide_content').val();
    var hide_imgs = $('#img_url2').val();
    if(hide_content == '' && hide_imgs == ''){
        toast('隐藏内容处内容、图片必须填写一项', 'fail');
        posting = false;
        return;
    }
    // console.log(222)
    var hide_price = $('.set_price').find('.active').data('money');
    // console.log(hide_price)
    if(parseFloat(hide_price) <= 0){
        toast('请填写正确的价格', 'fail');
        posting = false;
        return;
    }
    
    content = content.replace(/\n/g,'<br>');
    hide_content = hide_content.replace(/\n/g,'<br>');

    $.ajax({
        url:'/FrontChannel/post_praise',
        type:'post',
        data:{title:title, content:content, imgs:imgs, hide_content:hide_content, hide_imgs:hide_imgs, hide_price:hide_price, channel_id:channel_id},
        success:function(r){
            if(r.code == 0){
                toast(r.msg, 'succ');
                setTimeout(function(){
                    if(!is_wechat()){
                        native_listen('follow_user');//刷新我的页面，刷新页面相同，则用同一事件
                        native_listen('back_reload');
                    }else{
                        history.go(-1);
                    }     
                },2000);
                
            }else{
                toast(r.msg, 'fail');
                posting = false;
            }
        }
    })
})