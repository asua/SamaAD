/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   zhangfengjie
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
  {
    if(total<=firstRow)
    {
      $(".pullUpIcon").hide();
      $(".pullUpLabel").html("没有更多了");

    }
  });
var num = 1; //默认页面显示4条数据              num可变
function del_native(){
	native_listen('no_loading');
}
function pullDownAction(){
	setTimeout(function(){
		del_native();
		window.location.reload();
	},300);
	window.onload = function(){
		myScroll.refresh();
	}
}
function pullUpAction (){
	var sourceNode = $("#comment_list"); // 获得被克隆的节点对象
	if(total > num*firstRow){
      $.post('/FrontChannel/post_comment', {"firstRow":num*firstRow, post_id:post_id, see_lz:see_lz}, function(data, textStatus) 
       {
        if (data != '' || data != null)
        { 
          var len = data.length;
          var html = '';
          for (i = 0; i < len; i++)
          {
          	var lz = '';
          	if(parseInt(data[i].is_lz) == 1){
          		lz = '<span class="lz_tag">楼主</span>'
          	}
          	var title_list = '';
          	if(data[i].title_list != '' && data[i].title_list != null){
	          	for (var j = 0; j < data[i].title_list.length ; j++) {
	          		title_list += '<i class="tag" style="background-color:'+data[i].title_list[j].title_color+'">'+data[i].title_list[j].title_name+'</i>'
	          	}
	        }

          	var imgs = '';
          	if(data[i].imgs != '' && data[i].imgs != null){
          		for (var j = 0; j < data[i].imgs.length; j++) {
	          		imgs += '<li><img src="/Public/Images/img/no-img.png" data-src="'+data[i].imgs[j]+'?imageView2/1/w/200/h/200/imageslim" alt="" class="img" data-row="'+j+'"  data-img="'+data[i].imgs[j]+'?imageMogr2/auto-orient/imageslim"></li>'
	          	}
          	}
          	

          	var reply_comment_list = '';
          	if(data[i].reply_comment_list != '' && data[i].reply_comment_list != null){
          		reply_comment_list += '<div class="comment_cont"><ul>'
	          	for (var j = 0; j < data[i].reply_comment_list.length; j++) {
	          		reply_comment_list += '<li><span class="nick">'+data[i].reply_comment_list[j].nickname+'：</span>'+data[i].reply_comment_list[j].content+'</li>'
	          	}
	          	reply_comment_list += '</ul></div>';
	        }
	        var is_praise = '';
	        if(data[i].is_praise == 1){
	        	is_praise = 'hover';
	        }

          	html += '<div class="comment_item">\
						<div class="comment_top">\
							<a href="/FrontUserCenter/others_space/user_id/'+data[i].user_id+'" class="avatar native_go">\
								<img data-src="'+data[i].comment_user.headimgurl+'?imageView2/1/w/200/h/200/imageslim"  src="/Public/Images/img/no-img.png" alt="">'
								+lz+	
							'</a>\
							<div class="comment_user">\
								<div class="user_info">\
									<a class="nickname native_go" href="/FrontUserCenter/others_space/user_id/'+data[i].user_id+'">'+data[i].comment_user.nickname+'</a>'
									+title_list+	
								'</div>\
								<div class="other clearfix">\
									<span class="floor">'+data[i].floor+'楼</span>\
									<span class="time">'+data[i].addtime+'</span>\
									<span class="zan_num">'+data[i].praise_num+'</span>\
									<i class="icon_zan '+is_praise+'" data-comment_id="'+data[i].post_comment_id+'"></i>\
									<i class="icon_pl" data-comment_id="'+data[i].post_comment_id+'" data-nickname="'+data[i].comment_user.nickname+'"></i>\
								</div>\
								<div class="text">'
									+data[i].content+
								'</div>\
								<ul class="imgbox clearfix">'
									+imgs+
								'</ul>\
							</div>\
						</div>'
								+reply_comment_list+
							'</div>'
          	

          }
          sourceNode.append(html);
          var liw = $('.imgbox li').width();
		  $('.imgbox li').height(liw);

		  	 var lazyLoadImg = new LazyLoadImg({
		        el: document.querySelector('#load_wrapper'),
		        mode: 'default', //默认模式，将显示原图，diy模式，将自定义剪切，默认剪切居中部分
		        time: 200, // 设置一个检测时间间隔
		        complete: true, //页面内所有数据图片加载完成后，是否自己销毁程序，true默认销毁，false不销毁
		        position: { // 只要其中一个位置符合条件，都会触发加载机制
		          top: 0, // 元素距离顶部
		          right: 0, // 元素距离右边
		          bottom: 0, // 元素距离下面
		          left: 0 // 元素距离左边
		        },
		        before: function () { // 图片加载之前执行方法
		          
		        },
		        success: function (el) { // 图片加载成功执行方法
		          // el.classList.add('success')
		        },
		        error: function (el) { // 图片加载失败执行方法
		          // el.src = './images/error.png'
		        }	
		      })

		  	 
          setTimeout(function(){
            myScroll.refresh();
          },200);

          num ++;
        }
      }, 'json');      
    }
    else
    {
      myScroll.refresh();
      $('.pullUpIcon').hide();
      $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  pullUpEl = document.getElementById('pullUp');
  	pullUpOffset = pullUpEl.offsetHeight;
	pullDownEl = document.getElementById('pullDown');
	pullDownOffset = pullDownEl.offsetHeight;
  	myScroll = new iScroll('load_wrapper', {
       	useTransition: true,
	    topOffset: pullDownOffset,
	    onRefresh: function () {
	      	if (pullDownEl.className.match('loading')) {
				pullDownEl.className = '';
			}else if(pullUpEl.className.match('loading')) {
		        pullUpEl.className = '';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
		   	}
	    },
	    onScrollMove: function () {

	       	if(this.y >5  && !pullDownEl.className.match('flip')) {
			    pullDownEl.className = 'flip';
			    this.minScrollY = 0;
		   	}else if(this.y < 5 && pullDownEl.className.match('flip')) {
			    pullDownEl.className = '';
			    this.minScrollY = -pullDownOffset;
		   	}else if(this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
		        pullUpEl.className = 'flip';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
		        this.maxScrollY = this.maxScrollY;
	      	}else if(this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
		        pullUpEl.className = '';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
		        this.maxScrollY = pullUpOffset;
	      	}
	     
	       
	    },
	    onScrollEnd: function () {
	    	if (pullDownEl.className.match('flip')) {
				pullDownEl.className = 'loading';
				pullDownAction();
			}else if (pullUpEl.className.match('flip')) {
		        pullUpEl.className = 'loading';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
		        pullUpAction(); // ajax call?
	      	}
	     
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);

