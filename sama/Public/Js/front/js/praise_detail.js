
var buy_obj;
//购买
$('.buy_btn').on('click',function(){
	// $('#recharge').show();
	if(buying){
		return;
	}
	buy_obj = $(this);
	buying = true;
	item_id = $(this).data('item_id');
		
	$.ajax({
		url:'/FrontChannel/check_praise_pay',
		type:'post',
		data:{item_id:item_id},
		success:function(r){
			if(r.code == 1){
				toast(r.msg, 'fail');
				buying = false;
				buy_obj = null;
			}else if(r.code == 2){
				$('.balance').find('span').html(r.left_money + '元');
				if(isios == 1){//ios充值
					native_listen('ios_recharge', {});
				}else{
					$('#recharge').show();
				}
				buying = false;
				buy_obj = null;
			}else if(r.code == 5){
				tan_tishi(r.msg);
				setTimeout(function(){
					buying = false;
					if(!is_wechat()){
                        native_listen('to_jump', {url:'/FrontUserCenter/modify_pay_psw'});
                    }else{
                        history.go(-1);
                    }
				}, 2000)
			}else{
				var npw_pay = check_npw(r.price);
				console.log(npw_pay)
				//是否开启免密
				if(npw_pay){
					praise_buy(item_id);
				}else{
					$('#pwd_box').show();
					buying = false;
				}
			}
		}
	})

	
})

function praise_buy(item_id){
	$.ajax({
		url:'/FrontChannel/praise_buy',
		type:'post',
		data:{item_id:item_id},
		// async:false,
		beforeSend:function(){
			$('#loading').show()
		},
		success:function(r){
			$('#loading').hide();
			if(r.code == 0){
				toast(r.msg, 'succ');
				// setTimeout(function(){
				// 	location.reload();
				// }, 2000);
				if(r.hide != 'null' && r.hide != null){
					var html = '';
					var imgs = '';
					if(buy_obj.hasClass('main')){
						buy_obj.parent().remove();

						if(r['hide'].hide_imgs != '' && r['hide'].hide_imgs != 'null' && r['hide'].hide_imgs != null){
							for (var i = 0; i < r['hide'].hide_imgs.length; i++) {
								imgs += '<img  src="'+r['hide'].hide_imgs[i]+'?imageMogr2/auto-orient/thumbnail/800x/imageslim" data-src="'+r['hide'].hide_imgs[i]+'?imageMogr2/auto-orient/thumbnail/800x/imageslim" alt="" data-row="'+i+'"  data-img="'+r['hide'].hide_imgs[i]+'?imageMogr2/auto-orient/thumbnail/800x/imageslim" src="/Public/Images/img/no-img.png">'
							}
						}
						html += '<div class="hide_content" >\
									<p>'
										+r['hide'].content+
									'</p>\
									<div class="post_imgs">'
										+imgs+
									'</div></div>';
						$('.post_content').append(html);
						myScroll.refresh();
					}else{
						if(r['hide'].hide_imgs != '' && r['hide'].hide_imgs != 'null' && r['hide'].hide_imgs != null){
							for (var i = 0; i < r['hide'].hide_imgs.length; i++) {
								imgs += '<li ><img  src="'+r['hide'].hide_imgs[i]+'?imageView2/1/w/200/h/200/imageslim" data-src="'+r['hide'].hide_imgs[i]+'?imageMogr2/auto-orient/thumbnail/800x/imageslim" alt="" data-row="'+i+'"  data-img="'+r['hide'].hide_imgs[i]+'?imageMogr2/auto-orient/thumbnail/800x/imageslim" src="/Public/Images/img/no-img.png"></li>'
							}
						}
						html += '<div class="show_cont">\
									<div class="text">'
										+r['hide'].content+
									'</div>\
									<ul class="imgbox clearfix">'
									+imgs+						
									'</ul>\
								</div>';
						buy_obj.parent().before(html);
						var liw = $('.imgbox li').width();
						$('.imgbox li').height(liw);
						buy_obj.parent().remove();
						myScroll.refresh();
					}
				}
				$('#pwd_box').hide();
				$('#pwd_ipt').val('');
			}else{
				toast(r.msg, 'fail');
			}
			buy_obj = null;
			buying = false;
		}
	})
}