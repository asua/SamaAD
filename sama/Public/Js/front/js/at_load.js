/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   SongGuangHui
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
{
    if(total<=firstRow)
    {
        $(".pullUpIcon").hide();
        $(".pullUpLabel").html("——&nbsp;&nbsp;没有更多啦&nbsp;&nbsp;——");
    }
});
var num = 1; //默认页面显示4条数据              num可变

function pullUpAction (){
    var sourceNode = $(".at_list").eq(0); // 获得被克隆的节点对象
    /* console.log(total+'----'+num*firstRow);*/
    if(total > num*firstRow){
        $.post('/FrontIndex/reply_me', {"firstRow":num*firstRow}, function(data, textStatus)
        {
            console.log(data);
            if (data != 'failure')
            {
                var len = data.length;
                var html = '';
                for (var i = 0; i < len; i++)
                {
                    html+='<li class="">\
                                <a href="'+data[i].message_link+'" class="clearfix native_go">\
                                    <img src="'+data[i].send_headimg+'" alt=""><span class="user_name">'+data[i].send_nickname+'</span><span class="text">'+data[i].message_contents+'</span><span class="time">'+data[i].addtime+'</span>\
                                </a>\
                            </li>'

              }
                sourceNode.append(html);
                setTimeout(function(){
                    myScroll.refresh();
                },500);
                num ++;
            }
        }, 'json');
    }
    else
    {
        $('.pullUpIcon').hide();
        $('.pullUpLabel').html('——&nbsp;&nbsp;没有更多啦&nbsp;&nbsp;——');
    }
}
function loaded() {
  pullUpEl = document.getElementById('pullUp');
  pullUpOffset = pullUpEl.offsetHeight;
//	pullUpAction();
  myScroll = new iScroll('load_wrapper', {
//     useTransition: true,
    topOffset: pullDownOffset,
    onRefresh: function () {
      if (pullUpEl.className.match('loading')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
      }
    },
    onScrollMove: function () {
       if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
        pullUpEl.className = 'flip';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
        this.maxScrollY = this.maxScrollY;
      } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
        this.maxScrollY = pullUpOffset;
      }
    },
    onScrollEnd: function () {
      if (pullUpEl.className.match('flip')) {
        pullUpEl.className = 'loading';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
        pullUpAction(); // ajax call?
      }
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
