

$('.customize').blur(function(){
    var price = $(this).val();
    if(price != ''){
        $('.zdy_money').html(price+'元');
        $('.zdy_money').attr('data-money', price);
        $('.money').removeClass('active');
        $('.customize').hide();
        $('.zdy_money').addClass('active').show();

    }else{
        $('.money').eq(0).addClass('active');
    }
})

$('.zdy_money').on('click', function(){
    $(this).hide();
    $('.customize').show().focus();
})

$('#post').on('click',function(){
            $('.confirm_box').show();

        })
$('#confirmCancel').on('click',function(){
            $('.confirm_box').hide();
        })

$('#confirmOk').on('click', function(){
    $('.confirm_box').hide();
    if(posting){
        return;
    }
    posting = true;

    var title = $('#title').val();

    var content = $('#content').val();

    var imgs = $('#img_url').val();

    // if(title == "" && content == "" && imgs == ""){
    //     toast('展示内容处标题、内容、图片必须填写一项', 'fail');
    //     posting = false;
    //     return;
    // }

    if(title == ""){
        toast('展示内容处标题必须填写', 'fail');
        posting = false;
        return;
    }

    var price = $('.set_price').find('.active').data('money');
    // console.log(hide_price)
    if(parseFloat(price) <= 0){
        toast('请填写正确的价格', 'fail');
        posting = false;
        return;
    }

    content = content.replace(/\n/g,'<br>');
    // $('.submit_review').show();
    $.ajax({
        url:'/FrontChannel/post_crowdfunding',
        type:'post',
        data:{title:title, content:content, imgs:imgs, channel_id:channel_id, price:price},
        success:function(r){
            if(r.code == 0){
                native_listen('follow_user');//刷新我的页面，刷新页面相同，则用同一事件
                toast(r.msg, 'succ');
                setTimeout(function(){
                    $('.submit_review').show();    
                },2000);
                
            }else{
                toast(r.msg, 'fail');
                posting = false;
            }
        }
    })
})