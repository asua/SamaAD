/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   zhangfengjie
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
  {
    if(total<=firstRow)
    {
      $(".pullUpIcon").hide();
      $(".pullUpLabel").html("没有更多了");
    }
  });
var num = 1; //默认页面显示4条数据              num可变
function del_native(){
	native_listen('no_loading');
}
function pullDownAction(){
	setTimeout(function(){
		del_native();
		window.location.reload();
	},300);
	window.onload = function(){
		myScroll.refresh();
	}
}
function pullUpAction (){
	var sourceNode = $("#new_list"); // 获得被克隆的节点对象
	if(total > num*firstRow){
      $.post('/FrontChannel/post_list', {"firstRow":num*firstRow,channel_id:channel_id}, function(data, textStatus) 
       {
        if (data != '' || data != null)
        { 
          var len = data.length;
          var html = '';
          for (i = 0; i < len; i++)
          {
          	var user_title = '';
          	if(data[i].post_user_title != '' && data[i].post_user_title != null){
          		for (var j = 0; j < data[i].post_user_title.length; j++) {
	          		user_title += '<i class="tag" style="background-color: '+data[i].post_user_title[j].title_color+'">'+data[i].post_user_title[j].title_name+'</i>';
	          	}
          	}
          	

          	var imgs = '';
          	if(data[i].imgs != ''  && data[i].imgs != null){
	          	for (var j = 0; j < data[i].imgs.length; j++) {
	          		if(j < 5){
	          			imgs += '<li><img data-src="'+data[i].imgs[j].img+'?imageView2/1/w/200/h/200/imageslim" src="/Public/Images/img/no-img.png" alt=""></li>';
	          		}else{
	          			imgs += '<li><div class="mask"><div class="txt">包含共'+data[i].img_count+'张图</div></div><img data-src="'+data[i].imgs[j].img+'?imageView2/1/w/200/h/200/imageslim" alt="" src="/Public/Images/img/no-img.png"></li>'
	          		}
	          	}
          	}

          	html +='<div class="home_item">\
					<div class="user_info clearfix">\
						<a href="/FrontUserCenter/others_space/user_id/'+data[i].user_id+'" class="avatar native_go">\
							<img data-src="'+data[i].post_user.headimgurl+'?imageView2/1/w/200/h/200/imageslim" alt="" src="/Public/Images/img/no-img.png">\
						</a>\
						<div class="user_cont">\
							<a class="nickname native_go" href="/FrontUserCenter/others_space/user_id/'+data[i].user_id+'">'+data[i].post_user.nickname+'</a>'
							+user_title+
						'</div>\
					</div>\
					<a href="/FrontChannel/post_detail/post_id/'+data[i].post_id+'" class="native_go">\
						<div class="item_title">'
						+data[i].title+
						'</div>\
						<div class="item_desc">'
							+data[i].content+
						'</div>\
						<ul class="item_imgbox clearfix">'
						+imgs+
						'</ul>\
						<div class="item_bot clearfix">\
							<span class="time fl">'+data[i].addtime+'</span>\
							<span class="channel fl">'+channel_name+'</span>\
							<span class="fr pl">评论&nbsp;'+ data[i].comment_num+'</span><span class="fr see">查看&nbsp;'+data[i].clickdot+'</span>\
						</div>\
					</a>\
				</div>'
          	

          }
          sourceNode.append(html);
          var imgw = $('.item_imgbox li').width();
		  imgh = $('.item_imgbox li').height(imgw);

		    if(!is_ios()){
				$('.home_list .tag').css('padding-top','1px');
			  }
		  var lazyLoadImg = new LazyLoadImg({
			el: document.querySelector('#load_wrapper'),
			mode: 'default', //默认模式，将显示原图，diy模式，将自定义剪切，默认剪切居中部分
			time: 250, // 设置一个检测时间间隔
			complete: true, //页面内所有数据图片加载完成后，是否自己销毁程序，true默认销毁，false不销毁
			position: { // 只要其中一个位置符合条件，都会触发加载机制
			  top: 0, // 元素距离顶部
			  right: 0, // 元素距离右边
			  bottom: 0, // 元素距离下面
			  left: 0 // 元素距离左边
			},
			before: function () { // 图片加载之前执行方法
			  
			},
			success: function (el) { // 图片加载成功执行方法
			  // el.classList.add('success')
			},
			error: function (el) { // 图片加载失败执行方法
			  // el.src = './images/error.png'
			}
			})

          setTimeout(function(){
            myScroll.refresh();
          },500);
          num ++;
        }
      }, 'json');      
    }
    else
    {
      myScroll.refresh();
      $('.pullUpIcon').hide();
      $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  pullUpEl = document.getElementById('pullUp');
  	pullUpOffset = pullUpEl.offsetHeight;
	pullDownEl = document.getElementById('pullDown');
	pullDownOffset = pullDownEl.offsetHeight;
  	myScroll = new iScroll('load_wrapper', {
       	useTransition: true,
	    topOffset: pullDownOffset,
	    onRefresh: function () {
	      	if (pullDownEl.className.match('loading')) {
				pullDownEl.className = '';
			}else if(pullUpEl.className.match('loading')) {
		        pullUpEl.className = '';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
		   	}
	    },
	    onScrollMove: function () {

	       	if(this.y >5  && !pullDownEl.className.match('flip')) {
			    pullDownEl.className = 'flip';
			    this.minScrollY = 0;
		   	}else if(this.y < 5 && pullDownEl.className.match('flip')) {
			    pullDownEl.className = '';
			    this.minScrollY = -pullDownOffset;
		   	}else if(this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
		        pullUpEl.className = 'flip';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
		        this.maxScrollY = this.maxScrollY;
	      	}else if(this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
		        pullUpEl.className = '';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
		        this.maxScrollY = pullUpOffset;
	      	}
	     
	       
	    },
	    onScrollEnd: function () {
	    	if (pullDownEl.className.match('flip')) {
				pullDownEl.className = 'loading';
				pullDownAction();
			}else if (pullUpEl.className.match('flip')) {
		        pullUpEl.className = 'loading';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
		        pullUpAction(); // ajax call?
	      	}
	     
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);

