/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   zhangfengjie
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
  {
    if(total<=firstRow)
    {
      $(".pullUpIcon").hide();
      $(".pullUpLabel").html("没有更多了");
    }
  });
var num = 1; //默认页面显示4条数据              num可变
function del_native(){
	native_listen('no_loading');
}
function pullDownAction(){
	setTimeout(function(){
		del_native();
		window.location.reload();
	},300);
	window.onload = function(){
		myScroll.refresh();
	}
}

var c = 11;

function pullUpAction (){
	var sourceNode = $(".home_list"); // 获得被克隆的节点对象
	if(total > num*firstRow){
        $.post('/FrontIndex/sama_index', {"firstRow":num*firstRow,}, function(data, textStatus)
        {
            console.log(data)
            console.log(1111)
            if (data != '' || data != null)
            {
				var len = data.length;
				var html = '';
				for (i = 0; i < len; i++){
				var user_title = ''
				 if(data[i].post_user_title != null && data[i].post_user_title != ''){
					for (var j = 0; j < data[i].post_user_title.length; j++) {
						  user_title += '<i class="tag" style="background-color: '+data[i].post_user_title[j].title_color+'">'+data[i].post_user_title[j].title_name+'</i>';
					}
				 }

				  var imgs = '';
				  var blur = '';
				  for (var j = 0; j < data[i].imgs.length; j++) {
					  if(parseInt(data[i].imgs[j].is_show) == 0){
						  blur = 'class="blur"';
					  }

					  if(j < 5){
						  imgs += '<li '+blur+'><img data-src="'+data[i].imgs[j].img+'?imageView2/1/w/200/h/200/imageslim" alt="" src="/Public/Images/img/no-img.png"></li>';
					  }else{
						  imgs += '<li '+blur+'><div class="mask"><div class="txt">包含共'+data[i].img_count+'张图</div></div><img data-src="'+data[i].imgs[j].img+'?imageView2/1/w/200/h/200/imageslim" alt="" src="/Public/Images/img/no-img.png"></li>'
					  }
				  }

			  // wangtao 2017.9.18 -strat
			  if((i+1)%10 == 0){
                  html += '<div class="home_item">\
                      <a href="' + data[c].link + '">\
                      <div class="user_info clearfix" style="margin-bottom: 5px;">\
                      <div  class="avatar native_go" style="margin-right: 0.5rem;">\
                      <img alt="photo" src="/Public/Images/common/sama.png">\
                      </div>\
                      <div class="user_cont" style="display: inline-block; margin-left: 0;">\
                      <a class="nickname native_go" href="/FrontUserCenter/others_space/user_id/62202">官方娘推荐</a>\
                      </div>\
                      <div style="display: inline-block; float: right;">\
                      <span style="border: 1px solid #FF6600; color: #FF6600; font-size: 0.7rem; padding: 0.15rem 0.5rem;">推荐</span>\
                      </div>\
                      </div>\
                      <div class="native_go" style="height: 227.8px">\
                      <div class="swiper-slide">\
                      <a href="' + data[c].link + '">\
                      <img src="/Public/Images/img/no-img.png" alt="Photo" data-src="' + data[c++].pic + '"?imageMogr2/auto-orient/imageslim">\
                      </a>\
                      </div>\
                      </div>\
                      </a>\
                      </div>';
                  if (c >= len){
                      c = 10;
                  }
			  }
			  if(i > 9){
                  break;
              }else {
			  // wangtao 2017.9.18 -end


                  html += '<div class="home_item">\
						<div class="user_info clearfix">\
							<a href="/FrontUserCenter/others_space/user_id/' + data[i].user_id + '" class="avatar native_go">\
								<img src="' + data[i].post_user.headimgurl + '" alt="">\
							</a>\
							<div class="user_cont">\
								<a class="nickname native_go" href="/FrontUserCenter/others_space/user_id/' + data[i].user_id + '">' + data[i].post_user.nickname + '</a>'
                      + user_title +
                      '</div>\
                  </div>\
                    <a href="/FrontChannel/post_detail/post_id/' + data[i].post_id + '"  class="native_go">\
							<div class="item_title">'
                      + data[i].title +
                      '</div>\
                        <div class="item_desc">'
                      + data[i].content +
                      '</div>\
                      <ul class="imgbox clearfix">'
                      + imgs +
                      '</ul>\
                      <div class="item_bot clearfix">\
                          <span class="time fl">' + data[i].addtime + '</span>\
								<span class="time fl">' + data[i].channel_name + '</span>\
								<span class="time fr">评论 ' + data[i].comment_num + '</span>\
								<span class="time fr">查看 ' + data[i].clickdot + '</span>\
								</div>\
							</a>\
				</div>';
              }
          }
          sourceNode.append(html);
          var imgw = $('.imgbox li').width();
		  imgh = $('.imgbox li').height(imgw);

		  if(!is_ios()){
			$('.home_list .tag').css('padding-top','1px');
		  }

          setTimeout(function(){
            myScroll.refresh();
          },100);
          num ++;
        }
      }, 'json');      
    }
    else
    {
      myScroll.refresh();
      $('.pullUpIcon').hide();
      $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  pullUpEl = document.getElementById('pullUp');
  	pullUpOffset = pullUpEl.offsetHeight;
	pullDownEl = document.getElementById('pullDown');
	pullDownOffset = pullDownEl.offsetHeight;
  	myScroll = new iScroll('load_wrapper', {
       	useTransition: true,
	    topOffset: pullDownOffset,
	    onRefresh: function () {
	      	if (pullDownEl.className.match('loading')) {
				pullDownEl.className = '';
			}else if(pullUpEl.className.match('loading')) {
		        pullUpEl.className = '';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
		   	}
	    },
	    onScrollMove: function () {

	       	if(this.y >5  && !pullDownEl.className.match('flip')) {
			    pullDownEl.className = 'flip';
			    this.minScrollY = 0;
		   	}else if(this.y < 5 && pullDownEl.className.match('flip')) {
			    pullDownEl.className = '';
			    this.minScrollY = -pullDownOffset;
		   	}else if(this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
		        pullUpEl.className = 'flip';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
		        this.maxScrollY = this.maxScrollY;
	      	}else if(this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
		        pullUpEl.className = '';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
		        this.maxScrollY = pullUpOffset;
	      	}
	     
	       
	    },
	    onScrollEnd: function () {
	    	if (pullDownEl.className.match('flip')) {
				pullDownEl.className = 'loading';
				pullDownAction();
			}else if (pullUpEl.className.match('flip')) {
		        pullUpEl.className = 'loading';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
		        pullUpAction(); // ajax call?
	      	}
	     
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);

