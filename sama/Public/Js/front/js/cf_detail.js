//查看参与人员
$('.icon_zhankai').on('click',function(){
	$('.look_people').show();
	//列表滚动
	var list = new iScroll('people_wrapper');
	setTimeout(function(){
	          list.refresh();
	      },500);
})
//关闭参与人员
$('#guanbi').on('click',function(){
	$('.look_people').hide();
})


// 关闭
$('.btn_close').on('click',function(){
	$('.dialog').hide();
})
//我也参加
$('#canjia').on('click',function(){
	$('.dialog').show();
})

//我也参加
$('.btn_bz').on('click',function(){
	$('.dialog').show();
})

$('#support_num').on('input', function(){
/* var val = $(this).val();
 if(!val || val.length<1)
{
	$(this).val(1);
	
}*/
	var support_num = parseInt($(this).val());
	console.log(support_num)
	// support_num = support_num > 0 ? support_num : 1;
	if(!support_num || support_num <= 0){
		support_num = 0;
		// $(this).val(1);
	}
	var money = support_num * one_support_money;
	$('.support_money').html('<strong>¥'+money+'</strong>');
})


$('.zhifu').on('click', function(){
	if(buying){
		return;
	}
	buying = true;
	var support_num =  parseInt($('#support_num').val());
	if(!support_num || support_num <= 0){
		support_num = 1;
		// $(this).val(1);
	}
	$.ajax({
		url:'/FrontChannel/check_cf_pay',
		type:'post',
		data:{support_num:support_num, post_id:post_id},
		success:function(r){
			if(r.code == 1){
				toast(r.msg, 'fail');
				// tan_tishi(r.msg);
				buying = false;
			}else if(r.code == 2){
				$('.dialog').hide();
				$('.balance').find('span').html(r.left_money + '元');
				if(isios == 1){//ios充值
					native_listen('ios_recharge', {});
				}else{
					$('#recharge').show();
				}
				buying = false;
			}else if(r.code == 5){
				$('.dialog').hide();
				tan_tishi(r.msg);
				setTimeout(function(){
					buying = false;
					if(!is_wechat()){
                        native_listen('to_jump', {url:'/FrontUserCenter/modify_pay_psw'});
                    }else{
                        history.go(-1);
                    }
				}, 2000)
			}else{
				var npw_pay = check_npw(r.price);
				console.log(npw_pay)
				//是否开启免密
				if(npw_pay){
					$('.dialog').hide();
					cf_buy(post_id);
				}else{
					$('.dialog').hide();
					$('#pwd_box').show();
					buying = false;
				}
			}
		}
	})
})


function cf_buy(post_id){
	var support_num =  parseInt($('#support_num').val());
	$.ajax({
		url:'/FrontChannel/cf_buy',
		type:'post',
		data:{post_id:post_id, support_num:support_num},
		// async:false,
		beforeSend:function(){
			$('#loading').show()
		},
		success:function(r){
			$('#loading').hide();
			if(r.code == 0){
				console.log(r.code)
				toast(r.msg, 'succ');
				setTimeout(function(){
					location.reload();
				}, 2000);
			}else{
				toast(r.msg, 'fail');
				buying = false;
			}
		}
	})
}