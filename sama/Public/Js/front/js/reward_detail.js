var caina = false;
$('.caina').on('click', function(){
	if(caina){
		return;
	}
	caina = true;

	var answer = $(this).data('answer_id');
	if(answer == ''){
		toast('采纳答案失败', 'fail');
		caina = false;
		return;
	}

	$.ajax({
		url:'/FrontChannel/answer_caina',
		type:'post',
		data:{answer_id:answer, post_id:post_id},
		success:function(r){
			if(r.code == 1){
				toast(r.msg, 'fail');
				caina = false;
			}else{
				toast(r.msg, 'succ');
				setTimeout(function(){
					location.reload();
				}, 2000);
			}
		}
	})
	// console.log(2222)
	
})

$('.refund').on('click', function(){
	// console.log(5555555)
	$('.dialog').show();
})
//退款切换
$('.dialog_row').on('click',function(){
	$(this).addClass('active').siblings().removeClass('active');
})
// 关闭退款
$('.btn_close').on('click',function(){
	console.log(222)
	$('.dialog').hide();
})
var refunding = false;
$('.tijiao').on('click', function(){
	
	if(refunding){
		return;
	}
	refunding = true;
	var reason_id = $('.sq_refund').find('.active').data('reason_id');
	console.log(reason_id)
	$.ajax({
		url:'/FrontChannel/reward_refund',
		type:'post',
		data:{post_id:post_id, reason_id:reason_id},
		success:function(r){
			if(r.code == 1){
				toast(r.msg, 'fail');
				refunding = false;
			}else{
				toast(r.msg, 'succ');
				setTimeout(function(){
					location.href="/Index/submit_review"
				}, 2000)
			}
		}
	})
})

//围观
$('.look_buy').on('click',function(){
	// $('#recharge').show();
	if(buying){
		return;
	}
	buying = true;
		
	$.ajax({
		url:'/FrontChannel/check_reward_pay',
		type:'post',
		data:{post_id:post_id},
		success:function(r){
			if(r.code == 1){
				// toast(r.msg, 'fail');
				tan_tishi(r.msg);
			}else if(r.code == 2){
				$('.balance').find('span').html(r.left_money + '元');
				if(isios == 1){//ios充值
					native_listen('ios_recharge', {});
				}else{
					$('#recharge').show();
				}
			}else if(r.code == 5){
				tan_tishi(r.msg);
				setTimeout(function(){
					buying = false;
					if(!is_wechat()){
                        native_listen('to_jump', {url:'/FrontUserCenter/modify_pay_psw'});
                    }else{
                        history.go(-1);
                    }
				}, 2000)
			}else{
				var npw_pay = check_npw(r.price);
				console.log(npw_pay)
				//是否开启免密
				if(npw_pay){
					look(post_id);
				}else{
					$('#pwd_box').show();
					buying = false;
				}
			}
		}
	})

	
})

function look(post_id){
	$.ajax({
		url:'/FrontChannel/reward_buy',
		type:'post',
		data:{post_id:post_id},
		// async:false,
		beforeSend:function(){
			$('#loading').show()
		},
		success:function(r){
			$('#loading').hide();
			if(r.code == 0){
				toast(r.msg, 'succ');
				setTimeout(function(){
					location.reload();
				}, 2000);
			}else{
				toast(r.msg, 'fail');
			}
		}
	})
}