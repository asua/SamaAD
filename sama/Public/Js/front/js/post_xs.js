$('.money').on('click',function(){
	$(this).addClass('active').siblings().removeClass('active')
})

$('.customize').blur(function(){
    var price = $(this).val();
    if(price != ''){
        $('.zdy_money').html(price+'元');
        $('.zdy_money').attr('data-money', price);
        $('.money').removeClass('active');
        $('.customize').hide();
        $('.zdy_money').addClass('active').show();

    }else{
        $('.money').eq(0).addClass('active');
    }
})

$('.zdy_money').on('click', function(){
    $(this).hide();
    $('.customize').show().focus();
})

//选择我关注的人
   $('.user_list li').on('click',function(){
   
        $(this).toggleClass('active');  
   })
   //确认
   $('.ensure').on('click',function(){
        // var has_active = $('.user_list li.active').length;
        // if(has_active>5){
        //     tan_tishi('最多不能超过5个用户');
        //     // alert('最多不能超过5个用户');
        // }else{
            var sel_u = '';
            $('.user_list li.active').each(function(){
              sel_u += '<li data-user_id="'+$(this).data('user_id')+'"><img src="'+$(this).find('img').attr('src')+'" alt=""><span class="nickname">'+$(this).find('.nickname').html()+'</span><i class="icon_delete"></i></li>';
            })
            $('.select_list').find('li').remove();
            $('.select_list').append(sel_u);
             $('.my_follow').hide();
        // }
   })
   //取消
   $('.cancel').on('click',function(){
        $('.my_follow').hide();
   })
   //删除选中的人
    $('.select_list').on('click','.icon_delete',function(){
        $(this).parent().remove()
    })

    $('.select_friend').on('click', function(){
    	$('.my_follow').show();
    })

$('#post').on('click',function(){
            $('.confirm_box').show();

        })
$('#confirmCancel').on('click',function(){
            $('.confirm_box').hide();
        })

$('#confirmOk').on('click', function(){
    $('.confirm_box').hide();
  if(posting){
    return;
  }
  posting = true;
  var title = $('#title').val();

    var content = $('#content').val();

    var imgs = $('#img_url').val();

    // if(title == "" && content == "" && imgs == ""){
    //     toast('展示内容处标题、内容、图片必须填写一项', 'fail');
    //     posting = false;
    //     return;
    // }

    if(title == ""){
        toast('展示内容处标题必须填写', 'fail');
        posting = false;
        return;
    }

    var price = $('.set_price').find('.active').data('money');
    // console.log(hide_price)
    if(parseFloat(price) <= 0){
        toast('请填写正确的悬赏金额', 'fail');
        posting = false;
        return;
    }

    var sel_users = {};
    //邀请的用户
    $('.select_list').find('li').each(function(index){
      sel_users[index] = $(this).data('user_id');
    })

    content = content.replace(/\n/g,'<br>');
    // console.log(sel_users);return;
    // $('.submit_review').show();
    $.ajax({
        url:'/FrontChannel/post_reward',
        type:'post',
        data:{title:title, content:content, imgs:imgs, channel_id:channel_id, price:price, sel_users:sel_users},
        success:function(r){
            if(r.code == 0){
                toast(r.msg, 'succ');
                setTimeout(function(){
                    if(!is_wechat()){
                        native_listen('follow_user');//刷新我的页面，刷新页面相同，则用同一事件
                        native_listen('back_reload');
                    }else{
                        history.go(-1);
                    }  
                },2000);
                
            }else{
                toast(r.msg, 'fail');
                posting = false;
            }
        }
    })
})