$('#post').on('click',function(){
            $('.confirm_box').show();

        })
$('#confirmCancel').on('click',function(){
            $('.confirm_box').hide();
        })



$('#confirmOk').on('click', function(){
    $('.confirm_box').hide();
    if(posting){
        return;
    }
    posting = true;
    var title = $('#title').val();
    // if(title == ""){
    //     tan_tishi('请输入文章标题');
    //     posting = false;
    //     return;
    // }

    var content = $('#content').val();
    // if(content == ""){
    //     tan_tishi('请输入文章内容');
    //     posting = false;
    //     return;
    // }

    var imgs = $('#img_url').val();

    // if(title == "" && content == "" && imgs == ""){
    //     toast('标题、内容、图片必须填写一项', 'fail');
    //     posting = false;
    //     return;
    // }

    if(title == ""){
        toast('标题必须填写一项', 'fail');
        posting = false;
        return;
    }

    content = content.replace(/\n/g,'<br>');

    $.ajax({
        url:'/FrontChannel/post_normal',
        type:'post',
        data:{title:title, content:content, imgs:imgs, channel_id:channel_id},
        success:function(r){
            if(r.code == 0){
                toast(r.msg, 'succ');
                setTimeout(function(){
                    if(!is_wechat()){
                        native_listen('back_reload');
                    }else{
                        history.go(-1);
                    }    
                },2000);
                
            }else{
                toast(r.msg, 'fail');
                posting = false;
            }
        }
    })
})