//由input_icon的data-id设置,firstWord作为回复的last_word
var commentInpVal,commentDataId,firstWord,commentFlag = false;
var commentId,replyUserId;
//点击评论图标
$("body").on("focus",".input_icon",function(){
	if($(".submit_btn")){
		$(".submit_btn").hide();
	}
	var _this = $(this);
	$(".comment_input_wrap").removeClass("comment_hide");
	$(".comment_input").focus();
	setTimeout(function(){
		_this.blur();
	},100);
	commentFlag = true;
	commentDataId = $(this).attr("data-id");
	commentCont = $(this).parents(".circle_state_zan ").siblings(".comment_cont").html();
});
//回复评论
$("body").on("focus",".show_keybord",function(e){
	if($(".submit_btn")){
		$(".submit_btn").hide();
	}
	commentFlag = false;
	var _this = $(this);
	$(".comment_input_wrap").removeClass("comment_hide");
	$(".comment_input").focus();
	setTimeout(function(){
		_this.blur();
	},100);
	commentDataId = $(this).parents(".comment_cont").attr("data-id");
	firstWord = $(this).siblings(".list_title ").find(".first_word").text();
	commentCont = $(this).parents(".comment_cont").html();
	commentId = $(this).data('comment_id');
	replyUserId = $(this).data('user_id');
});
//点击发送评论
$(".comment_send").on("click",function(){
	$(this).addClass("no_send");
	commentInpVal = $(".comment_input").val();
	if(commentInpVal!=""){
		if($(".submit_btn")){
			$(".submit_btn").show();
		}
		$(".comment_input").blur();
		$(".comment_input_wrap").addClass("comment_hide");
		$.ajax({
			url:'/FrontEnjoy/view_comment',
			type:'post',
			data:{view_id:commentDataId, contents:commentInpVal, comment_type:2, reply_comment_id:commentId, reply_user_id:replyUserId},
			success:function(r){
				if(r.code == 0){
					if(commentFlag){
						var commentWrite = '<li class="com_list clearfix">\
											<div class="list_title fl">\
												<span class="first_word name">'+r.data.from_nickname+'</span>：\
												<span class="info_word">'+r.data.contents+'</span>\
											</div>\
											<input type="text" class="show_keybord" data-comment_id="'+r.data.comment_id+'" data-user_id="'+r.data.user_id+'" />\
										</li>';
						commentCont += commentWrite;
					}else{
						var commentReply = '<li class="com_list clearfix">\
											<div class="list_title fl">\
												<span class="first_word name">'+r.data.from_nickname+'</span>\
												<span class="hf_word">回复</span>\
												<span class="last_word name">'+firstWord+'</span>：\
												<span class="info_word">'+commentInpVal+'</span>\
											</div>\
											<input type="text" class="show_keybord"  data-comment_id="'+r.data.comment_id+'" data-user_id="'+r.data.user_id+'" />\
										</li>';	
						commentCont += commentReply;
					}
				}else{
						layer.open({
		                    content: r.msg,
		                    title: false,
		                    btn: ['确定'],
		                });
				}
				$(".comment_cont[data-id="+commentDataId+"]").html(commentCont);
				//点击发送之后清空输入框
				commentInpVal = "";
				$(".comment_input").val(commentInpVal);
			}
		})

		// if(commentFlag){
		// 	var commentWrite = '<li class="com_list clearfix">\
		// 						<div class="list_title fl">\
		// 							<span class="first_word name">张斌</span>：\
		// 							<span class="info_word">'+commentInpVal+'</span>\
		// 						</div>\
		// 						<input type="text" class="show_keybord" />\
		// 					</li>';
		// 	commentCont += commentWrite;
		// }else{
		// 	var commentReply = '<li class="com_list clearfix">\
		// 						<div class="list_title fl">\
		// 							<span class="first_word name">张斌</span>\
		// 							<span class="hf_word">回复</span>\
		// 							<span class="last_word name">'+firstWord+'</span>：\
		// 							<span class="info_word">'+commentInpVal+'</span>\
		// 						</div>\
		// 						<input type="text" class="show_keybord" />\
		// 					</li>';	
		// 	commentCont += commentReply;
		// }
		//console.log(commentCont);
		
	}else{
		if($(".submit_btn")){
			$(".submit_btn").show();
		}
		$(".comment_input").focus();
	}
});

//监听评论输入框的内容变化
$(".comment_input")[0].addEventListener("input",function(e){
	commentInpVal = $(".comment_input").val();
	if(commentInpVal!=''){
		$(".comment_send").removeClass("no_send");
	}else{
		$(".comment_send").addClass("no_send");
	}
});