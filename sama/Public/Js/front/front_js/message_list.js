/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   SongGuangHui
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
{
    if(total<=firstRow)
    {
        $(".pullUpIcon").hide();
        $(".pullUpLabel").html("没有更多了");
    }
});
var num = 1; //默认页面显示4条数据              num可变
/*-----插入新的内容------*/
/*function appendCont(n,obj,comment_list){
 var txt="";
 var m = n-num;
 for(var i=0;i<n;i++){
 txt += '<dl class="clearfix">\
 <dt class="fl">\
 <img src="'+comment_list[i].headimgurl+'"/>\
 </dt>\
 <dd class="fl">s\
 <div class="clearfix">\
 <h3 class="fl">'+comment_list[i].nickname+'</h3>\
 <span class="fr">'+comment_list[i].addtime+'</span>\
 </div>\
 <p>'+comment_list[i].contents+'</p>\
 </dd>\
 </dl>';
 num++;
 }
 obj.append(txt);
 myScroll.refresh();
 setTimeout(function(){
 myScroll.refresh();
 allData();
 },500);
 }*/
function pullUpAction (){
    var sourceNode = $(".myInfo-box").eq(0); // 获得被克隆的节点对象
    /* console.log(total+'----'+num*firstRow);*/
    if(total > num*firstRow){
        $.post('/FrontUserCenter/message', {"firstRow":num*firstRow}, function(data, textStatus)
        {
            console.log(data);
            if (data != 'failure')
            {
                var len = data.length;
                var html = '';
                for (var i = 0; i < len; i++)
                {
                    html+='<div class="myInfo-list clearfix">\
                        <div class="list-heade fl">\
                    <p class="fl">'+data[i].message_title+'</p>';
                    if (data[i].message_type == 1 || data[i].message_type == 6 || data[i].message_type == 2 || data[i].message_type == 3 || data[i].message_type == 5){

                    }else{
                        html +='<i class="fr icon icon_zhankai_default3x"></i>';
                    }
                html+= '<span class="fr">'+data[i].time+'</span>\
                    </div>';
                    // VIEW_COMMENT = 1;//话题观点的评论
                    // DYNAMIC_COMMENT = 6;//动态的评论
                    // VIEW_PRAISE  = 2; //话题动态,观点的点赞
                    // REPLY_COMMENT = 3;//回复他人评论
                    // EXPRESS_WILL = 4; //表达意向
                    // PROJECT_COMMENT = 5 ;// 项目的评论
                    // SERVICE_AUDIT = 7 ;// 申请服务状态审核变更
                    // PROVIDER = 8 ;// 申请服务商
                    // SERVICE  = 9 ; //申请服务
                    // VISIT = 10;//预约参观
                    // ENTER = 11;//预约入驻
                    // ACTIVITY_AUDIT = 12;//活动审核．
                    // SITE_AUDIT = 13;//场地审核．
                    if(data[i].message_type == 4){
                    html+= '<div class="list-card fl card-hide">\
                            <p class="expect-call">这是我的名片,期待您的联系!</p>\
                        <div class="card">\
                            <dl class="clearfix">\
                            <dt class="fl">\
                            <img src="'+data[i].contents.headimgurl+'"/>\
                            </dt>\
                            <dd class="fl">\
                            <ul class="clearfix">\
                            <li class="name">'+data[i].contents.realname+'</li>\
                        <li class="v-line"></li>\
                            <li class="identity">投资人身份</li>\
                            <li class="industry-select">'+data[i].contents.follow_field+'</li>\
                        </ul>\
                        </dd>\
                        </dl>\
                        <ul class="contact">\
                            <li class="clearfix phone">\
                            <i class="fl icon_five tuoyuan_default3x"></i>\
                            <span class="fl">'+data[i].contents.contact_phone+'</span>\
                        </li>\
                        <li class="clearfix email">\
                            <i class="fl icon_five tuoyuan_default3x"></i>\
                            <span class="fl">'+data[i].contents.email+'</span>\
                        </li>\
                        </ul>\
                        </div>\
                        </div>';
                    }else if(data[i].message_type == 10 || data[i].message_type == 11){
                        html+='<ul class="fl card-hide detail_list_wrap">';
                            if(data[i].message_type == 10){
                                html+='<li class="contact_title detail_list">预约参观详情</li>';
                            }
                        html+='<li class="clearfix detail_list">\
                                <span class="fl">拟入住园区</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].contents.incubator+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">预约时间</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].contents.time+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">公司/项目名称</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].contents.company_name+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">预约人姓名</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].contents.contacts+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">预约人电话</span>\
                                <i class="fl">：</i>\
                            <a href="tel:188;" class="fl">'+data[i].contents.contact_number+'</a>\
                            </li>';
                            if(data[i].message_type == 10){
                                <!--园区、众创空间预约参观-->
                            html+= '<li class="clearfix detail_list">\
                                    <span class="fl">参观人数</span>\
                                    <i class="fl">：</i>\
                                    <p class="fl">'+data[i].contents.visit_num+'</p>\
                                    </li>';
                            }
                            if(data[i].contents.incubator_type_val == 1 && data[i].message_type == 11){
                                <!--园区预约入住-->
                            html+='<li class="clearfix detail_list">\
                                    <span class="fl">预约面积</span>\
                                    <i class="fl">：</i>\
                                    <p class="fl">'+data[i].contents.station_num+'平米</p>\
                                    </li>';
                            }
                            if(data[i].contents.incubator_type_val == 2 && data[i].message_type == 11){
                                <!--众创空间预约入住-->
                            html+='<li class="clearfix detail_list">\
                                    <span class="fl">工位数量</span>\
                                    <i class="fl">：</i>\
                                <p class="fl">'+data[i].contents.station_num+'</p>\
                                </li>\
                                <li class="clearfix detail_list">\
                                    <span class="fl">工位类型</span>\
                                    <i class="fl">：</i>\
                                <p class="fl">'+data[i].contents.station_name+'</p>\
                                </li>';
                            }
                        html+= '<li class="clearfix detail_list">\
                                <span class="fl">备注</span>\
                                <i class="fl">：</i>\
                                <p class="fl">'+data[i].contents.user_remarks+'</p>\
                                </li>\
                                </ul>';
                    }else if(data[i].message_type == 13){
                        html+='<ul class="fl card-hide detail_list_wrap">\
                            <li class="contact_title detail_list">预约详情</li>\
                            <li class="clearfix detail_list">\
                            <span class="fl">预约时间</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.time+'</p>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">预定单位</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.order_company+'</p>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">联系人</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.linkman+'</p>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">联系人电话</span>\
                            <i class="fl">：</i>\
                        <a href="tel:188;" class="fl">'+data[i].contents.linkphone+'</a>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">与会人数</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.order_num+'</p>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">备注</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.mark+'</p>\
                        </li>\
                        </ul>';
                    }else if (data[i].message_type == 12){
                        html +='<ul class="fl card-hide detail_list_wrap">\
                            <li class="contact_title detail_list">活动报名详情</li>\
                            <li class="clearfix detail_list">\
                            <span class="fl">姓名</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.real_name+'</p>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">手机号</span>\
                            <i class="fl">：</i>\
                        <a href="tel:188;" class="fl">'+data[i].contents.phone+'</a>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">公司名称</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.company_name+'</p>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">职位</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.position+'</p>\
                        </li>\
                        </ul>';
                    }else if(data[i].message_type == 8){
                       html+='<ul class="fl card-hide detail_list_wrap">\
                                <li class="contact_title detail_list">申请服务商详情</li>\
                                <li class="clearfix detail_list">\
                                <span class="fl">企业名称</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].contents.service_providers_name+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">企业LOGO</span>\
                                <i class="fl">：</i>\
                            <img src="'+data[i].contents.service_providers_logo+'" class="fl"/>\
                                </li>\
                                <li class="clearfix detail_list">\
                                <span class="fl">企业简称</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].contents.enterprise_name+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">类别选择</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].providers_type+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">公司网址</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].contents.url+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">三合一证或营业执照</span>\
                                <i class="fl">：</i>\
                            <img src="'+data[i].contents.charter+'" class="fl"/>\
                                </li>\
                                <li class="clearfix detail_list">\
                                <span class="fl">企业负责人</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].contents.enterprise_user_name+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">负责人手机</span>\
                                <i class="fl">：</i>\
                            <a href="tel:188;" class="fl">'+data[i].contents.enterprise_user_mobile+'</a>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">联系人</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].contents.user_name+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">联系人手机</span>\
                                <i class="fl">：</i>\
                            <a href="tel:188;" class="fl">'+data[i].contents.mobile+'</a>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">联系人邮箱</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].contents.email+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">联系人地址</span>\
                                <i class="fl">：</i>\
                            <a href="" class="fl">'+data[i].contents.area_string+'-'+data[i].contents.address+'</a>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">公众号</span>\
                                <i class="fl">：</i>\
                            <p class="fl">'+data[i].contents.accounts+'</p>\
                            </li>\
                            <li class="clearfix detail_list">\
                                <span class="fl">公众号二维码</span>\
                                <i class="fl">：</i>\
                            <img src="'+data[i].contents.accounts_code+'" class="fl"/>\
                                </li>';
                            if(data[i].contents.service_providers_status == 2){
                                <!--审核失败-->
                            html+='<li class="clearfix detail_list">\
                                    <span class="fl">未通过理由</span>\
                                    <i class="fl">：</i>\
                                <p class="fl">'+data[i].contents.status_cause+'</p>\
                                </li>'
                            }
                        html+='</ul>';
                    }else if (data[i].message_type == 7 || data[i].message_type == 9){
                    html+= '<ul class="fl card-hide detail_list_wrap">\
                            <li class="contact_title detail_list">申请服务详情</li>\
                            <li class="clearfix detail_list">\
                            <span class="fl">真实姓名</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.user_name+'</p>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">手机号码</span>\
                            <i class="fl">：</i>\
                        <a href="tel:188;" class="fl">'+data[i].contents.mobile+'</a>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">公司名称</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.company_name+'</p>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">申请服务描述</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.service_sketch+'</p>\
                        </li>\
                        <li class="clearfix detail_list">\
                            <span class="fl">备注</span>\
                            <i class="fl">：</i>\
                        <p class="fl">'+data[i].contents.remark+'</p>\
                        </li>\
                        </ul>';
                    }
                html+='</div>'
                }
                sourceNode.append(html);
                setTimeout(function(){
                    myScroll.refresh();
                },500);
                num ++;
            }
        }, 'json');
    }
    else
    {
        $('.pullUpIcon').hide();
        $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  pullUpEl = document.getElementById('pullUp');
  pullUpOffset = pullUpEl.offsetHeight;
//	pullUpAction();
  myScroll = new iScroll('load_wrapper', {
//     useTransition: true,
    topOffset: pullDownOffset,
    onRefresh: function () {
      if (pullUpEl.className.match('loading')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
      }
    },
    onScrollMove: function () {
       if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
        pullUpEl.className = 'flip';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
        this.maxScrollY = this.maxScrollY;
      } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
        this.maxScrollY = pullUpOffset;
      }
    },
    onScrollEnd: function () {
      if (pullUpEl.className.match('flip')) {
        pullUpEl.className = 'loading';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
        pullUpAction(); // ajax call?
      }
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
