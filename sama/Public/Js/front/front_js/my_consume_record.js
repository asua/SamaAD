/**
 * Created by 辰 on 2017/7/13.
 */
//顶部导航
$(".head_tab").on("click",".normal",function(){
    consume_type = $(this).attr('data-consume_type');
    console.log(consume_type);
    $(this).addClass("active_tab").siblings().removeClass("active_tab");
    $.ajax({
        type:'post',
        url:'/FrontUserCenter/my_consume_record',
        data:{consume_type:consume_type},
        success:function (data) {

            num = 1;
            total = data.total;
            var html = '';
            if (data.list != '' && data.list != null)
            {
                var len = data.list.length;
                if(data.consume_type == 1 || data.consume_type == 2 || data.consume_type == 3){
                    for (var i = 0; i < len; i++)
                    {
                        if(data.consume_type == 1){
                           html+='<div class="expenditrue_list">\
                                <span class="h1">支出金额</span><span class="h5">-'+data.list[i].amount_out+'元</span>\
                                <span class="h2">支出类型</span><span class="h6">'+data.list[i].change_type+'</span>\
                                <span class="h3">支出频道</span><span class="h7">'+data.list[i].channel_name+'</span>\
                                <span class="h4">支付时间</span><span class="h8">'+data.list[i].add_time+'</span>\
                                </div>' ;
                        }else if(data.consume_type == 2){
                            html+='<div class="expenditrue_list">\
                                <span class="h1">收入金额</span><span class="h5">+'+data.list[i].amount_in+'元</span>\
                                <span class="h2">收入类型</span><span class="h6">'+data.list[i].change_type+'</span>\
                                <span class="h3">收入频道</span><span class="h7">'+data.list[i].channel_name+'</span>\
                                <span class="h4">进账时间</span><span class="h8">'+data.list[i].add_time+'</span>\
                                </div>'
                        }else if(data.consume_type == 3){
                            html+=' <div class="expenditrue_list small">\
                                    <span class="h1">充值金额</span><span class="h5">+'+data.list[i].amount_in+'元</span>\
                                    <span class="h2">充值时间</span><span class="h6">'+data.list[i].add_time+'</span>\
                                    </div>';
                        }
                    }
                }

                if(data.consume_type == 4){
                    console.log(data.list);
                    for (var i = 0; i < len; i++) {
                        html += '<div class="expenditrue_list mid">\
                        <div class="w_box">\
                        <span class="withdraw_time">'+data.list[i].add_time+'</span><span class="status verifying">'+data.list[i].state_name+'</span>\
                        </div>\
                        <span class="num">提现金额</span><span class="withdraw_money verifying">-'+data.list[i].money+'元</span>\
                        <span class="ac_num">提现账户</span><span class="withdraw_account">'+data.list[i].alipay_account+'</span>\
                        </div>';
                    }
                }

                $('.no_record').hide();
                $('#load_wrapper').show();
            }else{
                $('.no_record').show();
                $('#load_wrapper').hide();
            }

            $('.home_list').html(html);
            setTimeout(function(){
                myScroll.refresh();
            },500);
        }
    })
});