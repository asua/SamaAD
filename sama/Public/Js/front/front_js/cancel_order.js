/**
 * Created by 辰 on 2017/4/28.
 */
function cancel_order(order_type,order_id){
// console.log(order_id);
// console.log(order_type);
    layer.open({
        content:'确定取消订单嘛?',
        // title:  '提示',
        btn: ['确定','取消'],
        yes : function () {
            $.ajax({
                type : 'post',
                url :'/Front/cancel_order',
                data :{order_type:order_type , order_id:order_id},
                success:function (r) {
                    if(r.code == 1){
                        layer.open({
                            content: r.msg
                            ,skin: 'msg'
                            ,time: 2 //2秒后自动关闭
                        });
                        location.reload();
                    }else{
                        layer.open({
                            content: r.msg
                            ,skin: 'msg'
                            ,time: 2 //2秒后自动关闭
                        });
                    }


                }
            })
        }
    });
}