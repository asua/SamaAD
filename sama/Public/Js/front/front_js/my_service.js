/*
 * @Author: zhangfengjie
 * @Date:   2016-04-01 16:57:34
 * @Last Modified by:   SongGuangHui
 * @Last Modified time: 2017-03-04 14:35:29
 */
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll, pullDownEl, pullDownOffset, pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function () {
    if (total <= firstRow) {
        $(".pullUpIcon").hide();
        $(".pullUpLabel").html("没有更多了");
    }
});
var num = 1; //默认页面显示4条数据              num可变
/*-----插入新的内容------*/
/*function appendCont(n,obj,comment_list){
 var txt="";
 var m = n-num;
 for(var i=0;i<n;i++){
 txt += '<dl class="clearfix">\
 <dt class="fl">\
 <img src="'+comment_list[i].headimgurl+'"/>\
 </dt>\
 <dd class="fl">s\
 <div class="clearfix">\
 <h3 class="fl">'+comment_list[i].nickname+'</h3>\
 <span class="fr">'+comment_list[i].addtime+'</span>\
 </div>\
 <p>'+comment_list[i].contents+'</p>\
 </dd>\
 </dl>';
 num++;
 }
 obj.append(txt);
 myScroll.refresh();
 setTimeout(function(){
 myScroll.refresh();
 allData();
 },500);
 }*/
function pullUpAction() {
    var sourceNode = $(".project_list").eq(0); // 获得被克隆的节点对象
    /* console.log(total+'----'+num*firstRow);*/
    if (total > num * firstRow) {
        $.post('/FrontUserCenter/my_service', {"firstRow": num * firstRow}, function (data, textStatus) {
            console.log(data);
            if (data != 'failure' && data != null) {
                if(data){
                    var len = data.length;
                }
                var html = '';
                for (var i = 0; i < len; i++) {
                    var url = '';//连接跳转地址
                    var name = '';//导师服务商名字
                    var describe= '';//导师服务商简介
                    var type_name = '';//领域或服务商类型
                    if(data[i].service_providers_type==1){
                        url ='mentor_detail';
                        name = data[i].user_name;
                        describe = data[i].service_providers_describe;
                        type_name = data[i].domain_name;
                    }else{
                        url ='providers_detail';
                        name = data[i].service_providers_name;
                        describe = data[i].service_describe;
                        type_name = data[i].service_type_name;
                    }

                    html += '<li class="">\
                        <a href="/FrontService/'+ url +'/service_providers_id/'+ data[i].service_providers_id+'" class="clearfix">\
                        <img src="'+data[i].poster+'" alt="" class="fl" />\
                        <div class="fr clearfix project_cont">\
                        <h6 class="fl">'+ name +'</h6>\
                    <p>'+ describe +'</p>\
                    <dl class="clearfix">\
                        <dd class="fl">'+ type_name +'</dd>\
                    </dl>\
                    </div>\
                    </a>\
                    </li>';
                }
                sourceNode.append(html);
                setTimeout(function () {
                    myScroll.refresh();
                }, 500);
                num++;
            }
        }, 'json');
    }
    else {
        $('.pullUpIcon').hide();
        $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
    pullUpEl = document.getElementById('pullUp');
    pullUpOffset = pullUpEl.offsetHeight;
//	pullUpAction();
    myScroll = new iScroll('load_wrapper', {
//     useTransition: true,
        topOffset: pullDownOffset,
        onRefresh: function () {
            if (pullUpEl.className.match('loading')) {
                pullUpEl.className = '';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
            }
        },
        onScrollMove: function () {
            if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
                pullUpEl.className = 'flip';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
                this.maxScrollY = this.maxScrollY;
            } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
                pullUpEl.className = '';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
                this.maxScrollY = pullUpOffset;
            }
        },
        onScrollEnd: function () {
            if (pullUpEl.className.match('flip')) {
                pullUpEl.className = 'loading';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
                pullUpAction(); // ajax call?
            }
        }
    });
    setTimeout(function () {
        document.getElementById('load_wrapper').style.left = '0';
    }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) {
    e.preventDefault();
}, false);
document.addEventListener('DOMContentLoaded', function () {
    setTimeout(loaded, 200);
}, false);
