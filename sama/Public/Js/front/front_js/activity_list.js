/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   SongGuangHui
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
{
    if(total<=firstRow)
    {
        $(".pullUpIcon").hide();
        $(".pullUpLabel").html("没有更多了");
    }
});
var num = 1; //默认页面显示4条数据              num可变
/*-----插入新的内容------*/
/*function appendCont(n,obj,comment_list){
 var txt="";
 var m = n-num;
 for(var i=0;i<n;i++){
 txt += '<dl class="clearfix">\
 <dt class="fl">\
 <img src="'+comment_list[i].headimgurl+'"/>\
 </dt>\
 <dd class="fl">s\
 <div class="clearfix">\
 <h3 class="fl">'+comment_list[i].nickname+'</h3>\
 <span class="fr">'+comment_list[i].addtime+'</span>\
 </div>\
 <p>'+comment_list[i].contents+'</p>\
 </dd>\
 </dl>';
 num++;
 }
 obj.append(txt);
 myScroll.refresh();
 setTimeout(function(){
 myScroll.refresh();
 allData();
 },500);
 }*/
function del_native(){
	native_listen('no_loading');
}
function pullDownAction(){
	setTimeout(function(){
		del_native();
		window.location.reload();
	},300);
	window.onload = function(){
		myScroll.refresh();
	}
}
function pullUpAction (){
    var sourceNode = $(".activity_list").eq(0); // 获得被克隆的节点对象
    console.log(status+'------'+city_id)
    if(total > num*firstRow){
        $.post('/FrontActivity/activity_drop_loading', {"firstRow":num*firstRow,"status":status,"city_id":city_id,"key_words":key_words,"sign":sign}, function(data, textStatus)
        {
            console.log(data);
            if (data != 'failure')
            {
                var len = data.data.length;
                var html = '';
                for (i = 0; i < len; i++)
                {
                   /* if(data.data[i].status==0){
                        var status='下架';
                    }else if(data.data[i].status==1){
                        var status='预告中';
                    }else if(data.data[i].status==2){
                        var status='报名中';
                    }else if(data.data[i].status==3){
                        var status='进行中';
                    }else if(data.data[i].status==4){
                        var status='已结束';
                    }*/
                    var status=data.data[i].status;


                    html+='<a href="/FrontActivity/activity_detail/activity_id/'+data.data[i].activity_id+'" class="activity_bg">'+
                        '<img src="'+data.data[i].first_pic+'" alt="" />'+
                          '<div class="activity_bg_box">'+
                        '<h6>'+data.data[i].activity_title+'</h6>'+
                        '<p>'+data.data[i].sponsor+'</p>'+
                        '<span>'+status+'</span>'+
                          '</div>'+
                           '</a>';
                }
                sourceNode.append(html);
                setTimeout(function(){
                    myScroll.refresh();
                },500);
                num ++;
            }
        }, 'json');
    }
    else
    {
        $('.pullUpIcon').hide();
        $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  	pullUpEl = document.getElementById('pullUp');
  	pullUpOffset = pullUpEl.offsetHeight;
	pullDownEl = document.getElementById('pullDown');
	pullDownOffset = pullDownEl.offsetHeight;
  myScroll = new iScroll('load_wrapper', {
  	useTransition: true,
    topOffset: pullDownOffset,
    onRefresh: function () {
    	if (pullDownEl.className.match('loading')) {
			pullDownEl.className = '';
		}else if(pullUpEl.className.match('loading')) {
	        pullUpEl.className = '';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
	   }
    },
    onScrollMove: function () {
    	if (this.y >5  && !pullDownEl.className.match('flip')) {
		    pullDownEl.className = 'flip';
		    this.minScrollY = 0;
	   	}else if(this.y < 5 && pullDownEl.className.match('flip')) {
		    pullDownEl.className = '';
		    this.minScrollY = -pullDownOffset;
	   	}else if(this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
	        pullUpEl.className = 'flip';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
	        this.maxScrollY = this.maxScrollY;
      	}else if(this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
	        pullUpEl.className = '';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
	        this.maxScrollY = pullUpOffset;
      	}
    },
    onScrollEnd: function () {
    	if (pullDownEl.className.match('flip')) {
			pullDownEl.className = 'loading';
			pullDownAction();
		}else if(pullUpEl.className.match('flip')) {
	        pullUpEl.className = 'loading';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
	        pullUpAction(); // ajax call?
     }
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
