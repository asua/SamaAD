/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   SongGuangHui
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
  {
  	console.log(total);
    if(total<=firstRow)
    {
      $(".pullUpIcon").hide();
      $(".pullUpLabel").html("没有了");
    }
  });
var num = 1; //默认页面显示4条数据              num可变
function del_native(){
	native_listen('no_loading');
}
function pullDownAction(){
	setTimeout(function(){
		del_native();
		window.location.reload();
	},300);
	window.onload = function(){
		myScroll.refresh();
	}
}

function pullUpAction (){
	var sourceNode = $("#scroller .project_list").eq(0); // 获得被克隆的节点对象
  var phase_id = $('.phase').find('.active').data('phase_id');
  var field_id = $('.field').find('.active').data('field_id');
	if(total > num*firstRow){
      $.post('/FrontEnjoy/project_list', {"firstRow":num*firstRow,phase:phase_id, field:field_id}, function(r, textStatus) 
       {
        if (r.project_list != 'failure' && r.project_list != null)
        { 
          var data = r.project_list;
          var len = data.length;
          var html = '';
          for (var i = 0; i < len; i++)
          {
            var phase_name = '';
            if(data[i].phase_name != '' && data[i].phase_name != null){
              phase_name = '<dd class="fl">'+data[i].phase_name+'</dd>';
            }
            var field_name = '';
            if(data[i].field_name != '' && data[i].phase_name != null){
              field_name = '<dd class="fl">'+data[i].field_name+'</dd>';
            }
          	html += '<li class="">\
                    <a href="/FrontEnjoy/project_details/project_id/'+data[i].project_id+'" class="clearfix">\
                        <div class="find_pro_pic fl">\
                            <img src="'+data[i].pic+'" alt="" class="" />\
                        </div>\
                        <div class="clearfix project_cont">\
                            <h6 class="">'+data[i].project_name+'</h6>\
                            <p class="nick_name">'+data[i].project_highlight+'</p>\
                            <dl class="clearfix">'
                                + phase_name + field_name +
                           '</dl>\
                        </div>\
                    </a>\
                </li>';
          }
          sourceNode.append(html);
          setTimeout(function(){
            myScroll.refresh();
          },500);
          num ++;
        }
        total = r.totla
      }, 'json');      
    }
    else
    {
      $('.pullUpIcon').hide();
      $('.pullUpLabel').html('没有更多了');
    }
}

function loaded() {
 	pullUpEl = document.getElementById('pullUp');
  	pullUpOffset = pullUpEl.offsetHeight;
	pullDownEl = document.getElementById('pullDown');
	pullDownOffset = pullDownEl.offsetHeight;
  myScroll = new iScroll('load_wrapper', {
       	useTransition: true,
	    topOffset: pullDownOffset,
	    onRefresh: function () {
	      	if(pullDownEl.className.match('loading')) {
				pullDownEl.className = '';
			}else if(pullUpEl.className.match('loading')) {
		        pullUpEl.className = '';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
		   	}
	    },
    onScrollMove: function () {
       	if(this.y >5  && !pullDownEl.className.match('flip')) {
		    pullDownEl.className = 'flip';
		    this.minScrollY = 0;
	   	}else if(this.y < 5 && pullDownEl.className.match('flip')) {
		    pullDownEl.className = '';
		    this.minScrollY = -pullDownOffset;
	   	}else if(this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
	        pullUpEl.className = 'flip';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
	        this.maxScrollY = this.maxScrollY;
      	}else if(this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
	        pullUpEl.className = '';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
	        this.maxScrollY = pullUpOffset;
      	}
      //移动到一定位置固定头部导航
      var src_top=myScroll.y
      if(src_top<-193){
         $("#top_nav_copy").addClass("top_nav_fixed");
          $("#top_nav").hide();
          $("#top_nav_copy").show();
          $("#load_wrapper").css("top","52px");
      }
      else
       {
         $("#top_nav_copy").removeClass("top_nav_fixed");
         $("#top_nav").show();
         $("#top_nav_copy").hide();
         $("#load_wrapper").css("top","0px");
       }
    },
    onScrollEnd: function () {
    	if(pullDownEl.className.match('flip')) {
			pullDownEl.className = 'loading';
			pullDownAction();
		}else if(pullUpEl.className.match('flip')) {
	        pullUpEl.className = 'loading';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
	        pullUpAction(); // ajax call?
      	}
      //移动到一定位置固定头部导航
      var src_top=myScroll.y
      if(src_top<-193){
         $("#top_nav_copy").addClass("top_nav_fixed");
          $("#top_nav").hide();
          $("#top_nav_copy").show();
          $("#load_wrapper").css("top","52px");
      }
      else
       {
         $("#top_nav_copy").removeClass("top_nav_fixed");
         $("#top_nav").show();
         $("#top_nav_copy").hide();
         $("#load_wrapper").css("top","0px");
       }
       setTimeout(function(){
	      	//设置阴影高度
	        shadeBg();
	    },100);
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
function shadeBg(){
	var scrollerHei = $("#scroller").height();
    var topNavHei = $("#top_nav").height();
    var tabHei = $(".project_type_tab").height();
    var typeListHei = $(".project_type_list").height();
	var h = scrollerHei  - topNavHei - tabHei - typeListHei;
    $(".project_bg").height(h+"px");
}