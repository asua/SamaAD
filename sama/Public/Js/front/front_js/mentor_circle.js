/* 
* @Author: Marte
* @Date:   2017-03-08 00:17:35
* @Last Modified by:   Marte
* @Last Modified time: 2017-03-10 14:21:48
*/

$(document).ready(function(){

    $(".circle_tab li").on("click",function()
        {
            $(".project_tab_menu").hide();
            $(".project_type_tab li").removeClass('active_tab');
            $(".project_bg").hide();
            $(this).addClass('active').siblings().removeClass('active');
            var Obj = $(this).attr("id");
            var num = Obj.substring(2, Obj.length);
            $("#main"+num).show().siblings('.main').hide();
        });

        $(".circle_state_zan i").on("click",function()
        {
            $(this).toggleClass('icon_like_selected3x');
        });

        $(".project_type_tab li").on("click",function()
        {	
        	$(".project_type_tab").hide();
        	$(".project_type_list").show();
            $(this).toggleClass('active_tab').siblings().removeClass('active_tab');
            var index = $(this).index();
            $(".project_type_list dl").eq(index).toggle().siblings().hide();
            $(".project_type_list1 dl").eq(index).toggle().siblings().hide();
           	var t = $(".service_header").height()+$(".project_type_tab").height()+$(".project_type_list").height();
          	var h = $(window).height() - t;
//          var h = $(window).height() - [$(".project_type_tab").height() + $(".project_type_tab").offset().top-$(document).scrollTop()];
            console.log(h);
            $(".project_bg").height(h);
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".project_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".project_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
        });
        $(".project_tab_menu dd").on("click",function()
        {
			$(".project_type_tab").show();
			$(".project_type_list").hide();
            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().hide();
            $(".project_type_tab li").removeClass('active_tab');
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".project_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".project_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
            domain_id = $('.domain').find('.active').data('domain_id');
            // city_id = $('.city_id').find('.active').data('city_id');
            console.log(domain_id);
            // console.log(city_id);
            $.ajax({
                url:'/FrontService/get_mentor_list',
                type:'post',
                data:{domain_id:domain_id,key_words:key_words},
                success:function(data){
                    num=1;
                    total = data.total;

                    if(total < num*firstRow){
                        $('.pullUpIcon').hide();
                        $('.pullUpLabel').html('没有更多了');
                    }else if(total > num*firstRow){
                        $('.pullUpIcon').hide();
                        $('.pullUpLabel').html('<span class="pullUpIcon"></span>上拉加载...');
                    }

                    // $('.teacher_list').find('li').remove();
                    if(data != null){
                        if(data.mentor_list){
                            var len = data.mentor_list.length;
                        }
                        var html ='';
                        for(var i = 0; i < len; i++) {
                            var position_name;
                            if(data.mentor_list[i].position_name){
                                position_name = data.mentor_list[i].position_name
                            }else{
                                position_name = '';
                            }

                            html +='<li>'
                                +'<a href="/FrontService/mentor_detail/service_providers_id/'+ data.mentor_list[i].service_providers_id +'" class="clearfix">'
                                +'<img src="'+ data.mentor_list[i].service_providers_logo +'" alt="" class="fl" />'
                                +'<div class="fr teacher_cont">'
                                +'<h6>'+ data.mentor_list[i].user_name+'&nbsp;&nbsp;&nbsp;<span>'+ position_name +'</span></h6>'
                                +'<p>'+ data.mentor_list[i].service_providers_describe+'</p>'
                                +'<dl class="clearfix">'
                                +'<dt class="fl pic icon_dingwei_default3x"></dt>'
                                +'<dd class="fl">'+data.mentor_list[i].city_name+'</dd>'
                                +'</dl>'
                                +'</div>'
                                +'</a>'
                                +'</li>'
                        }
                        $('.teacher_list').html(html);
                        setTimeout(function(){
			                myScroll.refresh();
			            },500);
                    }
                }
            })
        });

        $(".circle_state_ping").each(function(index) 
        {
            var number = $("li",this).length;
            if(number>3)
            {
                $(this).addClass('circle_state_hide');  
            }
            else{
                $(this).addClass('circle_state_active');
                $(this).siblings('.li_zhan').hide();
            }
        });
        $('.li_zhan').on("click",function()
        {
            $(this).hide();
            $(this).siblings('.circle_state_ping').removeClass('circle_state_hide');
        });
});