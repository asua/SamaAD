/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   zhangfengjie
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
{
    if(total<=firstRow)
    {
        $(".pullUpIcon").hide();
        $(".pullUpLabel").html("没有更多了");
    }
});
var num = 1; //默认页面显示4条数据              num可变
/*-----插入新的内容------*/
/*function appendCont(n,obj,comment_list){
 var txt="";
 var m = n-num;
 for(var i=0;i<n;i++){
 txt += '<dl class="clearfix">\
 <dt class="fl">\
 <img src="'+comment_list[i].headimgurl+'"/>\
 </dt>\
 <dd class="fl">s\
 <div class="clearfix">\
 <h3 class="fl">'+comment_list[i].nickname+'</h3>\
 <span class="fr">'+comment_list[i].addtime+'</span>\
 </div>\
 <p>'+comment_list[i].contents+'</p>\
 </dd>\
 </dl>';
 num++;
 }
 obj.append(txt);
 myScroll.refresh();
 setTimeout(function(){
 myScroll.refresh();
 allData();
 },500);
 }*/
function del_native(){
	native_listen('no_loading');
}
function pullDownAction(){
	setTimeout(function(){
		del_native();
		window.location.reload();
	},300);
	window.onload = function(){
		myScroll.refresh();
	}
}
function pullUpAction (){
    var sourceNode = $(".orders_info_wrap").eq(0); // 获得被克隆的节点对象
    var state = $(".nav_acitive").data('state');
     console.log(total+'----'+num*firstRow);
    if(total > num*firstRow){
        $.post('/FrontUserCenter/my_orders_zc', {"firstRow":num*firstRow,"state":state}, function(data, textStatus)
        {
            console.log(data);
            if (data != 'failure')
            {
                if(data.list != null){
                    var len = data.list.length;
                }
                var html = '';
                for (var i = 0; i < len; i++)
                {
                    html += '<div class="orders_info_list">\
                        <div class="orders_head">\
                        <p class="orders_number">订单号：'+data.list[i].order_sn+'</p>\
                        <span class="orders_state">'+data.list[i].state+'</span>\
                        </div>\
                        <a class="orders_cont clearfix" href="/FrontIncubator/order_zhongchuang_detail/order_id/'+data.list[i].order_id+'">\
                        <img src="'+data.list[i].pic+'" class="orders_pic fl"/>\
                        <div class="fr orders_info">\
                        <div class="info_title">\
                        <h6 class="title_left">'+data.list[i].incubator_name+'</h6>\
                        <span class="title_right">¥'+data.list[i].unit_price+'</span>\
                        </div>\
                        <div class="info_mid">工位数量：'+data.list[i].station_num+'</div>\
                        <div class="info_bottom">'+data.list[i].address+'</div>\
                        </div>\
                        </a>\
                        <div class="orders_bottom">\
                        <div class="top">数量：'+data.list[i].station_num+'，实付：¥'+data.list[i].total_amount+'</div>'
                // <!--只有待付款时才有bottom-->
                 if(data.list[i].pay_status == 0){
                 html+= '<div class="bottom clearfix">\
                         <a href="/FrontIncubator/order_zhongchuang_detail/order_id/'+data.list[i].order_id+'" class="fr pay_btn">立即支付</a>\
                         <a href="javascript:;" onclick="cancel_order(2,'+data.list[i].order_id+')"  class="fr cancel_btn">取消订单</a>\
                         </div>'
                 }
                 html+= '</div>\
                         </div>'
                }
                sourceNode.append(html);
                setTimeout(function(){
                    myScroll.refresh();
                },50);
                num ++;
            }
        }, 'json');
    }
    else
    {
        $('.pullUpIcon').hide();
        $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  	pullUpEl = document.getElementById('pullUp');
  	pullUpOffset = pullUpEl.offsetHeight;
	pullDownEl = document.getElementById('pullDown');
	pullDownOffset = pullDownEl.offsetHeight;
  myScroll = new iScroll('load_wrapper', {
  	useTransition: true,
    topOffset: pullDownOffset,
    onRefresh: function () {
    	if(pullDownEl.className.match('loading')) {
			pullDownEl.className = '';
		}else if(pullUpEl.className.match('loading')) {
	        pullUpEl.className = '';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
	   	}
    },
    onScrollMove: function () {
    	if(this.y >5  && !pullDownEl.className.match('flip')) {
		    pullDownEl.className = 'flip';
		    this.minScrollY = 0;
	   	}else if(this.y < 5 && pullDownEl.className.match('flip')) {
		    pullDownEl.className = '';
		    this.minScrollY = -pullDownOffset;
	   	}else if(this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
	        pullUpEl.className = 'flip';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
	        this.maxScrollY = this.maxScrollY;
      	}else if(this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
	        pullUpEl.className = '';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
	        this.maxScrollY = pullUpOffset;
      	}
    },
    onScrollEnd: function () {
    	if(pullDownEl.className.match('flip')) {
			pullDownEl.className = 'loading';
			pullDownAction();
		}else if(pullUpEl.className.match('flip')) {
	        pullUpEl.className = 'loading';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
	        pullUpAction(); // ajax call?
     	}
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
