/* 
* @Author: Marte
* @Date:   2017-03-08 00:17:35
* @Last Modified by:   Marte
* @Last Modified time: 2017-03-10 14:21:48
*/

$(document).ready(function(){
    $(".circle_tab li").on("click",function()
        {
            $(".project_tab_menu").hide();
            $(".project_type_tab li").removeClass('active_tab');
            $(".shade_bg").hide();
            $(this).addClass('active').siblings().removeClass('active');
            var Obj = $(this).attr("id");
            var num = Obj.substring(2, Obj.length);
            $("#main"+num).show().siblings('.main').hide();
        });

        $(".circle_state_zan i").on("click",function()
        {
            $(this).toggleClass('icon_like_selected3x');
        });

        $(".project_type_tab li").on("click",function()
        {
            $(this).toggleClass('active_tab').siblings().removeClass('active_tab');
            var index = $(this).index();
            $(".project_type_list dl").eq(index).toggle().siblings().hide();
            $(".project_type_list1 dl").eq(index).toggle().siblings().hide();
            //          var h = $(window).height() - [$(".project_type_tab").height() + $(".project_type_tab").offset().top-$(document).scrollTop()];
            
            //设置阴影高度
          	// shadeBg();
            
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".shade_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".shade_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
            // setTimeout(function(){
            // 	myScroll.refresh();
            // },500);
        });
        $(".project_tab_menu dd").on("click",function()
        {
            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().hide();
            $(".project_type_tab li").removeClass('active_tab');
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".shade_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".shade_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
            var field_id = $('.field').find('.active').data('field_id');
            var city_id = $('.city_id').find('.active').data('city_id');
            var phase_id = $('.phase').find('.active').data('phase_id');
            $.ajax({
                url:'/FrontEnjoy/touzi_list',
                type:'post',
                data:{city_id:city_id, field:field_id, phase:phase_id},
                success:function(r){
                    $('.project_list').find('li').remove();
                    if(r.user_list != null){

                        var data = r.user_list;
                        var len = data.length;
                        var html ='';
                        
                        for(var i = 0; i < len; i++){
                            var phase_v = '';
                            var field_v = '';
                            if(data[i].follow_phase){
                                var phase_len = data[i].follow_phase.length;
                                for(var j = 0; j < phase_len ; j++){
                                    if(j < 2){
                                        phase_v += '<dd class="fl">'+data[i].follow_phase[j]+'</dd>';
                                    }
                                }
                            }
                            if(data[i].follow_field){
                                var field_len = data[i].follow_field.length;
                                for(var k = 0; k < field_len ; k++){
                                    if(k < 2){
                                        field_v += '<dd class="fl">'+data[i].follow_field[k]+'</dd>';
                                    }
                                }
                            }
                        // html += '<li class="">\
                        //             <a href="/FrontEnjoy/touzi_details/user_id/'+data[i].user_id+'" class="clearfix">\
                        //                 <img src="'+data[i].headimgurl+'" alt="" class="fl" />\
                        //                 <div class="fr clearfix project_cont">\
                        //                     <h6 class="fl">'+data[i].realname+'</h6>\
                        //                     <span class="fl">'+data[i].company_address+'</span>\
                        //                     <!--<p>'+data[i].introduction+'</p>\
                        //                     <dl class="clearfix">';
                        //                 html += phase_v;
                        //                 html += field_v;
                        //                 html += '</dl>\
                        //                 </div>\
                        //             </a>\
                        //         </li>';
                        //         
                            html += '<li class="">\
                                        <a href="/FrontEnjoy/touzi_details/user_id/'+data[i].user_id+'" class="clearfix">\
                                            <img src="'+data[i].headimgurl+'" alt="" class="fl" />\
                                            <div class="clearfix project_cont">\
                                                <h6 class="realname">'+data[i].realname+'</h6>\
                                                <p class="jigou">'+data[i].company_address+'</p>\
                                                <dl class="clearfix">'
                                                + phase_v + field_v +
                                                '</dl>\
                                            </div>\
                                        </a>\
                                    </li>';
                        }
                        $('.project_list').append(html);
                        total = r.total;
                    }
                    if(parseInt(total)<=parseInt(firstRow))
                    {
                      $(".pullUpIcon").hide();
                      $(".pullUpLabel").html("没有更多了");
                    }
                    setTimeout(function(){
                      	myScroll.refresh();
                    },500);
                }
            })
        });

        $(".circle_state_ping").each(function(index) 
        {
            var number = $("li",this).length;
            if(number>3)
            {
                $(this).addClass('circle_state_hide');  
            }
            else{
                $(this).addClass('circle_state_active');
                $(this).siblings('.li_zhan').hide();
            }
        });
        $('.li_zhan').on("click",function()
        {
            $(this).hide();
            $(this).siblings('.circle_state_ping').removeClass('circle_state_hide');
        });
  //       function shadeBg(){
		// 	var scrollerHei = $("#scroller").height();

		//     var bannerHei = $(".circle_banner").height();

  //           var topNavMargin =  parseInt($('#top_nav').css('margin-bottom'));
  //           var topNavHei = $("#top_nav").height() + topNavMargin;

  //           var hotMargin = parseInt($('.hot_block').css('margin-bottom'));
  //           var hotpadding = parseInt($('.hot_block').css('padding-bottom'));
  //           var hotBlock = $('.hot_block').height() + hotMargin + hotpadding;
  //           console.log(hotBlock);

		//     var tabHei = $(".project_type_tab").height();
		//     var typeListHei = $(".project_type_list").height();
		// 	var h = scrollerHei - bannerHei - topNavHei - tabHei - typeListHei -hotBlock;
  //           console.log(scrollerHei,topNavHei,hotBlock,typeListHei,h)
		//     $(".project_bg").height(h+"px");
		// }
});

