/* 
* @Author: Marte
* @Date:   2017-03-08 00:17:35
* @Last Modified by:   Marte
* @Last Modified time: 2017-03-10 14:21:48
*/
$(document).ready(function(){


    $(".circle_tab li").on("click",function()
        {
            $(".project_tab_menu").hide();
            $(".project_type_tab li").removeClass('active_tab');
            $(".project_bg").hide();
            $(this).addClass('active').siblings().removeClass('active');
            var Obj = $(this).attr("id");
            var num = Obj.substring(2, Obj.length);
            $("#main"+num).show().siblings('.main').hide();
        });
        $(".circle_state_zan i").on("click",function()
        {
            $(this).toggleClass('icon_like_selected3x');
        });

        $(".project_type_tab li").on("click",function()
        {
            $(this).toggleClass('active_tab').siblings().removeClass('active_tab');
            var index = $(this).index();
            $(".project_type_list dl").eq(index).toggle().siblings().hide();
            $(".project_type_list1 dl").eq(index).toggle().siblings().hide();
            var t = $(".service_header").height()+$(".project_type_tab").height()+$(".project_type_list").height();
            var h = $(window).height() - t;
//          var h = $(window).height() - [$(".project_type_tab").height() + $(".project_type_tab").offset().top-$(document).scrollTop()];
            console.log(h);
            $(".project_bg").height(h);
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".project_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".project_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
        });



        $(".project_tab_menu dd").on("click",function()
        {
                // var html = ($(this).children("p").html());
                //
                // if($(this).children("p").hasClass("service_type")){
                //     $("#service_type").html(html)
                // }else{
                //     $("#territory").html(html)
                // }


            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().hide();
            $(".project_type_tab li").removeClass('active_tab');
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".project_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".project_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
            type_id = $('.type').find('.active').data('type_id');
            city_id = $('.city_id').find('.active').data('city_id');
            console.log(type_id);
            console.log(city_id);
            $.ajax({
                url:'/FrontService/get_providers_list',
                type:'post',
                data:{type_id:type_id, city_id:city_id ,search_name : where,service_type_id:service_type_id},
                success:function(data){
                    total = data.total;
                    num =1;

                    if(total < num*firstRow){
                        $('.pullUpIcon').hide();
                        $('.pullUpLabel').html('没有更多了');
                    }else if(total > num*firstRow){
                    $('.pullUpIcon').hide();
                    $('.pullUpLabel').html('<span class="pullUpIcon"></span>上拉加载...');
                    }

                    // $('.service_list').find('li').remove();
                    if(data != null){
                        if(data.providers_list){
                            var len = data.providers_list.length;
                        }
                        var html ='';
                        for(var i = 0; i < len; i++) {
                            html +='<li>'
                            +'<a href="/FrontService/providers_detail/service_providers_id/'+data.providers_list[i].service_providers_id+'">'
                                +'<div class="service_bg_box">'
                                +'<img src="'+data.providers_list[i].poster+'" alt="" />'
                                +'<div class="clearfix service_bg">'
                                +'<img src="'+data.providers_list[i].service_providers_logo+'" alt="" class="fl" />'
                                +'<dl class="clearfix fl">'
                                +'<dt class="txt_limit">'+data.providers_list[i].service_providers_name+'</dt>'
                                +'<dd class="fl">'+data.providers_list[i].service_type_name+'</dd>'
                                // +'<dd class="fl">大数据应用</dd>'
                                +'</dl>'
                                +'</div>'
                                +'</div>'
                                +'<p class="txt_limit_tw">'+data.providers_list[i].service_describe+'</p>'
                                +'</a>'
                                +'</li>'
                        }
                        $('.service_list').html(html);
                        setTimeout(function(){
			                myScroll.refresh();
			            },500);
                    }
                }
            })
        });

        $(".circle_state_ping").each(function(index) 
        {
            var number = $("li",this).length;
            if(number>3)
            {
                $(this).addClass('circle_state_hide');  
            }
            else{
                $(this).addClass('circle_state_active');
                $(this).siblings('.li_zhan').hide();
            }
        });
        $('.li_zhan').on("click",function()
        {
            $(this).hide();
            $(this).siblings('.circle_state_ping').removeClass('circle_state_hide');
        });
});