/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   SongGuangHui
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
{
    if(total<=firstRow)
    {
        $(".pullUpIcon").hide();
        $(".pullUpLabel").html("没有更多了");
    }
});
var num = 1; //默认页面显示4条数据              num可变
/*-----插入新的内容------*/
/*function appendCont(n,obj,comment_list){
 var txt="";
 var m = n-num;
 for(var i=0;i<n;i++){
 txt += '<dl class="clearfix">\
 <dt class="fl">\
 <img src="'+comment_list[i].headimgurl+'"/>\
 </dt>\
 <dd class="fl">s\
 <div class="clearfix">\
 <h3 class="fl">'+comment_list[i].nickname+'</h3>\
 <span class="fr">'+comment_list[i].addtime+'</span>\
 </div>\
 <p>'+comment_list[i].contents+'</p>\
 </dd>\
 </dl>';
 num++;
 }
 obj.append(txt);
 myScroll.refresh();
 setTimeout(function(){
 myScroll.refresh();
 allData();
 },500);
 }*/
function pullUpAction (){
    var sourceNode = $(".project_list").eq(0); // 获得被克隆的节点对象
    /* console.log(total+'----'+num*firstRow);*/
    if(total > num*firstRow){
        $.post('/FrontUserCenter/collect_touzi', {"firstRow":num*firstRow}, function(data, textStatus)
        {
            console.log(data);
            if (data != 'failure')
            {
                var len = data.length;
                var html = '';
                for (var i = 0; i < len; i++)
                {
                    if(data[i].follow_phase){
                          var phase_len = data[i].follow_phase.length;
                          for(var j = 0; j < phase_len ; j++){
                              if(j < 2){
                                  phase_v = '<dd class="fl">'+data[i].follow_phase[j]+'</dd>';
                              }
                          }
                      }
                      if(data[i].follow_field){
                          var field_len = data[i].follow_field.length;
                          for(var j = 0; j < field_len ; j++){
                              if(j < 2){
                                  field_v = '<dd class="fl">'+data[i].follow_field[j]+'</dd>';
                              }
                          }
                      }

                    html+='<li class="">\
                        <a href="/FrontEnjoy/touzi_details/user_id/'+data[i].user_id +'" class="clearfix">\
                    <img src="'+data[i].headimgurl+'" alt="" class="fl" />\
                    <div class="fr clearfix project_cont">\
                    <h6 class="fl">'+data[i].realname+'</h6>\
                    <span class="fl">'+data[i].company_address+'</span>\
                    <p>'+data[i].introduction+'</p>\
                    <dl class="clearfix">';
                    html += phase_v;
                    html += field_v;
                    html+='</dl>\
                    </div>\
                    </a>\
                    </li>'
                }
                sourceNode.append(html);
                setTimeout(function(){
                    myScroll.refresh();
                },500);
                num ++;
            }
        }, 'json');
    }
    else
    {
        $('.pullUpIcon').hide();
        $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  pullUpEl = document.getElementById('pullUp');
  pullUpOffset = pullUpEl.offsetHeight;
//	pullUpAction();
  myScroll = new iScroll('load_wrapper', {
//     useTransition: true,
    topOffset: pullDownOffset,
    onRefresh: function () {
      if (pullUpEl.className.match('loading')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
      }
    },
    onScrollMove: function () {
       if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
        pullUpEl.className = 'flip';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
        this.maxScrollY = this.maxScrollY;
      } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
        this.maxScrollY = pullUpOffset;
      }
    },
    onScrollEnd: function () {
      if (pullUpEl.className.match('flip')) {
        pullUpEl.className = 'loading';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
        pullUpAction(); // ajax call?
      }
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
