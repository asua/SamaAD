/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   SongGuangHui
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
  {
    if(total<=firstRow)
    {
      $(".pullUpIcon").hide();
      $(".pullUpLabel").html("没有更多了");
    }
  });
var num = 1; //默认页面显示4条数据              num可变
function del_native(){
	native_listen('no_loading');
}
function pullDownAction(){
	setTimeout(function(){
		del_native();
		window.location.reload();
	},300);
	window.onload = function(){
		myScroll.refresh();
	}
}
function pullUpAction (){
  var field_id = $('.field').find('.active').data('field_id');
  var city_id = $('.city_id').find('.active').data('city_id');
console.log(field_id);console.log(city_id)
	var sourceNode = $("#scroller .project_list").eq(0); // 获得被克隆的节点对象
	if(total > num*firstRow){
      $.post('/FrontEnjoy/touzi_list', {"firstRow":num*firstRow,"field_id":field_id,"city_id":city_id}, function(r, textStatus) 
       {
        if (r != 'failure'  && r.user_list != null)
        { 
          var data = r.user_list;
          var len = data.length;
          var html = '';
          
          for (i = 0; i < len; i++)
          {
            var phase_v = '';
            var field_v = '';
              if(data[i].follow_phase){
                  var phase_len = data[i].follow_phase.length;
                  for(var j = 0; j < phase_len ; j++){
                      if(j < 2){
                          phase_v = '<dd class="fl">'+data[i].follow_phase[j]+'</dd>';
                      }
                  }
              }
              if(data[i].follow_field){
                  var field_len = data[i].follow_field.length;
                  for(var j = 0; j < field_len ; j++){
                      if(j < 2){
                          field_v = '<dd class="fl">'+data[i].follow_field[j]+'</dd>';
                      }
                  }
              }
          	html += '<li class="">\
                    <a href="/FrontEnjoy/touzi_details/user_id/'+data[i].user_id+'" class="clearfix">\
                        <img src="'+data[i].headimgurl+'" alt="" class="fl" />\
                        <div class="clearfix project_cont">\
                            <h6 class="realname">'+data[i].realname+'</h6>\
                            <p class="jigou">'+data[i].company_address+'</p>\
                            <dl class="clearfix">'
                            + phase_v + field_v +
                            '</dl>\
                        </div>\
                    </a>\
                </li>';
          }
          sourceNode.append(html);
          setTimeout(function(){
            myScroll.refresh();
          },500);
          num ++;
        }
      }, 'json');      
    }
    else
    {
      $('.pullUpIcon').hide();
      $('.pullUpLabel').html('没有更多了');
    }
}

function loaded() {
  	pullUpEl = document.getElementById('pullUp');
  	pullUpOffset = pullUpEl.offsetHeight;
	pullDownEl = document.getElementById('pullDown');
	pullDownOffset = pullDownEl.offsetHeight;
  myScroll = new iScroll('load_wrapper', {
       useTransition: true,
    topOffset: pullDownOffset,
    onRefresh: function () {
      	if(pullDownEl.className.match('loading')) {
			pullDownEl.className = '';
		}else if(pullUpEl.className.match('loading')) {
	        pullUpEl.className = '';
	        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
	   	}
    },
    onScrollMove: function () {
    	if(this.y >5  && !pullDownEl.className.match('flip')) {
		    pullDownEl.className = 'flip';
		    this.minScrollY = 0;
	   	}else if(this.y < 5 && pullDownEl.className.match('flip')) {
		    pullDownEl.className = '';
		    this.minScrollY = -pullDownOffset;
	   	}else  if(this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
        pullUpEl.className = 'flip';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
        this.maxScrollY = this.maxScrollY;
      } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
        this.maxScrollY = pullUpOffset;
      }
      //移动到一定位置固定头部导航
      var src_top=myScroll.y
      if(src_top<-193){
         $("#top_nav_copy").addClass("top_nav_fixed");
          $("#top_nav").hide();
          $("#top_nav_copy").show();
          $("#load_wrapper").css("top","52px");
      }
      else
       {
         $("#top_nav_copy").removeClass("top_nav_fixed");
         $("#top_nav").show();
         $("#top_nav_copy").hide();
         $("#load_wrapper").css("top","0px");
       }
    },
    onScrollEnd: function () {
    	if(pullDownEl.className.match('flip')) {
			pullDownEl.className = 'loading';
			pullDownAction();
		}else if(pullUpEl.className.match('flip')) {
        pullUpEl.className = 'loading';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
        pullUpAction(); // ajax call?
      }
      //移动到一定位置固定头部导航
      var src_top=myScroll.y
      if(src_top<-193){
         $("#top_nav_copy").addClass("top_nav_fixed");
          $("#top_nav").hide();
          $("#top_nav_copy").show();
          $("#load_wrapper").css("top","52px");
      }
      else
       {
         $("#top_nav_copy").removeClass("top_nav_fixed");
         $("#top_nav").show();
         $("#top_nav_copy").hide();
         $("#load_wrapper").css("top","0px");
       }
       //设置阴影高度
       setTimeout(function(){
       	shadeBg();
       },100);
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
 function shadeBg(){
	var scrollerHei = $("#scroller").height();
    var bannerHei = $(".circle_banner").height();
    var topNavHei = $("#top_nav").height();
    var tabHei = $(".project_type_tab").height();
    var typeListHei = $(".project_type_list").height();
	var h = scrollerHei - bannerHei - topNavHei - tabHei - typeListHei;
    $(".project_bg").height(h+"px");
}