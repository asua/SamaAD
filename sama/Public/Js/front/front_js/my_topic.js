/*
 * @Author: zhangfengjie
 * @Date:   2016-04-01 16:57:34
 * @Last Modified by:   SongGuangHui
 * @Last Modified time: 2017-03-04 14:35:29
 */
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll, pullDownEl, pullDownOffset, pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function () {
    if (total <= firstRow) {
        $(".pullUpIcon").hide();
        $(".pullUpLabel").html("没有更多了");
    }
});
var num = 1; //默认页面显示4条数据              num可变
/*-----插入新的内容------*/
/*function appendCont(n,obj,comment_list){
 var txt="";
 var m = n-num;
 for(var i=0;i<n;i++){
 txt += '<dl class="clearfix">\
 <dt class="fl">\
 <img src="'+comment_list[i].headimgurl+'"/>\
 </dt>\
 <dd class="fl">s\
 <div class="clearfix">\
 <h3 class="fl">'+comment_list[i].nickname+'</h3>\
 <span class="fr">'+comment_list[i].addtime+'</span>\
 </div>\
 <p>'+comment_list[i].contents+'</p>\
 </dd>\
 </dl>';
 num++;
 }
 obj.append(txt);
 myScroll.refresh();
 setTimeout(function(){
 myScroll.refresh();
 allData();
 },500);
 }*/
function pullUpAction() {
    var sourceNode = $(".tab_main").eq(0); // 获得被克隆的节点对象
    /* console.log(total+'----'+num*firstRow);*/
    if (total > num * firstRow) {
        $.post('/FrontUserCenter/my_topic', {"firstRow": num * firstRow}, function (data, textStatus) {
            console.log(data);
            if (data != 'failure' && data != null) {
                if(data){
                    var len = data.length;
                }
                var html = '';
                for (var i = 0; i < len; i++) {

                    html += '<li class="clearfix">\
                        <a href="/FrontEnjoy/topic_details/topic_id/{$v.topic_id}">\
                        <img src="'+data[i].pic+'" alt="" class="fl huati_img" />\
                        <dl class="fr huati_box">\
                        <dt class="txt_limit">'+data[i].title+'</dt>\
                    <dd class="txt_limit">'+data[i].contents+'</dd>\
                    <dd class="clearfix">\
                        <span class="fl">评论'+data[i].view_num+'</span><span class="fl">收藏'+data[i].collect_num+'</span>\
                    </dd>\
                    </dl>\
                    </a>\
                    </li>';
                }
                sourceNode.append(html);
                setTimeout(function () {
                    myScroll.refresh();
                }, 500);
                num++;
            }
        }, 'json');
    }
    else {
        $('.pullUpIcon').hide();
        $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
    pullUpEl = document.getElementById('pullUp');
    pullUpOffset = pullUpEl.offsetHeight;
//	pullUpAction();
    myScroll = new iScroll('load_wrapper', {
//     useTransition: true,
        topOffset: pullDownOffset,
        onRefresh: function () {
            if (pullUpEl.className.match('loading')) {
                pullUpEl.className = '';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
            }
        },
        onScrollMove: function () {
            if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
                pullUpEl.className = 'flip';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
                this.maxScrollY = this.maxScrollY;
            } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
                pullUpEl.className = '';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
                this.maxScrollY = pullUpOffset;
            }
        },
        onScrollEnd: function () {
            if (pullUpEl.className.match('flip')) {
                pullUpEl.className = 'loading';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
                pullUpAction(); // ajax call?
            }
        }
    });
    setTimeout(function () {
        document.getElementById('load_wrapper').style.left = '0';
    }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) {
    e.preventDefault();
}, false);
document.addEventListener('DOMContentLoaded', function () {
    setTimeout(loaded, 200);
}, false);
