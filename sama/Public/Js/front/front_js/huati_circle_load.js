/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   zhangfengjie
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
  {
    if(total<=firstRow)
    {
      $(".pullUpIcon").hide();
      $(".pullUpLabel").html("没有更多了");
    }
  });
var num = 1; //默认页面显示4条数据              num可变
function del_native(){
	native_listen('no_loading');
}
function pullDownAction(){
	setTimeout(function(){
		del_native();
		window.location.reload();
	},300);
	window.onload = function(){
		myScroll.refresh();
	}
}
function pullUpAction (){
	var sourceNode = $(".dynamic_list").eq(0); // 获得被克隆的节点对象
	if(total > num*firstRow){
      $.post('/FrontEnjoy/enjoy_index', {"firstRow":num*firstRow,}, function(data, textStatus) 
       {
        if (data != '' || data != null)
        { 
          var len = data.length;
          var html = '';
          for (i = 0; i < len; i++)
          {console.log(data)
          	html += '<div class="circle_state">\
			        <div class="circle_state_user">\
			            <img src="'+data[i].headimgurl+'" alt="" />\
			            <h6>'+data[i].nickname+'</h6>\
			            <span>'+data[i].position+'</span>\
			        </div>\
			        <div class="circle_state_cont">\
			            <p>'+data[i].contents+'</p>\
			        </div>\
			        <div class="circle_state_img clearfix">';

              if (data[i].imgs) {
                  var img_len = data[i].imgs.length;
                  if (img_len >= 3) {
                      for (var j = 0; j < img_len; j++) {
                          var class_name = '';
                          if (j == 0) {
                              class_name = 'fl dl_img'
                          } else if (j == 1) {
                              class_name = 'fr dr_img dr_one'
                          } else if (j == 2) {
                              class_name = 'fr dr_img'
                          }
                          if (j <= 2) {
                              html += '<img src="' + data[i].imgs[j] + '" class="dynamic_img ' + class_name + '" data-row="' + j + '" data-img="' + data[i].imgs[j] + '" />';
                          } else {
                              html += '<div class="dynamic_img hide" data-row="' + j + '" data-img="' + data[i].imgs[j] + '"></div>';
                          }
                      }
                  } else if (img_len == 2) {
                        for(var j =0;j < img_len; j++){
                            html+='<img src="' + data[i].imgs[j] + '" class="dynamic_img fl two" data-row="'+ j +'"  data-img="' + data[i].imgs[j] + '" />';
                        }
                  }else if (img_len == 1){
                      for(var j =0;j < img_len; j++){
                          html+='<img src="' + data[i].imgs[j] + '" class="dynamic_img fl one" data-row="'+ j +'"  data-img="' + data[i].imgs[j] + '" />';
                      }
                  }
              }
			var like_class = '';
			var zanNum = Number(data[i].praise_num);
			if(data[i].is_praise == 1){
				like_class = 'like-select';
			}else{
				like_class = 'like-default';
			}
			if(zanNum>999){
				zanNum = "(999+)";
			}else{
				zanNum = "("+zanNum+")";
			}
			var comment = '';
			if(data[i].comment_list != null){
				var comment_len = data[i].comment_list.length;
				var comments = data[i].comment_list;
				
				for(var z = 0; z < comment_len; z++){
					comment += '<li class="com_list clearfix">\
								<div class="list_title fl">\
									<span class="first_word name" >'+comments[z].from_nickname+'</span>';
									if(comments[z].reply_comment_id != 0){
										comment += '<span class="hf_word">回复</span><span class="last_word name">'+comments[z].to_nickname+'</span>'
									}
					comment += '：<span class="info_word">'+comments[z].contents+'</span>\
								</div>\
								<input type="text" class="show_keybord" data-user_id="{$comment.user_id}" data-comment_id="{$comment.comment_id}" />\
							</li>'
				}
			}
			
			html += '</div>\
			        <div class="circle_state_zan clearfix">\
			            <p class="">'+data[i].addtime+'</p>\
			            <div class="zan_icon" onclick="dianZan($(this))">\
			            	<i class="like '+like_class+'" data-id="'+data[i].view_id+'" data-praise_type="view"></i>\
			            	<span class="zan_num" data-zan="'+data[i].praise_num+'">'+zanNum+'</span>\
			            </div>\
			            <div class="comment_icon" data-id="'+data[i].view_id+'">\
							<span class="icon icon_pinglun3x"></span>\
							<input type="text" class="input_icon" data-id="'+data[i].view_id+'"/>\
						</div>\
			        </div>\
			        <ul class="comment_cont" data-id="'+data[i].view_id+'">'+
							comment
						+'</ul>';
              
             html += ' </div>';
          }
          sourceNode.append(html);
          setTimeout(function(){
            myScroll.refresh();
          },500);
          num ++;
        }
      }, 'json');      
    }
    else
    {
      $('.pullUpIcon').hide();
      $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  pullUpEl = document.getElementById('pullUp');
  	pullUpOffset = pullUpEl.offsetHeight;
	pullDownEl = document.getElementById('pullDown');
	pullDownOffset = pullDownEl.offsetHeight;
  	myScroll = new iScroll('load_wrapper', {
       	useTransition: true,
	    topOffset: pullDownOffset,
	    onRefresh: function () {
	      	if (pullDownEl.className.match('loading')) {
				pullDownEl.className = '';
			}else if(pullUpEl.className.match('loading')) {
		        pullUpEl.className = '';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
		   	}
	    },
	    onScrollMove: function () {
	    	$(".comment_input").blur();
	    	$(".comment_input_wrap").addClass("comment_hide");
	       	if(this.y >5  && !pullDownEl.className.match('flip')) {
			    pullDownEl.className = 'flip';
			    this.minScrollY = 0;
		   	}else if(this.y < 5 && pullDownEl.className.match('flip')) {
			    pullDownEl.className = '';
			    this.minScrollY = -pullDownOffset;
		   	}else if(this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
		        pullUpEl.className = 'flip';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
		        this.maxScrollY = this.maxScrollY;
	      	}else if(this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
		        pullUpEl.className = '';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
		        this.maxScrollY = pullUpOffset;
	      	}
	      //移动到一定位置固定头部导航
	      var src_top=myScroll.y
	      if(src_top<-193){
	         $("#top_nav_copy").addClass("top_nav_fixed");
	          $("#top_nav").hide();
	          $("#top_nav_copy").show();
	          $("#load_wrapper").css("top","52px");
	      }
	      else
	       {
	         $("#top_nav_copy").removeClass("top_nav_fixed");
	         $("#top_nav").show();
	         $("#top_nav_copy").hide();
	         $("#load_wrapper").css("top","0px");
	       }
	       
	    },
	    onScrollEnd: function () {
	    	if (pullDownEl.className.match('flip')) {
				pullDownEl.className = 'loading';
				pullDownAction();
			}else if (pullUpEl.className.match('flip')) {
		        pullUpEl.className = 'loading';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
		        pullUpAction(); // ajax call?
	      	}
	      //移动到一定位置固定头部导航
	      var src_top=myScroll.y
	      if(src_top<-193){
	         $("#top_nav_copy").addClass("top_nav_fixed");
	          $("#top_nav").hide();
	          $("#top_nav_copy").show();
	          $("#load_wrapper").css("top","52px");
	      }
	      else
	       {
	         $("#top_nav_copy").removeClass("top_nav_fixed");
	         $("#top_nav").show();
	         $("#top_nav_copy").hide();
	         $("#load_wrapper").css("top","0px");
	       }
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);

function dianZan(_this){
	var obj = _this.find("i");
    if(obj.hasClass('like-select')){
        var type = 0;//取消
    }else{
        var type = 1;//点赞
    }
    var id = obj.attr('data-id');
    var praise_type =obj.attr('data-praise_type');
    console.log(1111111);
    $.ajax({
        url:'/Front/praise',
        type:'post',
		async:false,
        data:{id:id, type:type, praise_type:praise_type},
        success:function(r){
            if(r.code == 0){
                obj.toggleClass("like-default").toggleClass("like-select");
                layer.open({
                    content: r.msg,
                    title: false,
                    btn: ['确定'],
                });
            }else{
            	console.log(r.msg);
                layer.open({
                    content: r.msg,
                    title: false,
                    btn: ['确定'],
                });
            }
        }
    })
}