/* 
* @Author: Marte
* @Date:   2017-03-08 00:17:35
* @Last Modified by:   Marte
* @Last Modified time: 2017-03-10 14:21:48
*/

$(document).ready(function(){
    $(".circle_tab li").on("click",function()
        {
            $(".project_tab_menu").hide();
            $(".project_type_tab li").removeClass('active_tab');
            $(".project_bg").hide();
            $(this).addClass('active').siblings().removeClass('active');
            var Obj = $(this).attr("id");
            var num = Obj.substring(2, Obj.length);
            $("#main"+num).show().siblings('.main').hide();
        }); 
		
        $(".project_type_tab li").on("click",function()
        {
            $(this).toggleClass('active_tab').siblings().removeClass('active_tab');
            var index = $(this).index();
            $(".project_type_list dl").eq(index).toggle().siblings().hide();
            $(".project_type_list1 dl").eq(index).toggle().siblings().hide();
            var t = $(".service_header").height()+$(".project_type_tab").height()+$(".project_type_list").height();
            var h = $(window).height() - t;
//          var h = $(window).height() - [$(".project_type_tab").height() + $(".project_type_tab").offset().top-$(document).scrollTop()];
            
            $(".project_bg").height(h);
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".project_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".project_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
        });
        $(".project_tab_menu dd").on("click",function()
        {
            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().hide();
            $(".project_type_tab li").removeClass('active_tab');
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".project_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".project_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
            var phase_id = $('.phase').find('.active').data('phase_id');
            var field_id = $('.field').find('.active').data('field_id');
        });

        $(".circle_state_ping").each(function(index) 
        {
            var number = $("li",this).length;
            if(number>3)
            {
                $(this).addClass('circle_state_hide');  
            }
            else{
                $(this).addClass('circle_state_active');
                $(this).siblings('.li_zhan').hide();
            }
        });
        //若没有评论
        var len = $(".circle_state_box").length;
        for(var i = 0;i<len;i++){
        	if($(".circle_state_box").eq(i).find("li").length==0){
        		$(".circle_state_box").eq(i).css("display","none");
        	}
        }
        //点击展开全部
        $('.li_zhan').on("click",function()
        {
            $(this).hide();
            $(this).siblings('.circle_state_ping').removeClass('circle_state_hide');
        });
        
    //评论    
//  $("body").on("click",".comment_icon",function(){
//  	layer.open({
//          content: "评论",
//          title: false,
//          btn: ['确定'],
//      });
//  });
    
});
//点赞
function dianZan(_this){
	var obj = _this.find("i");
    if(obj.hasClass('like-select')){
        var type = 0;//取消
    }else{
        var type = 1;//点赞
    }
    var id = obj.attr('data-id');
    var dataZan = Number(_this.find(".zan_num").attr("data-zan"));
    var praise_type =obj.attr('data-praise_type');
    console.log(111111);
    $.ajax({
        url:'/Front/praise',
        type:'post',
        async:false,
        data:{id:id, type:type, praise_type:praise_type},
        success:function(r){
            if(r.code == 0){
                obj.toggleClass("like-default").toggleClass("like-select");
                layer.open({
                    content: r.msg,
                    title: false,
                    btn: ['确定'],
                });
                if(type){
                	dataZan++;
                }else{
                	dataZan--;
                }
                if(dataZan>999){
            		_this.find(".zan_num").html("(999+)");
            	}else{
            		_this.find(".zan_num").html("("+dataZan+")");
            	}
                _this.find(".zan_num").attr("data-zan",dataZan);
            }else{
                layer.open({
                    content: r.msg,
                    title: false,
                    btn: ['确定'],
                });
            }
        }
    })
}