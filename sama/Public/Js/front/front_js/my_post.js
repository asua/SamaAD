/**
 * Created by 辰 on 2017/7/13.
 */
//顶部导航
$(".head_tab").on("click",".normal",function(){
    post_type = $(this).attr('data-post_type');
    console.log(post_type);
    $(this).addClass("active_tab").siblings().removeClass("active_tab");
    $.ajax({
        type:'post',
        url:'/FrontUserCenter/my_post_list',
        data:{post_type:post_type},
        success:function (data) {

            num = 1;
            total = data.total;
            var html = '';
            if (data.list != '' && data.list != null)
            {
                $('#pullUp').show();
                var len = data.list.length;
                for (var i = 0; i < len; i++)
                {
                    var user_title = ''
                        if(data.list[i].post_user_title){
                            for (var j = 0; j < data.list[i].post_user_title.length; j++) {
                                user_title += '<i class="tag" style="background-color: '+data.list[i].post_user_title[j].title_color+'">'+data.list[i].post_user_title[j].title_name+'</i>';
                            }
                        }

                    var imgs = '';
                    if(data.list[i].imgs){
                        for (var j = 0; j < data.list[i].imgs.length; j++) {
                            if(parseInt(data.list[i].imgs[j].is_show) == 0){
                                var blur = 'class="blur"';
                            }

                            if(j < 5){
                                imgs += '<li '+blur+'><img src="'+data.list[i].imgs[j].img+'?imageView2/1/w/200/h/200/imageslim" alt=""></li>';
                            }else{
                                imgs += '<li '+blur+'><div class="mask"><div class="txt">包含共'+data.list[i].img_count+'张图</div></div><img src="'+data.list[i].imgs[j].img+'?imageView2/1/w/200/h/200/imageslim" alt=""></li>'
                            }
                        }
                    }
                    

                    html +='<div class="home_item">\
					<div class="user_info clearfix">\
						<a href="/FrontUserCenter/others_space/user_id/'+data.list[i].user_id+'" class="avatar native_go">\
							<img src="'+data.list[i].post_user.headimgurl+'" alt="">\
						</a>\
						<div class="user_cont">\
							<span class="nickname">'+data.list[i].post_user.nickname+'</span>'
                        +user_title+
                        '</div>\
                    </div>\
                    <a href="/FrontChannel/post_detail/post_id/'+data.list[i].post_id+'" class="native_go">\
						<div class="item_title">'
                        +data.list[i].title+
                        '</div>\
                        <div class="item_desc">'
                        +data.list[i].content+
                        '</div>\
                        <ul class="imgbox clearfix">'
                        +imgs+
                        '</ul>';
                    if(data.list[i].post_type ==  normal){ //普通贴
                        html+='<div class="item_bot clearfix">\
                            <span class="time fl">'+data.list[i].addtime+'</span>\
                        <span class="time fl">'+data.list[i].channel_name+'</span>\
                        <span class="time fr">评论&nbsp;'+data.list[i].comment_num+'</span>\
                        <span class="time fr">查看&nbsp;'+data.list[i].clickdot+'</span>\
                        </div>';
                    }
                    if(data.list[i].post_type ==  praise){ //赞赏贴
                        html+='<div class="item_bot clearfix">\
                            <span class="time fl">'+data.list[i].addtime+'</span>\
                        <span class="time fl">查看&nbsp;'+data.list[i].clickdot+'</span><span class="time fl">评论&nbsp;'+data.list[i].comment_num+'</span>\
                        <span class="price fr">赞赏价：¥'+data.list[i].praise_price+'</span>\
                        <span class="wan fr">'+data.list[i].avg_score+'分</span>\
                        </div>';
                    }
                    if(data.list[i].post_type ==  reward) { //悬赏贴
                        html+='<div class="item_bot clearfix">\
                            <span class="time fl">'+data.list[i].addtime+'</span>\
                        <span class="time fl">查看 '+data.list[i].clickdot+'</span><span class="time fl">评论 '+data.list[i].comment_num+'</span>\
                        <span class="price fr">悬赏价：¥'+data.list[i].reward_money+'</span>\
                        </div>';
                    }
                    if(data.list[i].post_type ==  crowdfunding){
                        html+='<div class="item_bot clearfix">\
                            <span class="time fl">'+data.list[i].addtime+'</span>\
                        <span class="price fr">心愿价：¥'+data.list[i].goal_money+'</span>\
                        <span class="wan fr">已完成'+data.list[i].cf_rate+'%</span>\
                        </div>';
                    }
                    html+='</a>\
                        </div>';
                    $('.no_post').hide();
                    $('#load_wrapper').show();
                }
            }else{
                $('.no_post').show();
                $('#load_wrapper').hide();
            }
            $('.home_list').html(html);
            setTimeout(function(){
                myScroll.refresh();
            },500);
        }
    })







});