/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   zhangfengjie
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
  {
    if(total<=firstRow)
    {
      $(".pullUpIcon").hide();
      $(".pullUpLabel").html("没有更多了");
    }
  });
var num = 1; //默认页面显示4条数据              num可变
function del_native(){
	native_listen('no_loading');
}
function pullDownAction(){
	setTimeout(function(){
		del_native();
		window.location.reload();
	},300);
	window.onload = function(){
		myScroll.refresh();
	}
}
function pullUpAction (){
	var sourceNode = $(".dynamic_list").eq(0); // 获得被克隆的节点对象
	if(total > num*firstRow){
      $.post('/FrontEnjoy/enjoy_index', {"firstRow":num*firstRow,}, function(data, textStatus) 
       {
        if (data != '' || data != null)
        { 
          var len = data.length;
          var html = '';
          for (i = 0; i < len; i++)
          {
          	var imgs = '';
          	var comment = '';
          	console.log(data)
          	if(data[i].imgs != '' && data[i].imgs != null){
          		for (var j = 0; j < data[i].imgs.length; j++) {
          			if(data[i].imgs.length >= 3){
          				var img_num = 'three';
          			}else if(data[i].imgs.length == 2){
          				var img_num = 'two';
          			}else{
          				var img_num = 'one';
          			}
          			imgs += '<img src="'+data[i].imgs[j]+'" class="'+img_num+'" alt="">'
          		}
          	}

          	if(parseInt(data[i].is_praise) > 0){
          		var is_praise = 'like-select';
          	}else{
          		var is_praise = 'like-default';
          	}

          	if(parseInt(data[i].praise_num) > 999){
          		var praise_num = '(999+)';
          	}else{
          		var praise_num = '('+data[i].praise_num+')';
          	}

          	if(data[i].comment_list != null && data[i].comment_list != undefined){
	          	for (var k = 0; k < data[i].comment_list.length ; k++) {
	          		var reply_comment = '';
	          		if(parseInt(data[i].comment_list[k].reply_comment_id) > 0){
	          			var reply_comment = '<span class="hf_word">回复</span><span class="last_word name">'+data[i].comment_list[k].to_nickname+'</span>';
	          		}

	          		comment += '<li class="com_list clearfix">\
									<div class="list_title fl">\
										<span class="first_word name" >'+data[i].comment_list[k].from_nickname+'</span>'+
										reply_comment
										+'：<span class="info_word">'+data[i].comment_list[k].contents+'</span>\
									</div>\
									<input type="text" class="show_keybord" data-user_id="'+data[i].comment_list[k].user_id+'" data-comment_id="'+data[i].comment_list[k].comment_id+'" />\
								</li>'
	          	}
	        }
          	html += '<div class="circle_state">\
					<div class="circle_state_user">\
						<img src="'+data[i].headimgurl+'" />\
						<h6>'+data[i].realname+'</h6>\
						<span class="position">'+data[i].position+'</span>\
						<span class="timeago">'+data[i].addtime+'</span>\
					</div>\
					<div class="circle_state_cont">\
						<p class="text">'+data[i].contents+'</p><div class="circle_state_img clearfix">' + imgs;
					html += '</div>\
						<div class="circle_state_zan clearfix">\
							<div class="zan_icon" onclick="dianZan($(this))">\
								<i class="like '+is_praise+'" data-id="'+data[i].view_id+'" data-praise_type="view"></i>\
								<span class="zan_num" data-zan="'+data[i].praise_num+'">'+praise_num+'</span>\
							</div>\
							<div class="comment_icon">\
								<i class="img icon_pinglun3x"></i><span>评论</span>\
								<input type="text" class="input_icon" data-id="'+data[i].view_id+'" />\
							</div>\
						</div>\
						<ul class="comment_cont" data-id="'+data[i].view_id+'" >'+ comment;
						html += '</ul>\
					</div>\
				</div>';
          }
          sourceNode.append(html);
          setTimeout(function(){
            myScroll.refresh();
          },500);
          num ++;
        }
      }, 'json');      
    }
    else
    {
      $('.pullUpIcon').hide();
      $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  pullUpEl = document.getElementById('pullUp');
  	pullUpOffset = pullUpEl.offsetHeight;
	pullDownEl = document.getElementById('pullDown');
	pullDownOffset = pullDownEl.offsetHeight;
  	myScroll = new iScroll('load_wrapper', {
       	useTransition: true,
	    topOffset: pullDownOffset,
	    onRefresh: function () {
	      	if (pullDownEl.className.match('loading')) {
				pullDownEl.className = '';
			}else if(pullUpEl.className.match('loading')) {
		        pullUpEl.className = '';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
		   	}
	    },
	    onScrollMove: function () {
	    	$(".comment_input").blur();
	    	$(".comment_input_wrap").addClass("comment_hide");
	       	if(this.y >5  && !pullDownEl.className.match('flip')) {
			    pullDownEl.className = 'flip';
			    this.minScrollY = 0;
		   	}else if(this.y < 5 && pullDownEl.className.match('flip')) {
			    pullDownEl.className = '';
			    this.minScrollY = -pullDownOffset;
		   	}else if(this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
		        pullUpEl.className = 'flip';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
		        this.maxScrollY = this.maxScrollY;
	      	}else if(this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
		        pullUpEl.className = '';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
		        this.maxScrollY = pullUpOffset;
	      	}
	      //移动到一定位置固定头部导航
	      var src_top=myScroll.y
	      if(src_top<-193){
	         $("#top_nav_copy").addClass("top_nav_fixed");
	          $("#top_nav").hide();
	          $("#top_nav_copy").show();
	          $("#load_wrapper").css("top","52px");
	      }
	      else
	       {
	         $("#top_nav_copy").removeClass("top_nav_fixed");
	         $("#top_nav").show();
	         $("#top_nav_copy").hide();
	         $("#load_wrapper").css("top","0px");
	       }
	       
	    },
	    onScrollEnd: function () {
	    	if (pullDownEl.className.match('flip')) {
				pullDownEl.className = 'loading';
				pullDownAction();
			}else if (pullUpEl.className.match('flip')) {
		        pullUpEl.className = 'loading';
		        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
		        pullUpAction(); // ajax call?
	      	}
	      //移动到一定位置固定头部导航
	      var src_top=myScroll.y
	      if(src_top<-193){
	         $("#top_nav_copy").addClass("top_nav_fixed");
	          $("#top_nav").hide();
	          $("#top_nav_copy").show();
	          $("#load_wrapper").css("top","52px");
	      }
	      else
	       {
	         $("#top_nav_copy").removeClass("top_nav_fixed");
	         $("#top_nav").show();
	         $("#top_nav_copy").hide();
	         $("#load_wrapper").css("top","0px");
	       }
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);

function dianZan(_this){
	var obj = _this.find("i");
    if(obj.hasClass('like-select')){
        var type = 0;//取消
    }else{
        var type = 1;//点赞
    }
    var id = obj.attr('data-id');
    var praise_type =obj.attr('data-praise_type');
    console.log(1111111);
    $.ajax({
        url:'/Front/praise',
        type:'post',
		async:false,
        data:{id:id, type:type, praise_type:praise_type},
        success:function(r){
            if(r.code == 0){
                obj.toggleClass("like-default").toggleClass("like-select");
                layer.open({
                    content: r.msg,
                    title: false,
                    btn: ['确定'],
                });
            }else{
            	console.log(r.msg);
                layer.open({
                    content: r.msg,
                    title: false,
                    btn: ['确定'],
                });
            }
        }
    })
}