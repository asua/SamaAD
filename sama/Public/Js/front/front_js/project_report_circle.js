/* 
* @Author: Marte
* @Date:   2017-03-08 00:17:35
* @Last Modified by:   Marte
* @Last Modified time: 2017-03-10 14:21:48
*/

$(document).ready(function(){

    $(".circle_tab li").on("click",function()
        {
            $(".project_tab_menu").hide();
            $(".project_type_tab li").removeClass('active_tab');
            $(".project_bg").hide();
            $(this).addClass('active').siblings().removeClass('active');
            var Obj = $(this).attr("id");
            var num = Obj.substring(2, Obj.length);
            $("#main"+num).show().siblings('.main').hide();
        });

        $(".circle_state_zan i").on("click",function()
        {
            $(this).toggleClass('icon_like_selected3x');
        });

        $(".project_type_tab li").on("click",function()
        {
            $(this).toggleClass('active_tab').siblings().removeClass('active_tab');
            var index = $(this).index();
            $(".project_type_list dl").eq(index).toggle().siblings().hide();
            $(".project_type_list1 dl").eq(index).toggle().siblings().hide();
           	var t = $(".service_header").height()+$(".project_type_tab").height()+$(".project_type_list").height();
          	var h = $(window).height() - t;
//          var h = $(window).height() - [$(".project_type_tab").height() + $(".project_type_tab").offset().top-$(document).scrollTop()];
            console.log(h);
            $(".project_bg").height(h);
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".project_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".project_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
        });
        $(".project_tab_menu dd").on("click",function()
        {

            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().hide();
            $(".project_type_tab li").removeClass('active_tab');
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".project_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".project_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
            project_type_id = $('.project_type').find('.active').data('project_type_id');
            city_id = $('.city_id').find('.active').data('city_id');
            console.log(project_type_id);
            console.log(city_id);
            $.ajax({
                url:'/FrontService/get_project_list',
                type:'post',
                data:{project_type_id:project_type_id, city_id:city_id},
                success:function(data){
                    console.log(data);
                    num=1;
                    total = data.total;
                    if(total < num*firstRow){
                        $('.pullUpIcon').hide();
                        $('.pullUpLabel').html('没有更多了');
                    }else if(total > num*firstRow){
                        $('.pullUpIcon').hide();
                        $('.pullUpLabel').html('<span class="pullUpIcon"></span>上拉加载...');
                    }

                    if(data != null){
                        if(data.project_list){
                            var len = data.project_list.length;
                        }
                        var html ='';
                        for(var i = 0; i < len; i++) {
                            html+=' <li class="news-list">\
                                <a href="/FrontService/get_project_detail/id/'+ data.project_list[i].project_report_id +'" class="clearfix">\
                                <div class="cont_time fl">\
                                <p>'+ data.project_list[i].project_report_name+'</p>\
                                <span>'+data.project_list[i].add_time+'</span>\
                                </div>\
                                <img src="'+data.project_list[i].pic+'" alt="" class="fr" />\
                                </a>\
                                </li>'
                        }
                        $('#news_list').html(html);
                        setTimeout(function(){
			                myScroll.refresh();
			            },500);
                    }
                }
            })
        });

        $(".circle_state_ping").each(function(index) 
        {
            var number = $("li",this).length;
            if(number>3)
            {
                $(this).addClass('circle_state_hide');  
            }
            else{
                $(this).addClass('circle_state_active');
                $(this).siblings('.li_zhan').hide();
            }
        });
        $('.li_zhan').on("click",function()
        {
            $(this).hide();
            $(this).siblings('.circle_state_ping').removeClass('circle_state_hide');
        });
});