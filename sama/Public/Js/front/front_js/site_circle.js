/* 
* @Author: Marte
* @Date:   2017-03-08 00:17:35
* @Last Modified by:   Marte
* @Last Modified time: 2017-03-10 14:21:48
*/

$(document).ready(function(){

    $(".circle_tab li").on("click",function()
        {
            $(".project_tab_menu").hide();
            $(".project_type_tab li").removeClass('active_tab');
            $(".project_bg").hide();
            $(this).addClass('active').siblings().removeClass('active');
            var Obj = $(this).attr("id");
            var num = Obj.substring(2, Obj.length);
            $("#main"+num).show().siblings('.main').hide();
        });

        $(".circle_state_zan i").on("click",function()
        {
            $(this).toggleClass('icon_like_selected3x');
        });

        $(".project_type_tab li").on("click",function()
        {
            $(this).toggleClass('active_tab').siblings().removeClass('active_tab');
            var index = $(this).index();
            $(".project_type_list dl").eq(index).toggle().siblings().hide();
            $(".project_type_list1 dl").eq(index).toggle().siblings().hide();
            var t = $(".service_header").height()+$(".project_type_tab").height()+$(".project_type_list").height();
           var h = $(window).height() - t;
            
//          var h = $(window).height() - [$(".project_type_tab").height() + $(".project_type_tab").offset().top-$(document).scrollTop()];
            console.log(h);
            $(".project_bg").height(h);
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".project_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".project_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
        });
        $(".project_tab_menu dd").on("click",function()
        {

            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().hide();
            $(".project_type_tab li").removeClass('active_tab');
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".project_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".project_bg").hide();
                $("html,body").removeClass("body_scroll");
            }

            site_type_id = $('.site_type').find('.active').data('site_type_id');
            city_id = $('.city_id').find('.active').data('city_id');
            console.log(site_type_id);
            console.log(city_id);
            $.ajax({
                url:'/FrontSite/get_site_list',
                type:'post',
                data:{site_type_id:site_type_id, city_id:city_id,key_words:key_words},
                success:function(data){
                    total = data.total;
                    num = 1;

                    if(total < num*firstRow){
                        $('.pullUpIcon').hide();
                        $('.pullUpLabel').html('没有更多了');
                    }else if(total > num*firstRow){
                        $('.pullUpIcon').hide();
                        $('.pullUpLabel').html('<span class="pullUpIcon"></span>上拉加载...');
                    }

                    console.log(data);
                    // $('.site_list').find('a').remove();
                    if(data != null){
                        if(data.site_list){
                            var len = data.site_list.length;
                        }
                        var html ='';
                        for(var i = 0; i < len; i++) {
                            html +='<a href="/FrontSite/site_detail/site_id/'+ data.site_list[i].site_id +'" class="activity_bg">'
                                +'<img src="'+data.site_list[i].pic_arr[0]+'" alt="" />'
                                +'<div class="activity_bg_box">'
                                +'<h6>'+data.site_list[i].site_name+'</h6>'
                                +'<p>'+data.site_list[i].address+'</p>'
                                +'<span>'+data.site_list[i].site_type_name+'</span>'
                                +'</div>'
                                +'</a>'
                        }
                        $('.site_list').html(html)
                        setTimeout(function(){
				            myScroll.refresh();
				        },500);
                    }
                }
            })
        });

        $(".circle_state_ping").each(function(index) 
        {
            var number = $("li",this).length;
            if(number>3)
            {
                $(this).addClass('circle_state_hide');  
            }
            else{
                $(this).addClass('circle_state_active');
                $(this).siblings('.li_zhan').hide();
            }
        });
        $('.li_zhan').on("click",function()
        {
            $(this).hide();
            $(this).siblings('.circle_state_ping').removeClass('circle_state_hide');
        });
});