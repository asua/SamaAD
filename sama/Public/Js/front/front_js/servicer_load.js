/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   SongGuangHui
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
function allData(){
	$(".pullUpIcon").hide();
  	$(".pullUpLabel").html("没有更多了");
}
var num = 4; //默认页面显示4条数据              num可变
/*-----插入新的内容------*/
function appendCont(n,obj){
	var txt="";
	var m = n-num;
	for(var i=num;i<n;i++){
		txt += '';
		num++;
	}
	obj.append(txt);
	setTimeout(function(){
	    myScroll.refresh();
	    allData();
	},500);
}
function pullUpAction (){
	var sourceNode = $("#scroller .service_list"); // 获得被克隆的节点对象
	setTimeout(function(){
		/*   获得数据接口后的实际操作
			$.ajax({
				type:"get",
				url:"",     //数据接口
				success:function(reg){
					var len = reg.length;
					if(num>=len){
						allData();
					}else{
						appendCont(len,sourceNode);
					}
				}
			});
		*/
		/*---测试用----*/
		appendCont(6,sourceNode);
	},500);
}

function loaded() {
  pullUpEl = document.getElementById('pullUp');
  pullUpOffset = pullUpEl.offsetHeight;
//	pullUpAction();
  myScroll = new iScroll('load_wrapper', {
//     useTransition: true,
    topOffset: pullDownOffset,
    onRefresh: function () {
      if (pullUpEl.className.match('loading')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
      }
    },
    onScrollMove: function () {
       if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
        pullUpEl.className = 'flip';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
        this.maxScrollY = this.maxScrollY;
      } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
        this.maxScrollY = pullUpOffset;
      }
    },
    onScrollEnd: function () {
      if (pullUpEl.className.match('flip')) {
        pullUpEl.className = 'loading';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
        pullUpAction(); // ajax call?
      }
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
