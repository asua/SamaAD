/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   zhangfengjie
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
  {
    if(total<=firstRow)
    {
      $(".pullUpIcon").hide();
      $(".pullUpLabel").html("没有更多了");
    }
  });
var num = 1; //默认页面显示4条数据              num可变
// function del_native(){
// 	native_listen('no_loading');
// }
// function pullDownAction(){
// 	setTimeout(function(){
// 		del_native();
// 		window.location.reload();
// 	},300);
// 	window.onload = function(){
// 		myScroll.refresh();
// 	}
// }
function pullUpAction (){
	var sourceNode = $(".home_list").eq(0); // 获得被克隆的节点对象
	if(total > num*firstRow){
      $.post('/FrontUserCenter/my_post_list', {"firstRow":num*firstRow,post_type:post_type}, function(data, textStatus)
       {
		   var html = '';
			if (data.list != '' && data.list != null)
			{
				var len = data.list.length;
				for (var i = 0; i < len; i++)
				{
					var user_title = ''
					if(data.list[i].post_user_title){
						for (var j = 0; j < data.list[i].post_user_title.length; j++) {
							user_title += '<i class="tag" style="background-color: '+data.list[i].post_user_title[j].title_color+'">'+data.list[i].post_user_title[j].title_name+'</i>';
						}
					}

					var imgs = '';
					for (var j = 0; j < data.list[i].imgs.length; j++) {
						if(parseInt(data.list[i].imgs[j].is_show) == 0){
							var blur = 'class="blur"';
						}

						if(j < 5){
							imgs += '<li '+blur+'><img src="'+data.list[i].imgs[j].img+'?imageView2/1/w/200/h/200/imageslim" alt=""></li>';
						}else{
							imgs += '<li '+blur+'><div class="mask"><div class="txt">包含共'+data.list[i].img_count+'张图</div></div><img src="'+data.list[i].imgs[j].img+'?imageView2/1/w/200/h/200/imageslim" alt=""></li>'
						}
					}

					html +='<div class="home_item">\
						<div class="user_info clearfix">\
							<a href="/FrontUserCenter/others_space/user_id/'+data.list[i].user_id+'" class="avatar native_go">\
								<img src="'+data.list[i].post_user.headimgurl+'?imageView2/1/w/200/h/200/imageslim" alt="">\
							</a>\
							<div class="user_cont">\
								<span date-url="/FrontUserCenter/others_space/user_id/'+data[i].post_user.user_id+'" class="native_go nickname">'+data.list[i].post_user.nickname+'</span>'
						+user_title+
						'</div>\
					</div>\
					<a href="/FrontChannel/post_detail/post_id/'+data.list[i].post_id+'" class="native_go">\
							<div class="item_title">'
						+data.list[i].title+
						'</div>\
						<div class="item_desc">'
						+data.list[i].content+
						'</div>\
						<ul class="imgbox clearfix">'
						+imgs+
						'</ul>';
					if(data.list[i].post_type ==  normal){ //普通贴
						html+='<div class="item_bot clearfix">\
								<span class="time fl">'+data.list[i].addtime+'</span>\
							<span class="time fl">'+data.list[i].channel_name+'</span>\
							<span class="time fr">评论&nbsp;'+data.list[i].comment_num+'</span>\
							<span class="time fr">查看&nbsp;'+data.list[i].clickdot+'</span>\
							</div>';
					}
					if(data.list[i].post_type ==  praise){ //赞赏贴
						html+='<div class="item_bot clearfix">\
								<span class="time fl">'+data.list[i].addtime+'</span>\
							<span class="time fl">查看&nbsp;'+data.list[i].clickdot+'</span><span class="time fl">评论&nbsp;'+data.list[i].comment_num+'</span>\
							<span class="price fr">赞赏价：¥'+data.list[i].praise_price+'</span>\
							<span class="wan fr">'+data.list[i].avg_score+'分</span>\
							</div>';
					}
					if(data.list[i].post_type ==  reward) { //悬赏贴
						html+='<div class="item_bot clearfix">\
							<span class="time fl">'+data[i].addtime+'</span>\
							<span class="fl see">查看&nbsp;'+data[i].clickdot+'</span>\
							<span class="fl pl">评论&nbsp;'+data[i].comment_num+'</span>\
							<span class="price fr">悬赏价：¥'+data[i].reward_money+'</span>\
						</div>';
					}
					if(data.list[i].post_type ==  crowdfunding){
						html+='<div class="item_bot clearfix">\
							<span class="time fl">'+data[i].addtime+'</span>\
							<span class="price fr">心愿价：¥'+data[i].goal_money+'</span>\
							<span class="wan fr">已完成'+data[i].cf_rate+'%</span>\
						</div>';
					}
					html+='</a>\
							</div>';
				}
          sourceNode.append(html);
          var imgw = $('.imgbox li').width();
		  imgh = $('.imgbox li').height(imgw);

		  
          setTimeout(function(){
            myScroll.refresh();
          },500);
          num ++;
        }
      }, 'json');      
    }
    else
    {
      myScroll.refresh();
      $('.pullUpIcon').hide();
      $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  pullUpEl = document.getElementById('pullUp');
  pullUpOffset = pullUpEl.offsetHeight;
//	pullUpAction();
  myScroll = new iScroll('load_wrapper', {
//     useTransition: true,
    topOffset: pullDownOffset,
    onRefresh: function () {
      if (pullUpEl.className.match('loading')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
      }
    },
    onScrollMove: function () {
       if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
        pullUpEl.className = 'flip';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
        this.maxScrollY = this.maxScrollY;
      } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
        pullUpEl.className = '';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
        this.maxScrollY = pullUpOffset;
      }
    },
    onScrollEnd: function () {
      if (pullUpEl.className.match('flip')) {
        pullUpEl.className = 'loading';
        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
        pullUpAction(); // ajax call?
      }
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
