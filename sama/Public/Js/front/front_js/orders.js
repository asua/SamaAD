//点击展开收缩订单类型
$(".head_order_wrap").on("click",function(){
	$(".orders_type_wrap").toggleClass("wrap_hidden");
	$(this).find(".order_icon_down").toggleClass("order_icon_up");
});
$(".orders_type_wrap .orders_list ").on("click",function(){
	$(".orders_type_wrap .orders_list ").find(".type_icon ").removeClass("show_icon");
	//添加对号
	$(this).addClass("show").find(".type_icon ").addClass("show_icon");
	$(this).siblings().removeClass("show");
	var typeName = $(this).find(".type_name").html();
	$(".service_header .orders_name").html(typeName);
	$(".orders_type_wrap").addClass("wrap_hidden");
	//改变箭头的方向
	$(".head_order_wrap").find(".order_icon_down").removeClass("order_icon_up");
});

//导航栏
$(".order_nav li").on("click",function(){
	$(this).addClass("nav_acitive").siblings().removeClass("nav_acitive");
	var state = $(".nav_acitive").data('state');
	console.log(state);
	$.ajax({
		type:'post',
		url:'/FrontUserCenter/my_orders',
		data:{"state":state},
		success:function (data) {
			if (data != 'failure') {
                console.log(data);
				var len = data.list.length;
				var html = '';
				total = data.total;
				num =1;
				for (var i = 0; i < len; i++) {
					if(total <= num*firstRow){
						$('.pullUpIcon').hide();
						$('.pullUpLabel').html('没有更多了');
					}else if(total >= num*firstRow){
						$('.pullUpIcon').hide();
						$('.pullUpLabel').html('<span class="pullUpIcon"></span>上拉加载...');
					}

					if (data.list[i].order_type == 3) {
						html += '<div class="orders_info_list">\
                            <div class="orders_head">\
                            <p class="orders_number">订单号：' + data.list[i].site_order_info.order_sn + '</p>\
                        <span class="orders_state">' + data.list[i].state + '</span>\
                            </div>\
                            <a class="orders_cont clearfix" href="/FrontSite/site_order_detail/order_id/'+data.list[i].order_id+'">\
                            <img src="' + data.list[i].pic + '" class="orders_pic fl"/>\
                            <div class="fr orders_info">\
                            <div class="info_title">\
                            <h6 class="title_left">' + data.list[i].site_name + '</h6>\
                        <span class="title_right">¥' + data.list[i].site_order_info.unit_price + '</span>\
                        </div>\
                        <div class="info_mid">与会人数：' + data.list[i].site_order_info.order_num + '</div>\
                        <div class="info_bottom">' + data.list[i].address + '</div>\
                        </div>\
                        </a>\
                        <div class="orders_bottom">\
                            <div class="top">数量：' + data.list[i].num + '，实付：¥' + data.list[i].site_order_info.total_amount + '</div>'
						// <!--只有待付款时才有bottom-->
						if (data.list[i].site_order_info.pay_status == 0) {
							html += '<div class="bottom clearfix">\
                        <a href="/FrontSite/site_order_detail/order_id/'+data.list[i].order_id+'" class="fr pay_btn">立即支付</a>\
                        <a href="javascript:;" class="fr cancel_btn" onclick="cancel_order('+data.list[i].order_type+','+data.list[i].order_id+')">取消订单</a>\
                        </div>'
						}
						html += '</div>\
                        </div>'
					} else if (data.list[i].order_type == 2) {
						html += '<div class="orders_info_list">\
                            <div class="orders_head">\
                            <p class="orders_number">订单号：' + data.list[i].incubator_order_info.order_sn + '</p>\
                        <span class="orders_state">' + data.list[i].state + '</span>\
                            </div>\
                            <a class="orders_cont clearfix" href="/FrontIncubator/order_zhongchuang_detail/order_id/'+data.list[i].order_id+'">\
                            <img src="' + data.list[i].pic + '" class="orders_pic fl"/>\
                            <div class="fr orders_info">\
                            <div class="info_title">\
                            <h6 class="title_left">' + data.list[i].incubator_name + '</h6>\
                        <span class="title_right">¥' + data.list[i].incubator_order_info.unit_price + '</span>\
                        </div>\
                        <div class="info_mid">工位数量：' + data.list[i].incubator_order_info.station_num + '</div>\
                        <div class="info_bottom">' + data.list[i].address + '</div>\
                        </div>\
                        </a>\
                        <div class="orders_bottom">\
                        <div class="top">数量：' + data.list[i].incubator_order_info.station_num + '，实付：¥' + data.list[i].incubator_order_info.total_amount + '</div>'
						// <!--只有待付款时才有bottom-->
						if (data.list[i].incubator_order_info.pay_status == 0) {
							html += '<div class="bottom clearfix">\
                       	<a href="/FrontIncubator/order_zhongchuang_detail/order_id/'+data.list[i].order_id+'" class="fr pay_btn">立即支付</a>\
                        <a href="javascript:;" class="fr cancel_btn" onclick="cancel_order('+data.list[i].order_type+','+data.list[i].order_id+')">取消订单</a>\
                                </div>'
						}
						html += '</div>\
                           </div>'
					} else if (data.list[i].order_type == 1) {
						html += '<div class="orders_info_list">\
                            <div class="orders_head">\
                            <p class="orders_number">订单号：' + data.list[i].order_info.order_sn + '</p>\
                        <span class="orders_state">' + data.list[i].state + '</span>\
                            </div>\
                            <a class="orders_cont clearfix" href="/FrontActivity/activity_order_detail/order_id/'+data.list[i].order_id+'">\
                            <img src="' + data.list[i].pic + '" class="orders_pic fl"/>\
                            <div class="fr orders_info">\
                            <div class="info_title">\
                            <h6 class="title_left">' + data.list[i].activity_name + '</h6>\
                        <span class="title_right">¥' + data.list[i].order_info.unit_price + '</span>\
                        </div>\
                        <div class="info_mid">活动时间：' + data.list[i].hd_time + '</div>\
                        <div class="info_bottom">' + data.list[i].address + '</div>\
                        </div>\
                        </a>\
                        <div class="orders_bottom">\
                            <div class="top">数量：1，实付：¥' + data.list[i].order_info.total_amount + '</div>'
						// <!--只有待付款时才有bottom-->
						if (data.list[i].order_info.pay_status == 0) {
							html += '<div class="bottom clearfix">\
                                <a href="/FrontActivity/activity_order_detail/order_id/'+data.list[i].order_id+'" class="fr pay_btn">立即支付</a>\
                        <a href="javascript:;" class="fr cancel_btn" onclick="cancel_order('+data.list[i].order_type+','+data.list[i].order_id+')">取消订单</a>\
                                </div>'
						}
						html += '</div>\
                        </div>'
					}
				}

				$('.orders_info_wrap').html(html);
				setTimeout(function(){
					myScroll.refresh();
				},500);
			}
		}
	})

});
