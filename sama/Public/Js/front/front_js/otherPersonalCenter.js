/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   zhangfengjie
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
  {
    if(total<=firstRow)
    {
      $(".pullUpIcon").hide();
      $(".pullUpLabel").html("没有更多了");
    }
  });
var num = 1; //默认页面显示4条数据              num可变
function del_native(){
  native_listen('no_loading');
}
function pullDownAction(){
  setTimeout(function(){
    del_native();
    window.location.reload();
  },300);
  window.onload = function(){
    myScroll.refresh();
  }
}
function pullUpAction (){
  var sourceNode = $(".dynamic_list").eq(0); // 获得被克隆的节点对象
  if(total > num*firstRow){
      $.post('/FrontUserCenter/others_space', {"firstRow":num*firstRow,user_id:user_id}, function(data, textStatus)
       {
           
        if (data != '' && data != null)
        { 
            var len = data.length;
            console.log(data)
            
            for (i = 0; i < len; i++)
            {
                var html = '';
                var y_m_d = data[i].year_month_day;
                var obj = $('.post_box').find('[data-y_m_d="'+y_m_d+'"]');
                var m_len = obj.length;
                var imgs = '';
                for (var j = 0; j < data[i].imgs.length; j++) {
                    if(parseInt(data[i].imgs[j].is_show) == 0){
                        var blur = 'class="blur"';
                    }

                    if(j < 5){
                        imgs += '<li '+blur+'><img src="'+data[i].imgs[j].img+'?imageView2/1/w/200/h/200/imageslim" alt=""></li>';
                    }else{
                        imgs += '<li '+blur+'><div class="mask"><div class="txt">包含共'+data[i].img_count+'张图</div></div><img src="'+data[i].imgs[j].img+'?imageView2/1/w/200/h/200/imageslim" alt=""></li>'
                    }
                }

                if(m_len > 0){
                    html+='\
                            <ul class="post_content">\
                              <li class="channel_name">'+data[i].channel_name+'</li>\
                              <li class="post_title">'+data[i].title+'</li>\
                              <li class="post_brief">'+data[i].content+'</li>\
                              <ul class="imgbox clearfix">'
                              +imgs+
                              '</ul>\
                              <li class="look">查看</li><div class="look_number">'+data[i].clickdot+'</div>\
                              <li class="comment">评论</li><div class="comment_number">'+data[i].comment_num+'</div>\
                            </ul><div style="width:100%;height:0.05rem;background:#efefef;clear:both"></div>';
                   
                        var ul_len = $('.post_box').length;
                        var box_obj = $('.post_box').eq(ul_len - 1);
                        box_obj.append(html);
                }else{
                    // html+='</div>';
                    html+='<div class="post_box">\
                            <div class="time_box" data-y_m_d="'+data[i].year_month_day+'">\
                                <li class="date" >'+data[i].day+'</li>\
                                <li class="month">'+data[i].month+'</li>\
                            </div>\
                            <ul class="post_content native_go" data-url="/FrontChannel/post_detail/post_id/'+data[i].post_id+'">\
                                <li class="channel_name">'+data[i].channel_name+'</li>\
                                <li class="post_title">'+data[i].title+'</li>\
                                <li class="post_brief">'+data[i].content+'</li>\
                                <ul class="imgbox clearfix">'
                                +imgs+
                                '</ul>\
                                <li class="look">查看</li><div class="look_number">'+data[i].clickdot+'</div>\
                                <li class="comment">评论</li><div class="comment_number">'+data[i].comment_num+'</div>\
                            </ul>\
                            \
                          </div>';
                    $('.post_list').append(html);
                }
            }
          //sourceNode.append(html);
          setTimeout(function(){
            myScroll.refresh();
          },500);
          num ++;
        }
      }, 'json');      
    }
    else
    {
      $('.pullUpIcon').hide();
      $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
  pullUpEl = document.getElementById('pullUp');
    pullUpOffset = pullUpEl.offsetHeight;
  pullDownEl = document.getElementById('pullDown');
  pullDownOffset = pullDownEl.offsetHeight;
    myScroll = new iScroll('load_wrapper', {
        useTransition: true,
      topOffset: pullDownOffset,
      onRefresh: function () {
          if (pullDownEl.className.match('loading')) {
        pullDownEl.className = '';
      }else if(pullUpEl.className.match('loading')) {
            pullUpEl.className = '';
            pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
        }
      },
      onScrollMove: function () {

          if(this.y >5  && !pullDownEl.className.match('flip')) {
          pullDownEl.className = 'flip';
          this.minScrollY = 0;
        }else if(this.y < 5 && pullDownEl.className.match('flip')) {
          pullDownEl.className = '';
          this.minScrollY = -pullDownOffset;
        }else if(this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
            pullUpEl.className = 'flip';
            pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
            this.maxScrollY = this.maxScrollY;
          }else if(this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
            pullUpEl.className = '';
            pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
            this.maxScrollY = pullUpOffset;
          }
       
         
      },
      onScrollEnd: function () {
        if (pullDownEl.className.match('flip')) {
        pullDownEl.className = 'loading';
        pullDownAction();
      }else if (pullUpEl.className.match('flip')) {
            pullUpEl.className = 'loading';
            pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
            pullUpAction(); // ajax call?
          }
       
    }
  });
  setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);

