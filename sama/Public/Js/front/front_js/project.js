/* 
* @Author: Marte
* @Date:   2017-03-08 00:17:35
* @Last Modified by:   Marte
* @Last Modified time: 2017-03-10 14:21:48
*/

$(document).ready(function(){
    $(".circle_tab li").on("click",function()
        {
            $(".project_tab_menu").hide();
            $(".project_type_tab li").removeClass('active_tab');
            $(".shade_bg").hide();
            $(this).addClass('active').siblings().removeClass('active');
            var Obj = $(this).attr("id");
            var num = Obj.substring(2, Obj.length);
            $("#main"+num).show().siblings('.main').hide();
        });

        $(".circle_state_zan i").on("click",function()
        {
            $(this).toggleClass('icon_like_selected3x');
        });

        $(".project_type_tab li").on("click",function()
        {
            $(this).toggleClass('active_tab').siblings().removeClass('active_tab');
            var index = $(this).index();
            $(".project_type_list dl").eq(index).toggle().siblings().hide();
            $(".project_type_list1 dl").eq(index).toggle().siblings().hide();
          
            
            // var h = $(window).height() - [$(".project_type_tab").height() + $(".project_type_tab").offset().top-$(document).scrollTop()];
			//设置阴影高度
            
			// shadeBg();
            
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".shade_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".shade_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
            // setTimeout(function(){
            //     shadeBg();
            //     myScroll.refresh();
            // },500);
        });
        $(".project_tab_menu dd").on("click",function()
        {
            $(this).addClass('active').siblings().removeClass('active');
            $(this).parent().hide();
            $(".project_type_tab li").removeClass('active_tab');
            if($(".project_type_tab li").hasClass('active_tab'))
            {
                $(".shade_bg").show();
                $("html,body").addClass("body_scroll");
            }
            else
            {
                $(".shade_bg").hide();
                $("html,body").removeClass("body_scroll");
            }
            var phase_id = $('.phase').find('.active').data('phase_id');
            var field_id = $('.field').find('.active').data('field_id');
            $.ajax({
                url:'/FrontEnjoy/project_list',
                type:'post',
                data:{field:field_id, phase:phase_id},
                success:function(r){
                    $('.project_list').find('li').remove();
                    if(r.project_list != null){

                        var data = r.project_list;
                        var len = data.length;
                        var html ='';
                        for(var i = 0; i < len; i++){
                            // html += '<li class=""><a href="/FrontEnjoy/project_details/project_id/'+data[i].project_id+'" class="clearfix"><img src="'+data[i].pic+'" alt="" class="fl" /><div class="fr clearfix project_cont"><h6 class="fl">'+data[i].project_name+'</h6><span class="fl">'+data[i].nickname+'</span><p>'+data[i].project_desc+'</p><dl class="clearfix"><dd class="fl">'+data[i].phase_name+'</dd><dd class="fl">'+data[i].field_name+'</dd></dl></div></a></li>';
                            // 
                            
                            var phase_name = '';
                            if(data[i].phase_name != '' && data[i].phase_name != null){
                              phase_name = '<dd class="fl">'+data[i].phase_name+'</dd>';
                            }
                            var field_name = '';
                            if(data[i].field_name != '' && data[i].phase_name != null){
                              field_name = '<dd class="fl">'+data[i].field_name+'</dd>';
                            }
                            html += '<li class="">\
                                    <a href="/FrontEnjoy/project_details/project_id/'+data[i].project_id+'" class="clearfix">\
                                        <div class="find_pro_pic fl">\
                                            <img src="'+data[i].pic+'" alt="" class="" />\
                                        </div>\
                                        <div class="clearfix project_cont">\
                                            <h6 class="">'+data[i].project_name+'</h6>\
                                            <p class="nick_name">'+data[i].project_highlight+'</p>\
                                            <dl class="clearfix">'
                                                + phase_name + field_name +
                                           '</dl>\
                                        </div>\
                                    </a>\
                                </li>'; 
                            
                        }
                        $('.project_list').append(html);
                    }
                    total = r.total;
                    if(total <= num*firstRow){
                        $('.pullUpIcon').hide();
                        $('.pullUpLabel').html('没有更多了');
                    }
                    setTimeout(function(){
                      myScroll.refresh();
                    },500);
                }
            })
        });

        $(".circle_state_ping").each(function(index) 
        {
            var number = $("li",this).length;
            if(number>3)
            {
                $(this).addClass('circle_state_hide');  
            }
            else{
                $(this).addClass('circle_state_active');
                $(this).siblings('.li_zhan').hide();
            }
        });
        $('.li_zhan').on("click",function()
        {
            $(this).hide();
            $(this).siblings('.circle_state_ping').removeClass('circle_state_hide');
        });
        //阴影高度
  //       function shadeBg(){
  //           // scroller的高度
		// 	var scrollerHei = $("#scroller").height();
  //           // 轮播图的高度
		//     var bannerHei = $(".circle_banner").height();//150px

  //           var topNavMargin = parseInt($("#top_nav").css('margin-bottom'));
  //           var topNavHei = $("#top_nav").height() + topNavMargin;//70
  //           console.log(topNavHei)
  //           var hotBlockMargin = parseInt($('.hot_block').css('margin-bottom'));
  //           var hotBlockpaddding = parseInt($('.hot_block').css('padding-bottom'));
  //           var hotBlock = $('.hot_block').height() +hotBlockMargin + hotBlockpaddding;//
  //           console.log(hotBlock)//196
  //           var tabHei = $(".project_type_tab").height();
		//     var typeListHei = $(".project_type_list").height();

		// 	var h = scrollerHei - bannerHei - topNavHei -hotBlock -tabHei -50
  //           console.log(h)
		//     $(".shade_bg").height(h+"px");
		// }
});