/*
* @Author: zhangfengjie
* @Date:   2016-04-01 16:57:34
* @Last Modified by:   zhangfengjie
* @Last Modified time: 2017-03-04 14:35:29
*/
/*
 *myScroll 存储实例化的iScroll对象
 * */
var myScroll,pullDownEl, pullDownOffset,pullUpEl, pullUpOffset;
/*---当返回的后台数据全部加载，调用该方法---*/
$(function()
  {
    if(total<=firstRow)
    {
      $(".pullUpIcon").hide();
      $(".pullUpLabel").html("没有更多了");
    }
  });
var num = 1; //默认页面显示4条数据              num可变
function del_native(){
  native_listen('no_loading');
}

function pullUpAction (){
  var sourceNode = $(".home_list").eq(0); // 获得被克隆的节点对象
  if(total > num*firstRow){
      $.post('/FrontUserCenter/my_consume_record', {"firstRow":num*firstRow,consume_type:consume_type}, function(data, textStatus)
       {
           var html = '';
        if (data.list != '' || data.list != null)
        { 
          var len = data.list.length;
            if(data.consume_type == 1 || data.consume_type == 2 || data.consume_type == 3){
                for (var i = 0; i < len; i++)
                {
                    if(data.consume_type == 1){
                        html+='<div class="expenditrue_list">\
                            <span class="h1">支出金额</span><span class="h5">-'+data.list[i].amount_out+'元</span>\
                            <span class="h2">支出类型</span><span class="h6">'+data.list[i].change_type+'</span>\
                            <span class="h3">支出频道</span><span class="h7">'+data.list[i].channel_name+'</span>\
                            <span class="h4">支付时间</span><span class="h8">'+data.list[i].add_time+'</span>\
                            </div>' ;
                    }else if(data.consume_type == 2){
                        html+='<div class="expenditrue_list">\
                            <span class="h1">收入金额</span><span class="h5">+'+data.list[i].amount_in+'元</span>\
                        <span class="h2">收入类型</span><span class="h6">'+data.list[i].change_type+'</span>\
                        <span class="h3">收入频道</span><span class="h7">'+data.list[i].channel_name+'</span>\
                        <span class="h4">进账时间</span><span class="h8">'+data.list[i].add_time+'</span>\
                        </div>'
                    }else if(data.consume_type == 3){
                        html+=' <div class="expenditrue_list small">\
                            <span class="h1">充值金额</span><span class="h5">+'+data.list[i].amount_in+'元</span>\
                        <span class="h2">充值时间</span><span class="h6">'+data.list[i].add_time+'</span>\
                        </div>';
                    }
                }
            }


            if(data.consume_type == 4){
                for (var i = 0; i < len; i++) {
                    html += '<div class="expenditrue_list mid">\
                        <div class="w_box">\
                        <span class="withdraw_time">'+data.list[i].add_time+'</span><span class="status verifying">'+data.list[i].state_name+'</span>\
                    </div>\
                    <span class="num">提现金额</span><span class="withdraw_money verifying">-'+data.list[i].money+'元</span>\
                    <span class="ac_num">提现账户</span><span class="withdraw_account">'+data.list[i].alipay_account+'</span>\
                    </div>';
                }
            }

          sourceNode.append(html);
          setTimeout(function(){
            myScroll.refresh();
          },500);
          num ++;
        }
      }, 'json');      
    }
    else
    {
        myScroll.refresh();
      $('.pullUpIcon').hide();
      $('.pullUpLabel').html('没有更多了');
    }
}
function loaded() {
    pullUpEl = document.getElementById('pullUp');
    pullUpOffset = pullUpEl.offsetHeight;
//	pullUpAction();
    myScroll = new iScroll('load_wrapper', {
//     useTransition: true,
        topOffset: pullDownOffset,
        onRefresh: function () {
            if (pullUpEl.className.match('loading')) {
                pullUpEl.className = '';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
            }
        },
        onScrollMove: function () {
            if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
                pullUpEl.className = 'flip';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手开始刷新...';
                this.maxScrollY = this.maxScrollY;
            } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
                pullUpEl.className = '';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载...';
                this.maxScrollY = pullUpOffset;
            }
        },
        onScrollEnd: function () {
            if (pullUpEl.className.match('flip')) {
                pullUpEl.className = 'loading';
                pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';
                pullUpAction(); // ajax call?
            }
        }
    });
    setTimeout(function () { document.getElementById('load_wrapper').style.left = '0'; }, 300);
}
//初始化控件
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);

