$(function () {
    acp.tabsFromOrigin = "additem";//设置表单跳转的tabs origin

    $("#form_site").validate({
        // debug: true,
        ignore: "",
        rules: {
            //tabsindex1
            site_name: {
                tabform_required: true
            },
            incubator_id: {
                tabform_required: true
            },
            site_type_id: {
                tabform_required: true
            },
            price: {
                tabform_required: true,
                tabform_number: true
            },
            address: {
                tabform_required: true
            },
            people: {
                tabform_required: true,
                digits:true
            },
            user_name: {
                tabform_required: true
            },
            mobile: {
                tabform_required: true,
                digits:true
            },
            serial: {
                tabform_required: true,
                digits:true
            },
            start_time: {
                tabform_required: true
            },
            end_time: {
                tabform_required: true
            },
            site_sketch: {
                tabform_required: true
            }

        },
        messages: {
            //tabsindex1
            site_name: {
                tabform_required: "请输入场地名称"
            },
            incubator_id: {
                tabform_required: "请选择孵化器"
            },
            site_type_id: {
                tabform_required: "请选择场地类型"
            },
            price: {
                tabform_required: "请输入价格",
                tabform_number: "请输入正确的价格"
            },
            address: {
                tabform_required: "请输入地址"
            },
            people: {
                tabform_required: "请输入人数",
                digits:"请输入正确的人数"
            },
            user_name: {
                tabform_required: "请输入联系人"
            },
            mobile: {
                tabform_required: "请输入联系人电话",
                digits:"请输入联系人电话"
            },
            serial: {
                tabform_required: "请输入排序号",
                digits:"请输入正确排序号"
            },
            start_time: {
                tabform_required: "请输入时间"
            },
            end_time: {
                tabform_required: "请输入时间"
            },
            site_sketch: {
                tabform_required: "请输入场地的简介"
            }

        },
        errorPlacement: acp.form_ShowError,
        success: acp.form_HideError
    });
});
