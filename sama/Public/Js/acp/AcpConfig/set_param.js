$(function(){
	$("#set_param").validate({  
	     rules: { 
	    	 cash_rate: {  
	             required: true,
	             digits:true,
	         },
	         zhongchou_rate: {  
	             required: true, 
	             digits:true,
	         },
	         zhongchou_days:{  
	             required: true,  
	             digits:true,
	         },
	         offer_days:{  
	             required: true,  
	             digits:true,  
	         },  
	         offer_onlook_price_rate:{  
	             required: true,  
	             digits:true,  
	         },
	         offer_onlook_fc_rate:{  
	             required: true,  
	             digits:true,  
	         },
	         channel_sign_exp_num:{
	        	 required: true,  
	             digits:true,
	         },
	         post_exp_num:{
	        	 required: true,  
	             digits:true,
	         }
	     },  
	     messages: { 
	    	 cash_rate: {
	             required: "提现抽成比例不能为空",
	             digits:'必须输入整数。',
	         },
	         zhongchou_rate: {  
	             required: "心愿抽成比例不能为空",
	             digits:'必须输入整数。',
	         },  
	         zhongchou_days:{  
	             required: "心愿帖心愿天数不能为空",
	             digits:'必须输入整数。',  
	         }, 
	         offer_days:{  
	        	 required: "悬赏帖悬赏天数不能为空",
	             digits:'必须输入整数。',  
	         }, 
	         offer_onlook_price_rate:{  
	             required: "悬赏帖围观价格比例不能为空",
	             digits:'必须输入整数。',  
	         }, 
	         offer_onlook_fc_rate:{  
	             required: "悬赏帖围观分成比例不能为空",
	             digits:'必须输入整数。',  
	         },
	         channel_sign_exp_num:{
	        	 required: '频道签到经验数不能为空',
	             digits:'必须输入整数。',
	         },
	         post_exp_num:{
	        	 required: '发帖经验数不能为空',
	             digits:'必须输入整数。',
	         }
	     },  
	     errorPlacement: acp.form_ShowError,//显示出错信息(这段代码必须加)  
	     success:acp.form_HideError//验证成功隐藏错误信息(这段代码必须加)  
	 });
	
});


