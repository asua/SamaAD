$(function () {
    acp.tabsFromOrigin = "additem";//设置表单跳转的tabs origin

    $("#form_addServiceProviders").validate({
        // debug: true,
        ignore: "",
        rules: {
            //tabsindex1
            username: {
                tabform_required: true
            },
            password: {
                tabform_required: true
            },
            position: {
                tabform_required: true
            },
            service_providers_status: {
                tabform_required: true
            },
            service_type_id: {
                tabform_required: true
            },
            domain: {
                tabform_required: true
            },
            // service_providers_logo: {
            //     tabform_required: true
            // },
            service_providers_name: {
                tabform_required: true
            },
            province_id: {
                tabform_required: true
            },
            city_id: {
                tabform_required: true
            },
            area_id: {
                tabform_required: true
            },
            address: {
                tabform_required: true
            },
            user_name: {
                tabform_required: true
            },
            mobile: {
                tabform_required: true
            },
            url: {
                tabform_required: true,
                url: true
            },
            score: {
                tabform_required: true,
                tabform_number: true
            },
            enterprise_name: {
                tabform_required: true
            },
            enterprise_user_name: {
                tabform_required: true
            },
            email: {
                tabform_required: true,
                email: true
            },
            accounts: {
                tabform_required: true
            },
            business: {
                tabform_required: true
            },
            service_content: {
                tabform_required: true
            },
            service_describe: {
                tabform_required: true
            },
            service_providers_describe: {
                tabform_required: true
            },
            // charter: {
            //     tabform_required: true
            // },
            // accounts_code: {
            //     tabform_required: true
            // },
            // poster: {
            //     tabform_required: true
            // }
            //tabsindex3
            /*contents: {
             tabform_required: true
             }*/
        },
        messages: {
            //tabsindex1
            username: {
                tabform_required: "请输入用户帐号"
            },
            password: {
                tabform_required: "请输入用户密码"
            },
            position: {
                tabform_required: "请填写职位"
            },
            service_providers_status: {
                tabform_required: "请选择服务商状态"
            },
            domain: {
                tabform_required: "请选择领域"
            },
            service_type_id: {
                tabform_required: "请选择服务类型"
            },
            // service_providers_logo: {
            //     tabform_required: "请上传服务商LoGo"
            // },
            province_id: {
                tabform_required: "请选择省份"
            },
            city_id: {
                tabform_required: "请选择城市"
            },
            area_id: {
                tabform_required: "请选择区域"
            },
            service_providers_name: {
                tabform_required: "请输入服务商名称"
            },
            address: {
                tabform_required: "请输入地具体址"
            },
            user_name: {
                tabform_required: "请输入联系人姓名"
            },
            mobile: {
                tabform_required: "请输入联系人电话"
            },
            url: {
                tabform_required: "请输入网址",
                url: "	必须输入正确格式的网址"
            },
            score: {
                tabform_required: "评分不能为空！",
                tabform_number: "请填写正确的评分!"
            },
            enterprise_name: {
                tabform_required: "请输入企业简称"
            },
            enterprise_user_name: {
                tabform_required: "请输入企业负责人"
            },
            enterprise_user_mobile: {
                tabform_required: "请输入企业负责人电话"
            },
            email: {
                tabform_required: "请输入企业邮箱",
                email: "必须输入正确格式的电子邮件。"
            },
            accounts: {
                tabform_required: "请输入企业公众号"
            },
            business: {
                tabform_required: "请输入主营业务"
            },
            service_content: {
                tabform_required: "请输入服务内容"
            },
            service_describe: {
                tabform_required: "请输入服务简介"
            },
            service_providers_describe: {
                tabform_required: "请输入服务商简介"
            },
            // charter: {
            //     tabform_required: "请上传营业执照"
            // },
            // accounts_code: {
            //     tabform_required: "请上传二维码"
            // },
            // poster: {
            //     tabform_required: "请上传海报"
            // }
            //tabsindex3
            /*contents: {
             tabform_required: "" + item_name + "详情不能为空！"
             }*/
        },
        errorPlacement: acp.form_ShowError,
        success: acp.form_HideError
    });
});
