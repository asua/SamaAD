CREATE TABLE `tp_channel` (
  `channel_id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_name` varchar(64) NOT NULL DEFAULT '' COMMENT '频道名称',
  `channel_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '频道类型，1悬赏，2赞赏，3心愿',
  `class_id` int(11) NOT NULL DEFAULT '0' COMMENT '一级分类id',
  `sort_id` int(11) NOT NULL DEFAULT '0' COMMENT '二级分类id',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `icon` varchar(255) NOT NULL DEFAULT '' COMMENT '图标',
  `serial` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `isuse` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否显示，0否，1是',
  PRIMARY KEY (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tp_title` (
  `title_id` int(11) NOT NULL AUTO_INCREMENT,
  `title_name` varchar(32) NOT NULL DEFAULT '' COMMENT '头像名称',
  `color` varchar(32) NOT NULL DEFAULT '' COMMENT '颜色',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型，1普通头衔，2特殊头衔，3鉴定师',
  `tag` varchar(32) NOT NULL DEFAULT '' COMMENT '标签，3种普通头衔的标签',
  `channel_id` int(11) NOT NULL DEFAULT '0' COMMENT '鉴定师有权限的频道id',
  `rank_up_num` int(11) NOT NULL DEFAULT '0' COMMENT '升级所需的次数',
  PRIMARY KEY (`title_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='头衔表';

CREATE TABLE `tp_live_class` (
  `live_class_id` int(11) NOT NULL AUTO_INCREMENT,
  `live_class_name` varchar(32) NOT NULL DEFAULT '' COMMENT '视频分类',
  `serial` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `isuse` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否显示，0否，1是',
  PRIMARY KEY (`live_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='视频分类表';

CREATE TABLE `tp_live` (
  `live_id` int(11) NOT NULL AUTO_INCREMENT,
  `live_name` varchar(64) NOT NULL DEFAULT '' COMMENT '视频名称',
  `live_class_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类id',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `pic` varchar(255) NOT NULL DEFAULT '' COMMENT '封面图',
  `serial` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `isuse` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否显示，0否，1是',
  PRIMARY KEY (`live_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='视频频道表';

CREATE TABLE `tp_user_title` (
  `user_title_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `title_id` int(11) NOT NULL DEFAULT '0' COMMENT '头衔id',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '获得时间',
  PRIMARY KEY (`user_title_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户头衔表';

CREATE TABLE `tp_post_support` (
  `post_support_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '心愿帖id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `support_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '支持金额',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `is_refund` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已退款，0否，1是',
  PRIMARY KEY (`post_support_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='心愿帖支持表';

CREATE TABLE `tp_browse_history` (
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '帖子id',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='帖子浏览记录表';

CREATE TABLE `tp_mark_score` (
  `mark_score_id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '赞赏帖id',
  `score` decimal(2,1) NOT NULL DEFAULT '0.0' COMMENT '评分',
  `contents` varchar(255) NOT NULL DEFAULT '' COMMENT '评语',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`mark_score_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='赞赏帖评分表';

CREATE TABLE `tp_sign` (
  `sign_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `channel_id` int(11) NOT NULL DEFAULT '0' COMMENT '频道id',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '签到时间',
  `exp_num` int(11) NOT NULL DEFAULT '0' COMMENT '经验值',
  PRIMARY KEY (`sign_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='频道签到表';

CREATE TABLE `tp_at` (
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `at_user_id` int(11) NOT NULL DEFAULT '0' COMMENT '被@的用户id',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '链接',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户@表';

CREATE TABLE `tp_appreciate` (
  `appreciate_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `appreciate_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '赞赏类型，1赞赏帖的隐藏内容，2赞赏帖下的楼主评论的隐藏内容',
  `id` int(11) NOT NULL DEFAULT '0' COMMENT '类型对应的id',
  `fee` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '赞赏金额',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`appreciate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='赞赏帖赞赏表';


-- 2017-7-5
CREATE TABLE `tp_channel_sort` (
  `channel_sort_id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_sort_name` varchar(32) NOT NULL DEFAULT '' COMMENT '栏目名称',
  `serial` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `isuse` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否显示，0否，1是',
  `pic` varchar(255) NOT NULL DEFAULT '' COMMENT '图标',
  PRIMARY KEY (`channel_sort_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='频道栏目表';

-- 2017-7-6
ALTER TABLE `tp_channel`
ADD COLUMN `is_del`  tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除，0否，1是' AFTER `isuse`;

CREATE TABLE `tp_user_channel_rank` (
  `user_channel_rank_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `channel_id` int(11) NOT NULL DEFAULT '0' COMMENT '频道id',
  `channel_rank_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户在该频道的等级',
  `exp` int(11) NOT NULL DEFAULT '0' COMMENT '用户在该频道的经验值',
  PRIMARY KEY (`user_channel_rank_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户频道等级表';

CREATE TABLE `tp_channel_rank` (
  `channel_rank_id` int(11) NOT NULL AUTO_INCREMENT,
  `rank_name` varchar(32) NOT NULL DEFAULT '' COMMENT '等级名称',
  `need_exp` int(11) NOT NULL DEFAULT '0' COMMENT '升到该级所需的经验',
  PRIMARY KEY (`channel_rank_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='频道等级表';

CREATE TABLE `tp_follow` (
  `follow_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `followed_id` int(11) NOT NULL DEFAULT '0' COMMENT '被关注的用户/频道',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '关注时间',
  `follow_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型，1频道，2用户',
  PRIMARY KEY (`follow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='关注表';

-- 2017-7-7
ALTER TABLE `tp_at`
ADD COLUMN `content`  varchar(255) NOT NULL DEFAULT '' COMMENT '内容' AFTER `url`;

-- 2017-7-10
ALTER TABLE `tp_channel` ADD COLUMN `summary`  text NOT NULL COMMENT '简介' AFTER `is_del`;

CREATE TABLE `tp_post` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '标题',
  `text` text COLLATE utf8_unicode_ci NOT NULL COMMENT '内容',
  `imgs` text COLLATE utf8_unicode_ci NOT NULL COMMENT '图片',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '发布人',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '发布时间',
  `channel_id` int(11) NOT NULL DEFAULT '0' COMMENT '频道id',
  `post_type` tinyint(4) NOT NULL DEFAULT '4' COMMENT '帖子类型，1悬赏，2赞赏，3心愿，4普通',
  `clickdot` int(11) NOT NULL DEFAULT '0' COMMENT '点击量',
  `comment_num` int(11) NOT NULL DEFAULT '0' COMMENT '评论数',
  `is_del` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除，0否，1是',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='帖子表';

-- ccy 2017 07-11 用户表增加字段．
ALTER TABLE `tp_users`
ADD COLUMN `reply_num`  int NOT NULL COMMENT '用户回帖评论数';
ADD COLUMN `admire_money`  decimal(10,2) NOT NULL COMMENT '用户累计赞赏金额';
ADD COLUMN `share_num`  int NOT NULL COMMENT '分享次数．';

-- 增加佩戴时间 ccy 2017-07-11
ALTER TABLE `tp_user_title`
ADD COLUMN `adorn_time`  int(11) NOT NULL COMMENT '佩戴时间．';

-- 频道简介 ccy 2017-07-12
ALTER TABLE `tp_live_class`
ADD COLUMN `live_desc`  text NOT NULL COMMENT '频道简介' ;

ALTER TABLE `tp_post`
CHANGE COLUMN `text` `content`  text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '内容' AFTER `title`;

-- 帖子图片
CREATE TABLE `tp_post_img` (
  `post_img_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL DEFAULT '0' COMMENT '帖子id',
  `img_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '图片地址',
  PRIMARY KEY (`post_img_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `tp_post_comment` (
  `post_comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL DEFAULT '0' COMMENT '帖子id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `content` text COLLATE utf8_unicode_ci NOT NULL COMMENT '评论内容',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '评论时间',
  `floor` int(11) NOT NULL DEFAULT '0' COMMENT '楼层',
  `praise_num` int(11) NOT NULL DEFAULT '0' COMMENT '点赞数',
  `is_lz` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否楼主，0否，1是',
  PRIMARY KEY (`post_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='帖子评论表';

ALTER TABLE `tp_post_comment`
ADD COLUMN `reply_user_id`  int NOT NULL DEFAULT 0 COMMENT '被回复人id' AFTER `is_lz`,
ADD COLUMN `reply_comment_id`  int NOT NULL DEFAULT 0 COMMENT '被回复评论id' AFTER `reply_user_id`;
-- 增加佩戴时间 ccy 2017-07-12
ALTER TABLE `tp_users`
ADD COLUMN `sdasd`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '个性签名';

CREATE TABLE `tp_comment_img` (
  `comment_img_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_comment_id` int(11) NOT NULL DEFAULT '0' COMMENT '评论id',
  `img_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '图片地址',
  PRIMARY KEY (`comment_img_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='评论图片表';

-- 2017-7-12 ccy 帖子增加字段
ALTER TABLE `tp_post`
ADD COLUMN `praise_num`  int NOT NULL COMMENT '点赞数';
ADD COLUMN `collect_num`  int NOT NULL COMMENT '收藏数';

-- 2017-7-13
CREATE TABLE `tp_praise_hide` (
  `praise_hide_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `content` text COLLATE utf8_unicode_ci NOT NULL COMMENT '内容',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '解锁价格',
  `hide_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型，1帖子隐藏内容，2评论隐藏内容',
  `id` int(11) NOT NULL DEFAULT '0' COMMENT '类型对应的id',
  PRIMARY KEY (`praise_hide_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='赞赏帖隐藏内容表';

CREATE TABLE `tp_praise_hide_img` (
  `praise_hide_img_id` int(11) NOT NULL AUTO_INCREMENT,
  `praise_hide_id` int(11) NOT NULL DEFAULT '0' COMMENT '隐藏内容id',
  `img_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '图片地址',
  PRIMARY KEY (`praise_hide_img_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='赞赏帖隐藏图片';
ALTER TABLE `tp_praise_hide`
MODIFY COLUMN `praise_hide_id`  int(11) NOT NULL AUTO_INCREMENT FIRST ;

CREATE TABLE `tp_praise_score` (
  `praise_score_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `post_id` int(11) NOT NULL DEFAULT '0' COMMENT '帖子id',
  `score` decimal(2,1) NOT NULL DEFAULT '0.0' COMMENT '评分',
  `content` text COLLATE utf8_unicode_ci NOT NULL COMMENT '评语',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `floor` int(11) NOT NULL DEFAULT '0' COMMENT '楼层',
  PRIMARY KEY (`praise_score_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='赞赏帖评分表';


-- 2017-7-13 ccy 用户表增加字段
ALTER TABLE `tp_users`
ADD COLUMN `is_open`  int NOT NULL DEFAULT 0 COMMENT '是否公开 0不公开，1公开';

CREATE TABLE `tp_praise_hide_buy` (
  `praise_hide_buy_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `praise_hide_id` int(11) NOT NULL DEFAULT '0' COMMENT '隐藏内容id',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`praise_hide_buy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='赞赏帖隐藏内容购买表';

-- 2017 - 7 -14 ccy 频道增加精选
ALTER TABLE `tp_channel`
ADD COLUMN `is_featured`  tinyint(4) NOT NULL COMMENT '是否精选 1是 0否';

CREATE TABLE `tp_crowdfunding` (
  `crowdfunding_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL DEFAULT '0' COMMENT '帖子id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `goal_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '目标金额',
  `now_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '已筹金额',
  `support_num` int(11) NOT NULL DEFAULT '0' COMMENT '支持人数',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态，0审核中，1审核通过，2审核为通过，3成功，4失败',
  `total_days` int(11) NOT NULL DEFAULT '0' COMMENT '心愿天数',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `pass_time` int(11) NOT NULL DEFAULT '0' COMMENT '审核通过时间（即心愿开始时间）',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '心愿结束时间',
  `is_ht` tinyint(4) NOT NULL DEFAULT '0' COMMENT '楼主是否已回帖，0否，1是',
  `is_refund` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已退款，0否，1是',
  PRIMARY KEY (`crowdfunding_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='心愿帖心愿表';

ALTER TABLE `tp_post`
ADD COLUMN `review`  tinyint(4) NOT NULL DEFAULT 1 COMMENT '审核状态，除心愿帖外其他帖子默认通过' AFTER `is_del`;
-- 2017 - 7 -14 ccy
CREATE TABLE `tp_post_browse` (
  `post_browse_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '帖子浏览主键',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `post_id` int(11) NOT NULL COMMENT '帖子',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  PRIMARY KEY (`post_browse_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='浏览记录表';


CREATE TABLE `tp_cf_support` (
  `cf_support_id` int(11) NOT NULL,
  `crowdfunding_id` int(11) NOT NULL DEFAULT '0' COMMENT '心愿id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `support_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '支持金额',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '支持时间',
  `is_refund` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已退款，0否，1是',
  PRIMARY KEY (`cf_support_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='心愿支持表';

ALTER TABLE `tp_cf_support`
MODIFY COLUMN `cf_support_id`  int(11) NOT NULL AUTO_INCREMENT FIRST ;

CREATE TABLE `tp_cf_reply` (
  `cf_reply_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `post_id` int(11) NOT NULL DEFAULT '0' COMMENT '帖子id',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `content` text COLLATE utf8_unicode_ci NOT NULL COMMENT '内容',
  PRIMARY KEY (`cf_reply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='心愿楼主回帖表';

CREATE TABLE `tp_cf_reply_img` (
  `cf_reply_img_id` int(11) NOT NULL AUTO_INCREMENT,
  `cf_reply_id` int(11) NOT NULL DEFAULT '0' COMMENT '心愿回帖id',
  `img_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '图片地址',
  PRIMARY KEY (`cf_reply_img_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='心愿回帖图片表';

ALTER TABLE `tp_cf_reply`
ADD COLUMN `title`  varchar(255) NOT NULL DEFAULT '' COMMENT '标题' AFTER `content`;

ALTER TABLE `tp_users`
ADD COLUMN `npw_pay`  tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否开启免密支付，0否，1是' AFTER `is_open`;

-- 2017-7-16
CREATE TABLE `tp_reward` (
  `reward_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `post_id` int(11) NOT NULL DEFAULT '0' COMMENT '帖子id',
  `reward_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '悬赏金额',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `end_time` int(11) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态，0悬赏中，1已采纳答案，3退款中，4未采纳答案',
  `answer_id` int(11) NOT NULL DEFAULT '0' COMMENT '采纳的答案id',
  PRIMARY KEY (`reward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='悬赏表';

CREATE TABLE `tp_reward_answer` (
  `reward_answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `reward_id` int(11) NOT NULL DEFAULT '0' COMMENT '悬赏id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `content` text NOT NULL COMMENT '答案内容',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `is_adopt` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否被采纳，0否，1是',
  PRIMARY KEY (`reward_answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='悬赏答案表';

ALTER TABLE `tp_reward`
ADD COLUMN `adopted_user`  int NOT NULL DEFAULT 0 COMMENT '被采纳的用户id' AFTER `answer_id`;

CREATE TABLE `tp_reward_look` (
  `reward_look_id` int(11) NOT NULL AUTO_INCREMENT,
  `reward_id` int(11) NOT NULL DEFAULT '0' COMMENT '悬赏id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `look_money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '围观金额',
  PRIMARY KEY (`reward_look_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='悬赏围观表';

CREATE TABLE `tp_reward_answer_img` (
  `reward_answer_img_id` int(11) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(255) NOT NULL DEFAULT '' COMMENT '图片地址',
  `reward_answer_id` int(11) NOT NULL DEFAULT '0' COMMENT '悬赏答案id',
  PRIMARY KEY (`reward_answer_img_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='悬赏答案图片表';

ALTER TABLE `tp_reward`
ADD COLUMN `invite_users`  varchar(255) NOT NULL DEFAULT '' COMMENT '邀请的用户' AFTER `adopted_user`;

-- 2017-7-17
CREATE TABLE `tp_reward_refund_reason` (
  `reward_refund_reason_id` int(11) NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '原因',
  PRIMARY KEY (`reward_refund_reason_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='悬赏退款原因表';

ALTER TABLE `tp_reward`
ADD COLUMN `refund_reason_id`  tinyint(4) NOT NULL DEFAULT 0 COMMENT '退款原因' AFTER `invite_users`;

-- ccy 2017-07-18 帖子id
ALTER TABLE `tp_account`
ADD COLUMN `post_id`  int NOT NULL DEFAULT 0 COMMENT '帖子id' ;


-- 20707-7-19
CREATE TABLE `tp_live_buy` (
  `live_buy_id` int(11) NOT NULL AUTO_INCREMENT,
  `live_id` int(11) NOT NULL DEFAULT '0' COMMENT '视频id',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '购买时间',
  PRIMARY KEY (`live_buy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='视频购买记录';

-- ccy 2017-7-19 支付宝id
ALTER TABLE `tp_users`
ADD COLUMN `user_alipay_id`  int(11) NOT NULL COMMENT '用户最近的使用的支付宝id';

-- ccy 2017-7-19
CREATE TABLE `tp_user_alipay` (
  `alipay_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `alipay_account` varchar(255) NOT NULL COMMENT '支付宝帐号',
  `addtime` int(11) NOT NULL COMMENT '添加时间',
  `is_del` tinyint(4) NOT NULL COMMENT '是否删除 0否 1是',
  PRIMARY KEY (`alipay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户支付宝表';
-- ccy 2017-7-19
ALTER TABLE `tp_deposit_apply`
ADD COLUMN `alipay_id`  int NOT NULL COMMENT '支付宝id';

-- 2017-7-20
ALTER TABLE `tp_post_comment`
MODIFY COLUMN `content`  text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评论内容' AFTER `user_id`;

ALTER TABLE `tp_post_comment`
DEFAULT CHARACTER SET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- 2017-7-24
ALTER TABLE `tp_deposit_apply`
ADD COLUMN `apply_account`  varchar(255) NOT NULL DEFAULT '' COMMENT '提现账号' AFTER `pass_time`;


ALTER TABLE `tp_users`
ADD COLUMN `hx_account`  varchar(32) NOT NULL DEFAULT '' COMMENT '环信账号' AFTER `user_alipay_id`,
ADD COLUMN `hx_password`  varchar(32) NOT NULL DEFAULT '' COMMENT '环信密码' AFTER `hx_account`,
ADD COLUMN `hx_nickname`  varchar(32) NOT NULL DEFAULT '' COMMENT '环信昵称' AFTER `hx_password`;

-- 2017-7-24  ccy 意见反馈表
ALTER TABLE `tp_user_suggest`
MODIFY COLUMN `message`  text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '内容';

-- 2017-8-1
ALTER TABLE `tp_users`
ADD COLUMN `sx_num`  int NOT NULL DEFAULT 0 COMMENT '私信数' AFTER `hx_nickname`;

-- 2017-8-2
CREATE TABLE `tp_channel_title_priv` (
  `channel_id` int(11) NOT NULL DEFAULT '0' COMMENT '频道id',
  `post_title_ids` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '可发帖的头衔ids',
  `visit_title_ids` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '可访问的头衔ids'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='频道头衔权限表';

-- 2017-8-3
ALTER TABLE `tp_channel`
ADD COLUMN `all_visit`  tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否所有人可访问，0否，1是' AFTER `is_featured`,
ADD COLUMN `all_post`  tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否所有人可发帖，0否，1是' AFTER `all_visit`;
-- 2017-8-2 ccy
ALTER TABLE `tp_reward`
ADD COLUMN `is_del`  tinyint(4) NOT NULL COMMENT '是否删除 0否 1是';
ALTER TABLE `tp_crowdfunding`
ADD COLUMN `is_del`  tinyint(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0否 1是';

ALTER TABLE `tp_post`
ADD COLUMN `del_time`  int NOT NULL DEFAULT 0 COMMENT '删除时间' AFTER `review`;

ALTER TABLE `tp_users`
MODIFY COLUMN `is_open`  int(11) NOT NULL DEFAULT 1 COMMENT '是否公开 0不公开，1公开' AFTER `sdasd`;

-- 2017-8-14
ALTER TABLE `tp_title`
MODIFY COLUMN `title_name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头衔名称' AFTER `title_id`;

-- 2017-8-30
CREATE TABLE `tp_android_version` (
  `android_version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(16) NOT NULL DEFAULT '' COMMENT '版本号',
  `remark` text NOT NULL COMMENT '日志',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '下载地址',
  `addtime` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`android_version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='安卓版本';



