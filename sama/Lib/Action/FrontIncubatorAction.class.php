<?php

class FrontIncubatorAction extends FrontAction
{
    function _initialize()
    {
        parent::_initialize();
        $this->page_num = 3;
    }

    function park_list()
    {
        $where = 'incubator_type = 1';
        $key_words=$this->_request('key_words');
        if($key_words){
            $where.= ' AND incubator_name like "%'.$key_words.'%"';
        }

        $incubator_obj = new IncubatorModel();
        $total = $incubator_obj->getIncubatorNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $incubator_obj->setStart($firstRow);
        $incubator_obj->setLimit($this->page_num);
        //园区列表
        $yq_list = $incubator_obj->getIncubatorList('', $where, 'sort','');
       /* echo $incubator_obj->getLastSql();
        exit;*/
        //相关城市
        $city_obj = new AddressCityModel();

        $incubator_city_list = $incubator_obj->getAppearMostCity($where);//园区的type是1

        foreach ($incubator_city_list as $k => $v) {
            $city_name = $city_obj->getCityName($v['city_id']);
            $city_py = $city_obj->getCityPy($v['city_id']);
            $city_list[$v['city_id']]['city_name'] = $city_name;
            $city_list[$v['city_id']]['py'] = $city_py;
        }
        $city_list = array_sort($city_list, 'py');
        
        foreach ($yq_list as $k => $v) {
            $picarr = explode(',', $v['pic_url']);
            $yq_list[$k]['first_pic'] = $picarr[0];
        }
        $this->assign('city_list', $city_list);
		log_file(json_encode($city_list), 'ce', true);
        $this->assign('yq_list', $yq_list);
        $this->assign('key_words',$key_words);
        $this->assign('head_title', '园区列表');
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $this->display();
    }

    //异步加载园区
    function async_get_park_list()
    {
        $city_id = $this->_post('city_id');
        $key_words=$this->_request('key_words');
        $incubator_obj = new IncubatorModel();
        $incubator_obj->setLimit($this->page_num);
        //园区列表
        if ($city_id == 0) {//0代表全部
            $where='incubator_type = 1';
            if($key_words){
                $where.= ' AND incubator_name like "%'.$key_words.'%"';
            }
            $yq_list = $incubator_obj->getIncubatorList('', $where, 'sort');
            $total = $incubator_obj->getIncubatorNum($where);
        } else {
            $where='incubator_type = 1 AND city_id = ' . $city_id;
            if($key_words){
                $where.= ' AND incubator_name like "%'.$key_words.'%"';
            }
            $yq_list = $incubator_obj->getIncubatorList('', $where, 'sort');
            $total = $incubator_obj->getIncubatorNum($where);
        }
        if ($yq_list) {
            foreach ($yq_list as $k => $v) {
                $picarr = explode(',', $v['pic_url']);
                $yq_list[$k]['first_pic'] = $picarr[0];
            }
            $arr = array(
                'code' => 1,
                'total' => $total,
                'data' => $yq_list,
            );
        } else {
            $arr = array(
                'code' => -1,
                'total' => $total,
                'data' => '',
            );
        }
        echo json_encode($arr);
        exit;
    }

    //园区详情
    function park_detail()
    {
        $incubator_id = $this->_get('incubator_id');
        $incubator_obj = new IncubatorModel();
        $incubator_info = $incubator_obj->getIncubatorInfo('incubator_id =' . $incubator_id);
        $picstr = $incubator_info['pic_url'];
        $picarr = explode(',', $picstr);//轮播图

        $equipment_arr = explode(',', $incubator_info['equipment_id']);

        $site_obj = new SiteDeployModel();
        foreach ($equipment_arr as $k => $v) {
            $equipment_name_arr[$k]['equipment_name'] = $site_obj->getSiteDeployField($v, 'site_deploy_name');
            $equipment_name_arr[$k]['pic'] = $site_obj->getSiteDeployField($v, 'site_deploy_pic');
        }
        /*echo json_encode($equipment_name_arr);
        exit;*/

        /* $equipment_obj=new EquipmentModel();
         $equipment_list=$equipment_obj->getEquipmentlist('incubator_id ='.$incubator_id);*/

        $supporting_obj = new SupportingModel();
        $supporting_list = $supporting_obj->getSupportinglist('incubator_id =' . $incubator_id,'sort');

        $enter_team_obj = new EnterTeamModel();
        $domain_obj = new TypeModel();//把数据中的领域id转化为名字
        $enter_team_list = $enter_team_obj->getEnterTeamList('', 'incubator_id =' . $incubator_id,'sort');
        foreach ($enter_team_list as $k => $v) {
            $name_arr = $domain_obj->getTypeNameData($v['domain']);
            $enter_team_list[$k]['domain_name'] = implode(',', $name_arr);
        }

        $this->assign('enter_team_list', $enter_team_list);
        $this->assign('supporting_list', $supporting_list);
        $this->assign('equipment_name_arr', $equipment_name_arr);
        $this->assign('pic_arr', $picarr);
        $this->assign('incubator_info', $incubator_info);
        $this->assign('incubator_id', $incubator_id);
        $this->assign('head_title', '园区详情');
        $this->display();
    }

    //众创空间列表
    function zhongchuang_list()
    {
        /* //资讯
         $information_obj=new InformationArticleModel();
         $information_obj->setLimit(3);
         $information_list=$information_obj->getInformationList('','isuse = 1','addtime DESC',2);*/
        $where = 'incubator_type = 2';

        $key_words=$this->_request('key_words');
        if($key_words){
            $where.= ' AND incubator_name like "%'.$key_words.'%"';
        }

        $incubator_obj = new IncubatorModel();
        $total = $incubator_obj->getIncubatorNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $incubator_obj->setStart($firstRow);
        $incubator_obj->setLimit($this->page_num);

        //众创空间列表
        $zckj_list = $incubator_obj->getIncubatorList('', $where, 'sort','');
        $station_obj = new StationModel();//工位model


        //工位剩余量
        foreach ($zckj_list as $k => $v) {
            $station_info = $station_obj->getStationInfo('incubator_id = ' . $v['incubator_id'], 'sum(station_left_num) as num');
            $zckj_list[$k]['station_left_num'] = $station_info['num'] ? $station_info['num'] : 0;
            $pic_arr = explode(',', $v['pic_url']);
            $zckj_list[$k]['first_pic'] = $pic_arr[0];
        }

        //相关城市
        $city_obj = new AddressCityModel();
        $incubator_city_list = $incubator_obj->getAppearMostCity($where);


        foreach ($incubator_city_list as $k => $v) {
            $city_name = $city_obj->getCityName($v['city_id']);
            $city_py = $city_obj->getCityPy($v['city_id']);
            $city_list[$v['city_id']]['city_name'] = $city_name;
            $city_list[$v['city_id']]['py'] = $city_py;
        }
        $city_list = array_sort($city_list, 'py');

        $this->assign('head_title', '众创空间列表');
        $this->assign('key_words',$key_words);
        $this->assign('city_list', $city_list);
        $this->assign('zckj_list', $zckj_list);
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $this->display();
    }

//异步获取重创空间列表
    function async_get_zckj_list()
    {
        $city_id = $this->_post('city_id');
        $key_words=$this->_request('key_words');
        $incubator_obj = new IncubatorModel();
        $incubator_obj->setStart(0);
        $incubator_obj->setLimit($this->page_num);
        //众创空间列表
        if ($city_id == 0) {//0代表全部

            $where='incubator_type = 2';
            if($key_words){
                $where.= ' AND incubator_name like "%'.$key_words.'%"';
            }
            $zckj_list = $incubator_obj->getIncubatorList('', $where, 'sort');
            $total = $incubator_obj->getIncubatorNum($where);
        } else {
            $where='incubator_type = 2 AND city_id = ' . $city_id;
            if($key_words){
                $where.= ' AND incubator_name like "%'.$key_words.'%"';
            }
            $zckj_list = $incubator_obj->getIncubatorList('',$where, 'sort');
            $total = $incubator_obj->getIncubatorNum($where);
        }
        if ($zckj_list) {
            $station_obj = new StationModel();//工位model
            foreach ($zckj_list as $k => $v) {
                $station_info = $station_obj->getStationInfo('incubator_id = ' . $v['incubator_id'], 'sum(station_left_num) as num');
                $zckj_list[$k]['station_left_num'] = $station_info['num'] ? $station_info['num'] : 0;
                $pic_arr = explode(',', $v['pic_url']);
                $zckj_list[$k]['first_pic'] = $pic_arr[0];
            }
            $arr = array(
                'code' => 1,
                'total' => $total,
                'data' => $zckj_list,
            );
        } else {
            $arr = array(
                'code' => -1,
                'total' => $total,
                'data' => '',
            );
        }
        echo json_encode($arr);
        exit;
    }

    //众创空间详情
    function zhongchuang_detail()
    {
        $incubator_id = $this->_get('incubator_id');
        $incubator_obj = new IncubatorModel();
        $incubator_info = $incubator_obj->getIncubatorInfo('incubator_id =' . $incubator_id);
        $picstr = $incubator_info['pic_url'];
        $picarr = explode(',', $picstr);//轮播图

        $equipment_arr = explode(',', $incubator_info['equipment_id']);
        $site_obj = new SiteDeployModel();
        foreach ($equipment_arr as $k => $v) {
            $equipment_name_arr[$k]['equipment_name'] = $site_obj->getSiteDeployField($v, 'site_deploy_name');
            $equipment_name_arr[$k]['pic'] = $site_obj->getSiteDeployField($v, 'site_deploy_pic');
        }

        /*        $equipment_obj=new EquipmentModel();
                $equipment_list=$equipment_obj->getEquipmentlist('incubator_id ='.$incubator_id);*/

        $supporting_obj = new SupportingModel();
        $supporting_list = $supporting_obj->getSupportinglist('incubator_id =' . $incubator_id,'sort');

        $enter_team_obj = new EnterTeamModel();
        $domain_obj = new TypeModel();//把数据中的领域id转化为名字
        $enter_team_list = $enter_team_obj->getEnterTeamList('', 'incubator_id =' . $incubator_id,'sort');
        foreach ($enter_team_list as $k => $v) {
            $name_arr = $domain_obj->getTypeNameData($v['domain']);
            $enter_team_list[$k]['domain_name'] = implode(',', $name_arr);
        }
        //工位列表
        // $station_type_obj = new StationModel();
        // //$station_type_obj->setLimit(2);
        // $station_list = $station_type_obj->getStationList('', 'incubator_id = ' . $incubator_id, 'sort', 2);
        // $station_list = $station_type_obj->getListData($station_list);
        // $station_num = $station_type_obj->getStationNum('incubator_id = ' . $incubator_id);
        $class_obj = D('StationClass');
        $station_list = $class_obj->where('incubator_id = ' . $incubator_id. ' and is_del = 0')->order('serial')->limit(2)->select();
        $station_num = $class_obj->getStationClassNum('incubator_id = ' . $incubator_id. ' and is_del = 0');
        $this->assign('station_num', $station_num);
        $this->assign('station_list', $station_list);
        $this->assign('enter_team_list', $enter_team_list);
        $this->assign('supporting_list', $supporting_list);
        $this->assign('equipment_name_arr', $equipment_name_arr);
        $this->assign('pic_arr', $picarr);
        $this->assign('incubator_info', $incubator_info);
        $this->assign('head_title', '众创空间详情');
        $this->assign('incubator_id', $incubator_id);
        $this->display();
    }

    //异步获取工位列表
    function async_station_list()
    {
        $incubator_id = $this->_post('incubator_id');
        // $station_type_obj = new StationModel();
        // $num = $station_type_obj->getStationNum();
        // $station_list = $station_type_obj->getStationList('', 'incubator_id = ' . $incubator_id, '', "2,$num");
        // $station_list = $station_type_obj->getListData('', 'incubator_id = ' . $incubator_id, '', "2,$num");
        $class_obj = D('StationClass');
        $station_num = $class_obj->getStationClassNum('incubator_id = ' . $incubator_id. ' and is_del = 0');
        $station_list = $class_obj->where('incubator_id = ' . $incubator_id. ' and is_del = 0')->order('serial')->limit("2, $station_num")->select();
        
        echo json_encode($station_list);
        exit;
    }

    //预约入住
    function order_park_enter()
    {
        $data = $this->_post();
        $incubator_id = I('incubator_id');
        $incubator_obj = new IncubatorModel();
        $incubator_info = $incubator_obj->getIncubatorInfo('incubator_id = ' . $incubator_id);
         if($incubator_info['incubator_type']==1) {
           $this->assign('is_park', 1);
         }
        $station_obj = new StationModel();
        // $incubator_list = $station_obj->getStationList('', 'incubator_id = ' . $incubator_id);
        $class_obj = D('StationClass');
        $incubator_list = $class_obj->where('incubator_id = ' . $incubator_id. ' and is_del = 0')->order('serial')->select();
        $type = $this->_get('type');
        if ($data['opt'] == 'order') {
            $incubator_order_obj = new IncubatorOrderModel();
            //$station_info = $station_obj->getStationInfo('station_type_id ='.$data['station_type']);
            $station_info = $class_obj->where('station_class_id ='.$data['station_class']. ' and is_del = 0')->find();
            if(strtotime($data['order_time']) < time()){
                $return_data = array(
                    'code' => -1,
                    'msg' => '请提前一天预约!'
                );
                echo json_encode($return_data);
                exit;
            }


            //判断工位数量是否足够
            if($incubator_info['incubator_type'] == 2){
                
                // $total_station_num = $incubator_order_obj->getTotalStationNum('order_type = 2 and incubator_id ='.$incubator_id .' and order_time ='.strtotime($data['order_time']) .' and order_status <> 2 and station_type ='.$data['station_type']);
                // $left_station_num = $station_info['station_total_num'] - $total_station_num;
                 
                // if($left_station_num < $data['station_num']){

                //     $return_data = array(
                //         'code' => -1,
                //         'msg' => '工位剩余数量不足!'
                //     );
                //     $this->ajaxReturn($return_data);
                    
                // }
            }


            $arr = array(
                'order_type' => 2,//参观的类型号为1,入驻为2
                'incubator_name' => $data['incubator_name'],
                'incubator_id' => $data['incubator_id'],
                'user_id' => intval(session('user_id')),
                'order_time' => strtotime($data['order_time']),
                'company_name' => $data['company_name'],
                'contacts' => $data['contacts'],
                'contact_number' => $data['contact_number'],
                'station_num' => $data['station_num'],
                'user_remarks' => $data['user_remarks'],
                // 'station_type' => $data['station_type'],
                'addtime' => time(),
                'incubator_type' => $incubator_info['incubator_type'],
                // 'unit_price' => $station_info['price'],
                // 'total_amount' => $data['station_num'] * $station_info['price'],
                // 'order_sn' => 'ZC'.$incubator_order_obj->generateOrderSn(),
            );
            if($incubator_info['incubator_type'] == 2){

                if($data['day_num'] < 30){
                    $return_data = array(
                        'code' => -1,
                        'msg' => '预约入驻天数必须大于30天!'
                    );
                    echo json_encode($return_data);
                    exit;
                }
                $arr['day_num'] = $data['day_num'];
                // $arr['station_type'] =  $data['station_type'];
                $arr['station_class_id'] =  $data['station_class'];
                $arr['unit_price'] =  $station_info['price'];
                $arr['total_amount'] =  $data['station_num'] * $station_info['price'] * $data['day_num'];
                $arr['order_sn'] =  'ZC'.$incubator_order_obj->generateOrderSn();

            }
            $arr['prospectus'] = $data['cover'] ? $data['cover'] : '';//项目计划书

            // echo json_encode($arr);
            // exit;
            // dump($arr);die;
            $result = $incubator_order_obj->addIncubatorOrder($arr);
            if ($result) {
                if($incubator_info['incubator_type'] == 2){
                    $return_data = array(
                        'code' => 1,
                        'msg' => '预约成功',
                        'order_id' => $result,
                    );
                }else{
                    $return_data = array(
                        'code' => 2,
                        'msg' => '预约成功',
                        
                    );
                }
            } else {
                $return_data = array(
                    'code' => -1,
                    'msg' => '预约失败'
                );
            }
            echo json_encode($return_data);
            exit;
        }
        $this->assign('service_email', $GLOBALS['config_info']['CUSTOMER_SERVICE_EMAIL']);
        $this->assign('title', '预约入驻');
        $this->assign('head_title', '预约入驻');
        $this->assign('incubator_id', $incubator_id);
        $this->assign('incubator_info', $incubator_info);
        $this->assign('incubator_list', $incubator_list);
        $this->display();
    }

    //预约入住支付
    public function order_zhongchuang_detail(){
        $order_id = I('order_id',0,'int');

        $incubator_order_obj = new IncubatorOrderModel();
        $incubator_order_info = $incubator_order_obj->getIncubatorOrderInfo('order_id ='.$order_id);

        $incubator_obj = new IncubatorModel();
        $incubator_info = $incubator_obj->getIncubatorInfo('incubator_id ='.$incubator_order_info['incubator_id']);

        $pic_arr  = explode(',',$incubator_info['pic_url']);
        $incubator_info['pic'] = $pic_arr[0];

        $title = $incubator_order_info['pay_status']==0 ? '确认订单' : '订单详情';
        $this->assign('incubator_info', $incubator_info);
        $this->assign('incubator_order_info', $incubator_order_info);
        $this->assign('order_id', $order_id);
        $this->assign('head_title', $title);
        $this->display();
    }



    //预约参观
    function order_park_visit()
    {
        $data = $this->_post();
        $incubator_id = $this->_get('incubator_id');
        $incubator_obj = new IncubatorModel();
        $incubator_info = $incubator_obj->getIncubatorInfo('incubator_id = ' . $incubator_id);
        $station_obj = new StationModel();
        $incubator_list = $station_obj->getStationList('', 'incubator_id = ' . $incubator_id);


        $type = $this->_get('type');

        if ($data['opt'] == 'order') {

            if(strtotime($data['order_time']) < time()){
                $return_data = array(
                    'code' => -1,
                    'msg' => '请提前一天预约!'
                );
                echo json_encode($return_data);
                exit;
            }

            $arr = array(
                'order_type' => 1,//参观的类型号为1,入驻为2
                'incubator_name' => $data['incubator_name'],
                'incubator_id' => $data['incubator_id'],
                'user_id' => intval(session('user_id')),
                'order_time' => strtotime($data['order_time']),
                'company_name' => $data['company_name'],
                'contacts' => $data['contacts'],
                'contact_number' => $data['contact_number'],
                'visit_num' => $data['visit_num'],
                'addtime' => time(),
                'user_remarks' => $data['user_remarks'],
                'incubator_type' => $incubator_info['incubator_type'],
            );

            $incubator_order_obj = new IncubatorOrderModel();
            $result = $incubator_order_obj->addIncubatorOrder($arr);
            if ($result) {
                $return_data = array(
                    'code' => 1,
                    'msg' => '预约成功'
                );

            } else {
                $return_data = array(
                    'code' => -1,
                    'msg' => '预约失败'
                );
            }
            echo json_encode($return_data);
            exit;
        }
        $this->assign('title', '预约参观');
        $this->assign('head_title', '预约参观');

        $this->assign('type', $type);
        $this->assign('incubator_id', $incubator_id);
        $this->assign('incubator_info', $incubator_info);
        $this->assign('incubator_list', $incubator_list);
        $this->display();
    }

//    function order_zhongchuang()
//    {
//        $incubator_id = $this->_get('incubator_id');
//        $type = $this->_get('type');
//        if ($type == 1) {
//            $title = '预约入驻';
//        }
//        if ($type == 2) {
//            $title = '预约参观';
//        }
//        $data = $this->_post();
//        if ($data['opt'] == 'order') {
//            if ($data['type'] == 2) {
//                $arr['prospectus_url'] = $data['prospectus_url'];//项目计划书
//                $arr['station_num'] = $data['station_num'];
//            }else{
//                $arr['visit_num'] = $data['visit_num'];
//            }
//            $arr = array(
//                'order_type' => $data['type'],//参观的类型号为1,入驻为2
//                'incubator_name' => $data['incubator_name'],
//                'incubator_id' => $data['incubator_id'],
//                'order_time' => strtotime($data['order_time']),
//                'company_name' => $data['company_name'],
//                'contacts' => $data['contacts'],
//                'contact_number' => $data['contact_number'],
//                'user_remarks' => $data['user_remarks'],
//                'station_type' => $data['station_type'],
//                'addtime' => time(),
//                'unit_price' => $station_info['price'],
//                'total_amount' => $data['station_num'] * $station_info['price'],
//                'order_sn' => 'ZC'.$incubator_order_obj->generateOrderSn(),
//            );
//            $incubator_order_obj = new IncubatorOrderModel();
//            $result = $incubator_order_obj->addIncubatorOrder($arr);
//            if ($result) {
//                $return_data = array(
//                    'code' => 1,
//                    'msg' => '预约成功'
//                );
//
//            } else {
//                $return_data = array(
//                    'code' => -1,
//                    'msg' => '预约失败'
//                );
//            }
//            echo json_encode($return_data);
//            exit;
//        }
//        $this->assign('type', $type);
//        $this->assign('title', $title);
//        $this->assign('head_title', $title);
//
//        $this->assign('incubator_id', $incubator_id);
//        $this->display();
//    }

    //地图
    public function map()
    {
        $incubator_id = I('incubator_id', 0, 'intval');
        $incubator_obj = D('Incubator');
        $incubator = $incubator_obj->getIncubatorInfo('incubator_id =' . $incubator_id);
        $this->assign('lat', $incubator['latitude']);
        $this->assign('lng', $incubator['longitude']);
        $this->assign('head_title', $incubator['address']);
        $this->display();
    }

    //孵化器的下拉加载
    public function incubator_drop_loading()
    {
        $city_id = $this->_post('city_id');
        $type = $this->_post('type');
        $firstRow = $this->_post('firstRow');
        $key_words= $this->_request('key_words');
        $incubator_obj = new IncubatorModel();
        //孵化器列表
        $incubator_obj->setStart($firstRow);
        $incubator_obj->setLimit($this->page_num);
        if ($city_id == 0) {//0代表全部
            $where = 'incubator_type = ' . $type;
        } else {
            $where = 'incubator_type = ' . $type . ' AND city_id = ' . $city_id;
        }
        if($key_words){
            $where.= ' AND incubator_name like "%'.$key_words.'%"';
        }

        $incubator_list = $incubator_obj->getIncubatorList('', $where, 'sort');
        $station_obj = new StationModel();//工位model
        foreach ($incubator_list as $k => $v) {
            if ($type == 2) {//众创空间
                $station_info = $station_obj->getStationInfo('incubator_id = ' . $v['incubator_id'], 'sum(station_left_num) as num');
                $incubator_list[$k]['station_left_num'] = $station_info['num'] ? $station_info['num'] : 0;
            }
            $pic_arr = explode(',', $v['pic_url']);
            $incubator_list[$k]['first_pic'] = $pic_arr[0];
        }
        echo json_encode($incubator_list);
        exit;
    }

    //搜索
    public function search()
    {
        $hot_str = $GLOBALS['config_info']['HOT_NAME'];
        $hot_arr = explode(',', $hot_str);
        $from = $this->_request('from');
        $user_id = intval(session('user_id'));

        //历史搜索
        $search_obj = new SearchHistoryModel();
        $search_list = $search_obj->getSearchHistoryList('user_id=' . $user_id . ' AND is_del = 0', 'search_history_id DESC', 'search_value');
//        DUMP($search_obj->getLastSql());

        $this->assign('search_list', $search_list);

        $this->assign('hot_arr', $hot_arr);
        $this->assign('head_title', '搜索');
        $this->assign('from',$from);
        $this->display();
    }


    public function del_search()
    {

        $user_id = intval(session('user_id'));
        $search_obj = new SearchHistoryModel();
        $r = $search_obj->setSearchHistoryStatus('user_id=' . $user_id, array('is_del' => 1));
        if ($r) {
            $this->ajaxReturn('success');
        }
    }

    //首页搜索结果
    public function detail_search()
    {

        $key_words = $this->_get('search_name');
        $from = $this->_get('from');
        $this->add_search();
        redirect('/FrontIncubator/'.$from.'_list/key_words/'.$key_words);

    }



    //支付
    public function payment(){

        if(IS_POST && IS_AJAX){
            $order_id = I('order_id', 0, 'intval');
            $payway = I('payway');
            if(!$order_id || !$payway) $this->ajaxReturn(array('code'=>1));

            $order_obj = new IncubatorOrderModel();
            $order = $order_obj->getIncubatorOrderInfo('order_id ='.$order_id);
            
            if($order['pay_status'] != 0 || $order['total_amount'] <= 0) $this->ajaxReturn(array('code'=>1));
            if($payway == 'wx_pay'){
                $wxpay_obj = new WXPayModel();
                $result = $wxpay_obj->mobile_pay_code($order_id, 0, 0, false, 'incubator');
                $this->ajaxReturn(array('code'=>0,'parameter'=> $result));
            }elseif($payway == 'ali_pay'){
                $alipay_obj = new AlipayModel();
                $result = $alipay_obj->mobile_pay_code($order_id, 0, 'incubator');
                $this->ajaxReturn(array('code'=> 0,'parameter'=> $result));
            }
            $this->ajaxReturn(array('code'=> 1));
        }
    }
}
