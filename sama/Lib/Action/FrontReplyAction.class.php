<?php

//回复类
class FrontReplyAction extends FrontAction
{
    function _initialize()
    {
        parent::_initialize();
    }

    function reply()
    {
        $contents = $this->_request('contents');
        $user_id = session('user_id');
        $reply_user_id = $this->_request('reply_user_id');
        $comment_type=$this->_request('comment_type');
        $type_id=$this->_request('type_id');

        if($user_id==$reply_user_id){
            $return_data=array(
                'code'=>-1,
                'msg'=>'不能评论自己',
            );
            echo json_encode($return_data);
            exit;
        }
        $user_obj=new UserModel();

        $from_user_info=$user_obj->getUserInfo('nickname','user_id = '.$user_id);
        $from_nickname=$from_user_info['nickname'];//评论者的昵称

        $to_user_info=$user_obj->getUserInfo('nickname','user_id = '.$reply_user_id);
        $to_nickname=$to_user_info['nickname'];//被评论者的昵称


        $arr = array(
            'contents' => $contents,
            'from_nickname'=>$from_nickname,
            'to_nickname'=>$to_nickname,
            'user_id' => $user_id,
            'reply_user_id' =>$reply_user_id,
            'comment_type'=>$comment_type,
            'type_id'=>$type_id,
            'addtime'=>time(),
        );

        $comment_obj=new CommentModel();
        $result=$comment_obj->addComment($arr);
        if($result){
            $return_data=array(
                'code'=>1,
                'msg'=>'评论成功',
            );
        }else{
            $return_data=array(
                'code'=>-1,
                'msg'=>'评论失败',
            );
        }
        echo json_encode($return_data);
        exit;
    }
}