<?php

class FrontChannelAction extends FrontAction
{
    function _initialize()
    {
        parent::_initialize();

        $permission_url = $GLOBALS['config_info']['PERMISSION_URL'];

        $this->assign('permission_url', $permission_url);
        $this->assign('nav', 'channel');
        $this->page_num = 10;
    }

    function channel_index()
    {
        $user_id = intval(session('user_id'));

        if($this->is_hidden){
            $where_lb = 'serial < 20';
        }

        //是否可一键签到
        $all_sign = D('Sign')->checkAllSign($user_id);
        $this->assign('all_sign', $all_sign);

        //关注的频道
        $follow_obj = new FollowModel();
        $follow_channel_list = $follow_obj->getFollowList('user_id ='.$user_id. ' and follow_type ='.FollowModel::CHANNEL);
        $follow_channel_list = $follow_obj->getListData($follow_channel_list);
        $this->assign('follow_channel_list', $follow_channel_list);

        //推荐频道
        $recommend_channel = D('Channel')->recommendChannel();
        // dump($recommend_channel);die;
        $this->assign('recommend_channel', $recommend_channel);


        //轮播图
        $cust_flash_obj = new CustFlashModel();



        //wangtao 2017.9.15 -start


        if($this->is_hidden) {

            $cust_flash_list = $cust_flash_obj->getCustFlashList('', ' serial < 20 and isuse = 1 and cust_type = 0', ' serial ASC');

        }else{

            $cust_flash_list = $cust_flash_obj->getCustFlashList('', 'serial < 50 and cust_type = 0', ' serial ASC');

        }
        //wangtao 2017.9.15 -end

        $sort_list = $this->channel_center();
        $this->assign('sort_list', $sort_list);

        $this->assign('cust_flash_list', $cust_flash_list);
        $this->assign('head_title', 'Sama字母圈');
        $this->display();
    }


    //频道信息
    public function channel_info(){
        $channel_id = I('channel_id', 0, 'intval');
        $channel = D('Channel')->getChannelInfo('channel_id ='.$channel_id);
        $this->assign('channel', $channel);
        //今日新帖数
        $new_post_num = D('Post')->getTodayPostNum($channel_id);
        $this->assign('new_post_num', $new_post_num);

        //总关注
        $total_follow_num = D('Follow')->getFollowNum('followed_id ='.$channel_id .' and follow_type = '.FollowModel::CHANNEL);
        $this->assign('total_follow_num', $total_follow_num);
        //总帖子数
        $total_post_num = D('Post')->getPostNum('channel_id ='.$channel_id);
        $this->assign('total_post_num', $total_post_num);

        $user_id = intval(session('user_id'));
        //是否关注该频道
        $is_follow = D('Follow')->checkFollowChannel($user_id, $channel_id);
        $this->assign('is_follow', $is_follow);

        $this->assign('head_title', '频道信息');
        $this->display();
    }

    //一键签到
    public function all_sign(){
        if(IS_AJAX && IS_POST){
            $user_id = intval(session('user_id'));
            if(!$user_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'请先登录'));

            $followed_channel_list = D('Follow')->getFollowedChannelList($user_id);
            if(!$followed_channel_list) $this->ajaxReturn(array('code'=>1, 'msg'=>'您还未关注任何频道'));

            $not_sign_channel = D('Channel')->getNotSignChannel($user_id);
            if(!$not_sign_channel) $this->ajaxReturn(array('code'=>1, 'msg'=>'没有可签到的频道'));

            if(D('Sign')->allChannelSign($user_id, $not_sign_channel)){
                $this->ajaxReturn(array('code'=>0, 'msg'=>'签到成功'));
            }
            $this->ajaxReturn(array('code'=>0, 'msg'=>'签到失败'));
        }
    }

    //签到
    public function sign(){
        if(IS_AJAX && IS_POST){
            $channel_id = I('channel_id', 0, 'intval');

            $user_id = intval(session('user_id'));
            if(!$user_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'请先登录'));
            if(!$channel_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'签到失败'));

            $is_follow = D('Follow')->checkFollowChannel($user_id, $channel_id);
            if(!$is_follow) $this->ajaxReturn(array('code'=>1, 'msg'=>'请先关注'));

            $is_sign = D('Sign')->checkSign($user_id, $channel_id);
            if($is_sign) $this->ajaxReturn(array('code'=>1, 'msg'=>'今天已签到'));

            if(D('Sign')->addSign($user_id, $channel_id)){
                //经验条
                $user_rank = D('UserChannelRank')->getUserChannelRankInfo('user_id ='.$user_id. ' and channel_id ='.$channel_id);
                $next_rank = D('UserChannelRank')->getUserNextChannelRank($user_id, $channel_id);
                if($next_rank){
                    $up_need_exp = $next_rank['need_exp'] - $user_rank['exp'];
                    $exp_rate = (1 - round($up_need_exp / $next_rank['need_exp'], 2)) * 100;
                }else{
                    $exp_rate = 100;
                }
                $rank = D('ChannelRank')->getChannelRankInfo('channel_rank_id ='.$user_rank['channel_rank_id']);
                $this->ajaxReturn(array('code'=>0, 'exp_rate'=>$exp_rate, 'rank'=>$rank['rank_name'],'msg'=>'签到成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'签到失败'));
        }
    }


    //频道中心
    public function channel_center(){

        $hidden = 0;
        $sql = '';
        if($this->is_hidden){
            $hidden = 1;
            //wangtao 2017.9.19 -strat
            $sql = ' AND serial < 20 ';
            //wangtao 2017.9.19 -end
        }


        $sort_list = D('Channel')->getChannelBySort($hidden,$sql);

        return $sort_list;
//        $this->assign('sort_list', $sort_list);
//        $this->assign('head_title', '频道中心');
//        $this->display();
    }


    //频道-帖子列表
    public function post_list(){
        $channel_id = I('channel_id', 0, 'intval');
        $user_id = intval(session('user_id'));
        //频道信息
        $channel_obj = new ChannelModel();
        $channel = $channel_obj->getChannelInfo('channel_id = '.$channel_id . ' and is_del = 0');
        $post_obj = new PostModel();
        $new_post_num = $post_obj->getTodayPostNum($channel_id);
        $this->assign('channel', $channel);
        $this->assign('new_post_num', $new_post_num);


//        是否有权限访问该频道
        $can_visit = D('ChannelTitlePriv')->checkVisitPriv($user_id, $channel_id);
//        var_dump($channel,$channel['isuse'],$channel_id);//|| !$channel['isuse']
        if(!$can_visit || !$channel['isuse']){
            $this->assign('head_title', $channel['channel_name']);
            $this->display('no_priv_visit');
            exit;
        }

        //帖子列表
        $where = 'channel_id = '.$channel_id . ' and is_del = 0 and review = 1';
        $total = $post_obj->getPostNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $post_obj->setStart($firstRow);
        $post_obj->setLimit($this->page_num);
        $post_list = $post_obj->getChannelPostList($channel_id,'updatetime DESC');
        //wangtao 2017.9.15 -start
        $post_list = $post_obj->getListData($post_list,true);
        //wangtao 2017.9.15 -end
        if(IS_AJAX && IS_POST){
            $this->ajaxReturn($post_list);
        }
        // dump($post_list);die;
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $this->assign('post_list', $post_list);
        // dump($post_list);die;
       
        //用户等级
        $user_rank = D('UserChannelRank')->getUserChannelRankInfo('channel_id ='.$channel_id. ' and user_id ='.$user_id);
        if($user_rank){
            $rank_id = $user_rank['channel_rank_id'];
            $rank = D('ChannelRank')->getChannelRankInfo('channel_rank_id ='.$rank_id);
            $rank_name = $rank['rank_name'];
        }else{
            $rank_name = 'LV0';
        }
        $this->assign('rank_name', $rank_name);

        //经验条
        $next_rank = D('UserChannelRank')->getUserNextChannelRank($user_id, $channel_id);
        if($next_rank){
            $up_need_exp = $next_rank['need_exp'] - $user_rank['exp'];
            $exp_rate = (1 - round($up_need_exp / $next_rank['need_exp'], 2)) * 100;
        }else{
            $exp_rate = 100;
        }
        $this->assign('exp_rate', $exp_rate);

        //是否关注该频道
        $is_follow = D('Follow')->checkFollowChannel($user_id, $channel_id);
        $this->assign('is_follow', $is_follow);

        //是否已签到
        $is_sign = D('Sign')->checkSign($user_id, $channel_id);
        $this->assign('is_sign', $is_sign);

        //频道类型
        $channel_type = D('Channel')->channelType();
        $channel_type = $channel_type[$channel['channel_type']];

        $this->assign('channel_type', $channel_type);



        $this->assign('head_title', $channel['channel_name']);

//        var_dump($channel_type,$channel['channel_name'],$is_sign,$is_follow,$exp_rate,$post_list);
//        die;
        $this->display();
    }


    //发布普通帖
    public function post_normal(){
        $user_id = intval(session('user_id'));
        if(IS_POST && IS_AJAX){
            $common_arr = $this->post_common(PostModel::NORMAL);
            $post_obj = new PostModel();
            if($post_obj->addPost($common_arr)){
                $this->ajaxReturn(array('code'=>0, 'msg'=>'帖子发布成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'帖子发布失败，请重试'));

        }
        $channel_id = I('channel_id', 0, 'intval');

        //是否有发帖权限
        $can_post = D('ChannelTitlePriv')->checkPostPriv($user_id, $channel_id);
        if(!$can_post){
            $this->assign('head_title', '发帖');
            $this->display('no_priv_post');
            exit;
        }

        $this->assign('channel_id', $channel_id);

        $this->assign('opt', 'normal');
        $this->assign('head_title', '发帖');
        $this->display('post_tiezi');
    }


    //发布赞赏帖
    public function post_praise(){
        $user_id = intval(session('user_id'));
        if(IS_POST && IS_AJAX){
            // dump($_POST);die;
            $common_arr = $this->post_common(PostModel::PRAISE);

            $hide_content = $_POST['hide_content'];
            $hide_imgs = I('hide_imgs', '');
            $hide_imgs = trim($hide_imgs, ',');
            $hide_price = I('hide_price', 0.0 , 'floatval');
            if(!$hide_content && !$hide_imgs){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'隐藏内容处内容、图片必须填写一项'));
            }

            if($hide_price <= 0.0){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'请填写正确的价格'));
            }

            $post_obj = new PostModel();
            if($p_id = $post_obj->addPost($common_arr)){
                //添加隐藏内容
                $hide_arr = array(
                    'content' => $hide_content,
                    'user_id' => $user_id,
                    'price' => $hide_price,
                    'hide_type' => PraiseHideModel::POST,
                    'id' => $p_id,
                    'imgs' => $hide_imgs,
                    );
                D('PraiseHide')->addPraiseHide($hide_arr);

                $this->ajaxReturn(array('code'=>0, 'msg'=>'帖子发布成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'帖子发布失败，请重试'));

        }
        $channel_id = I('channel_id', 0, 'intval');
        //是否有发帖权限
        $can_post = D('ChannelTitlePriv')->checkPostPriv($user_id, $channel_id);
//        if(!$can_post){
//            $this->assign('head_title', '发帖');
//            $this->display('no_priv_post');
//            exit;
//        }

        $this->assign('channel_id', $channel_id);
        $this->assign('opt', 'praise');
        $this->assign('head_title', '发帖/楼主回帖');
        $this->display('post_tiezi');
    }

    //发布悬赏贴
    public function post_reward(){

        $user_id = intval(session('user_id'));


        if(IS_AJAX && IS_POST){
            // dump($_POST);die;

            $common_arr = $this->post_common(PostModel::REWARD);
            $reward_money = I('price', 0, 'floatval');
            if($reward_money <= 0){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'请填写正确的金额'));
            }  

            $user = M('Users')->where('user_id ='.intval(session('user_id')))->find();
            if($user['left_money'] < $reward_money){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'余额不足'));
            }

            //邀请的用户，最多5人
            $invite_users = I('sel_users');
            $invite_u = implode(',', $invite_users);
            // if(count($invite_users) > 5){
            //     $new_invite_users[0] = $invite_users[0];
            //     $new_invite_users[1] = $invite_users[1];
            //     $new_invite_users[2] = $invite_users[2];
            //     $new_invite_users[3] = $invite_users[3];
            //     $new_invite_users[4] = $invite_users[4];
            //     $invite_u = implode(',', $new_invite_users);
            // }
            $at_users = '';
            if($invite_u){
                foreach (explode(',', $invite_u) as $k => $v) {
                    $at_u = M('Users')->where('user_id ='.$v)->getField('nickname');
                    $at_users .= ' @'.$at_u;
                }
            }
            $common_arr['content'] .= $at_users;

            $post_obj = new PostModel();
            if($p_id = $post_obj->addPost($common_arr)){
                $reward_days = $GLOBALS['config_info']['OFFER_DAYS'] ? $GLOBALS['config_info']['OFFER_DAYS'] : 5;
                
                $reward_arr = array(
                    'post_id' => $p_id,
                    'user_id' => $user_id,
                    'reward_money' => $reward_money,
                    'end_time' => time() + $reward_days * 86400,
                    'invite_users' => $invite_u ? $invite_u : '',
                    );
                D('Reward')->addReward($reward_arr);

                $this->ajaxReturn(array('code'=>0, 'msg'=>'帖子发布成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'帖子发布失败，请重试'));
        }


        $follow_obj = new FollowModel();
        $followed_users = $follow_obj->getFollowedUserlids($user_id);
        
        foreach ($followed_users as $k => $v) {
            $user = M('Users')->where('user_id ='.$v)->find();
            $followed_users[$k] = array('user_id'=>$v, 'nickname'=>$user['nickname'], 'headimgurl'=>$user['headimgurl']);
        }
        // dump($followed_users);die;
        $this->assign('followed_users', $followed_users);
        $channel_id = I('channel_id', 0, 'intval');

        //是否有发帖权限
        $can_post = D('ChannelTitlePriv')->checkPostPriv($user_id, $channel_id);
        if(!$can_post){
            $this->assign('head_title', '发帖');
            $this->display('no_priv_post');
            exit;
        }

        $this->assign('channel_id', $channel_id);
        $this->assign('reward_days', $GLOBALS['config_info']['OFFER_DAYS'] ? $GLOBALS['config_info']['OFFER_DAYS'] : 5);
        $this->assign('look_rate', $GLOBALS['config_info']['OFFER_ONLOOK_FC_RATE'] ? $GLOBALS['config_info']['OFFER_ONLOOK_FC_RATE'] : 50);
        $this->assign('opt', 'reward');
        $this->assign('head_title', '发悬赏贴');
        $this->display('post_tiezi');
    }

    //发布心愿帖
    public function post_crowdfunding(){

        $channel_id = I('channel_id', 0, 'intval');
        $user_id = intval(session('user_id'));

        //是否有发帖权限
        $can_post = D('ChannelTitlePriv')->checkPostPriv($user_id, $channel_id);
        if(!$can_post){
            $this->assign('head_title', '发帖');
            $this->display('no_priv_post');
            exit;
        }

        if(IS_POST && IS_AJAX){
            // dump($_POST);die;

            $common_arr = $this->post_common(PostModel::CROWDFUNDING);
            $common_arr['review'] = 0;
            $total_money = I('price', 0, 'intval');
            if($total_money <= 0){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'请填写正确的金额'));
            }
            $post_obj = new PostModel();
            if($p_id = $post_obj->addPost($common_arr)){
                //添加心愿信息
                $total_days = $GLOBALS['config_info']['ZHONGCHOU_DAYS'];
                $cf_arr = array(
                    'goal_money' => $total_money,
                    'user_id' => $user_id,
                    'total_days' => $total_days,
                    'post_id' => $p_id,
                    );
                D('Crowdfunding')->addCrowdfunding($cf_arr);

                $this->ajaxReturn(array('code'=>0, 'msg'=>'帖子发布成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'帖子发布失败，请重试'));

        }

        $this->assign('channel_id', $channel_id);
        $this->assign('opt', 'crowdfunding');
        $this->assign('head_title', '发起心愿');
        $this->display('post_tiezi');
    }


    //发布帖子公共
    private function post_common($channel_type){
        $user_id = intval(session('user_id'));
        $channel_id = I('channel_id', 0, 'intval');

        $can_post = D('ChannelTitlePriv')->checkPostPriv($user_id, $channel_id);
        if(!$can_post) $this->ajaxReturn(array('code'=>1, 'msg'=>'无发帖权限'));

            if(!$channel_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'帖子发布失败，请重试'));
            $channel = D('Channel')->getChannelInfo('channel_id ='.$channel_id);
            if($channel['channel_type'] != $channel_type) $this->ajaxReturn(array('code'=>1, 'msg'=>'帖子发布失败，请重试'));

            //未关注不能发
            $is_follow = D('Follow')->checkFollowChannel($user_id, $channel_id);
            if(!$is_follow) $this->ajaxReturn(array('code'=>1, 'msg'=>'请先关注频道'));

            $title = I('title', '');
            $content = I('content', '');
            $imgs = I('imgs', '');
            $imgs = trim($imgs, ',');
            // if(!$title && !$content && !$imgs){
            //     $this->ajaxReturn(array('code'=>1, 'msg'=>'标题、内容、图片必须填写一项'));
            // }
            // 
            if(!$title){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'标题必须填写'));
            }
            $arr = array(
                'title' => $title,
                'content' => $content,
                'imgs' => $imgs,
                'user_id' => $user_id,
                'channel_id' => $channel_id,
                'post_type' => $channel_type,
                );

        return $arr;
    }


    //帖子详情
    public function post_detail(){
        $user_id = intval(session('user_id'));

        $see_lz = I('see_lz', 0, 'intval');
        $post_id = I('post_id', 0, 'intval');

        $post_obj = new PostModel();
        $post_obj->clickdotInc($post_id);
        $post = $post_obj->getPostInfo('post_id ='.$post_id);
        $is_lz = $post['user_id'] == $user_id ? true : false;//是否为楼主
        $this->assign('is_lz', $is_lz);

        //是否有权限访问该频道
        $can_visit = D('ChannelTitlePriv')->checkVisitPriv($user_id, $post['channel_id']);
        if(!$can_visit){
            $this->assign('head_title', '查看帖子');
            $this->display('no_priv_visit');
            exit;
        }


        $channel = M('Channel')->where('channel_id ='.$post['channel_id'])->find();
        //帖子是否已被删除
        if($post['is_del'] || $channel['is_del'] || !$channel['isuse']){
            $this->assign('head_title', '查看帖子');
            $this->display('del_post');
            exit;
        }

        if($user_id && $post_id){
            $post_browse_obj = new PostBrowseModel();
            $num = $post_browse_obj->getPostBrowseNum('user_id ='.$user_id.' AND post_id ='.$post_id);
            $browse_arr = array(
                'user_id' => $user_id,
                'post_id' => $post_id,
                );
            if(!$num){
                //添加浏览记录
                $post_browse_obj->addPostBrowse($browse_arr);
            }else{
                $post_browse_obj->setPostBrowse($user_id,$post_id);
            }
        }
        
        //是否收藏
        $is_collect = D('Collect')->checkCollect($post_id, $user_id);
        $this->assign('is_collect', $is_collect);

        $post_imgs = D('PostImg')->getPostImg($post_id);
        $post['addtime'] = get_time($post['addtime']);
        $channel = D('Channel')->getChannelInfo('channel_id ='.$post['channel_id']);
        $post['content'] = html_entity_decode($post['content']);
        $post_imgs = D('PostImg')->getPostImg($post_id);
        // $post['content'] = str_replace("\n", '<br>', $post['content']);
        $this->assign('post', $post);
        $this->assign('post_imgs', $post_imgs);
        $this->assign('channel', $channel);

        


        //帖子类型
        $post_type = D('Post')->postType();

        $opt = $post_type[$post['post_type']];

        $opt = $opt ? $opt : 'normal';

        // $opt = 'praise';
        // $opt = 'reward';
        // $opt = 'crowdfunding';
        $this->assign('opt', $opt);

        //wangtao 2017.9.21 添加赞赏贴可以删除 -start

        //是否可删除
        $del = false;
        if(($opt == 'normal' || $opt == 'praise') && $is_lz ){
            $del = true;
        }
        $this->assign('del', $del);

        //wangtao 2017.9.21 -end

        //如果为赞赏帖，则判断是否有查看隐藏内容的权限
        if($opt == 'praise'){
            //隐藏内容
            $hide_obj = D('PraiseHide');
            $praise_hide = $hide_obj->getPostHide($post_id);
            $see_hide = D('PraiseHide')->checkSeeHidePriv($user_id, $praise_hide['praise_hide_id']);
            // $see_hide = true;
            
            //是否为该频道鉴定师
            $is_jd = D('Channel')->checkChannelJd($user_id, $channel['channel_id']);
            $this->assign('is_jd', $is_jd);
            // dump($is_jd);die;
            if($is_jd){
                //为鉴定师则有权限查看隐藏内容，判断是否已评分
                $see_hide = true;

                $jd_num = D('PraiseScore')->getPraiseScoreNum('user_id ='.$user_id .' and post_id ='.$post_id);
                $can_jd = $jd_num ? false : true;
                $this->assign('can_jd', $can_jd);
            } 
            
            $this->assign('see_hide', $see_hide);
            $this->assign('praise_hide', $praise_hide);
            //鉴定师评分
            $avg_score = round(D('PraiseScore')->getPostAvgScore($post_id), 1);
            $this->assign('avg_score', $avg_score);

            //购买人数
            $hide_buy_num = D('PraiseHideBuy')->getPraiseHideBuyNum('praise_hide_id ='.$praise_hide['praise_hide_id']);
            $this->assign('hide_buy_num', $hide_buy_num);

            //鉴定师评论
            $jd_obj = D('PraiseScore');
            $jd_obj->setLimit(100000);
            $jd_list = $jd_obj->getPraiseScoreList($post_id);
            $jd_list = $jd_obj->getListData($jd_list);
            $this->assign('jd_list', $jd_list);
        }elseif($opt == 'crowdfunding'){
            $cf_obj = new CrowdfundingModel();
            $cf = $cf_obj->getCrowdfundingInfo('post_id ='.$post_id);
            if($cf['status'] == CrowdfundingModel::REVIEW || $cf['status'] == CrowdfundingModel::NOT_PASS){
                redirect('/FrontChannel/post_list/channel_id/'.$channel['channel_id']);
                exit;
            }

            $cf['cf_rate'] = intval($cf['now_money'] / $cf['goal_money'] * 100);
            $cf['left_day'] = $cf_obj->getCfLeftDay($cf['crowdfunding_id']);
            $cf['support_user_num'] = D('CfSupport')->getCfSupportUserNum($cf['crowdfunding_id']);
            $support_rate = $GLOBALS['config_info']['ONE_SUPPORT_MONEY'] ? $GLOBALS['config_info']['ONE_SUPPORT_MONEY'] : 10;
            $cf['one_support'] = $cf['goal_money'] * $support_rate / 100;
            $this->assign('cf', $cf);
            $support_obj = D('CfSupport');
            $support_obj->setLimit(10000);
            $support_list = $support_obj->getCfSupportUserByMoney($cf['crowdfunding_id']);
            $this->assign('support_list', $support_list);

            //用户是否支持过该心愿
            $user_support_num = $support_obj->getUserSupportNum($user_id, $cf['crowdfunding_id']);
            $this->assign('first_support', $user_support_num ? false : true);
            // dump($support_list);die;
        }elseif($opt == 'reward'){
            $reward_obj = new RewardModel();
            $reward = $reward_obj->getRewardInfo('post_id ='.$post_id);
            $this->assign('reward', $reward);

            //回答人数
            $answer_obj = new RewardAnswerModel();
            $answer_num = $answer_obj->getRewardAnswerNumByReward($reward['reward_id']);
            $this->assign('answer_num', $answer_num);
            // echo $reward_obj->getLastSql();
            // dump($reward);die;
            
            //围观价
            $look_rate = $GLOBALS['config_info']['OFFER_ONLOOK_PRICE_RATE'] ? $GLOBALS['config_info']['OFFER_ONLOOK_PRICE_RATE'] : 10;
            $look_price = round($reward['reward_money'] * $look_rate / 100, 2);
            $this->assign('look_price', $look_price);
        

            //如果为楼主则获取所有答案，不是获取自己的答案
            if($is_lz){
                $answer_obj->setLimit(10000);
                $answer_list = $answer_obj->getRewardAnswerList('', 'reward_id ='.$reward['reward_id']);
                $answer_list = $answer_obj->getListData($answer_list);
            }else{
                //我的答案
                $my_answer = $answer_obj->getRewardAnswerInfo('reward_id ='.$reward['reward_id']. ' and user_id ='.$user_id);
                $this->assign('is_hd', $my_answer ? true : false);//是否回答过

                $answer_list = $answer_obj->getListData(array($my_answer));
            }
            
            // $my_answer = $my_answer[0];
            $this->assign('answer_list', $answer_list);
             
            if($reward['status'] == RewardModel::ADOPTED){
                //获取最佳答案
                $best_answer = $answer_obj->getRewardAnswerInfo('reward_answer_id ='.$reward['answer_id']);
                $best_answer = $answer_obj->getListData(array($best_answer));
                $best_answer = $best_answer[0];
                $this->assign('best_answer', $best_answer);
                //最佳答案是否为自己回答
                $is_best_user = $best_answer['user_id'] == $user_id ? true : false;
                $this->assign('is_best_user', $is_best_user);
                // $this->assign('best_answer', $best_answer);
                // dump($best_answer);die;
                //是否可看最佳答案
                $see_answer = false;
                $look_obj = new RewardLookModel();
                $look_num = $look_obj->getRewardLookNum('user_id ='.$user_id .' and reward_id ='.$reward['reward_id']);
                $see_answer = $look_num ? true : false;
                if($is_lz || $is_best_user){
                    $see_answer = true;
                }
                $this->assign('see_answer', $see_answer);
            }
             
            //退款原因
            $reason_obj = new RewardRefundReasonModel();
            $reason_obj->setLimit(1000);
            $refund_reason_list = $reason_obj->getRewardRefundReasonList();
            $this->assign('reason_list', $refund_reason_list);
        }


        $post_user = M('Users')->field('user_id, nickname, headimgurl')->where('user_id ='.$post['user_id'])->find();
        $this->assign('post_user', $post_user);
        //用户佩戴中的头衔
        $title_list = D('UserTitle')->getUserAdornTitle($post['user_id']);
        $this->assign('title_list', $title_list);


        //获取评论
        $post_comment_obj = new PostCommentModel();
        $comment_where = 'post_id ='.$post_id.' and reply_comment_id = 0';
        if($see_lz){
            $comment_where .= ' and is_lz = 1';
        }
        $total = $post_comment_obj->getPostCommentNum($comment_where);
        $firstRow = intval($this->_request('firstRow'));
        $post_comment_obj->setStart($firstRow);
        $post_comment_obj->setLimit($this->page_num);
        $comment_list = $post_comment_obj->getPostCommentList('', $comment_where);
        $comment_list = $post_comment_obj->getListData($comment_list);

        if($opt == 'praise'){
            $comment_list = $post_comment_obj->checkCommentHide($user_id, $comment_list);
        }
        // dump($comment_list);die;
        // if(IS_AJAX && IS_POST){
        //     $this->ajaxReturn($comment_list);
        // }
        // dump($comment_list);die; 
        $share_desc = strip_tags($post['content']);
        $share_desc = str_replace(array("\r\n", "\r", "\n", "&nbsp;", "\t", " "), "", $share_desc);
        $share_desc = mb_substr($share_desc, 0, 30);
        $this->assign('share_desc', $share_desc);
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $this->assign('comment_list', $comment_list);
        $this->assign('see_lz', $see_lz);
        $this->assign('head_title', '查看帖子');
        $this->display();
            
    }


    public function post_comment(){
        if(IS_AJAX && IS_POST){
            $post_id = I('post_id');
            $see_lz = I('see_lz');
            $post_comment_obj = new PostCommentModel();
            $comment_where = 'post_id ='.$post_id.' and reply_comment_id = 0';
            if($see_lz){
                $comment_where .= ' and is_lz = 1';
            }
            $firstRow = intval($_POST['firstRow']);
            $post_comment_obj->setStart($firstRow);
            $post_comment_obj->setLimit($this->page_num);
            $comment_list = $post_comment_obj->getPostCommentList('', $comment_where);
            $comment_list = $post_comment_obj->checkPraise($comment_list);
            $comment_list = $post_comment_obj->getListData($comment_list);
            $this->ajaxReturn($comment_list);
        }
    }


    //赞赏帖评论列表
    public function post_praise_comment(){
        if(IS_AJAX && IS_POST){
            $user_id = intval(session('user_id'));
            $post_id = I('post_id');
            $see_lz = I('see_lz');
            $post_comment_obj = new PostCommentModel();
            $comment_where = 'post_id ='.$post_id.' and reply_comment_id = 0';
            if($see_lz){
                $comment_where .= ' and is_lz = 1';
            }
            $firstRow = intval($_POST['firstRow']);
            $post_comment_obj->setStart($firstRow);
            $post_comment_obj->setLimit($this->page_num);
            $comment_list = $post_comment_obj->getPostCommentList('', $comment_where);
            $comment_list = $post_comment_obj->checkPraise($comment_list);
            $comment_list = $post_comment_obj->getListData($comment_list);
            $comment_list = $post_comment_obj->checkCommentHide($user_id, $comment_list);
            $this->ajaxReturn($comment_list);
        }
    }


    //赞赏帖楼主回帖
    public function praise_lz_ht(){
        $post_id = I('post_id', 0, 'intval');
        $user_id = intval(session('user_id'));
        if(IS_AJAX && IS_POST){
            $post_id = I('post_id', 0, 'intval');
            if(!$post_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'回帖失败，请重试'));
            $post = D('Post')->getPostInfo('post_id ='.$post_id);
            if($post['post_type'] != PostModel::PRAISE) $this->ajaxReturn(array('code'=>1, 'msg'=>'回帖失败，请重试'));

            //是否为楼主
            if($user_id != $post['user_id']) $this->ajaxReturn(array('code'=>1, 'msg'=>'回帖失败，请重试'));

            $content = I('content', '');
            $imgs = I('imgs', '');
            $imgs = trim($imgs, ',');
            if(!$content && !$imgs){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'内容、图片必须填写一项'));
            }


            $post_comment_obj = new PostCommentModel();
            $floor = $post_comment_obj->getMaxFloor($post_id) + 1;
            $arr = array(
                'content' => $content,
                'user_id' => $user_id,
                'post_id' => $post_id,
                'floor' => $floor,
                'imgs' => $imgs,
                'is_lz' => 1,
                );
            // dump($arr);die;
            $hide_content = I('hide_content', '');
            $hide_imgs = I('hide_imgs', '');
            $hide_imgs = trim($hide_imgs, ',');
            $hide_price = I('hide_price', 0.0 , 'floatval');
            if(!$hide_content && !$hide_imgs){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'隐藏内容处内容、图片必须填写一项'));
            }

            if($hide_price <= 0.0){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'请填写正确的价格'));
            }

            
            if($c_id = $post_comment_obj->addPostComment($arr)){
                $hide_arr = array(
                    'user_id' => $user_id,
                    'content' => $hide_content,
                    'price' => $hide_price,
                    'hide_type' => PraiseHideModel::COMMENT,
                    'imgs' => $hide_imgs,
                    'id' => $c_id,
                    );
                D('PraiseHide')->addPraiseHide($hide_arr);
                $this->ajaxReturn(array('code'=>0, 'msg'=>'回帖成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'回帖失败，请重试'));
        }

        $this->assign('post_id', $post_id);
        $this->assign('opt', 'praise');
        $this->assign('is_ht', true);
        $this->assign('head_title', '楼主回帖');
        $this->display('post_tiezi');
    }


    //鉴定师打分
    public function jd_score(){

        $post_id = I('post_id', 0, 'intval');
        $post_obj = new PostModel();
        $post = $post_obj->getPostInfo('post_id ='.$post_id);
        $post_list = $post_obj->getListData(array($post));
        $post = $post_list[0];
        $this->assign('post', $post);
        $this->assign('head_title', '评分');
        $this->display();
    }

    //鉴定师打分
    public function praise_post_score(){
        if(IS_AJAX && IS_POST){
            $user_id = intval(session('user_id'));
            if(!$user_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'打分失败'));
            $post_id = I('post_id', 0, 'intval');
            if(!$post_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'打分失败'));

            $score = I('score');
            $score = round(floatval($score),1);
            if($score < 0.0 || $score > 10.0) $this->ajaxReturn(array('code'=>1, 'msg'=>'分数必须在0-10之间'));

            $content = I('content');
            if(!$content) $this->ajaxReturn(array('code'=>1, 'msg'=>'请填写评语'));

            /****判断是否可打分*****/

            $post = D('Post')->getPostInfo('post_id ='.$post_id);
            $channel = D('Channel')->getChannelInfo('channel_id ='.$post['channel_id']);
            //是否为该频道鉴定师
            $is_jd = D('Channel')->checkChannelJd($user_id, $channel['channel_id']);
            if(!$is_jd) $this->ajaxReturn(array('code'=>1, 'msg'=>'只有该频道鉴定师才可以评分'));
            $score_obj = D('PraiseScore');
            $jd_num = $score_obj->getPraiseScoreNum('user_id ='.$user_id .' and post_id ='.$post_id);
            if($jd_num) $this->ajaxReturn(array('code'=>1, 'msg'=> '您已经打过分了'));

            $floor = $score_obj->getMaxFloor($post_id) + 1;
            $arr = array(
                'user_id' => $user_id,
                'post_id' => $post_id,
                'score' => $score,
                'content' => $content,
                'floor' => $floor,
                );
            if($score_obj->addPraiseScore($arr)){
                $this->ajaxReturn(array('code'=>0, 'msg'=>'打分成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'打分失败'));
        }
    }


    //心愿帖楼主回帖页
    public function cf_reply(){
        $post_id = I('post_id', 0, 'intval');

        $this->assign('post_id', $post_id);
        $this->assign('head_title', '楼主回帖');
        $this->display();
    }

    //心愿回帖
    public function cf_lz_reply(){
       if(IS_AJAX && IS_POST){
            $user_id =intval(session('user_id'));
            $post_id = I('post_id', 0, 'intval');
            if(!$post_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'回帖失败，请重试'));
            $post = D('Post')->getPostInfo('post_id ='.$post_id);
            if($post['post_type'] != PostModel::CROWDFUNDING) $this->ajaxReturn(array('code'=>1, 'msg'=>'回帖失败，请重试'));

            //是否为楼主
            if($user_id != $post['user_id']) $this->ajaxReturn(array('code'=>1, 'msg'=>'回帖失败，请重试'));

            //心愿信息
            $cf = D('Crowdfunding')->getCrowdfundingInfo('post_id ='.$post_id);
            //只有成功的并且未回帖的心愿可回帖
            if($cf['status'] != CrowdfundingModel::SUCCESS) $this->ajaxReturn(array('code'=>1, 'msg'=>'回帖失败，请重试'));
            if($cf['is_ht'] == 1) $this->ajaxReturn(array('code'=>1, 'msg'=>'已回帖'));

            $content = I('content', '');
            $imgs = I('imgs', '');
            $imgs = trim($imgs, ',');
            if(!$content && !$imgs){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'内容、图片必须填写一项'));
            }


            $cf_reply_obj = new CfReplyModel();
            
            $arr = array(
                'content' => $content,
                'user_id' => $user_id,
                'post_id' => $post_id,
                'imgs' => $imgs,
                );
            // dump($arr);die;
            
            if($c_id = $cf_reply_obj->addCfReply($arr)){
                //将心愿设置为已回帖
                D('Crowdfunding')->where('crowdfunding_id ='.$cf['crowdfunding_id'])->save(array('is_ht'=>1));
                $this->ajaxReturn(array('code'=>0, 'msg'=>'回帖成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'回帖失败，请重试'));
        }
    }

    //心愿楼主回帖详情
    public function cf_reply_detail(){
        $post_id = I('post_id', 0, 'intval');
        $user_id = intval(session('user_id'));
        $post_obj = new PostModel();
        $post = $post_obj->getPostInfo('post_id ='.$post_id);
        if($post['post_type'] != PostModel::CROWDFUNDING){
            redirect('/FrontChannel/channel_index');
            exit;
        }

        //用户是否有查看权限
        $cf_obj = new CrowdfundingModel();
        $cf = $cf_obj->getCrowdfundingInfo('post_id ='.$post_id);
        if($cf['status'] != CrowdfundingModel::SUCCESS){
            redirect('/FrontChannel/post_detail/post_id/'.$post_id);
            exit;   
        }

        $support_obj = new CfSupportModel();
        $support_num = $support_obj->getCfSupportNum('crowdfunding_id ='.$cf['crowdfunding_id']. ' and user_id ='.$user_id);
        if(!$support_num){
            redirect('/FrontChannel/post_detail/post_id/'.$post_id);
            exit; 
        }

        $cf_reply_obj = new CfReplyModel();
        $cf_reply = $cf_reply_obj->getCfReplyInfo('post_id ='.$post_id);
        $cf_reply['addtime'] = get_time($cf_reply['addtime']);
        $this->assign('cf_reply', $cf_reply);


        $channel = D('Channel')->getChannelInfo('channel_id ='.$post['channel_id']);
        $this->assign('channel', $channel);
        $post_user = M('Users')->field('user_id, nickname, headimgurl')->where('user_id ='.$post['user_id'])->find();
        $this->assign('post_user', $post_user);
        //用户佩戴中的头衔
        $title_list = D('UserTitle')->getUserAdornTitle($post['user_id']);
        $this->assign('title_list', $title_list);
        $this->assign('head_title', '楼主回帖');    
        $this->display();
    }


    //悬赏-我来满足
    public function my_answer(){
        $user_id = intval(session('user_id'));
        $post_id = I('post_id', 0, 'intval');
        if(IS_AJAX && IS_POST){
            // dump($_POST);die;

            $post = D('Post')->getPostInfo('post_id ='.$post_id);

            $reward_obj = new RewardModel();
            $reward = $reward_obj->getRewardInfo('post_id ='.$post_id);
            if(!$reward) $this->ajaxReturn(array('code'=>1, 'msg'=>'回答失败，请重试'));
            
            //是否为进行中的悬赏
            if($reward['status'] != RewardModel::REWARDING) $this->ajaxReturn(array('code'=>1, 'msg'=>'悬赏已结束'));
            if($post['user_id'] == $user_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'不能回答自己的悬赏'));

            $answer_obj = new RewardAnswerModel();

            //是否已回答过
            $answer = $answer_obj->getRewardAnswerInfo('reward_id ='.$reward['reward_id'] . ' and user_id ='.$user_id);
            if($answer) $this->ajaxReturn(array('code'=>1, 'msg'=>'一人只能回答一次'));

            $content = I('content', '');
            $imgs = I('imgs', '');
            $imgs = trim($imgs, ',');
            if(!$content && !$imgs){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'内容、图片必须填写一项'));
            }

            $arr = array(
                'reward_id' => $reward['reward_id'],
                'user_id' => $user_id,
                'imgs' => $imgs,
                'content' => $content,
                );
            
            if($answer_obj->addRewardAnswer($arr)){
                $this->ajaxReturn(array('code'=>0, 'msg'=>'回答成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'回答失败，请重试'));
        }

        $this->assign('post_id', $post_id);
        $this->assign('reward_days', $GLOBALS['config_info']['OFFER_DAYS'] ? $GLOBALS['config_info']['OFFER_DAYS'] : 5);
        $this->assign('look_rate', $GLOBALS['config_info']['OFFER_ONLOOK_FC_RATE'] ? $GLOBALS['config_info']['OFFER_ONLOOK_FC_RATE'] : 50);
        $this->assign('head_title', '我来回答');
        $this->display();
    }

    //楼主采纳答案
    public function answer_caina(){
        if(IS_AJAX && IS_POST){
            $user_id = intval(session('user_id'));
            $post_id = I('post_id', 0, 'intval');
            $answer_id = I('answer_id', 0, 'intval');
            if(!$user_id || !$post_id || !$answer_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'答案采纳失败'));

            $answer_obj = new RewardAnswerModel();
            $answer = $answer_obj->getRewardAnswerInfo('reward_answer_id ='.$answer_id);
            $reward_obj = new RewardModel();
            $reward = $reward_obj->getRewardInfo('reward_id ='.$answer['reward_id']);
            if($reward['post_id'] != $post_id || $reward['status'] != RewardModel::REWARDING) $this->ajaxReturn(array('code'=>1, 'msg'=>'答案采纳失败'));
            if($reward['answer_id']) $this->ajaxReturn(array('code'=>1, 'msg'=>'答案采纳失败'));

            if($reward_obj->cainaAnswer($answer['reward_id'], $answer_id)){
                //推消息给答主
                $title_name = M('Post')->where('post_id ='.$post_id)->getField('title');
                D('Message')->addMessage($post_id, 6, 0, $answer['user_id'], '恭喜，您在悬赏贴《'.$title_name.'》中的回答已被楼主采纳', '恭喜，您在悬赏贴《'.$title_name.'》中的回答已被楼主采纳', '/FrontChannel/post_detail/post_id/'.$post_id);
                $this->ajaxReturn(array('code'=>0, 'msg'=>'采纳成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'答案采纳失败'));
        }
    }


    //支付判断
    private function check_pay($type){
        $user_id = intval(session('user_id'));
        if(!$user_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'请先登录'));
        $user = M('Users')->where('user_id ='.$user_id)->find();
        if(!$user['pay_password']) $this->ajaxReturn(array('code'=> 5, 'msg'=>'您还未设置支付密码，请先设置后再购买'));

        if($type == 'praise'){
            $item_id = I('item_id', 0, 'intval');
           

            $praise_hide_obj = new PraiseHideModel();
            $hide = $praise_hide_obj->getPraiseHideInfo('praise_hide_id ='.$item_id);
            $this->price = $hide['price'];
            if(!$hide)  $this->ajaxReturn(array('code'=>1, 'msg'=>'购买内容不存在'));
            
            $price = $hide['price'];

            if($hide['price'] > $user['left_money']) $this->ajaxReturn(array('code'=>2,'msg'=>'余额不足', 'left_money'=>$user['left_money'], 'price'=>$price));
            
            // $this->ajaxReturn(array('code'=>0, 'price'=>$price));
        }elseif($type == 'cf'){
            $post_id = I('post_id', 0, 'intval');
            $cf = D('Crowdfunding')->getCfByPost($post_id);
            if(!$cf) $this->ajaxReturn(array('code'=>1, 'msg'=>'心愿不存在'));
            if($cf['status'] != CrowdfundingModel::PASSED) $this->ajaxReturn(array('code'=>1, 'msg'=>'心愿已结束'));
            $support_rate = $GLOBALS['config_info']['ONE_SUPPORT_MONEY'] ? $GLOBALS['config_info']['ONE_SUPPORT_MONEY'] : 10;
            $one_support = $cf['goal_money'] * $support_rate / 100;
            $support_num = I('support_num', 1, 'intval');
            $support_num = $support_num > 0 ? $support_num : 1;
            $price = $one_support * $support_num;
            if($price > $user['left_money']) $this->ajaxReturn(array('code'=>2,'msg'=>'余额不足', 'left_money'=>$user['left_money'], 'price'=>$price));
        }elseif($type == 'reward'){
            $post_id = I('post_id', 0, 'intval');
            $reward_obj = new RewardModel();
            $reward = $reward_obj->getRewardInfo('post_id ='.$post_id);
            if(!$reward) $this->ajaxReturn(array('code'=>1, 'msg'=>'悬赏不存在'));
            if($reward['status'] != RewardModel::ADOPTED) $this->ajaxReturn(array('code'=>1, 'msg'=>'围观失败'));
            //围观价
            $look_rate = $GLOBALS['config_info']['OFFER_ONLOOK_PRICE_RATE'] ? $GLOBALS['config_info']['OFFER_ONLOOK_PRICE_RATE'] : 10;
            $price = round($reward['reward_money'] * $look_rate / 100, 2);
            if($price > $user['left_money']) $this->ajaxReturn(array('code'=>2,'msg'=>'余额不足', 'left_money'=>$user['left_money'], 'price'=>$price));
        }elseif($type == 'live'){
            $live_id = I('live_id', 0,'intval');
            $live_obj = new LiveModel();
            $live = $live_obj->getLiveInfo('live_id ='.$live_id);
            if(!$live) $this->ajaxReturn(array('code'=>1, 'msg'=>'视频内容不存在'));

            //是否已购买
            $live_buy_obj = new LiveBuyModel();
            $live_buy_num = $live_buy_obj->getLiveBuyNum('live_id ='.$live_id .' and user_id ='.$user_id);
            if($live_buy_num) $this->ajaxReturn(array('code'=>3));

            $price = $live['price'];
            if($price > $user['left_money']) $this->ajaxReturn(array('code'=>2,'msg'=>'余额不足', 'left_money'=>$user['left_money'], 'price'=>$price));
        }

        return $price;
    }

    //是否可免密支付
    public function check_npw_pay(){
        $price = I('price');
        $npw_pay = M('Users')->where('user_id ='.intval(session('user_id')))->getField('npw_pay');
        if($npw_pay){
            if($price > $GLOBALS['config_info']['NPW_PAY_MONEY']){
                $npw_pay  = 0;
                //session(array('name'=>'check_pwd', 'expire'=>10), false);//支付金额大于免密金额
             }else{
                // session(array('name'=>'check_pwd', 'expire'=>10), true);
             }
            
        }
        $this->ajaxReturn($npw_pay);
    }


    /***************视频支付判断*********************/
    //赞赏帖支付判断
    public function check_live_pay(){
        if(IS_AJAX && IS_POST){
            $r = $this->check_pay('live');
            $this->ajaxReturn(array('code'=>0, 'price'=>$r));
        }
    }

    //视频啊支付
    public function live_buy(){
        if(IS_AJAX && IS_POST){
            $user_id = intval(session('user_id'));
            $live_id = I('live_id', 0, 'intval');
            $r = $this->check_pay('live');
            // dump($r);die;
            $live_buy_obj = new LiveBuyModel();
            $arr = array(
                'user_id' => $user_id,
                'live_id' => $live_id,
                'price' => $r,
                );
            if($l_id = $live_buy_obj->addLiveBuy($arr)){
                D('Account')->addAccount($user_id, AccountModel::LIVE_BUY, -$r, '视频购买-'.$live_id, $l_id ,$live_id);
                $this->ajaxReturn(array('code'=>0, 'msg'=>'购买成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'购买失败'));
        }
    }



    //判断密码是否正确
    public function check_pwd(){
        if(IS_AJAX && IS_POST){
            $user_id = intval(session('user_id'));
            $user_obj = new UserModel($user_id);
            $user = $user_obj->getUserInfo();
            $pwd = I('pwd', '', 'md5');
            if(!$user['pay_password']) $this->ajaxReturn(array('code'=>1, 'msg'=>'您还未设置支付密码，请先设置后再购买'));
            if($pwd != $user['pay_password']){
                $this->ajaxReturn(array('code'=>1, 'msg'=>'密码错误'));
            }
             $this->ajaxReturn(array('code'=>0));
        }
    }

    //赞赏帖支付判断
    public function check_praise_pay(){
        if(IS_AJAX && IS_POST){
            $r = $this->check_pay('praise');
            $this->ajaxReturn(array('code'=>0, 'price'=>$r));
        }
    }

    //赞赏帖购买
    public function praise_buy(){
        if(IS_AJAX && IS_POST){


            $user_id = intval(session('user_id'));
            $item_id = I('item_id', 0, 'intval');
            $r = $this->check_pay('praise');
            // dump($r);die;

            //判断购买的帖子id
            $praise_hide_obj = new PraiseHideModel();
            $praise_info = $praise_hide_obj->getPraiseHideInfo('praise_hide_id ='.$item_id);
            $post_id = 0;
            //如果隐藏内容的类型是帖子．则直接取他的id;
            if($praise_info['hide_type'] == PraiseHideModel::POST){
                $post_id = $praise_info['id'];
            }
            //如果隐藏内容的类型是评论．则找到评论对应的帖子id;
            if($praise_info['hide_type'] == PraiseHideModel::COMMENT){
                $comment_obj = new PostCommentModel();
                $post_id = $comment_obj->getPostCommentField('post_comment_id ='.$praise_info['id'],'post_id');
            }

            $hide_buy_obj = new PraiseHideBuyModel();
            $arr = array(
                'user_id' => $user_id,
                'praise_hide_id' => $item_id,
                );
            if($o_id = $hide_buy_obj->addPraiseHideBuy($arr)){
                D('Account')->addAccount($user_id, AccountModel::PRAISE_BUY, -$r, '赞赏帖购买—'.$item_id, $o_id,$post_id);

                $hide = D('PraiseHide')->getPraiseHideInfo('praise_hide_id ='.$item_id);

                //wangtao 2017.9.15 -start
                //扣取用户的资金分成
                $post_chas = D('Reward')->getRewarMoney($hide['user_id'],$post_id);
                $post_chas = round($r * $post_chas / 100,2);
                $r = $r - $post_chas;
                //wangtao 2017.9.15 -end

                D('Account')->addAccount($hide['user_id'], AccountModel::PRAISE_INCOME, $r, '赞赏帖收入—'.$item_id, $o_id,$post_id);

                $u_nick = M('Users')->where('user_id ='.$user_id)->getField('nickname');
                //推消息给楼主
                $p_t = D('Post')->getTitleById($post_id);
                $content = $u_nick . '刚刚购买了您的赞赏帖《'.$p_t.'》里的内容';
                D('Message')->addMessage($hide['praise_hide_id'], MessageModel::PRAISE, 0, $hide['user_id'], $content, $content);
                $praise_hide = D('PraiseHide')->getHide($item_id);
                D('Post')->setPost($post_id,array( 'updatetime' => time()));
                $this->ajaxReturn(array('code'=>0, 'msg'=>'购买成功', 'hide'=>$praise_hide));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'购买失败'));
        }
    }
    

    //心愿支持支付判断
    public function check_cf_pay(){
        if(IS_AJAX && IS_POST){
            $r = $this->check_pay('cf');
            $this->ajaxReturn(array('code'=>0, 'price'=>$r));
        }
    }

    //心愿支持支付
    public function cf_buy(){
        if(IS_AJAX && IS_POST){
            $user_id = intval(session('user_id'));
            $post_id = I('post_id', 0, 'intval');
            $r = $this->check_pay('cf');
            // dump($r);die;
            $cf = D('Crowdfunding')->getCfByPost($post_id);
            $support_obj = new CfSupportModel();
            $arr = array(
                'user_id' => $user_id,
                'crowdfunding_id' => $cf['crowdfunding_id'],
                'support_money' => $r,
                );
            if($s_id = $support_obj->addCfSupport($arr)){
                D('Account')->addAccount($user_id, AccountModel::CF_SUPPORT, -$r, '心愿支持', $s_id ,$post_id);
                //推消息给楼主
                $post_title = M('Post')->where('post_id ='.$post_id)->getField('title');
                $support_name = M('Users')->where('user_id ='.$user_id)->getField('nickname');
                $content = $support_name.'支持了您的心愿《'.$post_title.'》';
                D('Message')->addMessage($post_id, MessageModel::CF, $user_id, $cf['user_id'], $content, $content, '/FrontChannel/post_detail/post_id/'.$post_id);
                D('Post')->setPost($post_id,array( 'updatetime' => time()));
                $this->ajaxReturn(array('code'=>0, 'msg'=>'支持成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'支持失败'));
        }
    }


    //悬赏支付判断
    public function check_reward_pay(){
        if(IS_AJAX && IS_POST){
            $r = $this->check_pay('reward');
            $this->ajaxReturn(array('code'=>0, 'price'=>$r));
        }
    }

    //悬赏支持支付
    public function reward_buy(){
        if(IS_AJAX && IS_POST){
            $user_id = intval(session('user_id'));
            $post_id = I('post_id', 0, 'intval');
            $r = $this->check_pay('reward');
            // dump($r);die;
            $reward = D('Reward')->getRewardInfo('post_id ='.$post_id);
            $look_obj = new RewardLookModel();
            $arr = array(
                'user_id' => $user_id,
                'reward_id' => $reward['reward_id'],
                'look_money' => $r,
                );
            if($l_id = $look_obj->addRewardLook($arr)){
                D('Account')->addAccount($user_id, AccountModel::REWARD_LOOK, -$r, '悬赏围观',$l_id,$post_id);

                //wangtao 2017.9.15 -start

//                $fc_rate = $GLOBALS['config_info']['OFFER_ONLOOK_FC_RATE'] ? $GLOBALS['config_info']['OFFER_ONLOOK_FC_RATE'] : 50;
                $fc_rate = round( $r / 2, 2);

                $lz_get = $this->get_post_chas($reward['user_id'],$post_id,$fc_rate);
                $answer_get = $this->get_post_chas($reward['adopted_user'],$post_id,$fc_rate);

                //wangtao 2017.9.15 -end


                D('Account')->addAccount($reward['user_id'], AccountModel::REWARD_LOOK_LZ, $lz_get, '悬赏围观-楼主获得', $l_id ,$post_id);
                D('Account')->addAccount($reward['adopted_user'], AccountModel::REWARD_LOOK_ANSWER, $answer_get, '悬赏围观-最佳答主获得', $l_id ,$post_id);
                D('Post')->setPost($post_id,array( 'updatetime' =>time()));
                $this->ajaxReturn(array('code'=>0, 'msg'=>'围观成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'围观失败'));
        }
    }

    //wangtao 2017.9.15 -start

    protected function get_post_chas($user_id,$post_id,$r){

        $post_chas = intval(D('Reward')->getRewarMoney($user_id,$post_id));
        $post_chas = round($r * $post_chas / 100,2);
        $r = $r - $post_chas;

        return $r;
    }
    //wangtao 2017.9.15 -end


    public function reward_refund(){
        if(IS_AJAX && IS_POST){
            $user_id = intval(session('user_id'));
            $post_id = I('post_id', 0, 'intval');
            $reason_id = I('reason_id', 0, 'intval');
            if(!$user_id || !$post_id || !$reason_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'退款申请提交失败'));

            $reward_obj = new RewardModel();
            $reward = $reward_obj->getRewardInfo('post_id ='.$post_id);
            if($reward['status'] != RewardModel::REWARDING) $this->ajaxReturn(array('code'=>1, 'msg'=>'只有悬赏中才可退款'));
            if($reward['user_id'] != $user_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'只有楼主才能申请退款'));

            if($reward_obj->rewardRefund($reward['reward_id'], $reason_id)){
                $this->ajaxReturn(array('code'=>0, 'msg'=>'提交成功'));
            }echo $reward_obj->getLastSql();
            $this->ajaxReturn(array('code'=>1, 'msg'=>'退款申请提交失败'));
        }
    }



    //普通帖删除
    public function del_post(){
        if(IS_AJAX && IS_POST){
            $user_id = intval(session('user_id'));
            $post_id = I('post_id', 0, 'intval');
            if(!$user_id || !$post_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'帖子删除失败，请重试'));

            $post_obj = new PostModel();
            $post = $post_obj->getPostInfo('post_id ='.$post_id);

            //wangtao 2017.9.21 添加赞赏贴可以删除 -start

            $channel = D('Channel')->getChannelInfo('channel_id ='.$post['channel_id']);
            //是否为该频道鉴定师
            $is_jd = D('Channel')->checkChannelJd($user_id, $channel['channel_id']);

            if($user_id != $post['user_id'] || ($post['post_type'] != PostModel::NORMAL &&  $post['post_type'] != PostModel::PRAISE) ){
                if(!$is_jd){
                    $this->ajaxReturn(array('code'=>1, 'msg'=>'帖子删除失败，请重试'));
                }
            }
            //wangtao 2017.9.21 -end
            $r = $post_obj->delPost($post_id);
            if($r){
                $this->ajaxReturn(array('code'=>0));
            }


            $this->ajaxReturn(array('code'=>1, 'msg'=>'帖子删除失败，请重试'));
        }
    }

}
