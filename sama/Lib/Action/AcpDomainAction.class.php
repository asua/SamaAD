<?php
/**
 * Acp后台领域
 */
class AcpDomainAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
        $this->assign('action_title', '领域');
        $this->assign('action_src', U('/AcpDomain/get_domain_list'));
    }

    public function get_domain_list()
    {
        $domain_obj = new TypeModel();
        $where     = 'type_type ='.TypeModel::DOMAIN;
        //数据总量
        $total = $domain_obj->getTypeNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $domain_obj->setStart($Page->firstRow);
        $domain_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $domain_list = $domain_obj->getTypeListAcp($where);

        $this->assign('domain_list', $domain_list);
        $this->assign('head_title', '领域列表');
        $this->display();

    }

    //添加领域
    public function add_domain()
    {

        $act = $this->_post('act');
        if ($act == 'add') {
            $_post      = $this->_post();
            $type_name = $_post['type_name'];
            $serial     = $_post['serial'];

            //表单验证
            if (!$type_name) {
                $this->error('请填写领域名称！');
            }

            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }


            $arr = array(
                'type_name' => $type_name,
                'serial'     => $serial,
                'type_type' => TypeModel::DOMAIN,
            );

            $domain_obj = new TypeModel();
            $success   = $domain_obj->addType($arr);

            if ($success) {
                $this->success('恭喜您，领域添加成功！', '/AcpDomain/add_domain');
            } else {
                $this->error('抱歉，领域添加失败！', '/AcpDomain/add_domain');
            }
        }

        $this->assign('act', 'add');
        $this->assign('head_title', '添加领域');
        $this->display();
    }



    //修改领域
    public function edit_domain()
    {
        $redirect = U('/AcpDomain/get_domain');
        $domain_id = intval($this->_get('type_id'));
        if (!$domain_id) {
            $this->error('对不起，非法访问！', $redirect);
        }

        $domain_obj  = new TypeModel();
        $domain_info = $domain_obj->getTypeInfo('type_id ='.$domain_id);

        if (!$domain_info) {
            $this->error('对不起，不存在相关领域！', $redirect);
        }

        $act = $this->_post('act');

        if ($act == 'edit') {
            $_post      = $this->_post();
            $domain_name = $_post['type_name'];
            $serial     = $_post['serial'];

            //表单验证
            if (!$domain_name) {
                $this->error('请填写领域名称！');
            }

            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }


            $arr = array(
                'type_name' => $domain_name,
                'serial'     => $serial,
            );

            $url       = '/AcpDomain/edit_domain/type_id/' . $domain_id;

            if ($domain_obj->setType($domain_id, $arr)) {
                $this->success('恭喜您，领域修改成功！', $url);
            } else {
                $this->error('抱歉，领域修改失败！', $url);
            }
        }
        $this->assign('domain_info', $domain_info);
        $this->assign('act', 'edit');
        $this->assign('head_title', '修改领域');
        $this->display();

    }

    /**
     * 删除领域
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据领域ID删除领域
     */
    public function delete_domain()
    {
        $domain_id = intval($this->_post('type_id'));
        if ($domain_id) {
            $domain_obj = new TypeModel($domain_id);
            $service_provides_obj           = new ServiceProvidersModel();
            $num                = $service_provides_obj->getServiceProvidersNum('FIND_IN_SET('.$domain_id.',domain)');
            //存在服务商则删除失败；
            if ($num) {
                exit('failure');
            }
            $success = $domain_obj->delType('type_id ='.$domain_id);
            exit($success ? 'success' : 'failure');
        }

        exit('failure');
    }

    public function batch_delete_domain()
    {

        $domain_ids = $this->_post('type_ids');
        if ($domain_ids) {
            $domain_ary = explode(',', $domain_ids);
            $success_num     = 0;
            $service_provides_obj           = new ServiceProvidersModel();
            foreach ($domain_ary as $domain_id) {
                $num = $service_provides_obj->getServiceProvidersNum('FIND_IN_SET('.$domain_id.',domain)');
//                dump($service_provides_obj->getLastSql());die;
                if ($num) {
                    continue;
                }

                $domain_obj = new TypeModel($domain_id);
                $success_num += $domain_obj->delType('type_id ='.$domain_id);
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }

        exit('failure');
    }


}
