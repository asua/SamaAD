<?php
/**
 * about类
 */
class AcpAboutAction extends AcpAction {
	public function _initialize()
	{
		parent::_initialize();
	}

	/**
     * 编辑有tag的文章
     * @author 姜伟
	 * @param $tag 文章标签
	 * @param $page_name 页面标题
     * @return void
     * @todo 编辑有tag的文章
     */
	public function edit_about($tag,$page_name)
	{
		$act = $this->_post('act');
		$about_obj = M('Article');
		$text_obj = new ArticleTxtModel();
		$id = $about_obj->where('article_tag = "'.$tag.'"')->getField('article_id');
		if($act == 'submit') {
			$_post = $this->_post();
			$contents 		  = $_post['contents'];
			$data=[
				'article_tag'=>$tag
			];
			if($id){
				//执行修改
				$data=[
					'contents'=>$contents
				];
				$r = $text_obj ->editArticleTxt($data,$id);
				if($r){
					$this->success('恭喜您，关于我们修改成功！');
				}else{
					$this->error('对不起，关于我们修改失败！');
				}
			}else{
				if($id = $about_obj->add($data)){
					$text_obj = new ArticleTxtModel();
					$data=[
						'article_id'=>$id,
						'contents'=>$contents
					];
					$text_obj ->addArticleTxt($data);
					$this->success('恭喜您，关于我们修改成功！');
				}else{
					$this->error('对不起，关于我们修改失败！');
				}
			}
		}
		$text_contents = $text_obj->where('article_id='.$id)->getField('contents');
		$this->assign('text_contents', $text_contents);

		$this->assign('head_title', '修改' . $page_name);
		$this->display(APP_PATH . 'Tpl/AcpAbout/edit_about.html');
	}

	public function edit_my_about(){

		$this->edit_about('about','关于我们');
	}

//	public function edit_project(){
//		$this->edit_about('project','项目申报');
//	}

}
?>
