<?php
/**
 * acp官方消息
 */
class AcpOfficialNewsAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
//        $this->assign('action_title', '官方消息');
//        $this->assign('action_src', U('/AcpClass/list_class'));
    }

    //官方消息列表
    public function get_official_news_list()
    {
        $official_news_obj = new OfficialNewsModel();
        $where     = '';
        //数据总量
        $total = $official_news_obj->getOfficialNewsNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $official_news_obj->setStart($Page->firstRow);
        $official_news_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $list = $official_news_obj->getOfficialNewsList();

        $list = $official_news_obj->getListData($list);
        $this->assign('list', $list);
        $this->assign('head_title', '官方消息列表');
        $this->display();

    }

    //添加官方消息
    public function add_edit_official_news()
    {
        $official_news_id = I('official_news_id',0,'int');
        $act = I('act');
        $official_news_obj = new OfficialNewsModel();
        $txt_obj= new ArticleTxtModel();
        if($official_news_id){
            $official_news_info = $official_news_obj->getOfficialNews($official_news_id);
            $txt_info = $txt_obj->getArticleTxtInfo('article_id ='.$official_news_id,'contents');
            $this->assign('act','edit');
            $this->assign('txt_info',$txt_info);
            $this->assign('info',$official_news_info);
            $this->assign('head_title','修改官方消息');
        }else{
            $this->assign('head_title','添加官方消息');
            $this->assign('act','add');
        }

        if(IS_POST){
            $_post      = $this->_post();
            $title = $_post['title'];
            $serial     = $_post['serial'];
            $isuse      = $_post['isuse'];
            $path_img  = $_post['path_img'];
            $contents  = $_POST['contents'];

            //表单验证
            if (!$title) $this->error('请填写官方消息标题！');
            if (!$path_img) $this->error('请添加封面图！');
            if (!ctype_digit($serial)) $this->error('请填写排序号！');
            if (!ctype_digit($isuse)) $this->error('请选择是否显示！');
            if (!$contents) $this->error('请填写内容！');
            $arr = array(
                'title' => $title,
                'serial' => $serial,
                'isuse' => $isuse,
                'path_img' => $path_img,
            );

            if ($act == 'add') {
                $arr['addtime'] = time();
                $success   = $official_news_obj->addOfficialNews($arr);
                if ($success) {
                    $txt_obj ->addArticleTxt(array('article_id' => $success,'contents' => $contents));
                    $this->success('恭喜您，官方消息添加成功！', '/AcpOfficialNews/get_official_news_list');
                } else {
                    $this->error('抱歉，官方消息添加失败！', '/AcpOfficialNews/get_official_news_list');
                }
             }

             if($act == 'edit'){
                 $success = $official_news_obj->setOfficialNews($official_news_id,$arr);;
                 if ($success !== false) {
                     $txt_obj ->editArticleTxt(array('contents' => $contents),$official_news_id);
                     $this->success('恭喜您，官方消息修改成功！', '/AcpOfficialNews/get_official_news_list');
                 } else {
                     $this->error('抱歉，官方消息修改失败！', '/AcpOfficialNews/get_official_news_list');
                 }
             }
        }


        // 视频图标
        $this->assign('pic_data', array(
            'name'  => 'path_img',
            'title' => '封面图',
            'help'  => '点击查看大图',
            'url'   => $official_news_info['path_img'],
            'dir'   => 'class',
        ));
        $this->display();
    }


    /**
     * 删除官方消息
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据问题ID删除官方消息
     */
    public function batch_delete_official_news()
    {
        $question_ids = $this->_post('obj_ids');
        if ($question_ids) {
            $question_id_ary = explode(',', $question_ids);
            $success_num     = 0;
            foreach ($question_id_ary as $question_id) {
                $question_obj = new OfficialNewsModel($question_id);
                $success_num += $question_obj->delOfficialNews();
                if($success_num){
                    $txt_obj= new ArticleTxtModel();
                    $txt_obj ->delArticleTxt($question_id);
                }
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }
        exit('failure');
    }
}
