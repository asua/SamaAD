<?php
/**
 * acp后台商品分类
 */
class AcpTopicAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
    }


    private function get_search_condition(){
        $where = '';

        $nickname = I('nickname');
        if($nickname){
            $user_obj = new UserModel();
            $user_ids = $user_obj->getUserField('user_id', 'nickname like "%'.$nickname.'%"');
            if(count($user_ids)){
                $user_ids = implode(',', $user_ids);
                $where .= ' and user_id in('.$user_ids.')';
            }
            $this->assign('nickname', $nickname);
        }

        $title = I('title');
        if($title){
            $where .= ' and title like "%'.$title.'%"';
            $this->assign('title', $title);
        }
        
        $start_time = I('start_time');
        if($start_time){
            $start_time = strtotime($start_time);
            $where .= ' and addtime >='.$start_time;
            $this->assign('start_time', $start_time);
        }
        $end_time = I('end_time');
        if($end_time){
            $end_time = strtotime($end_time);
            $where .= ' and addtime <='.$end_time;
            $this->assign('end_time', $end_time);
        }

        return $where;
    }

    //话题列表
    public function topic_list($where, $head_title, $opt){
        $where .= $this->get_search_condition();
        $topic_obj = D('Topic');
        $show = getPageList($topic_obj, 'getTopicNum', $where);
        $this->assign('show', $show);

        $topic_list = $topic_obj->getTopicList('', $where, 'addtime desc');
        $topic_list =  $topic_obj->getListData($topic_list);
        #dump($topic_list);die;
        $this->assign('topic_list', $topic_list);
        $this->assign('opt', $opt);
        $this->assign('head_title', $head_title);
        $this->display('get_topic_list');
    }

    public function get_topic_list(){
        $this->topic_list('tp_topic.is_del = 0', '话题列表', 'all');
    }


    //添加话题话题
    public function add_topic(){

        $topic_obj = new TopicModel();

        $act = I('act');
        if($act == 'add' && IS_POST){
            $title = I('title');
            $collect_num = I('collect_num', 0, 'intval');
            $view_num = I('view_num', 0, 'intval');
            $contents = I('contents');
            $pics = I('pic');
            $img = I('img');
            $serial = I('serial', 0, 'intval');
//            $is_hot = I('is_hot', 0, 'intval');
            if(!$title){
                $this->error('请填写标题');
            }
            if(!$contents){
                $this->error('请填写话题内容');
            }
            $pics = count($pics) > 0 ? implode(',',$pics) : '';
            $data = array(
                'title' => $title,
                'collect_num' => $collect_num,
                'view_num' => $view_num,
                'contents' => $contents,
                'imgs' => $pics,
                'pic' => $img,
                'is_hot' => 1,
                'serial' => $serial,
            );
            if($topic_obj->addTopic($data)){
                $this->success('添加成功');
            }else{
                $this->error('添加失败');
            }
        }


        $this->assign('pic', array(
//            'batch'   => true,
            'name'    => 'img',
            'help'    => '点击可查看大图',
        ));


        $this->assign('pic_data', array(
            'batch'   => true,
            'name'    => 'pic',
            'dir'     => 'topic',
            'help'    => '点击可查看大图',
        ));

        $this->assign('head_title', '添加话题');
        $this->display();
    }

    //修改话题
    public function edit_topic(){
        $topic_id = I('topic_id', 0, 'intval');
        if(!$topic_id){
            $this->error('话题不存在');
        }

        $topic_obj = new TopicModel($topic_id);
        $topic = $topic_obj->getTopicInfo('topic_id ='.$topic_id);
        if($topic['is_del'] == 1 || !$topic){
            $this->error('话题不存在或已删除');
        }

        $act = I('act');
        if($act == 'edit' && IS_POST){
            $title = I('title');
            $collect_num = I('collect_num', 0, 'intval');
            $view_num = I('view_num', 0, 'intval');
            $contents = I('contents');
            $pics = I('pic');
            $img = I('img');
            $serial = I('serial', 0, 'intval');
//            $is_hot = I('is_hot', 0, 'intval');
            if(!$title){
                $this->error('请填写标题');
            }
            if(!$contents){
                $this->error('请填写话题内容');
            }
            $pics = count($pics) > 0 ? implode(',',$pics) : '';
            $data = array(
                'title' => $title,
                'collect_num' => $collect_num,
                'view_num' => $view_num,
                'contents' => $contents,
                'imgs' => $pics,
                'pic' => $img,
                'is_hot' => 1,
                'serial' => $serial,
                );
            if($topic_obj->editTopic($data)){
                $this->success('修改成功');
            }else{
                $this->error('修改失败');
            }
        }

        $user_obj = new UserModel($topic['user_id']);
        $user = $user_obj->getUserInfo('nickname');
        $this->assign('nickname', $user['nickname']);

        $imgs = array();
        if($topic['imgs']){
            $imgs = explode(',', $topic['imgs']);
        }

        $this->assign('pic', array(
//            'batch'   => true,
            'name'    => 'img',
            'url' =>$topic['pic'],
            'help'    => '点击可查看大图',
        ));


        $this->assign('pic_data', array(
            'batch'   => true,
            'name'    => 'pic',
            'pic_arr' => $imgs,
            'dir'     => 'topic',
            'help'    => '点击可查看大图',
        ));
        $this->assign('topic_id', $topic_id);
        $this->assign('topic', $topic);
        $this->assign('head_title', '修改话题');
        $this->display();
    }


    //删除话题
    public function del_topic(){
        $topic_ids = I('ids');
        $topic_obj = new TopicModel();
        if($topic_obj->delTopic('topic_id in('.$topic_ids.')')){
            $this->ajaxReturn('success');
        }else{
            $this->ajaxReturn('failure');
        }
    }


    public function view_list($where, $head_title, $opt){
        $where .= $this->get_search_condition();
        $view_obj = D('View');
        $show = getPageList($view_obj, 'getViewNum', $where);
        $this->assign('show', $show);

        $view_list = $view_obj->getViewList('', $where, 'addtime desc');
        $view_list =  $view_obj->getListData($view_list);
        
        $this->assign('view_list', $view_list);
        $this->assign('opt', $opt);
        $this->assign('head_title', $head_title);
        $this->display('get_view_list');
    }

    //话题观点
    public function get_view_list(){
        $topic_id = I('topic_id', 0, 'intval');
        if(!$topic_id){
            $this->error('话题不存在');
        }
        $topic_obj = new TopicModel();
        $topic = $topic_obj->getTopicInfo('topic_id ='.$topic_id);
        if($topic['is_del'] == 1 || !$topic){
            $this->error('话题不存在');
        }

        $this->view_list('is_del = 0 and topic_id ='.$topic_id.' and view_type ='.ViewModel::TOPIC, '话题观点', 'topic_view');
    }

    //修改观点/动态
    public function edit_view(){
        $view_id = I('view_id', 0, 'intval');
        if(!$view_id){
            $this->error('非法访问');
        }
        $view_obj = new ViewModel($view_id);
        $view = $view_obj->getViewInfo('view_id ='.$view_id);

        $act = I('act');
        if($act == 'edit' && IS_POST){
            $praise_num = I('praise_num', 0, 'intval');
            $comment_num = I('comment_num', 0, 'intval');
            $contents = I('contents');
            $pics = I('pic');
            $serial = I('serial', 0, 'intval');
            if(!$contents){
                $this->error('请填写话题内容');
            }
            $pics = count($pics) > 0 ? implode(',',$pics) : '';
            $data = array(
                'praise_num' => $praise_num,
                'comment_num' => $comment_num,
                'contents' => $contents,
                'imgs' => $pics,
                'serial' => $serial,
                );
            if($view_obj->editView($data)){
                $this->success('修改成功');
            }else{
                $this->error('修改失败');
            }
        }

        $user_obj = new UserModel($view['user_id']);
        $user = $user_obj->getUserInfo('nickname');
        $this->assign('nickname', $user['nickname']);

        $imgs = array();
        if($view['imgs']){
            $imgs = explode(',', $view['imgs']);

        }

        $this->assign('pic_data', array(
            'batch'   => true,
            'name'    => 'pic',
            'pic_arr' => $imgs,
            'dir'     => 'topic',
            'help'    => '点击可查看大图',
        ));

        $this->assign('view', $view);
        $this->assign('view_id', $view_id);
        $this->assign('head_title', '修改观点');
        $this->display();
    }

    //删除话题观点/动态
    public function del_view(){
        $view_ids = I('ids');
        $view_obj = new ViewModel();
        if($view_obj->delView('view_id in('.$view_ids.')')){
            $this->ajaxReturn('success');
        }else{
            $this->ajaxReturn('failure');
        }
    }


    //动态列表
    public function get_dynamic_list(){
        $this->view_list('is_del = 0 and view_type = 2', '动态列表', 'dynamic');
    }

    //修改观点/动态
    public function edit_dynamic(){
        $view_id = I('view_id', 0, 'intval');
        if(!$view_id){
            $this->error('非法访问');
        }
        $view_obj = new ViewModel($view_id);
        $view = $view_obj->getViewInfo('is_del = 0 and view_type = '.ViewModel::DYNAMIC.' and  view_id ='.$view_id);
        #echo $view_obj->getLastSql();
        #dump($view);die;
        $act = I('act');
        if($act == 'edit' && IS_POST){
            $praise_num = I('praise_num', 0, 'intval');
            $comment_num = I('comment_num', 0, 'intval');
            $contents = I('contents');
            $pics = I('pic');
            $serial = I('serial', 0, 'intval');
            if(!$contents){
                $this->error('请填写话题内容');
            }
            $pics = count($pics) > 0 ? implode(',',$pics) : '';
            $data = array(
                'praise_num' => $praise_num,
                'comment_num' => $comment_num,
                'contents' => $contents,
                'imgs' => $pics,
                'serial' => $serial,
                );
            if($view_obj->editView($data)){
                $this->success('修改成功');
            }else{
                $this->error('修改失败');
            }
        }

        $user_obj = new UserModel($view['user_id']);
        $user = $user_obj->getUserInfo('nickname');
        $this->assign('nickname', $user['nickname']);

        $imgs = array();
        if($view['imgs']){
            $imgs = explode(',', $view['imgs']);

        }

        $this->assign('pic_data', array(
            'batch'   => true,
            'name'    => 'pic',
            'pic_arr' => $imgs,
            'dir'     => 'topic',
            'help'    => '点击可查看大图',
        ));

        $this->assign('view', $view);
        $this->assign('view_id', $view_id);
        $this->assign('head_title', '修改观点');
        $this->display('edit_view');
    }

    public function comment_list($where, $head_title, $opt){
        $where .= $this->get_search_condition();
        $comment_obj = D('Comment');
        $show = getPageList($comment_obj, 'getCommentNum', $where);
        $this->assign('show', $show);

        $comment_list = $comment_obj->getCommentList('', $where, 'addtime desc');
        $comment_list =  $comment_obj->getListData($comment_list);
        $this->assign('comment_list', $comment_list);
        $this->assign('opt', $opt);
        $this->assign('head_title', $head_title);
        $this->display('get_comment_list');
    }

    //话题观点评论
    public function get_topic_comment_list(){
        $view_id = I('view_id', 0, 'intval');
        if(!$view_id){
            $this->error('观点不存在');
        }
        $view_obj = new ViewModel();
        $view = $view_obj->getViewInfo('view_id ='.$view_id);
        if($view['is_del'] == 1 || !$view){
            $this->error('话题不存在');
        }
        $this->comment_list('comment_type ='.CommentModel::TOPIC, '话题评论列表', 'topic');
    }

    //动态评论列表
    public function get_dynamic_comment_list(){
        $view_id = I('view_id', 0, 'intval');
        if(!$view_id){
            $this->error('动态不存在');
        }
        $view_obj = new ViewModel();
        $view = $view_obj->getViewInfo('view_id ='.$view_id);
        if($view['is_del'] == 1 || !$view){
            $this->error('动态不存在');
        }
        $this->comment_list('comment_type ='.CommentModel::DYNAMIC, '动态评论列表', 'dynamic');
    }


    //删除评论
    public function del_comment(){
        $comment_ids = I('ids');
        $comment_obj = new CommentModel();
        if($comment_obj->delComment('comment_id in('.$comment_ids.')')){
            $this->ajaxReturn('success');
        }else{
            $this->ajaxReturn('failure');
        }
    }
}
