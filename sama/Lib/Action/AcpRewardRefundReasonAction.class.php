<?php
class AcpRewardRefundReasonAction extends AcpAction
{

    public function _initialize()
    {
        parent::_initialize();
    }
    

    //所有退款原因列表
    public function get_reward_refund_reason_list(){

        $reward_refund_reason_obj = new RewardRefundReasonModel();
        //数据总量
        $total = $reward_refund_reason_obj->getRewardRefundReasonNum();

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $reward_refund_reason_obj->setStart($Page->firstRow);
        $reward_refund_reason_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('show', $show);

        $refund_reason_list = $reward_refund_reason_obj->getRewardRefundReasonList();
        // dump($channel_list);
        $this->assign('refund_reason_list', $refund_reason_list);


        $this->assign('head_title', '退款原因列表');
        $this->display();
    }


    //添加退款原因
    public function add_edit_reward_refund(){

        $reward_refund_reason_id = I('reward_refund_reason_id',0,'intval');
        $act = I('act');
        $reward_refund_reason_obj = new RewardRefundReasonModel();
        if($reward_refund_reason_id){
            $refund_info = $reward_refund_reason_obj->getRewardRefundReasonInfo('reward_refund_reason_id ='.$reward_refund_reason_id);
            $this->assign('act','edit');
            $this->assign('refund_info',$refund_info);
            $this->assign('head_title','修改退款原因');
        }else{
            $this->assign('head_title','添加退款原因');
            $this->assign('act','add');
        }

        if(IS_POST){
            $reason = I('reason');

            //表单验证
            if(!$reason) $this->error('请填写退款原因');


            $arr = array(
                'reason' => $reason,
            );
            // dump($arr);die;
            if ($act == 'add') {
                $success   = $reward_refund_reason_obj->addRewardRefundReason($arr);
                if ($success) {
                    $this->success('恭喜您，退款原因添加成功！', '/AcpRewardRefundReason/get_reward_refund_reason_list');
                } else {
                    $this->error('抱歉，退款原因添加失败！', '/AcpRewardRefundReason/get_reward_refund_reason_list');
                }
            }

            if($act == 'edit'){
                 $success = $reward_refund_reason_obj->setRewardRefundReason($reward_refund_reason_id,$arr);
                 if ($success !== false) {
                     $this->success('恭喜您，退款原因修改成功！', '/AcpRewardRefundReason/get_reward_refund_reason_list');
                 } else {
                     $this->error('抱歉，退款原因修改失败！', '/AcpRewardRefundReason/get_reward_refund_reason_list');
                 }
            }
        }
        $this->display();
    }


    //删除退款原因
    public function del_channel(){
        if(IS_POST && IS_AJAX){
            $channel_id = I('channel_id', 0, 'intval');
            if(!$channel_id) $this->ajaxReturn('failure');

            $channel_obj = new ChannelModel($channel_id);
            if($channel_obj->delChannel()){
                $this->ajaxReturn('success');
            }
            $this->ajaxReturn('failure');
        }
    }
}
