<?php
/**
 * Acp后台合同
 */
class AcpContractAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
//        $this->assign('action_title', '合同列表');
//        $this->assign('action_src', U('/AcpContract/get_contract_list'));
    }


    public function search(){

        //初始化where
        $where = 'true';
        $company_name =I('company_name');
        if($company_name){
            $this->assign('company_name',$company_name);
            $where.= ' AND company_name LIKE "%'.$company_name.'%"';
        }

        $linkman =I('linkman');
        if($linkman){
            $this->assign('linkman',$linkman);
            $where.= ' AND linkman LIKE "%'.$linkman.'%"';
        }


        /*押金搜索start*/
        //添加时间范围起始时间
        $deposit_obj = new DepositModel();
        $start_date = $this->_request('start_date');
        $start_date = str_replace('+', ' ', $start_date);
        $start_date = strtotime($start_date);
        if ($start_date) {
            $contract_ids = $deposit_obj->getDepositWhereFields('end_time >= ' . $start_date,'contract_id');
            $contract_str = implode(',',$contract_ids);
            if($contract_str){
                $where .= ' AND contract_id IN (' . $contract_str .')';
            }else{
                $where .=' AND false';
            }
            $this->assign('start_date',$start_date);
        }

        //添加时间范围结束时间
        $end_date = $this->_request('end_date');
        $end_date = str_replace('+', ' ', $end_date);
        $end_date = strtotime($end_date);
        if ($end_date) {
            $contract_ids = $deposit_obj->getDepositWhereFields('end_time <= ' . $end_date,'contract_id');
            $contract_str = implode(',',$contract_ids);
            if($contract_str){
                $where .= ' AND contract_id IN (' . $contract_str .')';
            }else{
                $where .=' AND false';
            }
            $this->assign('end_date',$end_date);
        }

        //押金是否已退
        $is_out = I('is_out');
        $is_out = ($is_out == '' || $is_out == -1 ) ?  -1 : intval($is_out);
        if($is_out != -1 && $is_out == 0){
            $contract_ids = $deposit_obj->getDepositWhereFields('end_time = 0','contract_id');
            $contract_str = implode(',',$contract_ids);
            if($contract_str){
                $where .= ' AND contract_id IN (' . $contract_str .')';
            }else{
                $where .=' AND false';
            }
        }elseif($is_out != -1 && $is_out == 1){
            $contract_ids = $deposit_obj->getDepositWhereFields('end_time <> 0','contract_id');
            $contract_str = implode(',',$contract_ids);
            if($contract_str){
                $where .= ' AND contract_id IN (' . $contract_str .')';
            }else{
                $where .=' AND false';
            }
        }
        $this->assign('is_out',$is_out);

        //收据是否已开
        $is_receipt = I('is_receipt');
        $is_receipt = ($is_receipt == '' || $is_receipt == -1 ) ?  -1 : intval($is_receipt);
        if($is_receipt != -1 && $is_receipt == 0){
            $contract_ids = $deposit_obj->getDepositWhereFields('is_receipt = 0','contract_id');
            $contract_str = implode(',',$contract_ids);
            if($contract_str){
                $where .= ' AND contract_id IN (' . $contract_str .')';
            }else{
                $where .=' AND false';
            }
        }elseif($is_receipt != -1 && $is_receipt == 1){
            $contract_ids = $deposit_obj->getDepositWhereFields('is_receipt = 1','contract_id');
            $contract_str = implode(',',$contract_ids);
            if($contract_str){
                $where .= ' AND contract_id IN (' . $contract_str .')';
            }else{
                $where .=' AND false';
            }
        }
        $this->assign('is_receipt',$is_receipt);
        /*押金搜索end*/

        return $where;

    }


    public function get_contract_list()
    {
        $contract_obj = new ContractModel();
        $where  = $this->search();
        //数据总量
        $total = $contract_obj->getContractNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $contract_obj->setStart($Page->firstRow);
        $contract_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $contract_list = $contract_obj->getContractList($where);
        $this->assign('contract_list', $contract_list);
        $this->assign('head_title', '合同列表');
        $this->display();

    }

    //添加合同
    public function add_edit_contract($opt)
    {

        $act = $this->_post('act');
        $contract_obj = new ContractModel();
        $contract_id =I('contract_id',0,'int');

        if(!$contract_id){
            if($opt=='park_contract') $this->assign('head_title','添加园区合同');
            if($opt=='zhongchuang_contract') $this->assign('head_title','添加众创空间合同');
            $this->assign('act','add');
        }else{
            $contract_info = $contract_obj->getContractInfo('contract_id ='.$contract_id);
            $this->assign('info',$contract_info);
            // dump($contract_info);die;
            //房源信息
            $house_info = unserialize($contract_info['house_info']);
            $lease_info = unserialize($contract_info['lease_info']);
            
            if($opt=='park_contract'){//园区
                foreach ($house_info as $k => $v) {
                    $storey_list = D('Storey')->getStoreyList('incubator_id ='.$v['incubator_id']);
                    $house_info[$k]['storey_list'] = $storey_list;
                    $station_list= D('Station')->getStationList('','incubator_id ='.$v['incubator_id'] . ' and storey_id ='.$v['storey_id']);
                    $house_info[$k]['station_list'] = $station_list;
                }
            }else{//众创空间
                foreach ($house_info as $k => $v) {
                    $storey_list = D('Storey')->getStoreyList('incubator_id ='.$v['incubator_id']);
                    $house_info[$k]['storey_list'] = $storey_list;
                    $station_class= D('StationClass')->getStationClassList('class_name, station_class_id','incubator_id ='.$v['incubator_id']. ' and is_del = 0');
                    $house_info[$k]['station_class'] = $station_class;
                    $station_list = D('Station')->getStationList('', 'station_type_id in('.implode(',', $v['station_ids']).')');
                    $house_info[$k]['station_list'] = $station_list;

                    $all_station_list= D('Station')->getStationList('station_type_id, station_num, price','incubator_id ='.$v['incubator_id'] . ' and storey_id ='.$v['storey_id'] .' and station_class_id ='.$v['station_class_id']);
                    $house_info[$k]['all_station_list'] = $all_station_list;
                }
            }
            
             // dump($house_info[0]);die;
            $this->assign('house_info', $house_info);
            $this->assign('lease_info', $lease_info);

            //附件
            if($contract_info['file']){
                $files = explode(',', $contract_info['file']);
            }
            $this->assign('contract_id', $contract_id);
            $this->assign('files', $files);
            $this->assign('file_val', $contract_info['file']);
            if($opt=='park_contract') $this->assign('head_title','修改园区合同');
            if($opt=='zhongchuang_contract') $this->assign('head_title','修改众创空间合同');
            $this->assign('act','edit');
        }

        if(IS_POST){
            // dump($_POST);die;
            $_post      = $this->_post();
            $type_id = $_post['type_id'];
            $linkman     = $_post['linkman'];
            $link_way      = $_post['link_way'];
            $deposit = $_post['deposit'];
            $start_time = $_post['start_time'];
            $file = $_post['files'];
            $file = trim($file, ',');

            /* 房源信息 start */
            $incubator_ids  = $_post['incubator_id'];
            $storey_ids   = $_post['storey_id'];
            $station_type_ids   = $_post['station_type_id'];
            
            /* 房源信息 end */
            $house_info =array();
            if($opt=='park_contract'){
                $lease_year = I('lease_year', 0, 'intval');
                $lease_year = $lease_year + 1; 
                //根据房间id计算租金信息
                $lease_info = calculate_park_money($station_type_ids, $lease_year);
                $lease_total_money = $lease_info['lease_total_money'];
                $consult_total_money = $lease_info['consult_total_money'];
                $total_area = 0;
                unset($lease_info['consult_total_money']);
                unset($lease_info['lease_total_money']);
                foreach ($incubator_ids as $k => $incubator_id){

                    $house_info[$k]=array(
                        'incubator_id' =>$incubator_id,
                        'storey_id' =>$storey_ids[$k],
                        'station_type_id' =>$station_type_ids[$k],
                    );

                    $room_info = M('StationType')->where('station_type_id ='.$station_type_ids[$k])->find();
                    $house_info[$k]['area'] = $room_info['covered_area'];
                    $house_info[$k]['unit_price'] = $room_info['price'];
                    $total_area += $house_info[$k]['area'];
                }
                //过期时间
                $end_time = (substr($start_time, 0, strpos($start_time, '-')) + $lease_year) . substr($start_time, strpos($start_time, '-'));
                $price_up_rate = 16;//单价递增比例   
                $consult_fee_rate = 42.68;//咨询服务费比例
                $data = array(
                    'company_name' => I('company_name'),
                    'type_id' => $type_id,
                    'linkman' => $linkman,
                    'link_way' => $link_way,
                    'house_info' => serialize($house_info),
                    'lease_time' => $lease_year,
                    'deposit' => $deposit,
                    'lease_total_money' => $lease_total_money,
                    'consult_total_money' => $consult_total_money,
                    'start_time' => strtotime($start_time),
                    'addtime' => time(),
                    'total_area' => $total_area,
                    'contract_type' => 1,
                    'price_up_rate' => $price_up_rate,
                    'consult_fee_rate' => $consult_fee_rate,
                    'lease_info' => serialize($lease_info['zj']),
                    'file' => $file,
                    'end_time' => strtotime($end_time),
                    );
                // dump($data);die;
                $data['total_rooms'] = count($station_type_ids);
                $data['station_ids'] = trim(implode(',', $station_type_ids), ',');
            }elseif($opt=='zhongchuang_contract'){
                // dump($_POST);
                $consult_fee_rate = 42.68;//咨询服务费比例
                $lease_day = I('lease_day', 0, 'intval');
                //到期时间
                $end_time = strtotime($start_time) + $lease_day * 3600 * 24;
                
                //根据工位id计算租金信息
                $station_ids_str = I('station_ids_str');
                $station_ids_str = trim($station_ids_str, ',');
                $station_ids_arr = explode(',', $station_ids_str);
                $station_ids_arr = array_unique($station_ids_arr);
                $lease_info = calculate_zc_money($station_ids_arr, $lease_day);
                $lease_total_money = $lease_info['lease_total_money'];
                $consult_total_money = $lease_info['consult_total_money'];

                $station_ids = I('station_ids');
                $station_class = I('station_class');

                //房源信息
                foreach ($incubator_ids as $k => $v) {
                    $house_info[$k]=array(
                        'incubator_id' =>$v,
                        'storey_id' =>$storey_ids[$k],
                        'station_class_id' =>$station_class[$k],
                    );
                    $stations = trim($station_ids[$k], ',');
                    $house_info[$k]['station_ids'] = explode(',', $stations);
                 }
                 
                
                $data = array(
                    'company_name' => I('company_name'),
                    'type_id' => $type_id,
                    'linkman' => $linkman,
                    'link_way' => $link_way,
                    'house_info' => serialize($house_info),
                    'lease_time' => $lease_day,
                    'deposit' => $deposit,
                    'lease_total_money' => $lease_total_money,
                    'consult_total_money' => $consult_total_money,
                    'start_time' => strtotime($start_time),
                    'addtime' => time(),
                    'contract_type' => 2,
                    'consult_fee_rate' => $consult_fee_rate,
                    'file' => $file,
                    'end_time' => $end_time,
                    );
                $data['station_ids'] = $station_ids_str;
                $data['total_station_num'] = count($station_ids_arr);
            }
            $data['incubator_ids'] = trim(implode(',', $incubator_ids), ',');
            $data['storey_ids'] = trim(implode(',', $storey_ids), ',');
            // dump($data);die;
            $contract_obj = new ContractModel();

                if($act == 'add'){
                    if($contract_obj->addContract($data)){
                        $this->success('合同添加成功');
                    }else{
                        $this->error('合同添加失败');
                    }
                }elseif($act == 'edit'){
                    if($contract_obj->setContract($contract_id, $data)){
                        $this->success('合同修改成功');
                    }else{
                        $this->error('合同修改失败');
                    }
                }



        }


        //所属行业
        $type_obj = new TypeModel();
        $domain_list = $type_obj->getTypeList('type_type ='.TypeModel::DOMAIN);
        $this->assign('domain_list', $domain_list);

        //园区
        $incubator_obj = new IncubatorModel();
        $incubator_park_list = $incubator_obj->getIncubatorParkList();

        $incubator_zhongc_list = $incubator_obj->getIncubatorZhongcList();

        $this->assign('incubator_park_list',$incubator_park_list);
        $this->assign('incubator_zhongc_list',$incubator_zhongc_list);
        $this->assign('opt', $opt);
        $this->display('add_edit_contract');
    }

    //添加园区合同
    public function add_edit_park_contract(){
        $this->add_edit_contract('park_contract');
    }


    //添加众创空间合同
    public function add_edit_zhongchuang_contract(){
        $this->add_edit_contract('zhongchuang_contract');
    }
    

    /**
     * 删除合同
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据合同ID删除合同
     */
    public function delete_contract()
    {
        $contract_id = intval($this->_post('contract_id'));
        if ($contract_id) {
            $contract_obj = new ContractModel($contract_id);
            $service_provides_obj           = new ServiceProvidersModel();
            $num                = $service_provides_obj->getServiceProvidersNum('contract_id = ' . $contract_id);

            //存在服务商则删除失败；
            if ($num) {
                exit('failure');
            }

            $success = $contract_obj->delContract();
            exit($success ? 'success' : 'failure');
        }

        exit('failure');
    }

    public function batch_delete_contract()
    {

        $contract_ids = $this->_post('contract_ids');
        if ($contract_ids) {
            $contract_ary = explode(',', $contract_ids);
            $success_num     = 0;
            $service_provides_obj           = new ServiceProvidersModel();
            foreach ($contract_ary as $contract_id) {
                $num = $service_provides_obj->getServiceProvidersNum('contract_id = ' . $contract_id);
                if ($num) {
                    continue;
                }

                $contract_obj = new ContractModel($contract_id);
                $success_num += $contract_obj->delContract();
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }

        exit('failure');
    }


    //计算园区费用
    public function calculate_park_money(){
        
        $years = I('lease_year', 0, 'intval');
        $rooms = I('room_id');
        if(!$years || !count($rooms)){
            $this->ajaxReturn(0);
        }
        $rooms[] = 0;
        $r = calculate_park_money($rooms, $years);
        $this->ajaxReturn($r);
    }

    /**
     * 押金列表
     */
    public function get_deposit_list(){

        $contract_obj = new ContractModel();
        $where  = 'is_del = 0 ';
        $where  .= $this->search();
        //数据总量
        $total = $contract_obj->getContractNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $contract_obj->setStart($Page->firstRow);
        $contract_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $contract_list = $contract_obj->getContractList($where);
        $contract_list = $contract_obj->getListData($contract_list);

        //退租原因
        $type_obj = new TypeModel();
        $surrender_list =  $type_obj->getTypeList('type_type ='.TypeModel::SURRENDER.' AND is_del = 0');
        $this->assign('surrender_list', $surrender_list);
        $this->assign('contract_list', $contract_list);
        $this->assign('head_title', '押金列表');
        $this->display();

    }

    /**
     * 添加押金
     */
    public function add_deposit(){

        $contract_id = I('contract_id',0,'int');
        $start_time = I('start_time');
        $is_receipt = I('is_receipt');
        $pay_deposit = I('pay_deposit',0,'float');

        if($start_time){
            $start_time = strtotime(I('start_time'));
        }else{
            $start_time =time();
        }
        if(!$pay_deposit) $this->ajaxReturn(array('code'=>0,'msg'=>'请填写押金'));
        if($pay_deposit && !is_float($pay_deposit)) $this->ajaxReturn(array('code'=>0,'msg'=>'请正确填写押金'));

        $data= array(
            'contract_id'=>$contract_id,
            'start_time'=>$start_time,
            'is_receipt'=>$is_receipt,
            'pay_deposit'=>$pay_deposit,
        );

        $deposit_obj = new DepositModel();
        if($deposit_obj->addDeposit($data)) $this->ajaxReturn(array('code'=>1,'msg'=>'登记押金成功'));
        $this->ajaxReturn(array('code'=>0,'msg'=>'登记押金失败'));

    }

    /**
     * 退押金
     */
    public function end_deposit(){

        $deposit_id = I('deposit_id',0,'int');
        $end_time = I('end_time');
        $surrender_id = I('surrender_id',0,'int');

        if($end_time){
            $end_time = strtotime(I('end_time'));
        }else{
            $end_time =time();
        }
        if(!$surrender_id) $this->ajaxReturn(array('code'=> 0,'msg'=>'请选择退租原因'));

        $data= array(
            'end_time'=>$end_time,
            'surrender'=>$surrender_id,
        );
        $deposit_obj = new DepositModel();
        if($deposit_obj->setDeposit($deposit_id,$data)) $this->ajaxReturn(array('code'=>1,'msg'=>'退回押金成功'));
        $this->ajaxReturn(array('code'=>0,'msg'=>'退回押金失败'));
    }


    /**
     *  房源信息
     */
    public function get_look_housing_info(){
        $contract_id = I('contract_id',0,'int');
        $contract_obj = new ContractModel();
        $contract = $contract_obj->getContractInfo('contract_id ='.$contract_id,'contract_type, house_info');
        $house_info = unserialize($contract['house_info']);
        // dump($house_info);die;
        if($contract['contract_type'] == 1){
            foreach ($house_info as $k => $v) {
                $incubator_obj = new IncubatorModel();
                $house_info[$k]['incubator_name'] = $incubator_obj->getIncubatorField('incubator_name','incubator_id ='.$v['incubator_id']);
                $storey_obj = new StoreyModel();
                $house_info[$k]['storey_name'] = $storey_obj->getStoreyField($v['storey_id'],'storey_name');
                $station_obj = new StationModel();
                $house_info[$k]['station_type_name'] = $station_obj->getStationField('station_type_id ='.$v['station_type_id'],'station_type_name');
            }
        }else{
            foreach ($house_info as $k => $v) {
                $incubator_obj = new IncubatorModel();
                $house_info[$k]['incubator_name'] = $incubator_obj->getIncubatorField('incubator_name','incubator_id ='.$v['incubator_id']);
                $storey_obj = new StoreyModel();
                $house_info[$k]['storey_name'] = $storey_obj->getStoreyField($v['storey_id'],'storey_name');
                $class_obj = new StationClassModel();
                $house_info[$k]['class_name'] = $class_obj->where('station_class_id ='.$v['station_class_id'])->getField('class_name');

                $station_list = D('Station')->getStationList('', 'station_type_id in('.implode(',', $v['station_ids']).')');
                $house_info[$k]['station_list'] = $station_list;
            }
        }

        // $house_list = unserialize($house_info['house_info']);

        // foreach ($house_list as $k => $v){
        //     $incubator_obj = new IncubatorModel();
        //     $house_list[$k]['incubator_name'] = $incubator_obj->getIncubatorField('incubator_name','incubator_id ='.$v['incubator_id']);

        //     $storey_obj = new StoreyModel();
        //     $house_list[$k]['storey_name'] = $storey_obj->getStoreyField($v['storey_id'],'storey_name');
        //     $station_obj = new StationModel();
        //     $house_list[$k]['station_type_name'] = $station_obj->getStationField('station_type_id ='.$v['station_type_id'],'station_type_name');
        // }
        // dump($house_info[0]);die;
        $this->ajaxReturn(array('type'=>$contract['contract_type'] , 'house_info'=>$house_info));
    }

    /**
     *  租金列表
     */
    public function get_rent_list(){

        $contract_obj = new ContractModel();
        //数据总量
        $total = $contract_obj->getContractNum();

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $contract_obj->setStart($Page->firstRow);
        $contract_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $contract_list = $contract_obj->getContractList();
        $contract_list = $contract_obj->getListData($contract_list);

        $this->assign('contract_list', $contract_list);
        $this->assign('head_title', '租金管理列表');
        $this->display();

    }

    /**
     *  收租登记．
     */
    public function add_rent(){
        $contract_id = I('contract_id',0,'int');//合同id
        $rent_time = I('rent_time');//交租日期
        $payable_money = I('payable_money');//本期应缴金额
        $overdue_money = I('overdue_money');//本期应缴逾期金额
        $actual_money = floatval(I('actual_money'));//实交金额
        $actual_time = I('actual_time');//实交日期
        if($rent_time){
            $rent_time = strtotime($rent_time);
        }else{
            $rent_time = time();
        }
        if($actual_time){
            $actual_time = strtotime($actual_time);
        }else{
            $actual_time = time();
        }
        if(!$actual_money) $this->ajaxReturn(array('code'=>0,'msg'=>'请填写实交金额'));
        if($actual_money && !is_float($actual_money)) $this->ajaxReturn(array('code'=>0,'msg'=>'请填写正确实交金额'));
        $data =array(
            'contract_id'=> $contract_id,
            'rent_time'=> $rent_time,
            'payable_money'=> $payable_money,
            'overdue_money'=> $overdue_money,
            'actual_money'=> $actual_money,
            'actual_time'=> $actual_time,
        );

        $rent_obj = new RentModel();
        if($rent_obj->addRent($data)) $this->ajaxReturn(array('code'=> 1 ,'msg' => '添加收租明细成功'));
        $this->ajaxReturn(array('code'=> 0 ,'msg' => '添加收租明细失败'));
    }

    /**
     *  获取本期应缴金额,应缴逾期金额．
     */
    public function get_payable_overdue_money(){
        $contract_id = I('contract_id',0,'int');
        $contract_obj = new ContractModel();
        $contract_info = $contract_obj->getContractInfo('contract_id ='.$contract_id);
        $rent_obj = new RentModel();
        //获取当前期次．
        $count = $rent_obj->getRentNum('contract_id ='.$contract_id);
        $count ++;

        //应交总租金
        $total_payable_money = $rent_obj->getSumMoney('contract_id ='.$contract_id,'payable_money');
        //已收总租金
        $total_actual_money = $rent_obj->getSumMoney('contract_id ='.$contract_id,'actual_money');
        //应缴逾期总金额
        $overdue_money = sprintf("%.2f",($total_payable_money - $total_actual_money));

        //重创空间
        if($contract_info['contract_type'] == 2){
            $total_money = sprintf("%.2f",$contract_info['lease_total_money'] + $contract_info['consult_total_money']);
            $this->ajaxReturn(array('count'=>$count,'payable_money'=>$total_money,'overdue_money'=>$overdue_money));
        }
        //园区
        if($contract_info['contract_type'] == 1){

            $year = ceil($count/2);
            $lease_info=(unserialize($contract_info['lease_info']));

            //每期应交的金额．
            $total_money_arr = $lease_info[$year];
            $total_money = sprintf("%.2f",$total_money_arr['lease_money'] + $total_money_arr['consult_money'])/2;
            $this->ajaxReturn(array('count'=>$count,'payable_money'=>$total_money,'overdue_money'=>$overdue_money));

        }
    }

    /**
     *  租金明细列表．
     */
    public function get_rent_details_list(){

        $contract_id =I('contract_id',0,'int');
        $where = 'contract_id ='.$contract_id;
        $rent_obj = new RentModel();
        //数据总量
        $total = $rent_obj->getRentNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $rent_obj->setStart($Page->firstRow);
        $rent_obj->setLimit($Page->listRows);
        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $rent_list = $rent_obj->getRentList($where);
        $rent_list = $rent_obj->getListData($rent_list);
        $this->assign('rent_list', $rent_list);

        $this->assign('head_title', '租金明细列表');
        $this->display();
    }



    public function get_station_no(){
        if(IS_POST && IS_AJAX){
            $storey_id = I('storey_id', 0, 'intval');
            $incubator_id = I('incubator_id', 0, 'intval');
            $station_class = I('station_class', 0, 'intval');
            // dump($_POST);die;
            if(!$storey_id || !$incubator_id || !$station_class){
                $this->ajaxReturn(array('code'=>1));
            } 

            $station_obj = new StationModel();
            $station_nos = $station_obj->get_station_no($storey_id, $incubator_id, $station_class);
            $this->ajaxReturn(array('code'=>0, 'station_nos'=> $station_nos));

        }
    }


    //计算众创空间租金
    public function calculate_zc_money(){
        if(IS_POST && IS_AJAX){
            $lease_day = I('lease_day', 0, 'intval');
            $station_ids = I('station_ids');
            if(!$lease_day || !count($station_ids)){
                $this->ajaxReturn(0);
            }
            
            $zj_arr = calculate_zc_money($station_ids, $lease_day);
            $this->ajaxReturn($zj_arr);
        }
    }




    //合同详情
    public function contract_detail(){
        $contract_id = I('contract_id', 0, 'intval');
        $contract_obj = new ContractModel();

        $contract_info = $contract_obj->getContractInfo('contract_id ='.$contract_id);
            $this->assign('info',$contract_info);
            // dump($contract_info);die;
            //房源信息
            $house_info = unserialize($contract_info['house_info']);
            $lease_info = unserialize($contract_info['lease_info']);
            // dump($lease_info);die;
            if($contract_info['contract_type'] == 1){
                foreach ($house_info as $k => $v) {
                    $incubator_obj = new IncubatorModel();
                    $house_info[$k]['incubator_name'] = $incubator_obj->getIncubatorField('incubator_name','incubator_id ='.$v['incubator_id']);
                    $storey_obj = new StoreyModel();
                    $house_info[$k]['storey_name'] = $storey_obj->getStoreyField($v['storey_id'],'storey_name');
                    $station_obj = new StationModel();
                    $house_info[$k]['station_type_name'] = $station_obj->getStationField('station_type_id ='.$v['station_type_id'],'station_type_name');
                }
            }else{
                foreach ($house_info as $k => $v) {
                    $incubator_obj = new IncubatorModel();
                    $house_info[$k]['incubator_name'] = $incubator_obj->getIncubatorField('incubator_name','incubator_id ='.$v['incubator_id']);
                    $storey_obj = new StoreyModel();
                    $house_info[$k]['storey_name'] = $storey_obj->getStoreyField($v['storey_id'],'storey_name');
                    $class_obj = new StationClassModel();
                    $house_info[$k]['class_name'] = $class_obj->where('station_class_id ='.$v['station_class_id'])->getField('class_name');

                    $station_list = D('Station')->getStationList('', 'station_type_id in('.implode(',', $v['station_ids']).')');
                    $house_info[$k]['station_list'] = $station_list;
                }
            }
            
              // dump($house_info[0]);die;
            $this->assign('house_info', $house_info);
            $this->assign('lease_info', $lease_info);

            //附件
            if($contract_info['file']){
                $files = explode(',', $contract_info['file']);
            }
            $this->assign('contract_id', $contract_id);
            $this->assign('files', $files);
            $this->assign('file_val', $contract_info['file']);
            
            //所属行业
            $field = M('Type')->where('type_id ='.$contract_info['type_id'])->getField('type_name');
            $this->assign('field', $field);

        $this->assign('head_title', '合同详情');
        $this->display();
    }
}
