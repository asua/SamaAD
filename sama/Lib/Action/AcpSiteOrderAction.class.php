<?php
/**
 * Acp后台场地预约
 */
class AcpSiteOrderAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
        $this->assign('action_title', '场地预约列表');
        $this->assign('action_src', U('/AcpSiteOrder/get_site_order_list'));
    }

    /**
     * 接收搜索表单数据，组织返回where子句
     * @author 姜伟
     * @param void
     * @return void
     * @todo 接收表单提交的参数，过滤合法性，组织成where子句并返回
     */
    public function get_search_condition()
    {
        //初始化查询条件
        $where = '';

        //id
        $site_id = $this->_request('site_id');
        if ($site_id) {
            $where .= ' AND site_id = ' . $site_id;
        }

        //联系人名称
        $linkman = $this->_request('linkman');
        if ($linkman) {
            $where .= ' AND linkman LIKE "%' . $linkman . '%"';
        }

        //手机号
        $linkphone = $this->_request('linkphone');
        if ($linkphone) {
            $where .= ' AND linkphone LIKE "%' . $linkphone . '%"';
        }


        //添加时间范围起始时间
        $start_date = $this->_request('start_date');
        $start_date = str_replace('+', ' ', $start_date);
        $start_date = strtotime($start_date);
        if ($start_date) {
            $where .= ' AND addtime >= ' . $start_date;
        }

        //添加时间范围结束时间
        $end_date = $this->_request('end_date');
        $end_date = str_replace('+', ' ', $end_date);
        $end_date = strtotime($end_date);
        if ($end_date) {
            $where .= ' AND addtime <= ' . $end_date;
        }

        //重新赋值到表单
        $this->assign('start_date', $start_date ? $start_date : '');
        $this->assign('end_date', $end_date ? $end_date : '');
        $this->assign('linkman', $linkman);
        $this->assign('linkphone', $linkphone);

        return $where;
    }

    //公用方法
    public function get_site_order_list($where,$opt)
    {
        $site_order_obj = new SiteOrderModel();
        $where.=$this->get_search_condition();
        //数据总量
        $total = $site_order_obj->getSiteOrderNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $site_order_obj->setStart($Page->firstRow);
        $site_order_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $site_order_list = $site_order_obj->getSiteOrderList($where,'addtime desc');
        $site_order_list = $site_order_obj->getListData($site_order_list);
//        dump($site_order_list);die;
        $this->assign('opt', $opt);
        $this->assign('site_order_list', $site_order_list);
        $this->assign('head_title', '场地预约列表');
        $this->display('get_site_order_list');
    }

    //已通过的场地列表
    public function get_site_passed_list(){
        $this->get_site_order_list('is_check =1',$opt= 'passed');
    }
    //待通过的场地列表
    public function get_site_bypass_list(){
        $this->get_site_order_list('is_check = 0',$opt='not_pass');
    }

    //所有的场地列表
    public function get_site_all_list(){
        $site_id = I('site_id');
        $this->get_site_order_list('site_id ='.$site_id,$opt='all');
    }

    //公用详情
    public function detail_site_order($title){
        $site_order_id = I('site_order_id',0,'int');
        $site_order_obj =new SiteOrderModel();
        $site_order_info  =$site_order_obj->getSiteOrderInfo('site_order_id ='.$site_order_id);

        $site_obj = new SiteModel();
        $site_info = $site_obj ->getSiteInfo('site_id ='.$site_order_info['site_id']);

        $site_order_info['site_name'] = $site_info['site_name'];

        //支付状态
        $pay_status_arr = $site_order_obj->payStatus();
        $site_order_info['state'] = $pay_status_arr[$site_order_info['pay_status']];

        //预约时间
        $site_order_info['order_time'] = date('Y-m-d H:i',$site_order_info['order_time']);

        //范围时间
        $range_time_arr = explode(',',$site_order_info['range_time']);
        $range_arr= array();
        foreach($range_time_arr as $k1 => $v1){
            $range_arr[] = $v1.':00-'.($v1+1).':00  ';
        }
        $site_order_info['time_str'] = implode(',',$range_arr);

        //支付方式
        $payway_obj = new PaywayModel();
        $payway_list = $payway_obj->getAllPaywayList();
        $site_order_info['pay_way']= $payway_list[$site_order_info['payway']];

        $this->assign('info',$site_order_info);
        $this->assign('head_title',$title);
        $this->display('detail_site_order');
    }

    //已通过场地预约详情
    public function detail_passed_site_order(){
        $this->detail_site_order('已通过场地预约详情');
    }
    //待通过场地预约详情
    public function detail_bypass_site_order(){
        $this->detail_site_order('待通过场地预约详情');
    }



    //通过,拒绝
    public function set_check(){
        $obj_id = I('obj_id',0,'int');
        $type= I('type',0,'int');
        $site_order_obj = new SiteOrderModel();
        $site_order_info =$site_order_obj ->getSiteOrderInfo('site_order_id ='.$obj_id);
        $site_order_info['time'] = date('Y-m-d',$site_order_info['order_time']);
        $contents = serialize($site_order_info);

        $site_obj = new SiteModel();
        $site_info = $site_obj ->getSiteInfo('site_id ='.$site_order_info['site_id'],'site_name');

        if($type == 1){
            if($site_order_obj->setSiteOrderStatus($obj_id,array('is_check'=>1))){

                $message_obj = new MessageModel();
                $title = '预约'.$site_info['site_name'].'场地成功';
                $message_obj ->addMessage($obj_id,MessageModel::SITE_AUDIT,0,$site_order_info['user_id'],$title,$contents);

                $this->ajaxReturn(array('code'=>1,'msg'=>'预约通过成功!'));
            }else{
                $this->ajaxReturn(array('code'=>0,'msg'=>'预约通过失败!'));
            }
        }elseif ($type == 2){
            if($site_order_obj->setSiteOrderStatus($obj_id,array('is_check'=>2))){

                $message_obj = new MessageModel();
                $title = '预约'.$site_info['site_name'].'场地失败';
                $message_obj ->addMessage($obj_id,MessageModel::SITE_AUDIT,0,$site_order_info['user_id'],$title,$contents);

                if($site_order_info['is_check'] == 2){
                    //退款
                }

                $this->ajaxReturn(array('code'=>1,'msg'=>'拒绝预约成功!'));
            }else{
                $this->ajaxReturn(array('code'=>0,'msg'=>'拒绝预约失败!'));
            }
        }else{
            $this->ajaxReturn(array('code'=>0,'msg'=>'操作失败!'));
        }
    }


    //修改场地预约
    public function edit_site_order()
    {
        $redirect = U('/AcpSiteOrder/get_site_order_order_list');
        $site_id = intval($this->_get('site_id'));
        if (!$site_id) {
            $this->error('对不起，非法访问！', $redirect);
        }

        $site_order_obj  = new SiteModel();
        $site_info = $site_order_obj->getSiteInfo('site_id ='.$site_id);

        if (!$site_info) {
            $this->error('对不起，不存在相关场地预约！', $redirect);
        }

        $act = $this->_post('act');

        if ($act == 'edit') {
            $_post      = $this->_post();
            $site_name = $_post['site_name'];
            $incubator_id     = $_post['incubator_id'];
            $site_type_id     = $_post['site_type_id'];
            $price     = $_post['price'];
            $address     = $_post['address'];
            $people      = $_post['people'];
            $user_name  = $_post['user_name'];
            $mobile  = $_post['mobile'];
            $start_time  = $_post['start_time'];
            $end_time  = $_post['end_time'];
            $serial  = $_post['serial'];
            $site_sketch  = $_post['site_sketch'];

            //表单验证
            if (!$site_name) {
                $this->error('请填写场地预约名称！');
            }
            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }

            $arr = array(
                'site_name' => $site_name,
                'incubator_id'     => $incubator_id,
                'site_type_id'      => $site_type_id,
                'price'   => $price,
                'address'   => $address,
                'people'   => $people,
                'user_name'   => $user_name,
                'mobile'   => $mobile,
                'start_time'   => strtotime($start_time),
                'end_time'   => strtotime($end_time),
                'serial'   => $serial,
                'site_sketch'   => $site_sketch,
            );

            $url       = '/AcpSiteOrder/edit_site_order/site_id/' . $site_id;

            if ($site_order_obj->setSite($site_id, $arr)) {
                $this->success('恭喜您，场地预约修改成功！', $url);
            } else {
                $this->error('抱歉，场地预约修改失败！', $url);
            }
        }

        //孵化器
        $incubator_obj = new IncubatorModel();
        $incubator_list = $incubator_obj->getIncubatorList();
        $this->assign('incubator_list', $incubator_list);

        //场地预约类型
        $site_type_obj = new TypeModel();
        $site_type_list = $site_type_obj->getTypeList('type_type ='.TypeModel::SITE);
        $this->assign('site_type_list', $site_type_list);

        $this->assign('site_info', $site_info);
        $this->assign('head_title', '修改场地预约');
        $this->display();

    }

    /**
     * 删除场地预约
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据场地预约ID删除场地预约
     */
    public function delete_site_order()
    {
        $site_order_id = intval($this->_post('obj_id'));
        if ($site_order_id) {
            $site_order_obj = new SiteOrderModel($site_order_id);
            $success = $site_order_obj->delSiteOrder($site_order_id);
            exit($success ? 'success' : 'failure');
        }

        exit('failure');
    }

    public function batch_delete_site_order()
    {
        $site_order_ids = $this->_post('obj_ids');
        if ($site_order_ids) {
            $site_order_ary = explode(',', $site_order_ids);
            $success_num     = 0;
            foreach ($site_order_ary as $site_order_id) {
                $site_order_obj = new SiteOrderModel($site_order_id);
                $success_num += $site_order_obj->delSiteOrder($site_order_id);
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }
        exit('failure');
    }

    //退款
    public function refund(){
        $order_id = I('order_id', 0, 'intval');
        if(!$order_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，操作失败'));

        $order_obj = D('SiteOrder');
        $order = $order_obj->getSiteOrderInfo('site_order_id ='.$order_id);
        if(!$order || $order['pay_status'] != 1 || !$order_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，操作失败'));
        // dump($site_order);die;
        $payway = $order['payway'];
        $pay_code = $order['pay_code'];
        $refund_fee = $order['pay_amount'];
        if($payway == 'wxpay'){
            $wxpay_obj = new WXPayModel();
            $r = $wxpay_obj->wx_refund($pay_code, $refund_fee);
            if($r){
                $order_obj->where('site_order_id ='.$order_id)->save(array('pay_status'=>3, 'refund_time'=>time()));

                $this->ajaxReturn(array('code'=>0, 'msg'=>'退款成功'));
            }else{
                $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，退款失败'));
            }
            // dump($r);die;
        }elseif($payway == 'mobile_alipay'){
            $alipay_obj = new AlipayModel();
            // $r = $alipay_obj->ali_refund($pay_code, $refund_fee, 'site');
            $r = $alipay_obj->ali_refund($order_id, 'site');
            $this->ajaxReturn(array('code'=>2, 'link'=> $r));
            // die;
            // redirect($r);
        }
        $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，退款失败'));
    }
}
