<?php

class FrontUserCenterAction extends FrontAction
{
    function _initialize()
    {
        parent::_initialize();
        $this->page_num = 10;
        $this->assign('nav', 'me');
    }

    //个人中心
    public function personal()
    {
        $user_id = intval(session('user_id'));
        //用户信息
        $user_obj = new UserModel($user_id);
        // $user_info = $user_obj->getUserInfo('user_id, nickname, headimgurl , sex');
        $user_info = M('Users')->field('user_id, nickname, headimgurl , sex')->where('user_id ='.$user_id)->find();
        $this->assign('user_info', $user_info);

        //我所佩戴的头衔．
        $user_title_obj = new UserTitleModel();
        $my_wear_title =  $user_title_obj->getUserTitleList('is_adorn = 1 AND user_id ='.$user_id,'adorn_time');
        $my_wear_title =  $user_title_obj->getListData($my_wear_title);
        $this->assign('my_wear_title',$my_wear_title);


        //我关注的用户,我的粉丝
        $follow_obj = new FollowModel();
        $my_focus_num  = $follow_obj->getFollowedTypeNum($user_id,FollowModel::USER);
        $my_fans_num  = $follow_obj->getFansUserNum($user_id);
        //我的帖子数
        $post_obj = new PostModel();
        $my_post_num  = $post_obj -> getPostNum('user_id ='.$user_id);

        $my_num = array(
          'my_focus_num'=> $my_focus_num,
          'my_fans_num'=> $my_fans_num,
          'my_post_num'=> $my_post_num,
        );

        //sama信息
        $about_code = $GLOBALS['config_info']['ABOUT_CODE'];


        $this->assign('about_code', $about_code);
        $this->assign('my_num', $my_num);
        $this->assign('head_title', '我的');
        $this->display();
    }

    //别人的空间
    public function others_space(){
        $suser_id = intval(session('user_id'));
        $user_id = I('user_id',0,'intval');
        //如果id相同则去自己的空间
        // if($suser_id == $user_id) {
        //     redirect('/FrontUserCenter/personal');
        //     exit;
        // }

        if($this->is_hidden){
            $where = ' and post_type = '.PostModel::NORMAL;
            #$where_c = ' and channel_type = '.ChannelModel::NORMAL;
        }

        //该用户所发的帖子
        $post_obj = new PostModel();
        $where = 'user_id ='.$user_id . ' and is_del = 0 and review = 1'.$where;
        $total = $post_obj->getPostNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $post_obj->setStart($firstRow);
        $post_obj->setLimit($this->page_num);
        $post_list = $post_obj->getPostList('',$where);
        $post_list = $post_obj->getListData($post_list);
        if(IS_POST && IS_AJAX){
            $this->ajaxReturn($post_list);
        }
        // dump($post_list);die;

        #dump($new_post_list);die;
        $y_m = array_column($post_list, 'year_month_day');
        $y_m = array_unique($y_m);
        // dump($y_m);die;
        $new_post_list = array();
        if(count($post_list) > 0){
            foreach ($y_m as $k => $v) {
                foreach ($post_list as $key => $value) {
                    if($value['year_month_day'] == $v){
                        $new_post_list[$v][] = $value;
                    }
                }
            }
        }

        //用户信息
        $user_obj = new UserModel($user_id);
        // $user_info = $user_obj->getUserInfo('user_id, nickname, headimgurl , sex , is_open');
        $user_info = M('Users')->field('user_id, nickname, headimgurl , sex, is_open, hx_account, hx_nickname')->where('user_id ='.$user_id)->find();
        $this->assign('user_info', $user_info);

        $follow_nmu = D('Follow')->getFollowNum('user_id ='.$suser_id .' and follow_type ='.FollowModel::USER .' and followed_id ='.$user_id);
        $this->assign('is_follow', $follow_nmu ? true : false);

        //该用户所佩戴的头衔．
        $user_title_obj = new UserTitleModel();
        $wear_title =  $user_title_obj->getUserTitleList('is_adorn = 1 AND user_id ='.$user_id,'adorn_time');
        $wear_title =  $user_title_obj->getListData($wear_title);
        $this->assign('wear_title',$wear_title);

        //该用户关注的用户,粉丝,频道
        $follow_obj = new FollowModel();
        $user_focus_num  = $follow_obj->getFollowedTypeNum($user_id,FollowModel::USER);
        $user_fans_num  = $follow_obj->getFansUserNum($user_id);
        $user_channel_num  = $follow_obj->getFollowedTypeNum($user_id,FollowModel::CHANNEL);

        $user_num = array(
            'focus_num'=> $user_focus_num,
            'fans_num'=> $user_fans_num,
            'channel_num'=> $user_channel_num,
        );
        $this->assign('user_num',$user_num);
        
        $my_info = M('Users')->field('headimgurl, nickname,hx_account')->where('user_id ='.$suser_id)->find();
        
        $this->assign('my_info', $my_info);


        //是否有私聊权限
        //双方必须都有一个普通初级头衔才可私聊
        //我的
        // $normal_titles = D('Title')->getTitleIdsByType(TitleModel::ORDINARY);
        // $user_titles = D('UserTitle')->getUserTitleFields('user_id ='.$suser_id, 'title_id');
        // $my_normal_title = array_intersect($normal_titles, $user_titles);
        // //对方的
        // $his_user_titles = D('UserTitle')->getUserTitleFields('user_id ='.$user_id, 'title_id');
        // $his_normal_title = array_intersect($normal_titles, $his_user_titles);
        // $check_chat = 3;
        // if(count($my_normal_title) > 0 && count($his_normal_title) > 0){
        //     $check_chat = 1;
        // }elseif(count($my_normal_title) > 0 && count($his_normal_title) == 0){
        //     $check_chat = 2;
        // }
        // elseif(count($my_normal_title) == 0 && count($his_normal_title) > 0){
        //     $check_chat = 3;
        // }
        $check_chat = D('User')->checkChat($suser_id, $user_id);

        $this->assign('check_chat', $check_chat);

        
        $this->assign('is_me', $suser_id == $user_id ? true : false);
        $this->assign('new_post_list',$new_post_list);
        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('head_title','他人的空间');
        $this->display();
    }


    //用户的关注
    public function user_concern(){
        $suser_id = intval(session('user_id'));
        $user_id = I('user_id',0,'int');
        $title = '他关注的人';
        $is_my = 0;
        if($suser_id == $user_id) {
            $title = '我关注的人';
            $user_id = $suser_id;
            $is_my = 1;
        }

        $follow_obj = new FollowModel();
        $total  = $follow_obj->getFollowedTypeNum($user_id,FollowModel::USER);
        $firstRow = intval($this->_request('firstRow'));

        $follow_obj->setStart($firstRow);
        $follow_obj->setLimit($this->page_num);

        $user_list = $follow_obj->getFollowedUserList($user_id);
        $user_list = $follow_obj->getListData($user_list);
        if(IS_POST && IS_AJAX){
            $this->ajaxReturn(array('user_id'=>$user_id,'list'=>$user_list));
        }
        #dump($user_list);die;
        $this->assign('is_my',$is_my);
        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('user_list',$user_list);
        $this->assign('head_title',$title);
        $this->display();
    }

    //用户的粉丝
    public function user_fans(){

        $suser_id = intval(session('user_id'));
        $user_id = I('user_id',0,'int');
        $title = '他的粉丝';
        $is_my = 0;
        if($suser_id == $user_id) {
            $title = '我的粉丝';
            $user_id = $suser_id;
            $is_my = 1;
        }

        $follow_obj = new FollowModel();
        $total  = $follow_obj->getFansUserNum($user_id);
        $firstRow = intval($this->_request('firstRow'));

        $follow_obj->setStart($firstRow);
        $follow_obj->setLimit($this->page_num);

        $user_list = $follow_obj->getFansUserList($user_id);
        $user_list = $follow_obj->getListData($user_list);
        #dump($user_list);die;
        if(IS_POST && IS_AJAX){
            $this->ajaxReturn(array('user_id'=>$user_id,'list'=>$user_list));
        }

        #dump($user_list);die;
        $this->assign('is_my',$is_my);
        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('user_list',$user_list);
        $this->assign('head_title',$title);
        $this->display();
    }


    //关注的频道．
    public function others_channel(){
        $user_id = I('user_id',0,'int');
        $follow_obj = new FollowModel();
        $total  = $follow_obj->getFollowedTypeNum($user_id,FollowModel::CHANNEL);
        $firstRow = intval($this->_request('firstRow'));

        $follow_obj->setStart($firstRow);
        $follow_obj->setLimit($this->page_num);

        $channel_list = $follow_obj->getFollowChannelList($user_id);
        $channel_list = $follow_obj->getListData($channel_list);
        #dump($channel_list);die;
        if(IS_POST && IS_AJAX){
            $this->ajaxReturn(array('user_id'=>$user_id,'list'=>$channel_list));
        }

        $this->assign('user_id',$user_id);
        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('channel_list',$channel_list);
        $this->assign('head_title','他关注的频道');
        $this->display();
    }


    //我的帖子．
    public function my_post_list(){

        $user_id = intval(session('user_id'));
        $post_type = I('post_type',0,'int');
        if(!$post_type){
            $post_type = PostModel::PRAISE;
        }

        $post_obj = new PostModel();
        $where = 'post_type = '.$post_type . ' and user_id ='.$user_id. ' and is_del = 0';
        $total = $post_obj->getPostNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $post_obj->setStart($firstRow);
        $post_obj->setLimit($this->page_num);
        $post_list = $post_obj->getPostList('',$where);
        #echo $post_obj->getLastSql();
        $post_list = $post_obj->getListData($post_list);
        #dump($post_list);die;
        if(IS_AJAX && IS_POST){
            $this->ajaxReturn(array('list'=>$post_list,'total'=>$total));
        }
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $this->assign('post_list', $post_list);

        $this->assign('head_title','我的帖子');
        $this->display();
    }

    //个人信息
    public function user_info()
    {
        $user_id = intval(session('user_id'));
        $user_obj = new UserModel($user_id);
        // $user_info = $user_obj->getUserInfo('user_id, nickname, headimgurl , sex , sdasd','user_id ='.$user_id);
        $user_info = M('Users')->field('user_id, nickname, headimgurl , sex, sdasd')->where('user_id ='.$user_id)->find();

        if(IS_POST && IS_AJAX){
            $sdasd = I('sdasd');
            $success = $user_obj->editUserInfo(array('sdasd' => $sdasd));
            if($success !== false) $this->ajaxReturn(array('code'=>1,'msg'=>'修改成功'));
            $this->ajaxReturn(array('code'=>1,'msg'=>'修改失败'));
        }

        $this->assign('user_info', $user_info);
        $this->assign('head_title', '我的资料');
        $this->display();
    }

    //我的收藏
    public function my_collection(){
        $user_id = intval(session('user_id'));
        $collect_obj = new CollectModel();
        $where = 'user_id ='.$user_id;
        $total = $collect_obj->getCollectNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $collect_obj->setStart($firstRow);
        $collect_obj->setLimit($this->page_num);
        $collect_list = $collect_obj->getCollectList('',$where,'addtime desc');
        $collect_list = $collect_obj->getListData($collect_list);
        // dump($collect_list);die;
        if(IS_POST && IS_AJAX){
            $this->ajaxReturn($collect_list);
        }
        $this->assign('collect_list',$collect_list);
        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('head_title', '我的收藏');
        $this->display();
    }

    //浏览历史
    public function my_scan_record(){

        $user_id = intval(session('user_id'));
        $where = 'user_id ='.$user_id;
        $post_browse_obj = new PostBrowseModel();
        $total = $post_browse_obj->getPostBrowseNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $post_browse_obj->setStart($firstRow);
        $post_browse_obj->setLimit($this->page_num);
        $browse_list = $post_browse_obj->getPostBrowseList('',$where,'addtime desc');
        $browse_list = $post_browse_obj->getListData($browse_list);

        if(IS_POST && IS_AJAX){
            $this->ajaxReturn($browse_list);
        }

        $this->assign('browse_list',$browse_list);
        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('head_title','浏览记录');
        $this->display();
    }

    //清空历史记录
    public function del_record()
    {
        if(IS_AJAX && IS_POST){
            $user_id = intval(session('user_id'));
            $post_browse_obj = new PostBrowseModel();
            $success = $post_browse_obj->delPostBrowse($user_id);
            if($success) $this->ajaxReturn(array('code' => 1));
            $this->ajaxReturn(array('code' => 0));
        }

    }

    //参与的心愿
    public function my_crowd_funding(){
        $user_id = intval(session('user_id'));
        //查询出我参与过的心愿id
        $cf_support_obj = new CfSupportModel();
        $crowdfunding_ids = $cf_support_obj->getSupportFields('user_id ='.$user_id,'crowdfunding_id','crowdfunding_id');
        $crowdfunding_id_str = implode(',',$crowdfunding_ids);
        //查询出对应的帖子id
        $crowdfunding_obj = new CrowdfundingModel();
        $post_ids = $crowdfunding_obj->getCrowdfundingIFields('crowdfunding_id IN ('.$crowdfunding_id_str.')','post_id');
        $post_id_str = implode(',',$post_ids);

        $post_obj = new PostModel();
        $where = 'post_id IN ('.$post_id_str.') and is_del = 0';
        $total = $post_obj->getPostNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $post_obj->setStart($firstRow);
        $post_obj->setLimit($this->page_num);
        $post_list = $post_obj->getPostList('',$where);
        #echo $post_obj->getLastSql();
        $post_list = $post_obj->getListData($post_list);
        #dump($post_list);die;
        if(IS_AJAX && IS_POST){
            $this->ajaxReturn($post_list);
        }
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $this->assign('post_list', $post_list);


        $this->assign('head_title','我参与的心愿');
        $this->display();
    }


    //我赞赏过的
    public function my_praise_post(){
        $user_id = intval(session('user_id'));
        //找出用户购买的隐藏内容id
        $praise_hide_buy_obj = new PraiseHideBuyModel();
        $praise_hide_ids = $praise_hide_buy_obj->getPraiseHideBuyFields('user_id ='.$user_id,'praise_hide_id');
        $praise_hide_id_str = implode(',',$praise_hide_ids);
        //找出用户购买的帖子id
        $praise_hide_obj = new PraiseHideModel();
        $post_ids = $praise_hide_obj->getPraiseHideFields('praise_hide_id IN ('.$praise_hide_id_str.') AND hide_type ='.PraiseHideModel::POST,'id');

        //找出用户购买的评论id
        $comment_ids = $praise_hide_obj->getPraiseHideFields('praise_hide_id IN ('.$praise_hide_id_str.') AND hide_type ='.PraiseHideModel::COMMENT,'id');
        $comment_id_str = implode(',',$comment_ids);
        //找出评论所对应的帖子
        $comment_obj = new PostCommentModel();
        $comment_post_ids = $comment_obj->getPostCommentFields('post_comment_id IN ('.$comment_id_str.')','post_id','post_id');

        //判断数组是否为空
        $new_post_ids = array();
        //如果都不为空的情况则合并去重
        if(count($comment_post_ids) && count($post_ids)){
            $new_post_ids = array_merge($post_ids,$comment_post_ids);
            $new_post_ids = array_unique($new_post_ids);
        }elseif(count($comment_post_ids) && !count($post_ids)){
            //如果未购买帖子,则取评论．
            $new_post_ids = $comment_post_ids;
        }elseif(count($post_ids) && !count($comment_post_ids)){
            //如果未购买评论,则取帖子．
            $new_post_ids = $post_ids;
        }

        $post_id_str = implode(',',$new_post_ids);
        $post_obj = new PostModel();
        $where = 'post_id IN ('.$post_id_str.') and is_del = 0';
        $total = $post_obj->getPostNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $post_obj->setStart($firstRow);
        $post_obj->setLimit($this->page_num);
        $post_list = $post_obj->getPostList('',$where);
        #echo $post_obj->getLastSql();
        $post_list = $post_obj->getListData($post_list);
        #dump($post_list);die;
        if(IS_AJAX && IS_POST){
            $this->ajaxReturn($post_list);
        }
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $this->assign('post_list', $post_list);

        $this->assign('head_title','我赞赏过的');
        $this->display();
    }

    //参与的悬赏
    public function my_reward(){
        $user_id = intval(session('user_id'));
        //找出我回答过的悬赏id
        $reward_answer_obj = new RewardAnswerModel();
        $reward_ids = $reward_answer_obj->getRewardAnswerFields('user_id ='.$user_id,'reward_id');

        //找出我也想看的悬赏id
        $look_reward_obj = new RewardLookModel();
        $look_reward_ids = $look_reward_obj->getRewardLookFields('user_id ='.$user_id,'reward_id');

        //判断数组是否为空
        $new_reward_ids = array();
        //如果都不为空的情况则合并去重
        if(count($reward_ids) && count($look_reward_ids)){
            $new_reward_ids = array_merge($reward_ids,$look_reward_ids);
            $new_reward_ids = array_unique($new_reward_ids);
        }elseif(count($reward_ids) && !count($look_reward_ids)){
            //如果没有我也想看，则取回答的帖子
            $new_reward_ids = $reward_ids;
        }elseif(count($look_reward_ids) && !count($reward_ids)){
            //如果没有回答的帖子,则取我也想看．
            $new_reward_ids = $look_reward_ids;
        }
        $reward_id_str = implode(',',$new_reward_ids);
        //根据悬赏id 去找悬赏贴id
        $reward_obj = new RewardModel();
        $post_ids = $reward_obj->getRewardFields('reward_id IN ('.$reward_id_str.')','post_id');
        $post_id_str = implode(',',$post_ids);
        $post_obj = new PostModel();
        $where = 'post_id IN ('.$post_id_str.') and is_del = 0';
        $total = $post_obj->getPostNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $post_obj->setStart($firstRow);
        $post_obj->setLimit($this->page_num);
        $post_list = $post_obj->getPostList('',$where);
        #echo $post_obj->getLastSql();
        $post_list = $post_obj->getListData($post_list);
        #dump($post_list);die;
        if(IS_AJAX && IS_POST){
            $this->ajaxReturn($post_list);
        }
        #dump($post_list);die;
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $this->assign('post_list', $post_list);

        $this->assign('head_title','我参与的悬赏');
        $this->display();
    }

    //我的钱包
    public function my_wallet(){
        $user_id = intval(session('user_id'));
        $user_obj = new UserModel($user_id);
        $left_money = $user_obj->getLeftMoney();

        $user_post_cash = D('UserTitle')->getUserPostCash($user_id);

//        var_dump($user_post_cash,$user_id);
//        die;
        $this->assign('user_post_cash',$user_post_cash);

        $this->assign('left_money',$left_money);
        $this->assign('head_title','我的钱包');
        $this->display();
    }

    //我的消费记录
    public function my_consume_record(){

        $user_id = intval(session('user_id'));
        $consume_type = I('consume_type',0,'int');
        if(!$consume_type){
            $consume_type = 1;
        }

        $where = 'user_id ='.$user_id;
        if($consume_type == 1 || $consume_type == 2 || $consume_type == 3){
            // 1.支出记录  2.收入记录 3.充值记录 4.提现记录
            if($consume_type == 1){
                $where .= ' AND (change_type ='.AccountModel::PRAISE_BUY.' or change_type ='.AccountModel::CF_SUPPORT.' or change_type ='.AccountModel::REWARD_LOOK.' or change_type ='.AccountModel::POST_REWARD.')';
            }
            if($consume_type == 2){
                $where .= ' AND (change_type ='.AccountModel::REWARD_LOOK_LZ.' or change_type ='.AccountModel::REWARD_LOOK_ANSWER.' or change_type ='.AccountModel::REWARD_SUCCESS.' or change_type ='.AccountModel::CF_REFUND.' or change_type ='.AccountModel::PRAISE_INCOME.' or change_type ='.AccountModel::REWARD_REFUND.' or change_type = '.AccountModel::REWARD_REJECT.' or change_type = '.AccountModel::CF_SUCCESS.')';
            }
            if($consume_type == 3){
                $where .=' AND change_type ='.AccountModel::ONLINE_VOUCHER;
            }

            $account_obj = new AccountModel();
            $total = $account_obj->getAccountNum($where);
            $firstRow = intval($this->_request('firstRow'));
            $account_obj->setStart($firstRow);
            $account_obj->setLimit($this->page_num);
            $account_list = $account_obj->getAccountList('',$where);
            #echo $account_obj->getLastSql();die;
            $account_list = $account_obj->getListData($account_list);
            #dump($account_list);die;
            if(IS_AJAX && IS_POST){
                $this->ajaxReturn(array('list'=>$account_list,'total'=>$total,'consume_type'=>$consume_type));
            }
            $this->assign('total', $total);
            $this->assign('firstRow', $this->page_num);
            $this->assign('account_list', $account_list);
        }

        if($consume_type == 4){
            $deposit_obj  = new DepositApplyModel();
            $total = $deposit_obj->getDepositApplyNum($where);
            $firstRow = intval($this->_request('firstRow'));
            $deposit_obj->setStart($firstRow);
            $deposit_obj->setLimit($this->page_num);
            $deposit_list = $deposit_obj->getDepositApplyList('',$where);
            #echo $account_obj->getLastSql();die;
            $deposit_list = $deposit_obj->getListData($deposit_list);

            if(IS_AJAX && IS_POST){
                //dump($deposit_list);die;
                $this->ajaxReturn(array('list'=>$deposit_list,'total'=>$total,'consume_type'=>$consume_type));
            }
            $this->assign('total', $total);
            $this->assign('firstRow', $this->page_num);
            $this->assign('deposit_list', $deposit_list);
        }

        $this->assign('consume_type',$consume_type);
        $this->assign('head_title','消费记录');
        $this->display();
    }

    //余额充值
    public function recharge_pay(){

        $this->assign('head_title','余额充值');
        $this->display();
    }



    //小额免密支付．
    public function  close_to_pay(){

        $user_id = intval(session('user_id'));
        $user_obj = new UserModel($user_id);
        $npw_pay =$user_obj->getUserField('npw_pay','user_id ='.$user_id);
        $min_money = $GLOBALS['config_info']['NPW_PAY_MONEY'];
        if(IS_POST && IS_AJAX) {
            $is_npw_pay = I('is_npw_pay',0,'int');
            $success = $user_obj->editUserInfo(array('npw_pay'=>$is_npw_pay));
            if(!$success) $this->ajaxReturn(array('code'=>0,'msg'=>'操作失败!'));
            $this->ajaxReturn(array('code'=>1,'msg'=>'操作成功!'));
        }

        $this->assign('min_money',$min_money);
        $this->assign('npw_pay',$npw_pay);
        $this->display();
    }


    //提现到支付宝
    public function withdraw()
    {
        $user_id = intval(session('user_id'));
        $user_obj = new UserModel($user_id);

        // $user_info = $user_obj->getUserInfo('left_money,user_alipay_id','user_id ='.$user_id);
        $user_info = M('Users')->field('left_money,user_alipay_id')->where('user_id ='.$user_id)->find();
        $alipay_obj = new UserAlipayModel();
        $user_info['alipay_account'] = $alipay_obj->getUserAlipayField($user_info['user_alipay_id'],'alipay_account');
        #dump($user_info);die;
        if(IS_POST && IS_AJAX){
            $user_alipay_id = I('user_alipay_id',0,'int');
            $money = I('money',0,'floatval');
            //验证
            if(!$user_alipay_id || !$money) $this->ajaxReturn(array('code'=> 0, 'msg' => '提现申请失败!'));
            if($money && (!is_numeric($money) || $money < 0)) $this->ajaxReturn(array('code'=> 0, 'msg' => '请输入正确金额!'));
            if($money > $user_info['left_money']) $this->ajaxReturn(array('code'=> 0, 'msg' => '余额不足!'));
            $ali_account = D('UserAlipay')->getUserAlipayField($user_alipay_id,'alipay_account');
            $data = array(
                'user_id' => $user_id,
                'deposit_type' => 1,
                'alipay_id' => $user_alipay_id,
                'money' => $money,
                'apply_account' => $ali_account
            );

            $deposit_apply_obj  =new DepositApplyModel();
            $success = $deposit_apply_obj->addDepositApply($data);
            if(!$success) $this->ajaxReturn(array('code'=> 0, 'msg' => '提现申请失败!'));
            $this->ajaxReturn(array('code'=> 1, 'msg' => '提现申请成功!'));
        }

        $this->assign('user_info',$user_info);
        $this->assign('head_title','提现');
        $this->display();
    }

    //设置支付宝
    public function set_alipay(){
        $user_id = intval(session('user_id'));
        $user_obj = new UserModel($user_id);
        $user_alipay_id = $user_obj->getUserField('user_alipay_id','user_id ='.$user_id);
        $alipay_obj = new UserAlipayModel();
        $alipay_list = $alipay_obj->getUserAlipayAllList('user_id='.$user_id.' and  is_del = 0');

        if(IS_POST && IS_AJAX){
            $alipay_id = I('alipay_id',0,'int');
            if(!$alipay_id) $this->ajaxReturn(array('code' => 0,'msg' => '没有该支付宝帐号'));
            if($user_alipay_id == $alipay_id) $this->ajaxReturn(array('code' => 0,'msg' => '不要重复设置相同支付宝'));

            $success = $user_obj->editUserInfo(array('user_alipay_id' => $alipay_id));
            if($success) $this->ajaxReturn(array('code' => 1,'msg' => '设置成功'));

            $this->ajaxReturn(array('code' => 0,'msg' => '设置失败'));

        }

        $this->assign('user_alipay_id',$user_alipay_id);
        $this->assign('alipay_list',$alipay_list);
        $this->assign('head_title','提现');
        $this->display();
    }

    //添加支付宝
    public function add_user_alipay(){
        if (IS_POST && IS_AJAX){
            $user_id = intval(session('user_id'));
            $alipay_account = I('alipay_account');

            $alipay_obj = new UserAlipayModel();
            $alipay_accounts = $alipay_obj->getUserAlipayFields($user_id,'alipay_account');

            if(!$alipay_account) $this->ajaxReturn(array('code'=>0,'msg'=>'请输入支付宝帐号' ));
            if(!check_mobile($alipay_account) && !check_email($alipay_account)){
                $this->ajaxReturn(array('code'=>0,'msg'=>'请输入正确支付宝帐号' ));
            }

            if($alipay_accounts){
                if (in_array($alipay_account,$alipay_accounts)){
                    $this->ajaxReturn(array('code'=>0,'msg'=>'已绑定过该支付宝帐号' ));
                }
            }
            $data =array(
                'user_id' => $user_id,
                'alipay_account' => $alipay_account,
                'addtime' => time(),
            );

            if($id = $alipay_obj->addUserAlipay($data)){
                $this->ajaxReturn(array('code'=>1,'msg'=>'添加支付宝帐号成功','id' => $id));
            }
            $this->ajaxReturn(array('code'=>0,'msg'=>'添加支付宝帐号失败' ));
        }
    }

    //删除支付宝
    public function del_alipay(){
        if(IS_POST && IS_AJAX){
            $user_id = intval(session('user_id'));
            $alipay_id = I('alipay_id',0,'int');
            if(!$alipay_id) $this->ajaxReturn(array('code' => 0,'msg' => '没有该支付宝帐号'));

            $alipay_obj = new UserAlipayModel($alipay_id);
            $success = $alipay_obj -> setUserAlipay($alipay_id,array('is_del' => 1));
            if($success){
                $user_obj = new UserModel($user_id);
                $user_alipay_id = $user_obj->getUserField('user_alipay_id','user_id ='.$user_id);
                if($user_alipay_id == $alipay_id){
                    $user_obj ->editUserInfo(array('user_alipay_id' => 0));
                }
                $this->ajaxReturn(array('code'=>1,'msg'=>'删除成功' ));
            }
            $this->ajaxReturn(array('code'=>1,'msg'=>'删除失败' ));
        }
    }



//
        //关于我们
    public function aboutus()
    {

        $microblog = $GLOBALS['config_info']['MICROBLOG'];
        $about_code = $GLOBALS['config_info']['ABOUT_CODE'];
        $wx_name = $GLOBALS['config_info']['WX_NAME'];
        $company_name = $GLOBALS['config_info']['COMPANY_NAME'];
        $official_record = $GLOBALS['config_info']['OFFICIAL_RECORD'];

        $this->assign('company_name', $company_name);
        $this->assign('official_record', $official_record);
        $this->assign('wx_name', $wx_name);
        $this->assign('microblog', $microblog);
        $this->assign('about_code', $about_code);
        $this->assign('head_title', '关于我们');
        $this->display();
    }


    //意见反馈．
    public function feedback(){
        if(IS_POST && IS_AJAX){
            $user_id = intval(session('user_id'));
            $content = $_POST['content'];
            if(!$user_id) $this->ajaxReturn(array('code'=>0,'msg'=>'请先登录!'));
            if(!$content) $this->ajaxReturn(array('code'=>0,'msg'=>'请填写内容!'));
            if(mb_strlen($content,'utf-8') < 20) $this->ajaxReturn(array('code'=>0,'msg'=>'内容不能少于20字!'));
            if(mb_strlen($content,'utf-8') > 50) $this->ajaxReturn(array('code'=>0,'msg'=>'内容不能多于50字!'));

            $data = array(
                'user_id'=>  $user_id,
                'message'=>  $content,
                'addtime'=>  time(),
            );
            $user_suggest_obj = new UserSuggestModel();
            $success = $user_suggest_obj -> addAdvice($data);
            if($success) $this->ajaxReturn(array('code'=>1,'msg'=>'提交成功!'));
            $this->ajaxReturn(array('code'=>1,'msg'=>'提交失败!'));

        }
        $this->display();
    }

    //分享
    public function share(){

        $this->display();
    }


//
//
//    //修改头像
//    public function upload_headimg()
//    {
//        $user_id = intval(session('user_id'));
//        $r = uploadImage($_FILES['upfile'], '/user_file/' . $user_id);
//        if (!is_array($r)) {
//            $user_obj = new UserModel($user_id);
//            $user_obj->setUserInfo(array('headimgurl' => $r));
//            if ($user_obj->saveUserInfo()) {
//                $this->ajaxReturn(['status' => 1, 'img_url' => $r]);
//            }
//        }
//        $this->ajaxReturn($r);
//    }
//
//    //我的消息
//    public function message()
//    {
//        $user_id = intval(session('user_id'));
//
//        $message_obj = new MessageModel();
//        $where = 'reply_user_id ='.$user_id;
//
//        $total = $message_obj->getMessageNum($where);
//        $firstRow = intval($this->_request('firstRow'));
//
//        $message_obj->setStart($firstRow);
//        $message_obj->setLimit($this->page_num);
//
//        $message_list = $message_obj ->getMessageList('',$where,'addtime desc');
//        $message_list = $message_obj->getListData($message_list);
////        dump($message_list);die;
//        if(IS_POST && IS_AJAX){
//            $this->ajaxReturn($message_listothers_space//        }
//        $this->assign('total', $total);
//        $this->assign('firstRow', $this->page_num);
//        $this->assign('message_list', $message_list);
//        $this->assign('head_title', '我的消息');
//        $this->display();
//    }
//
//
//    //修改手机号
//    function change_mobile()
//    {
//        $user_id = session('user_id');
//        $user_obj = new UserModel($user_id);
//        $user_info = $user_obj->getUserInfo('mobile', 'user_id = ' . $user_id);
//        $data = $this->_post();
//        if ($data['opt'] == 'submit') {
//            $mobile = $data['phone'];
//            $verify_code = $data['code'];
//            if (!$mobile || !$verify_code) {//验证手机和验证码
//                $return_data = array(
//                    'code' => -1,
//                    'msg' => '请输入完整验证信息'
//                );
//                echo json_encode($return_data);
//                exit;
//            }
//            if(!preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $mobile)){//验证手机号
//                $return_data = array(
//                    'code' => -1,
//                    'msg' => '请输入正确的手机号'
//                );
//                echo json_encode($return_data);
//                exit;
//            }
//
//            $verify_code_obj = new VerifyCodeModel();
//            $result = $verify_code_obj->checkVerifyCodeValid($verify_code, $mobile);//检查验证码的有效性
//            if ($result) {
//                $arr = array(
//                    'mobile' => $mobile,
//                );
//                $result = $user_obj->editUserInfo($arr);//修改用户表
//                if ($result) {
//                    $return_data = array(
//                        'code' => 1,
//                        'msg' => '修改成功'
//                    );
//                } else {
//                    $return_data = array(
//                        'code' => -1,
//                        'msg' => '修改成功'
//                    );
//                }
//            } else {//无效的验证码
//                $return_data = array(
//                    'code' => -1,
//                    'msg' => '无效的验证码'
//                );
//            }
//            echo json_encode($return_data);
//            exit;
//        }
//        $this->assign('head_title', '修改手机号');
//        $this->assign('mobile', $user_info['mobile']);
//        $this->display();
//    }

    //系统设置
    public function system_set(){

        $user_id = intval(session('user_id'));
        $user_obj = new UserModel($user_id);
        $is_open = $user_obj->getUserField('is_open', 'user_id ='.$user_id);

        $this->assign('is_open',$is_open);
        $this->assign('head_title','系统设置');
        $this->display();
    }

    //动态是否公开
    public function privacy_set(){

        $user_id = intval(session('user_id'));
        $user_obj = new UserModel($user_id);
        $is_open = $user_obj->getUserField('is_open', 'user_id ='.$user_id);

        if(IS_AJAX && IS_POST){
            $is_open = I('is_open',0,'int');
            $success = $user_obj->editUserInfo(array('is_open' => $is_open));
            if($success !== false) $this->ajaxReturn(array('code' => 1 ,'msg' => '设置成功'));
            $this->ajaxReturn(array('code' => 0 ,'msg' => '设置失败'));
        }
        $this->assign('is_open',$is_open);
        $this->assign('head_title','系统设置');
        $this->display();
    }

    //帐号安全
    public function account_safe(){
        $this->assign('head_title','帐号安全');
        $this->display();
    }

    //设置支付密码|修改支付密码页面
    public function modify_pay_psw(){
        $user_id = intval(session('user_id'));
        $user_obj = new UserModel();
        $pay_password = $user_obj->getUserField('pay_password','user_id ='.$user_id);
        $is_pw = 0;
        $is_mobile_pw = I('is_mobile_pw',0,'int');
        if($pay_password){
            $is_pw = 1;
        }

        $this->assign('is_pw',$is_pw);
        $this->assign('is_mobile_pw',$is_mobile_pw);
        $this->assign('head_title','帐号安全');
        $this->display();
    }

    //输入原密码判断
    public function pay_password(){
        if(IS_AJAX && IS_POST) {
            $user_id = intval(session('user_id'));
            $pwd_str = I('pwd');
            $pwd_md5 = MD5($pwd_str);
            $user_obj = new UserModel();
            $pay_password = $user_obj->getUserField('pay_password', 'user_id =' . $user_id);

            if ($pwd_md5 == $pay_password) $this->ajaxReturn(array('code' => 1, 'msg' => '密码输入正确!', 'status' => 1));

            $this->ajaxReturn(array('code' => 0, 'msg' => '原密码输入错误!'));
        }
    }

    //输入新密码
    public function pay_password_new(){
        if(IS_AJAX && IS_POST) {
            $new_pwd_str = I('pwd');
            if (strlen($new_pwd_str) == 6) {
                $new_pwd_md5 = MD5($new_pwd_str);
                session('new_pwd_md5', $new_pwd_md5);
                $this->ajaxReturn(array('code' => 1, 'status' => 2));
            }
            $this->ajaxReturn(array('code' => 0, 'msg' => '新密码必须6位数!'));
        }
    }

    //支付密码 确定密码
    public function pay_password_ensure(){
        if(IS_AJAX && IS_POST) {
            $user_id = intval(session('user_id'));
            $again_pwd_str = I('pwd');
            $again_pwd_md5 = MD5($again_pwd_str);
            $new_pwd_md5 = session('new_pwd_md5');
            if ($again_pwd_md5 == $new_pwd_md5) {
                $user_obj = new UserModel($user_id);

                $data = array(
                    'pay_password' => $again_pwd_md5
                );

                $user_obj->setUserInfo($data);
                $r = $user_obj->saveUserInfo();
                if ($r !== false) {
                    session('new_pwd_md5', null);
                    $this->ajaxReturn(array('code' => 1, 'msg' => '支付密码修改成功!', 'status' => 3, 'url' => '/FrontUserCenter/personal'));
                }
                $this->ajaxReturn(array('code' => 0, 'msg' => '支付密码修改失败!'));
            } else {
                $this->ajaxReturn(array('code' => 0, 'msg' => '两次密码不一致!'));
            }
        }
    }

    //忘记支付密码
    public function forget_pay_psw(){
        $user_id = session('user_id');
        if(!$user_id) return;
        $user_obj = new UserModel($user_id);
        // $user_info = $user_obj->getUserInfo('mobile', 'user_id = ' . $user_id);
        $user_info = M('Users')->field('mobile')->where('user_id ='.$user_id)->find();
        $mobile = $user_info['mobile'];
        //进来页面就发送验证码．
        $verify_code_obj = new VerifyCodeModel();
        $verify_code = $verify_code_obj->generateVerifyCode($user_info['mobile']);
        if ($verify_code) {
            $sms_obj = new SMSModel();
            $sms_obj->sendVerifyCode($user_info['mobile'], $verify_code);
        }
        if(IS_POST && IS_AJAX){
            $v_code = I('v_code',0,'int');
            $verify_code_obj = new VerifyCodeModel();
            $result = $verify_code_obj->checkVerifyCodeValid($v_code, $mobile);//检查验证码的有效性
            if ($result) $this->ajaxReturn(array('code'=>1,'msg'=>'验证成功!'));
            $this->ajaxReturn(array('code'=>0,'msg'=>'无效验证码!'));
        }
        $this->assign('mobile',substr_replace($mobile,'****',3,4));
        $this->display();
    }

    //重新获取验证码．
    public function verify_code(){
        if(IS_POST && IS_AJAX){
            $user_id = session('user_id');
            $user_obj = new UserModel($user_id);
            // $user_info = $user_obj->getUserInfo('mobile', 'user_id = ' . $user_id);
            $user_info = M('Users')->field('mobile')->where('user_id ='.$user_id)->find();
            $mobile = $user_info['mobile'];
            //生成验证码．
            $verify_code_obj = new VerifyCodeModel();
            $verify_code = $verify_code_obj->generateVerifyCode($mobile);
            if ($verify_code) {
                //发送验证码
                $sms_obj = new SMSModel();
                $result = $sms_obj->sendVerifyCode($mobile, $verify_code);
                if($result) $this->ajaxReturn(array('code'=>1,'msg'=>'验证码发送成功!'));
                $this->ajaxReturn(array('code'=>0,'msg'=>'验证码发送失败!'));
            }
            $this->ajaxReturn(array('code'=>0,'msg'=>'系统繁忙!请稍候!'));

        }
    }

    //退出登录
    function logout()
    {
        session('user_info', null);
        session('user_id', null);
        redirect('/');
    }



    //充值
    public function payment(){
        if(IS_AJAX && IS_POST){
            $recharge_num = I('recharge_num', 0.0, 'floatval');
            $payway = I('payway');
            if(!$recharge_num || !$payway) $this->ajaxReturn(array('code'=>1));

            if($payway == 'wx_pay'){
                #$this->ajaxReturn(array('code'=>1));
                $wxpay_obj = new WXPayModel();
                $result = $wxpay_obj->mobile_pay_code(0, $recharge_num);
                $this->ajaxReturn(array('code'=>0,'parameter'=> $result));
            }elseif($payway == 'alipay'){
                $alipay_obj = new AlipayModel();
                $result = $alipay_obj->mobile_pay_code(0, $recharge_num);
                $this->ajaxReturn(array('code'=> 0,'parameter'=> $result));
            }
            $this->ajaxReturn(array('code'=> 1));
        }
    }
    //PY协议
    public function py(){
        $this->assign('head_title','PY币用户协议');
        $this->display();
    }
    //帮助中心
    public function help(){
        $this->assign('head_title','帮助中心');
        $this->display();
    }

}
