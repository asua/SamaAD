<?php
class FrontInformationAction extends FrontAction
{
    function _initialize()
    {
        parent::_initialize();
        $this->page_num =4;
    }

    function information_list()
    {
        $information_obj=new InformationArticleModel();
        $article_sort_id=$this->_get('article_sort_id');
        $where='is_project = 0';
        if($article_sort_id){
            $where=' AND article_sort_id = '.$article_sort_id;
        }
        $total = $information_obj->getInformationNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $information_obj->setStart($firstRow);
        $information_obj->setLimit($this->page_num);

        $information_type_obj=new ArticleSortModel();
        $information_type_list = $information_type_obj->getArticleSortList('','','serial');

        $information_list=$information_obj->getInformationList('',$where,'serial');

        $this->assign('total', $total);
        $this->assign('head_title', '资讯');
        $this->assign('firstRow', $this->page_num);
        $this->assign('information_list',$information_list);
        $this->assign('information_type_list',$information_type_list);
        $this->display();
    }
    //根据标签筛选资讯
    public function  filter_information(){
        $article_sort_id=$this->_post('article_sort_id');
       /* if($article_sort_id){*/
            $where = 'article_sort_id = '.$article_sort_id .' AND is_project=0';
            if($article_sort_id==0){//0代表全部
                $where='is_project = 0';
            }
            $information_obj=new InformationArticleModel();
            $total=$information_obj->getInformationNum($where);
            $information_obj->setLimit($this->page_num);
            $information_list=$information_obj->getInformationList('',$where,'serial');
            foreach($information_list as $k=>$v){
                $information_list[$k]['time']=date('Y-m-d',$v['addtime']);
            }
            $return_data=array(
               'code'=>1,
               'total'=>$total,
               'data'=>$information_list,
            );
        echo json_encode($return_data);
        exit;
    }
    //异步获取资讯(下拉)
    public function get_information(){

        $firstRow = $this->_request('firstRow');
        $information_obj=new InformationArticleModel();
        $information_obj->setStart($firstRow);
        $information_obj->setLimit($this->page_num);

        $article_sort_id=$this->_request('article_sort_id');//文章的分类id
        $where='is_project = 0';
        if($article_sort_id){
            $where.=' AND article_sort_id = '.$article_sort_id;
        }
        $information_list=$information_obj->getInformationList('',$where,'serial');

        foreach($information_list as $k=>$v){
            $information_list[$k]['time']=date('Y-m-d',$v['addtime']);
        }
        $this->json_exit($information_list);
    }

    function information_detail()
    {
        $information_id=$this->_get('information_id');
        $information_obj=new InformationArticleModel();
        $information_info=$information_obj->getInformationInfo('article_id = '.$information_id);

        $information_txt_obj=new ArticleTxtModel();
        $information_txt=$information_txt_obj->getArticleTxtInfo('article_id = '.$information_id);
        $information_txt['desc'] = strip_tags($information_txt['contents']);
        $information_txt['desc'] = str_replace(array("\r\n", "\r", "\n", "&nbsp;", "\t", " "), "", $information_txt['desc']);  
        
        $this->assign('information_txt',$information_txt);
        $this->assign('information_info',$information_info);
        $this->display();
    }
}