<?php
class AcpAppPushAction extends AcpAction{

	//APP通知推送列表
	public function push_list(){
		$push_obj = new AppPushLogModel();

		//分页处理
        import('ORG.Util.Pagelist');
        $count = $push_obj->getAppPushLogNum();
        $Page = new Pagelist($count,C('PER_PAGE_NUM'));
        $push_obj->setStart($Page->firstRow);
        $push_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('show', $show);

        $push_list = $push_obj->getAppPushLogList('title, addtime');
        $this->assign('push_list', $push_list);
		$this->assign('head_title', '通知列表');
		$this->display();
	}

	//app推送
	public function app_push(){
		if(IS_AJAX && IS_POST){
			$content = $this->_post('content');

			if(!$content){
				$this->ajaxReturn(array('msg'=>'请填写推送内容'));
			}
			$content = htmlspecialchars_decode($content);
			$title = filterAndSubstr($content);
			$message_obj = new MessageModel();
			$r = $message_obj->addMessage(0, 0, 0, 0, $title, $content);	

			if($r){
				$this->ajaxReturn(array('message'=>'推送成功'));
			}else{
				$this->ajaxReturn(array('message'=>'推送失败'));
			}
			
			
		}

		$this->assign('head_title', 'APP通知推送');
		$this->display();
	}
}

