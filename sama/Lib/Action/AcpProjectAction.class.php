<?php
/**
 * acp后台商品分类
 */
class AcpProjectAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
    }


    private function get_search_condition(){
        $where = '';

        $nickname = I('nickname');
        if($nickname){
            $user_obj = new UserModel();
            $user_ids = $user_obj->getUserField('user_id', 'nickname like "%'.$nickname.'%"');
            if(count($user_ids)){
                $user_ids = implode(',', $user_ids);
                $where .= ' and user_id in('.$user_ids.')';
            }
            $this->assign('nickname', $nickname);
        }

        $title = I('title');
        if($title){
            $where .= ' and title like "%'.$title.'%"';
            $this->assign('title', $title);
        }

        $start_time = I('start_time');
        if($start_time){
            $start_time = strtotime($start_time);
            $where .= ' and addtime >='.$start_time;
            $this->assign('start_time', $start_time);
        }
        $end_time = I('end_time');
        if($end_time){
            $end_time = strtotime($end_time);
            $where .= ' and addtime <='.$end_time;
            $this->assign('end_time', $end_time);
        }

        $phase = I('phase');
        if($phase){
            /*$project_obj = D('Project');
            $project_ids = $project_obj->getProjectField('project_id', 'find_in_set('.$phase.',)');*/
            $where .= ' and investment_phase ='.$phase;
            $this->assign('phase', $phase);
        }

        $field = I('field');
        if($field){
            $project_obj = D('Project');
            $project_ids = $project_obj->getProjectField('project_id', 'find_in_set('.$field.',project_field)');
            $project_ids[] = 0;
            $where .= ' and project_id in('.implode(',', $project_ids).')';
            $this->assign('field', $field);
        }
        return $where;
    }


    //阶段列表
    public function phase_list(){
        $type_obj = D('Type');
        $where = 'type_type ='.TypeModel::PHASE;
        $show = getPageList($type_obj, 'getTypeNum', $where);
        $this->assign('show', $show);
        $phase_list = $type_obj->getTypeList($where);
        $this->assign('phase_list', $phase_list);
        $this->assign('head_title', '阶段列表');
        $this->display();
    }

    //添加阶段
    public function add_phase(){

        $act = I('act');
        if($act == 'add'){
            $phase_name = I('phase_name');
            $serial = I('serial', 0, 'intval');
            if(!$phase_name){
                $this->error('请填写阶段名称');
            }
            $data = array(
                'type_name' => $phase_name,
                'type_type' => TypeModel::PHASE,
                'serial' => $serial,
                );
            $type_obj = new TypeModel();
            if($type_obj->addType($data)){
                $this->success('添加成功');
            }else{
                $this->error('添加失败');
            }
        }

        $this->assign('act', 'add');
        $this->assign('head_title', '添加阶段');
        $this->display();
    }

    //修改阶段
    public function edit_phase(){
        $phase_id = I('phase_id', 0, 'intval');
        if(!$phase_id){
            $this->error('阶段不存在');
        }
        $type_obj = D('Type');
        $phase = $type_obj->getTypeInfo('type_id ='.$phase_id.' and type_type ='.TypeModel::PHASE);
        if(!$phase){
            $this->error('阶段不存在');
        }

        $act = I('act');
        if($act == 'edit'){
            $phase_name = I('phase_name');
            $serial = I('serial', 0, 'intval');
            if(!$phase_name){
                $this->error('请填写阶段名称');
            }
            $data = array(
                'type_name' => $phase_name,
                'type_type' => TypeModel::PHASE,
                'serial' => $serial,
                );
            $type_obj = new TypeModel();
            if($type_obj->setType($phase_id, $data)){
                $this->success('修改成功');
            }else{
                $this->error('修改失败');
            }
        }
        $this->assign('phase', $phase);
        $this->assign('type_id', $phase_id);
        $this->assign('act', 'edit');
        $this->assign('head_title', '修改阶段');
        $this->display('add_phase');
    }


    //删除阶段
    public function del_phase(){
        $type_ids = I('ids');
        $type_obj = new TypeModel();
        if($type_obj->delType('type_id in('.$type_ids.')')){
            $this->ajaxReturn('success');
        }else{
            $this->ajaxReturn('failure');
        }
    }


    //项目列表
    public function get_project_list(){
        $project_obj = D('Project');
        $where = 'is_del = 0';
        $where .= $this->get_search_condition();
        $show = getPageList($project_obj, 'getProjectNum', $where);
        $this->assign('show', $show);
        $project_list = $project_obj->getProjectList('project_name, project_id, user_id, financing_money, project_field, investment_phase, addtime',$where);
        $project_list = $project_obj->getListData($project_list);

        //阶段
        $type_obj = D('Type');
        $phase_list = $type_obj->getTypeList('type_type ='.TypeModel::PHASE);
        $this->assign('phase_list', $phase_list);
        $field_list = $type_obj->getTypeList('type_type ='.TypeModel::DOMAIN);
        $this->assign('field_list', $field_list);

        $this->assign('project_list', $project_list);

        $this->assign('head_title', '项目列表');
        $this->display();
    }

    //上传商业计划书
    public function add_plan_book(){
        $id = I('id',0,'int');
        $url = I('url');
        $project_obj = new ProjectModel($id);
        if($url){
            $project_obj->editProject(['plan_book'=>$url]);
            $this->ajaxReturn(['code'=>1,'msg'=>'上传成功']);
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'上传失败']);
    }
    //上传自荐材料
    public function add_cover(){
        $id = I('id',0,'int');
        $url = I('url');
        $project_obj = new ProjectModel($id);
        if($url){
            $project_obj->editProject(['cover'=>$url]);
            $this->ajaxReturn(['code'=>1,'msg'=>'上传成功']);
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'上传失败']);
    }


    //项目详情
    public function project_details(){
        $project_id = I('project_id', 0, 'intval');
        if(!$project_id){
            $this->error('项目不存在');
        }
        $project_obj = D('Project');
        $project_list = $project_obj->getProjectList('','is_del = 0 and project_id ='.$project_id);
        $project_list = $project_obj->getListData($project_list);

        $this->assign('project', $project_list[0]);
        $this->assign('head_title', '项目详情');
        $this->display();
    }

    //删除项目
    public function del_project(){
        $project_ids = I('ids');
        $project_obj = new ProjectModel();
        if($project_obj->delProject('project_id in('.$project_ids.')')){
            $this->ajaxReturn('success');
        }else{
            $this->ajaxReturn('failure');
        }
    }


    //项目评论
    public function get_project_comment_list(){
        $project_id = I('project_id', 0, 'intval');
        if(!$project_id){
            $this->error('项目不存在');
        }
        $where = 'comment_type ='.CommentModel::PROJECT .' and type_id ='.$project_id;
        $where .= $this->get_search_condition();
        $comment_obj = D('Comment');
        $show = getPageList($comment_obj, 'getCommentNum', $where);
        $this->assign('show', $show);

        $comment_list = $comment_obj->getCommentList('', $where, 'addtime desc');
        $comment_list =  $comment_obj->getListData($comment_list);
        $this->assign('comment_list', $comment_list);
        $this->assign('project_id', $project_id);
        $this->assign('head_title','项目评论');
        $this->display();
    }


    //币种列表
    public function currency_list(){
        $type_obj = D('Type');
        $where = 'type_type ='.TypeModel::CURRENCY;
        $show = getPageList($type_obj, 'getTypeNum', $where);
        $this->assign('show', $show);
        $currency_list = $type_obj->getTypeList($where);

        $this->assign('currency_list', $currency_list);
        $this->assign('head_title', '币种列表');
        $this->display();
    }

    //添加币种
    public function add_currency(){
        $act = I('act');
        if($act == 'add'){
            $currency_name = I('currency_name');
            $serial = I('serial', 0, 'intval');
            if(!$currency_name){
                $this->error('请填写阶段名称');
            }
            $data = array(
                'type_name' => $currency_name,
                'type_type' => TypeModel::CURRENCY,
                'serial' => $serial,
                );
            $type_obj = new TypeModel();
            if($type_obj->addType($data)){
                $this->success('添加成功');
            }else{
                $this->error('添加失败');
            }
        }
        $this->assign('act', 'add');
        $this->assign('head_title', '添加币种');
        $this->display();
    }

    //修改币种
    public function edit_currency(){
        $currency_id = I('currency_id', 0, 'intval');
        if(!$currency_id){
            $this->error('币种不存在');
        }
        $type_obj = D('Type');
        $currency = $type_obj->getTypeInfo('type_id ='.$currency_id.' and type_type ='.TypeModel::CURRENCY);
        if(!$currency){
            $this->error('阶段不存在');
        }

        $act = I('act');
        if($act == 'edit'){
            $currency_name = I('currency_name');
            $serial = I('serial', 0, 'intval');
            if(!$currency_name){
                $this->error('请填写阶段名称');
            }
            $data = array(
                'type_name' => $currency_name,
                'type_type' => TypeModel::CURRENCY,
                'serial' => $serial,
                );
            $type_obj = new TypeModel();
            if($type_obj->setType($currency_id, $data)){
                $this->success('修改成功');
            }else{
                $this->error('修改失败');
            }
        }
        $this->assign('currency', $currency);
        $this->assign('type_id', $currency_id);
        $this->assign('act', 'edit');
        $this->assign('head_title', '添加币种');
        $this->display('add_currency');
    }

    //删除币种
    public function del_currency(){
        $type_ids = I('ids');
        $type_obj = new TypeModel();
        if($type_obj->delType('type_id in('.$type_ids.')')){
            $this->ajaxReturn('success');
        }else{
            $this->ajaxReturn('failure');
        }
    }
}
