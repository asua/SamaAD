<?php
/**
 * 预约孵化器类
 */

class AcpIncubatorOrderAction extends AcpAction
{
    public function AcpIncubatorAction()
    {
        parent::_initialize();
    }
    public function get_search_condition(){

        //初始化查询条件
        $where ='';

        $incubator_id = I('incubator_id');
        if($incubator_id){
            $where = ' and incubator_id = '.$incubator_id;
        }


        //孵化器名字
        $incubator_name = $this->_request('incubator_name');
        if($incubator_name){
            $incubator_obj = new IncubatorModel();
            $incubator_ids =$incubator_obj->getIncubatorFields('incubator_id','incubator_name LIKE "%'.$incubator_name.'%"');
            if($incubator_ids){
                $incubator_str = implode(',',$incubator_ids);
                $where .= ' AND incubator_id IN('.$incubator_str.')';
            }else{
                $where .= ' AND false';
            }
        }


        //联系人
        $contacts = $this->_request('contacts');
        if($contacts){
            $where .= ' AND contacts LIKE "%'.$contacts.'%"';
        }

        //联系方式
        $contact_number = $this->_request('contact_number');
        if($contact_number){
            $where .= ' AND contact_number LIKE "%'.$contact_number.'%"';
        }

        //预约状态
        $order_status = $this->_request('order_status');
        $order_status = ($order_status=='' || $order_status== -1 ) ? -1 : intval($order_status);
        if($order_status != -1){
            $where .= ' AND order_status ='.$order_status;
        }


        //重新渲染到页面
        $this->assign('contacts',$contacts);
        $this->assign('incubator_name',$incubator_name);
        $this->assign('contact_number',$contact_number);
        $this->assign('order_status',$order_status);


        return $where;
    }
    //查看园区预约参观
    public function  get_park_visit_list(){
        $this->get_visit_list(1,'incubator_type = 1 and order_type ='.IncubatorOrderModel::VISIT);
    }
    //查看众创空间参观
    public function  get_zhongchuang_visit_list(){
        $this->get_visit_list(2,'incubator_type = 2 and order_type ='.IncubatorOrderModel::VISIT);
    }

    //查看预约参观
    public function get_visit_list($type,$where)//1为园区2为重创空间
    {
        $incubator_order_obj = new IncubatorOrderModel();
        $where.= $this->get_search_condition();
        //数据总量
        $total = $incubator_order_obj->getIncubatorOrderNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $incubator_order_obj->setStart($Page->firstRow);
        $incubator_order_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        //$incubator_order_list = $incubator_order_obj->getIncubatorOrder($where,'b.addtime DESC');
        $incubator_order_list = $incubator_order_obj->getIncubatorOrderList('', $where,'addtime DESC');

        $incubator_order_list = $incubator_order_obj->getListData($incubator_order_list);
//        dump($incubator_order_obj->getLastSql());die;

        //预约状态数组
        $this->assign('order_status_list', $incubator_order_obj->order_status_arr);
        $this->assign('type',$type);
        $this->assign('incubator_order_list', $incubator_order_list);
        $this->assign('head_title', '预约参观列表');
        $this->display('get_visit_list');
    }

    //查看园区预约入住
    public function  get_park_enter_list(){
        $this->get_stay_list(1,'incubator_type = 1 and order_type ='.IncubatorOrderModel::STAY);
    }
    //查看众创预约入住
    public function  get_zhongchuang_enter_list(){
        $this->get_stay_list(2,'incubator_type = 2 and order_type ='.IncubatorOrderModel::STAY);
    }

    //查看预约入住
    public function get_stay_list($type,$where)//1为园区2为重创空间
    {
        $incubator_order_obj = new IncubatorOrderModel();
        $where.= $this->get_search_condition();

        //数据总量
        // $total = $incubator_order_obj->getIncubatorJoinOrderNum($where);
        $total = $incubator_order_obj->getIncubatorOrderNum($where);
        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $incubator_order_obj->setStart($Page->firstRow);
        $incubator_order_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        //$incubator_order_list = $incubator_order_obj->getIncubatorOrder($where,'b.addtime DESC');
        $incubator_order_list = $incubator_order_obj->getIncubatorOrderList('', $where,'addtime DESC');
//        dump($incubator_order_obj->getLastSql());die;

        $incubator_order_list = $incubator_order_obj->getListData($incubator_order_list);
//        dump($incubator_order_obj->getLastSql());die;

        //预约状态数组
        $this->assign('order_status_list', $incubator_order_obj->order_status_arr);
        $this->assign('type',$type);
        $this->assign('incubator_order_list', $incubator_order_list);
        $this->assign('head_title', '预约入住列表');
        $this->display('get_stay_list');
    }


    //上传企业计划书
    public function incubator_prospectus(){
        $id = I('id','O','int');
        $url = I('url');
        if($url){
            $incubator_obj = new IncubatorOrderModel();
            if($incubator_obj->editIncubatorOrder($id,['prospectus'=>$url])){
                $this->ajaxReturn(['code'=>1,'msg'=>'上传成功!']);
            }
            $this->ajaxReturn(['code'=>0,'msg'=>'上传失败!']);
        }
        $this->ajaxReturn(['code'=>0,'msg'=>'上传失败!']);
    }


    // 删除孵化器
    public function delete_incubator_order()
    {
        $incubator_order_id = $this->_post('obj_id');
        if($incubator_order_id){
            $incubator_order_obj = new IncubatorOrderModel();
            $result = $incubator_order_obj->delIncubatorOrder($incubator_order_id);
            exit($result ? 'success' : 'failure');
        }
        exit('failure');
    }
    //批量删除孵化器
    public function batch_delete_incubator_order(){
        $ids=$this->_post('obj_ids');
        if($ids){
            $idarr=explode(',',$ids);
            $result     = 0;
            foreach($idarr as $id) {
                $incubator_order_obj = new IncubatorOrderModel();
                $result += $incubator_order_obj->delIncubatorOrder($id);
            }
            exit($result ? 'success' : 'failure');
        }
        exit('failure');
    }

//    function uploadFileHandler(){
//        $id=$this->_request('id');
//
//        $user_id = intval(session('user_id'));
//        $url=upFileHandler($_FILES['upfile'], '/user/' . $user_id);
//        $arr=array(
//          'prospectus'=>$url,
//        );
//        $incubator_order_obj = new IncubatorOrderModel();
//        $result = $incubator_order_obj->editIncubatorOrder($id,$arr);
//
//
//    }


    public function set_state(){
        if(IS_POST && IS_AJAX){
            $o_id = I('obj_id', 0, 'intval');
            $state = I('state', 0, 'intval');

            if(!$o_id) $this->ajaxReturn('failure');
            $incubator_order_obj = new IncubatorOrderModel();
            if($incubator_order_obj->setOrderState($o_id, $state)){
                $this->ajaxReturn('success');
            }
            $this->ajaxReturn('failure');
        }

    }
    //园区参观详情
    public function park_visit_detail(){
        $this->visit_order_detail();
    }
    //众创空间参观详情
    public function zhongchuang_visit_detail(){
        $this->visit_order_detail();
    }

    //预约参观详情
    public function visit_order_detail(){
        $order_id=$this->_get('order_id');
        $incubator_order_obj=new IncubatorOrderModel();
        $incubator_order_info=$incubator_order_obj->getIncubatorOrderInfo('order_id = '.$order_id);

        $incubator_obj =  new IncubatorModel();
        $incubator_info  =$incubator_obj ->getIncubatorInfo('incubator_id ='.$incubator_order_info['incubator_id']);

        $incubator_order_info['incubator_name'] = $incubator_info['incubator_name'];

        
        $this->assign('order_info',$incubator_order_info);
        $this->assign('head_title','预约参观详情');
        $this->display('visit_detail');
    }

    //园区入驻详情
    public function park_enter_detail(){
        $this->visit_enter_detail();
    }
    //众创空间注入详情
    public function zhongchuang_enter_detail(){
        $this->visit_enter_detail();
    }

    //预约入驻详情
    public function visit_enter_detail(){
        $order_id=$this->_get('order_id');
        $incubator_order_obj=new IncubatorOrderModel();
        $incubator_order_info=$incubator_order_obj->getIncubatorOrderInfo('order_id = '.$order_id);

        $incubator_obj =  new IncubatorModel();
        $incubator_info  =$incubator_obj ->getIncubatorInfo('incubator_id ='.$incubator_order_info['incubator_id']);
        
        // $station_obj = new StationModel();
        // $station_info = $station_obj->getStationInfo('incubator_id ='.$incubator_order_info['incubator_id']);
        $class_obj = new StationClassModel();
        $station_info = $class_obj->getStationClassInfo('station_class_id ='.$incubator_order_info['station_class_id']);
        $incubator_order_info['station_name'] = $station_info['class_name'];
        $incubator_order_info['incubator_name'] = $incubator_info['incubator_name'];
        $pay_status = $incubator_order_obj->payStatus();
        $incubator_order_info['state'] = $pay_status[$incubator_order_info['pay_status']];
        $incubator_order_info['incubator_type'] = $incubator_info['incubator_type'];
        

        $this->assign('order_info',$incubator_order_info);
        $this->assign('head_title','预约入驻详情');
        $this->display('visit_detail');
    }


    //退款
    public function refund(){
        $order_id = I('order_id', 0, 'intval');
        if(!$order_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，操作失败'));

        $incubator_order_obj = D('IncubatorOrder');
        $incubator_order = $incubator_order_obj->getIncubatorOrderInfo('order_id ='.$order_id);
        if(!$incubator_order || $incubator_order['pay_status'] != 1) if(!$order_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，操作失败'));
        // dump($incubator_order);die;
        $payway = $incubator_order['payway'];
        $pay_code = $incubator_order['pay_code'];
        $refund_fee = $incubator_order['pay_amount'];
        if($payway == 'wxpay'){
            $wxpay_obj = new WXPayModel();
            $r = $wxpay_obj->wx_refund($pay_code, $refund_fee);
            if($r){
                $incubator_order_obj->where('order_id ='.$order_id)->save(array('pay_status'=>3, 'refund_time'=>time()));

                $this->ajaxReturn(array('code'=>0, 'msg'=>'退款成功'));
            }else{
                $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，退款失败'));
            }
            // dump($r);die;
        }elseif($payway == 'mobile_alipay'){
            $alipay_obj = new AlipayModel();
            // $r = $alipay_obj->ali_refund($pay_code, $refund_fee, 'incubator');
            $r = $alipay_obj->ali_refund($order_id, 'incubator');
            $this->ajaxReturn(array('code'=>2, 'link'=> $r));
            // die;
            // redirect($r);
        }
        $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，退款失败'));
    }
    
}
?>
