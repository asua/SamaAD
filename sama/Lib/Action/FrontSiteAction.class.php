<?php

/**
 * Created by PhpStorm.
 * User: 辰
 * Date: 2017/3/13
 * Time: 9:39
 */
class FrontSiteAction extends FrontAction
{
    function _initialize()
    {
        parent::_initialize();
        $this->page_num = 5;
    }

    //场地列表
    public function get_site_list(){
        //场地类型
        $site_type  = new TypeModel();
        $site_type_list = $site_type->getTypeList('type_type ='.TypeModel::SITE);
        $this->assign('site_type_list',$site_type_list);

        //接收条件
        $where = 'status != 2';

        $key_words = I('key_words');
        if($key_words){
            $where.=' AND site_name LIKE "%'.$key_words.'%"';
            $this->assign('key_words',$key_words);
        }

        $city_id = intval(session('city_id'));

        $site_type_id = I('site_type_id', 0, 'intval');

        if(IS_AJAX && IS_POST){
            $city_id = I('city_id', 0, 'intval');
        }

        if($site_type_id){
            $where .= ' and site_type_id ='.$site_type_id;
            $this->assign('site_type_id', $site_type_id);
        }

        if($city_id){
            $incubator_obj = new IncubatorModel();
            $incubator_ids= $incubator_obj->getIdFields('incubator_id','city_id ='.$city_id);
            $incubator_str = implode(',',$incubator_ids);
            $where .= ' and incubator_id IN ('.$incubator_str.')';
            $this->assign('city_id', $city_id);
        }


        //场地列表
        $site_obj = new SiteModel();

        $total = $site_obj ->getSiteNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $site_obj ->setStart($firstRow);
        $site_obj->setLimit($this->page_num);

        $site_list = $site_obj->getSiteList($where,'serial');
        $site_list = $site_obj->getListData($site_list);

        //城市列表
        $incubator_ids_arr = $site_obj->getGroupIdFields('incubator_id',$where,'incubator_id');
        $incubator_str = implode(',',$incubator_ids_arr);
        $incubator_obj = new IncubatorModel();
        $city_ids_arr= $incubator_obj->getGroupIdFields('city_id','incubator_id IN ('.$incubator_str.')','city_id');
        $city_str = implode(',',$city_ids_arr);
        $city_obj = new AddressCityModel();
        $city_list = $city_obj->getCityList('city_id IN('.$city_str.')');

        if(IS_POST && IS_AJAX){
            $this->ajaxReturn(array('total'=>$total,'site_list'=>$site_list));
        }

        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('city_list',$city_list);
        $this->assign('site_list',$site_list);
        $this->assign('head_title','找场地');
        $this->display();
    }

    //场地详情
    public function site_detail(){
        $redirect = U('/FrontSite/get_site_list');
        $site_id  = intval($this->_get('site_id'));
        if (!$site_id) {
            $this->alert('对不起，非法访问！', $redirect);
        }
        $site_obj  = new SiteModel($site_id);
        $site_info = $site_obj->getSiteInfo('site_id = ' . $site_id);

        $incubator_id = $site_info['incubator_id'];

        if (!$site_info) {
            $this->alert('对不起，不存在相关场地！', $redirect);
        }

        //时间修改
        $site_info['start_hour'] = $site_info['start_time']-8*3600;
        $site_info['end_hour'] = $site_info['end_time']-8*3600;


        //场地图片
        $pic_arr = explode(',',$site_info['pic']);
        $this->assign('pic_arr',$pic_arr);

        $site_deploy_obj = new SiteDeployModel();

        //配套设施
        $site_facility_list = $site_deploy_obj->getSiteDeployList('site_deploy_id IN('.$site_info['site_facility_ids'].')','','site_deploy_name,site_deploy_pic');


        //支持活动
        $site_activity_list = $site_deploy_obj->getSiteDeployList('site_deploy_id IN('.$site_info['site_activity_ids'].')','','site_deploy_name,site_deploy_pic');

        $this->assign('incubator_id',$incubator_id);
        $this->assign('site_facility_list',$site_facility_list);
        $this->assign('site_activity_list',$site_activity_list);
        $this->assign('site_info',$site_info);
        $this->assign('head_title','场地详情');
        $this->display();
    }
    
    //申请场地
    public function site_order(){

        $site_id  = I('site_id',0,'int');
        $user_id = intval(session('user_id'));
        if (!$site_id) {
            $this->ajaxReturn(['status'=>1,'msg'=>'非法访问!']);
        }
        $site_obj  = new SiteModel($site_id);
        $site_info = $site_obj->getSiteInfo('site_id = ' . $site_id);
        if (!$site_info) {
            $this->ajaxReturn(['status'=>1,'msg'=>'没有该场地!']);
        }

        if(IS_AJAX && IS_POST) {
            $_post = $this->_post();
            $order_company = $_post['order_company'];
            $linkman = $_post['linkman'];
            $linkphone = $_post['linkphone'];
            $order_time = strtotime($_post['order_time']);
            $order_num = intval($_post['order_num']);
            $mark = $_post['mark'];
            $range_time_str = $_post['range_time'];

            if(!$linkman){
                $this->ajaxReturn(['status'=>1,'msg'=>'请输入联系人!']);
            }
            if(!$linkphone){
                $this->ajaxReturn(['status'=>1,'msg'=>'请输入手机号!']);
            }
            if(!$order_company){
                $this->ajaxReturn(['status'=>1,'msg'=>'请输入公司名称!']);
            }
            if(!is_numeric($order_num)){
                $this->ajaxReturn(['status'=>1,'msg'=>'请输入与会人数!']);
            }
            if(!$order_time){
                $this->ajaxReturn(['status'=>1,'msg'=>'请选择预约时间!']);
            }
            if($order_time && $order_time < time()){
                $this->ajaxReturn(['status'=>1,'msg'=>'请提前一天预约!']);
            }

            $range_time_arr = explode(',',$range_time_str);

            $site_order_obj = new SiteOrderModel();
            //取出当天已经呗预约的时间段．重组数组．
            $range_time = $site_order_obj->where('order_time ='.$order_time.' and site_id ='.$site_id.' and pay_status != 2')->getField('range_time', true);
            $range_time = implode(',',$range_time);
            $range_time = explode(',',$range_time);

            if(array_intersect($range_time_arr,$range_time)){
                $this->ajaxReturn(['status'=>1,'msg'=>'该时间段已经被预约,请刷新!']);
            }

            $data=[
                'linkman'=>$linkman,
                'user_id'=>$user_id,
                'site_id'=>$site_id,
                'linkphone'=>$linkphone,
                'order_company'=>$order_company,
                'order_num'=>$order_num,
                'order_time'=>$order_time,
                'range_time'=>$range_time_str,
                'mark'=>$mark,
                'unit_price'=>$site_info['price'],
                'addtime'=>time(),
                'order_sn'=>'CD'.$site_order_obj->generateOrderSn(),
                'total_amount'=> count(explode(',',$range_time_str)) * $site_info['price'],
            ];
            
            $success = $site_order_obj->addSiteOrder($data);
            if($success){
                $this->ajaxReturn(['status'=>0,'msg'=>'在线预约场地成功!','order_id' =>$success]);
            }else{
                $this->ajaxReturn(['status'=>1,'msg'=>'在线预约场地失败!']);
            }
        }

//        //开始时间
//        $start_time = $site_info['start_time']-8*3600;
//        $start_hour = date('H',$start_time);
//        //结束时间
//        $end_time = $site_info['end_time']-8*3600;
//        $end_hour = date('H',$end_time);
//        #dump($start_hour);dump($end_hour);die;
//        //总的时间范围
//        $date_range = array();
//        for($i=intval($start_hour);$i<$end_hour;$i++){
//            $date_range[$i]=array('start_hour'=>$i,'end_hour'=>$i+1);
//        }
//        $this->assign('date_range',$date_range);


        $this->assign('head_title','在线预约场地');
        $this->assign('site_id',$site_id);
        $this->assign('site_info',$site_info);
        $this->display();
    }

    public function choose_time(){

        $site_id = I('site_id', 0, 'intval');
        $my_date = I('my_date');
        $my_date = strtotime($my_date);

        
        $site_order_obj = D('SiteOrder');
        $range_time = $site_order_obj->where('order_time ='.$my_date.' and site_id ='.$site_id.' and pay_status != 2')->getField('range_time', true);
        $range_time = implode(',', $range_time);
        $range_time = explode(',', $range_time);

        $site_obj  = new SiteModel($site_id);
        $site_info = $site_obj->getSiteInfo('site_id = ' . $site_id);

        //开始时间
        $start_time = $site_info['start_time']-8*3600;
        $start_hour = date('H:i',$start_time);
        //结束时间
        $end_time = $site_info['end_time']-8*3600;
        $end_hour = date('H:i',$end_time);

        //总的时间范围
        $date_range = array();
        for($i=intval($start_hour);$i<$end_hour;$i++){
            $date_range[$i]=array('start_hour'=>$i,'end_hour'=>$i+1);
        }
        $range = array_keys($date_range);
        #dump($date_range);dump($range_time);

        $isuse_time = array_diff($range, $range_time);

        #dump($isuse_time);
        $can_use_time = array();
        foreach ($date_range as $k => $v) {
            if(in_array($k, $isuse_time)){
                $v['key'] = $k;
                $can_use_time[] = $v;
            }
        }
        ksort($can_use_time);
        #dump($can_use_time);die;
        $this->ajaxReturn($can_use_time);
    }

    //场地确认订单
    public function site_order_detail(){

        $order_id = I('order_id',0,'int');

        $site_order_obj = new SiteOrderModel();
        $site_order_info = $site_order_obj->getSiteOrderInfo('site_order_id ='.$order_id);
        $site_order_info['time']  = date('Y-m-d',$site_order_info['addtime']);

        $site_obj = new SiteModel();
        $site_info = $site_obj->getSiteInfo('site_id ='.$site_order_info['site_id'],'');
        $site_type_obj = new TypeModel();

        $site_info['type_name'] = $site_type_obj->getTypeSiteField('type_name','type_id ='.$site_info['site_type_id']);
        //开始时间
        $start_time = $site_info['start_time']-8*3600;
        $site_info['start_hour'] = date('H:i',$start_time);
        //结束时间
        $end_time = $site_info['end_time']-8*3600;
        $site_info['end_hour'] = date('H:i',$end_time);

        $site_pic = explode(',',$site_info['pic']);
        $site_info['pic_first'] = $site_pic[0];

        $title = $site_order_info['pay_status'] == 0 ? '确认订单' : '订单详情';

        $this->assign('site_info',$site_info);
        $this->assign('site_order_info',$site_order_info);
        $this->assign('order_id', $order_id);
        $this->assign('head_title',$title);
        $this->display();
    }


    //支付
    public function payment(){

        if(IS_POST && IS_AJAX){
            $order_id = I('order_id', 0, 'intval');
            $payway = I('payway');
            if(!$order_id || !$payway) $this->ajaxReturn(array('code'=>1));

            $order_obj = new SiteOrderModel();
            $order = $order_obj->getSiteOrderInfo('site_order_id ='.$order_id);
           
            if($order['pay_status'] != 0 || $order['total_amount'] <= 0) $this->ajaxReturn(array('code'=>1));
            if($payway == 'wx_pay'){
                $wxpay_obj = new WXPayModel();
                $result = $wxpay_obj->mobile_pay_code($order_id, 0, 0, false, 'site');
                $this->ajaxReturn(array('code'=>0,'parameter'=> $result));
            }elseif($payway == 'ali_pay'){
                $alipay_obj = new AlipayModel();
                $result = $alipay_obj->mobile_pay_code($order_id, 0, 'site');
                $this->ajaxReturn(array('code'=> 0,'parameter'=> $result));
            }
            $this->ajaxReturn(array('code'=> 1));
        }
    }
}
