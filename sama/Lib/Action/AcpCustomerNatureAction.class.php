<?php

class AcpCustomerNatureAction extends AcpAction
{
    public function AcpUserAction()
    {
        parent::_initialize();

    }


    /**
     * 统计类型公用方法．
     */
    public function get_nature_list($where,$title,$opt)
    {
        $nature_obj = new TypeModel();
        $where.=' AND is_del = 0';
        //数据总量
        $total = $nature_obj->getTypeNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $nature_obj->setStart($Page->firstRow);
        $nature_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $nature_list = $nature_obj->getTypeListAcp($where);

        $this->assign('nature_list', $nature_list);
        $this->assign('head_title', $title);
        $this->assign('opt', $opt);
        $this->display('get_nature_list');
    }

    //客户性质列表
    public function get_user_nature_list(){
        $this->get_nature_list('type_type ='.TypeModel::CUSTOMER_NATURE,'客户性质列表','customer_nature');
    }
    //项目性质列表
    public function get_project_nature_list(){
        $this->get_nature_list('type_type ='.TypeModel::PROJECT_NATURE,'项目性质列表','project_nature');
    }


    /**
     * 统计类型公用添加修改方法．
     */
    public function add_edit_nature($title,$type_id,$opt)
    {
        $act = I('act');
        $type_obj = new TypeModel();
        if(!$type_id){
            $this->assign('act', 'add');
        }else{
            $type_info = $type_obj->getTypeInfo('type_id ='.$type_id);
            $this->assign('info',$type_info);
            $this->assign('type_id',$type_id);
            $this->assign('act', 'edit');
        }

        if (IS_POST) {
            $_post      = $this->_post();
            $type_name = $_post['type_name'];
            $serial     = $_post['serial'];
            $type_id     = $_post['type_id'];

            //表单验证
            if (!$type_name) {
                $this->error('请填写类型名称！');
            }

            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }

            $arr = array(
                'type_name' => $type_name,
                'serial'     => $serial,
            );

            if($opt == 'customer_nature') {
                $arr['type_type'] = TypeModel::CUSTOMER_NATURE;
                $method_name ='get_user_nature_list';
            }
            if($opt == 'project_nature'){
                $arr['type_type'] = TypeModel::PROJECT_NATURE;
                $method_name ='get_project_nature_list';
            }

            $url = '/AcpCustomerNature/'.$method_name;

            if($act == 'add'){
                if($type_obj->addType($arr))$this->success('添加成功！', $url);
                $this->error('抱歉，添加失败！');
            }
            if($act == 'edit'){
                if($type_obj->setType($type_id,$arr)) $this->success('修改成功！', $url);
                $this->error('抱歉，修改失败！');
            }
        }

        $this->assign('head_title', $title);
        $this->assign('opt', $opt);
        $this->display('add_edit_nature');
    }

    //添加修改客户性质
    public function add_edit_user_nature(){
        $type_id = I('type_id',0,'int');
        $title ='添加客户性质';
        if($type_id) $title = '修改客户性质';
        $this->add_edit_nature($title,$type_id,'customer_nature');
    }
    //添加修改项目性质
    public function add_edit_project_nature(){
        $type_id = I('type_id',0,'int');
        $title ='添加项目性质';
        if($type_id) $title = '修改项目性质';
        $this->add_edit_nature($title,$type_id,'project_nature');
    }

    public function batch_delete_obj()
    {

        $type_ids = $this->_post('type_ids');
        if ($type_ids) {
            $type_ary = explode(',', $type_ids);
            $success_num     = 0;
            foreach ($type_ary as $type_id) {
                $statistics_type_obj = new TypeModel($type_id);
                $success_num += $statistics_type_obj->setType($type_id,array('is_del'=>1));
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }

        exit('failure');
    }






}
?>
