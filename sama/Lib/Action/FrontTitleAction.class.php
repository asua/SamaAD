<?php

class FrontTitleAction extends FrontAction
{
    function _initialize()
    {
        parent::_initialize();
    }

    //头衔首页．
    public function title_index(){

        $user_id = intval(session('user_id'));
//        $user_id = I('user_id',0,'int');
        $user_obj = new UserModel();
        $user_info = $user_obj->getUserInfo('headimgurl,nickname,sex','user_id ='.$user_id);
        $this->assign('user_info',$user_info);

        //我的所有头衔．
        $user_title_obj = new UserTitleModel();
        $my_title_list =$user_title_obj->getMyTotalTitleList($user_id);
        $my_title_list = $user_title_obj->getListData($my_title_list);
        //dump($my_title_list);die;
        $this->assign('my_title_list',$my_title_list);

        //我佩戴的头衔．
        $my_wear_title =  $user_title_obj->getUserTitleList('is_adorn = 1 AND user_id ='.$user_id,'adorn_time');
        $my_wear_title =  $user_title_obj->getListData($my_wear_title);
        $this->assign('my_wear_title',$my_wear_title);

        //普通头衔．
        $ordinary_list = $user_title_obj->getUserOrdinaryTitle($user_id);
        // dump($ordinary_list);die;
        $this->assign('head_title','头衔');

        if($this->is_hidden){
            $ordinary_list = null;
            $title_list = null;
        }

//wangtao 2017.9.20 获取联系方式 -start
        $kf_head = $GLOBALS['config_info']['KF_HEAD'];
        $kf_qq = $GLOBALS['config_info']['CUSTOMER_SERVICE_QQ'];
        $kf_wx = $GLOBALS['config_info']['CUSTOMER_SERVICE_WX'];
        $kf_info = array(
            'head_img' =>   $kf_head,
            'qq' =>   $kf_qq,
            'wx' =>   $kf_wx,
        );


        $this->assign('kf_info',$kf_info);
//wangtao 2017.9.20 end-
        $title_list = $user_title_obj->getUserTitleLiss($user_id);
//        dump($title_list);die;
        $this->assign('ordinary_list',$ordinary_list);

        $this->assign('title_list',$title_list);

        $this->display();
    }

    //佩戴头衔．
    public function set_wear(){

        $title_id = I('title_id',0,'int');
        $user_id  = intval(session('user_id'));
        $user_title_obj = new UserTitleModel();
        $is_wear = $user_title_obj->getUserTitleIsWear($user_id,$title_id);

        if($is_wear){
            $success = $user_title_obj->setUserTitleIsWear($user_id,$title_id,array('is_adorn' => 0,'adorn_time' => 0 ));
        }else{
            $adorn_num = $user_title_obj->where('user_id ='.$user_id .' and is_adorn = 1')->count();
            if($adorn_num > 5) $this->ajaxReturn(array('code'=>0,'msg'=>'最多只能佩戴6个头衔!'));

            $success = $user_title_obj->setUserTitleIsWear($user_id,$title_id,array('is_adorn' => 1,'adorn_time' => time()));
        }

        if($success) {
            $title = new TitleModel();
            $tmp = $title->getTitle($title_id,array('color'));//这里 获取头衔颜色
            $this->ajaxReturn(array('code'=>1,'msg'=>'佩戴头衔成功!','title_color'=>$tmp['color']));//返回头衔颜色
        }

        $this->ajaxReturn(array('code'=>0,'msg'=>'佩戴头衔失败!'));

    }

    //普通头衔详情．
    public function ordinary_title_detail(){

        $title_id = I('title_id',0,'int');
        #dump($title_id);
        //判断他是否是初级中级还是高级．
        $title_obj = new TitleModel();
        $title_info =$title_obj ->getJudgeTitleLevel($title_id);

        #dump($title_info);die;
        $this->assign('title_info',$title_info);
        $this->assign('head_title','领取头衔');
        $this->display();
    }

    //领取头衔．
    public function receive_title(){
        $user_id  = intval(session('user_id'));
        $title_id = I('title_id',0,'int');

        $title_obj = new TitleModel();
        $new_title_info = $title_obj->getTitleInfo('title_id ='.$title_id);

        $user_obj = new UserModel();
        $user_info = $user_obj->getUserInfo('','user_id ='.$user_id);
        if($new_title_info['tag_id'] == TitleModel::REPLY){
            if($user_info['reply_num'] < $new_title_info['rank_up_num']){
                $this->ajaxReturn(array('code' => 0,'msg' => '领取失败，回帖次数不够哦!'));
            }
        }

        if($new_title_info['tag_id'] == TitleModel::ADMIRE){
            if($user_info['admire_money'] < $new_title_info['rank_up_num']){
                $this->ajaxReturn(array('code' => 0,'msg' => '领取失败，赞赏金额不够哦!'));
            }
        }

        if($new_title_info['tag_id'] == TitleModel::SHARE){
            if($user_info['share_num'] < $new_title_info['rank_up_num']){
                $this->ajaxReturn(array('code' => 0,'msg' => '领取失败，分享次数不够哦!'));
            }
        }

        $user_title_obj = new UserTitleModel();
        $now_title_info = $user_title_obj->getIsOrdinaryTypeTitle($user_id,$new_title_info['tag_id']);
        if(!$now_title_info){
            $data = array(
                'user_id' => $user_id,
                'title_id' => $title_id,
                'addtime' => time(),
            );
            $success = $user_title_obj->addUserTitle($data);
        }else{
            $success = $user_title_obj->editUserTitle($now_title_info['user_title_id'],array('title_id' =>$title_id));
        }

        if($success) $this->ajaxReturn(array('code'=> 1 ,'msg' => '领取成功!'));
        $this->ajaxReturn(array('code'=> 0 ,'msg' => '领取失败!!'));

    }

    public function apply_special_title(){

        //客服信息．
        $kf_head = $GLOBALS['config_info']['KF_HEAD'];
        $kf_qq = $GLOBALS['config_info']['CUSTOMER_SERVICE_QQ'];
        $kf_wx = $GLOBALS['config_info']['CUSTOMER_SERVICE_WX'];
        $kf_info = array(
            'head_img' =>   $kf_head,
            'qq' =>   $kf_qq,
            'wx' =>   $kf_wx,
        );
        //特殊头衔
        $title_obj = new TitleModel();
        $title_list = $title_obj->getTotalTypeTitleList(TitleModel::SPECIAL);


        $this->assign('title_list',$title_list);
        $this->assign('kf_info',$kf_info);
        $this->assign('head_title','申请特殊头衔');
        $this->display();
    }
}
