<?php
class AcpChannelAction extends AcpAction
{

    public function _initialize()
    {
        parent::_initialize();


        //频道类型
        $this->assign('channel_type', D('Channel')->channelTypeName());

        //频道栏目
        $channel_sort_obj = new ChannelSortModel();
        $sort_list = $channel_sort_obj->getChannelSortList('isuse = 1');
        $this->assign('channel_sort_list', $sort_list);    
    }


    private function get_search_condition(){
        $where = '';

        $channel_name = I('channel_name');
        if($channel_name){
            $where .= ' and channel_name like "%'.$channel_name.'%"';
            $this->assign('channel_name', $channel_name);
        }

        $sort = I('sort');
        if($sort){
            $where .= ' and sort_id ='.$sort;
            $this->assign('sort', $sort);
        }
        $type = I('type');
        if($type){
            $where .= ' and channel_type ='.$type;
            $this->assign('type', $type);
        }

        return $where;
    }

    //所有频道列表
    public function get_all_channel_list(){

        $channel_obj = new ChannelModel();
        $where = 'is_del = 0';
        $where .= $this->get_search_condition();
        //数据总量
        $total = $channel_obj->getChannelNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $channel_obj->setStart($Page->firstRow);
        $channel_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('show', $show);

        $channel_list = $channel_obj->getChannelList('',$where);
        $channel_list = $channel_obj->getListData($channel_list);
        // dump($channel_list);
        $this->assign('channel_list', $channel_list);


        $this->assign('head_title', '频道列表');
        $this->display();
    }


    //添加频道
    public function add_edit_channel(){

        $channel_id = I('channel_id',0,'intval');
        $act = I('act');


        //头衔列表
        $title_obj = new TitleModel();
        $title_obj->setLimit(10000);
        $title_list = $title_obj->getTitleList();
        // dump($title_list);die;
        $this->assign('title_list', $title_list);

        $channel_obj = new ChannelModel();
        if($channel_id){
            $channel = $channel_obj->getChannelInfo('channel_id ='.$channel_id);
            $this->assign('act','edit');
            $this->assign('channel',$channel);

            //获取权限
            $visit_priv = D('ChannelTitlePriv')->getVisitPriv($channel_id);
            $this->assign('visit_priv', $visit_priv);
            $post_priv = D('ChannelTitlePriv')->getPostPriv($channel_id);
            $this->assign('post_priv', $post_priv);

            $this->assign('head_title','修改频道');
        }else{
            $this->assign('head_title','添加频道');
            $this->assign('act','add');
        }

        if(IS_POST){
            // dump($_POST);die;
            $channel_name = I('channel_name');
            $serial     = I('serial', 0, 'intval');
            $isuse      = I('isuse', 0, 'intval');
            $pic  = I('pic');
            $channel_type = I('channel_type', 0, 'intval');
            $channel_sort = I('channel_sort', 0, 'intval');
            $summary = I('summary');
            //wangtao 2017.9.15 -start
            $channel_rates = intval(I('channel_rates'));
            //wangtao 2017.9.15 -end

            //表单验证
            if (!$channel_name) $this->error('请填写频道名称！');
            if(!$channel_type && $act == 'add') $this->error('请选择频道类型');
            if(!$channel_sort) $this->error('请选择频道栏目');
            if(!$pic) $this->error('请添加频道图标！');
            if(!$summary) $this->error('请填写频道简介');
            //wangtao 2017.9.15 -start
            if(!is_numeric($channel_rates)) $this->error('请正确填写频道分成！');
            //wangtao 2017.9.15 -end
            // if (!$serial) $this->error('请填写排序号！');
            // if (!$isuse) $this->error('请选择是否显示！');

            $arr = array(
                'channel_name' => $channel_name,
                'channel_type' => $channel_type,
                'sort_id' => $channel_sort,
                'serial'     => $serial,
                'isuse'      => $isuse,
                'icon' => $pic,
                'summary' => $summary,
                'channel_rates' => $channel_rates,
            );

            $priv_arr = array();
            //访问权限
            $visit_all = I('visit_all', 0, 'intval');
            $arr['all_visit'] = $visit_all;
            if(!$visit_all){
                $visit_ids = I('visit_id', array());
                $visit_ids = implode(',', $visit_ids);
                $visit_ids = $visit_ids ? $visit_ids : '';
                $priv_arr['visit_title_ids'] = $visit_ids;
            }

            //发帖权限
            $post_all = I('post_all', 0, 'intval');
            $arr['all_post'] = $post_all;
            if(!$post_all){
                $post_ids = I('post_id', array());
                $post_ids = implode(',', $post_ids);
                $post_ids = $post_ids ? $post_ids : '';
                $priv_arr['post_title_ids'] = $post_ids;
            }

            // dump($arr);die;
            if ($act == 'add') {

                $success   = $channel_obj->addChannel($arr);
                if ($success) {
                    D('ChannelTitlePriv')->setChannelTitlePriv($success, $priv_arr);

                    $this->success('恭喜您，频道添加成功！', '/AcpChannel/get_all_channel_list');
                } else {
                    $this->error('抱歉，频道添加失败！', '/AcpChannel/get_all_channel_list');
                }
            }

            if($act == 'edit'){
                unset($arr['channel_type']);
                 $success = $channel_obj->editChannel($channel_id,$arr);
                 
                 if ($success !== false) {
                     D('ChannelTitlePriv')->setChannelTitlePriv($channel_id, $priv_arr);
                     $this->success('恭喜您，频道修改成功！', '/AcpChannel/get_all_channel_list');
                 } else {
                     $this->error('抱歉，频道修改失败！', '/AcpChannel/get_all_channel_list');
                 }
            }
        }



        // 频道图标
        $this->assign('class_img_data', array(
            'name'  => 'pic',
            'title' => '图标',
            'help'  => '点击查看大图',
            'url'   => $channel['icon'],
            'dir'   => 'channel',
        ));

        $this->display();
    }


    //删除频道
    public function del_channel(){
        if(IS_POST && IS_AJAX){
            $channel_id = I('channel_id', 0, 'intval');
            if(!$channel_id) $this->ajaxReturn('failure');

            $channel_obj = new ChannelModel($channel_id);
            if($channel_obj->delChannel()){
                $this->ajaxReturn('success');
            }
            $this->ajaxReturn('failure');
        }
    }


    //wangtao 2017.9.15 -start
    //设置精选频道
    public function is_featured(){
        if(IS_POST && IS_AJAX){
            $channel_id = I('channel_id', 0, 'intval');
            $type = I('type', 0, 'intval');
            if(!$channel_id) $this->ajaxReturn('failure');
            $channel_obj = new ChannelModel($channel_id);

            $num = $channel_obj->getChannelNum('is_featured = 1');
            if($num >= 4 && $type)  $this->ajaxReturn('failure');

            if($channel_obj->editChannel($channel_id,array('is_featured'=>$type))){
                $this->ajaxReturn('success');
            }
            $this->ajaxReturn('failure');
        }
    }

    //wangtao 2017.9.15 -end


    //频道栏目列表
    public function channel_sort_list(){

        $channel_sort_obj = new ChannelSortModel();
        //数据总量
        $total = $channel_sort_obj->getChannelSortNum();

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $channel_sort_obj->setStart($Page->firstRow);
        $channel_sort_obj->setLimit($Page->listRows);

        $show = $Page->show();
        $this->assign('show', $show);

        $channel_sort_list = $channel_sort_obj->getChannelSortList();
        $this->assign('channel_sort_list', $channel_sort_list);
        $this->assign('head_title', '频道栏目列表');
        $this->display();
    }


    //添加频道栏目
    public function add_edit_channel_sort(){

        $channel_sort_id = I('channel_sort_id',0,'int');
        $act = I('act');
        $channel_sort_obj = new ChannelSortModel();
        if($channel_sort_id){
            $sort = $channel_sort_obj->getChannelSort($channel_sort_id);
            $this->assign('act','edit');
            $this->assign('sort',$sort);
            $this->assign('head_title','修改栏目');
        }else{
            $this->assign('head_title','添加栏目');
            $this->assign('act','add');
        }

        if(IS_POST){

            $_post      = $this->_post();
            $channel_sort_name = I('channel_sort_name');
            $serial     = I('serial', 0, 'intval');
            $isuse      = I('isuse', 0, 'intval');
            $pic  = I('pic');

            //表单验证
            if (!$channel_sort_name) $this->error('请填写栏目名称！');
            if(!$pic) $this->error('请添加栏目图标！');
            // if (!$serial) $this->error('请填写排序号！');
            // if (!$isuse) $this->error('请选择是否显示！');

            $arr = array(
                'channel_sort_name' => $channel_sort_name,
                'serial'     => $serial,
                'isuse'      => $isuse,
                'pic' => $pic,
            );

            if ($act == 'add') {
                $success   = $channel_sort_obj->addChannelSort($arr);
                if ($success) {
                    $this->success('恭喜您，栏目添加成功！', '/AcpChannel/channel_sort_list');
                } else {
                    $this->error('抱歉，栏目添加失败！', '/AcpChannel/channel_sort_list');
                }
             }

             if($act == 'edit'){
                 $success = $channel_sort_obj->editChannelSort($channel_sort_id,$arr);;
                 if ($success !== false) {
                     $this->success('恭喜您，栏目修改成功！', '/AcpChannel/channel_sort_list');
                 } else {
                     $this->error('抱歉，栏目修改失败！', '/AcpChannel/channel_sort_list');
                 }
             }
        }


        // 频道图标
        $this->assign('class_img_data', array(
            'name'  => 'pic',
            'title' => '栏目图标',
            'help'  => '点击查看大图',
            'url'   => $sort['pic'],
            'dir'   => 'class',
        ));

        $this->display();
    }

    //删除栏目
    public function del_channel_sort(){
        if(IS_POST && IS_AJAX){
            $channel_sort_id = I('sort_id', 0, 'intval');
            if(!$channel_sort_id) $this->ajaxReturn('failure');

            $channel_sort_obj = new ChannelSortModel($channel_sort_id);
            if($channel_sort_obj->delChannelSort()){
                $this->ajaxReturn('success');
            }
            $this->ajaxReturn('failure');
        }
    }




    //频道栏目等级
    public function channel_rank_list(){

        $channel_rank_obj = new ChannelRankModel();
        //数据总量
        $total = $channel_rank_obj->getChannelRankNum();

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $channel_rank_obj->setStart($Page->firstRow);
        $channel_rank_obj->setLimit($Page->listRows);

        $show = $Page->show();
        $this->assign('show', $show);

        $channel_rank_list = $channel_rank_obj->getChannelRankList();
        $this->assign('rank_list', $channel_rank_list);
        $this->assign('head_title', '频道等级列表');
        $this->display();
    }


    //添加频道等级
    public function add_channel_rank(){
        if(IS_AJAX && IS_POST){
            $rank_name = I('rank_name');
            $need_exp = I('need_exp');
            if(!$rank_name) $this->ajaxReturn(array('code'=> 1, 'msg'=>'请输入等级名称'));
            if(!ctype_digit($need_exp)) $this->ajaxReturn(array('code'=>1, 'msg'=>'所需经验必须为整数'));
            
            $arr = array(
                'rank_name' => $rank_name,
                'need_exp' => $need_exp,
                );
            $channel_rank_obj = new ChannelRankModel();
            if($channel_rank_obj->addChannelRank($arr)){
                $this->ajaxReturn(array('code'=>0, 'msg'=>'频道等级添加成功'));
            }

            $this->ajaxReturn(array('code'=>1, 'msg'=>'添加失败'));
        }
    }

    //修改频道等级
    public function edit_channel_rank(){
        if(IS_AJAX){
            $rank_id = I('id', 0, 'intval');
            $rank_name = I('rank_name');
            $need_exp = I('need_exp');
            if(!$rank_id) $this->ajaxReturn(array('code'=>1,'修改失败'));
            if(!$rank_name) $this->ajaxReturn(array('code'=> 1, 'msg'=>'请输入等级名称'));
            if(!ctype_digit($need_exp)) $this->ajaxReturn(array('code'=>1, 'msg'=>'所需经验必须为整数'));
            
            $arr = array(
                'rank_name' => $rank_name,
                'need_exp' => $need_exp,
                );
            $channel_rank_obj = new ChannelRankModel();
            if($channel_rank_obj->editChannelRank($rank_id, $arr) !== false){
                $this->ajaxReturn(array('code'=>0,'msg'=>'修改成功'));
            }
            $this->ajaxReturn(array('code'=>1, 'msg'=>'修改失败'));
        }
    }


    public function get_channel(){
        if(IS_AJAX && IS_POST){
            $id = $this->_post('id');
            if(!ctype_digit($id))
            {
                $this->_ajaxFeedback(0, null, '参数无效！');
            }
            
            $channel_rank_obj = new ChannelRankModel();
            $rank = $channel_rank_obj->getChannelRankInfo('channel_rank_id ='.$id);
            if($rank)
            {
                $this->_ajaxFeedback(1, $rank);
            }
            $this->_ajaxFeedback(0, null, '对不起，服务器忙，稍后请重试！');
        }
    }
}
