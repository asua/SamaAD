<?php
/**
 * Acp后台统计类型
 */
class AcpStatisticsTypeAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 统计类型公用方法．
     */
    public function get_statistics_type_list($where,$title,$opt)
    {
        $statistics_type_obj = new TypeModel();
        $where.=' AND is_del = 0';
        //数据总量
        $total = $statistics_type_obj->getTypeNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $statistics_type_obj->setStart($Page->firstRow);
        $statistics_type_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $statistics_type_list = $statistics_type_obj->getTypeListAcp($where);

        $this->assign('statistics_type_list', $statistics_type_list);
        $this->assign('head_title', $title);
        $this->assign('opt', $opt);
        $this->display('get_statistics_type_list');
    }

    //客户来源列表
    public function get_source_list(){
        $this->get_statistics_type_list('type_type ='.TypeModel::SOURCE,'客户来源列表','source');
    }
    //流失类型列表
    public function get_drain_list(){
        $this->get_statistics_type_list('type_type ='.TypeModel::DRAIN,'流失类型列表','drain');
    }
    //退租类型列表
    public function get_surrender_list(){
        $this->get_statistics_type_list('type_type ='.TypeModel::SURRENDER,'退租类型列表','surrender');
    }


    /**
     * 统计类型公用添加修改方法．
     */
    public function add_edit_statistics_type($title,$type_id,$opt)
    {
        $act = I('act');
        $type_obj = new TypeModel();
        if(!$type_id){
            $this->assign('act', 'add');
        }else{
            $type_info = $type_obj->getTypeInfo('type_id ='.$type_id);
            $this->assign('info',$type_info);
            $this->assign('type_id',$type_id);
            $this->assign('act', 'edit');
        }

        if (IS_POST) {
            $_post      = $this->_post();
            $type_name = $_post['type_name'];
            $serial     = $_post['serial'];
            $type_id     = $_post['type_id'];

            //表单验证
            if (!$type_name) {
                $this->error('请填写类型名称！');
            }

            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }

            $arr = array(
                'type_name' => $type_name,
                'serial'     => $serial,
            );


            if($opt == 'source') {
                $arr['type_type'] = TypeModel::SOURCE;
                $method_name ='get_source_list';
            }
            if($opt == 'drain'){
                $arr['type_type'] = TypeModel::DRAIN;
                $method_name ='get_drain_list';
            }
            if($opt == 'surrender'){
                $arr['type_type'] = TypeModel::SURRENDER;
                $method_name ='get_surrender_list';
            }

            $url = '/AcpStatisticsType/'.$method_name;

            if($act == 'add'){
                if($type_obj->addType($arr))$this->success('添加成功！', $url);
                $this->error('抱歉，添加失败！');
            }
            if($act == 'edit'){
                if($type_obj->setType($type_id,$arr)) $this->success('修改成功！', $url);
                $this->error('抱歉，修改失败！');
            }
        }

        $this->assign('head_title', $title);
        $this->assign('opt', $opt);
        $this->display('add_edit_statistics_type');
    }

    //客户来源列表
    public function add_edit_source(){
        $type_id = I('type_id',0,'int');
        $title ='添加来源';
        if($type_id) $title = '修改来源';
        $this->add_edit_statistics_type($title,$type_id,'source');
    }
    //流失类型列表
    public function add_edit_drain(){
        $type_id = I('type_id',0,'int');
        $title ='添加流失类型';
        if($type_id) $title = '修改流失类型';
        $this->add_edit_statistics_type($title,$type_id,'drain');
    }
    //退租类型列表
    public function add_edit_surrender(){
        $type_id = I('type_id',0,'int');
        $title ='添加退租类型';
        if($type_id) $title = '修改退租类型';
        $this->add_edit_statistics_type($title,$type_id,'surrender');
    }


    public function batch_delete_obj()
    {

        $statistics_type_ids = $this->_post('type_ids');
        if ($statistics_type_ids) {
            $statistics_type_ary = explode(',', $statistics_type_ids);
            $success_num     = 0;
            foreach ($statistics_type_ary as $statistics_type_id) {
                $statistics_type_obj = new TypeModel($statistics_type_id);
                $success_num += $statistics_type_obj->setType($statistics_type_id,array('is_del'=>1));
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }

        exit('failure');
    }


}
