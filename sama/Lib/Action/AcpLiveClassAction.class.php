<?php
/**
 * acp视频类型后台
 */
class AcpLiveClassAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
//        $this->assign('action_title', '视频频道');
//        $this->assign('action_src', U('/AcpClass/list_class'));
    }

    //视频频道列表
    public function get_live_class_list()
    {
        $live_class_obj = new LiveClassModel();
        $where     = '';
        //数据总量
        $total = $live_class_obj->getLiveClassNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $live_class_obj->setStart($Page->firstRow);
        $live_class_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $class_list = $live_class_obj->getLiveClassList();

        $this->assign('question_class_list', $class_list);
        $this->assign('head_title', '视频频道列表');
        $this->display();

    }

    //添加视频频道
    public function add_edit_live_class()
    {
        $live_class_id = I('live_class_id',0,'int');
        $act = I('act');
        $live_class_obj = new LiveClassModel();
        if($live_class_id){
            $live_class_info = $live_class_obj->getLiveClass($live_class_id);
            $this->assign('act','edit');
            $this->assign('info',$live_class_info);
            $this->assign('head_title','修改视频频道');
        }else{
            $this->assign('head_title','添加视频频道');
            $this->assign('act','add');
        }

        if(IS_POST){

            $_post      = $this->_post();
            $live_class_name = $_post['live_class_name'];
            $serial     = $_post['serial'];
            $isuse      = $_post['isuse'];
            $class_img  = $_post['class_img'];
            $live_desc  = $_post['live_desc'];

            //表单验证
            if (!$live_class_name) $this->error('请填写频道名称！');
            if(!$class_img) $this->error('请添加频道图片！');
            if (!ctype_digit($serial)) $this->error('请填写排序号！');
            if (!ctype_digit($isuse)) $this->error('请选择是否显示！');
            if (!$live_desc) $this->error('请填写频道简介！');

            $arr = array(
                'live_class_name' => $live_class_name,
                'serial'     => $serial,
                'isuse'      => $isuse,
                'class_img' => $class_img,
                'live_desc' => $live_desc,
            );

            if ($act == 'add') {
                $success   = $live_class_obj->addLiveClass($arr);
                if ($success) {
                    $this->success('恭喜您，频道添加成功！', '/AcpLiveClass/get_live_class_list');
                } else {
                    $this->error('抱歉，问题修改失败！', '/AcpLiveClass/get_live_class_list');
                }
             }

             if($act == 'edit'){
                 $success = $live_class_obj->setLiveClass($live_class_id,$arr);;
                 if ($success !== false) {
                     $this->success('恭喜您，修改成功！', '/AcpLiveClass/get_live_class_list');
                 } else {
                     $this->error('抱歉，频道修改失败！', '/AcpLiveClass/get_live_class_list');
                 }
             }
        }

        // 频道图标
        $this->assign('class_img_data', array(
            'name'  => 'class_img',
            'title' => '频道图标',
            'help'  => '点击查看大图',
            'url'   => $live_class_info['class_img'],
            'dir'   => 'class',
        ));
        $this->display();
    }


    /**
     * 删除视频频道
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据问题ID删除视频频道
     */
    public function batch_delete_live_class()
    {
        $question_ids = $this->_post('obj_ids');
        if ($question_ids) {
            $question_id_ary = explode(',', $question_ids);
            $success_num     = 0;
            $live_obj        = new LiveModel();
            foreach ($question_id_ary as $question_id) {
                $num = $live_obj->getLiveNum('live_class_id = ' . $question_id);
                if ($num) {
                    continue;
                }

                $question_obj = new LiveClassModel($question_id);
                $success_num += $question_obj->delLiveClass();
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }
        exit('failure');
    }
}
