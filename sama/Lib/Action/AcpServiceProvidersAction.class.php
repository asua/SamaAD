<?php
/**
 * acp服务商类
 */
class AcpServiceProvidersAction extends AcpAction
{

    // 服务商模型对象
    protected $ServiceProviders;

    /**
     * 初始化
     * @author 姜伟
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();

        // 引入服务商公共函数库
//        require_cache('Common/func_service_providers.php');

        // 实例化服务商模型类
        $this->ServiceProviders = D('ServiceProviders');
    }

    /**
     * 接收搜索表单数据，组织返回where子句
     * @author 姜伟
     * @param void
     * @return void
     * @todo 接收表单提交的参数，过滤合法性，组织成where子句并返回
     */
    public function get_search_condition()
    {
        //初始化查询条件
        $where = '';

        //id
        $service_providers_id = $this->_request('service_providers_id');
        if ($service_providers_id) {
            $where .= ' AND service_providers_id = ' . $service_providers_id;
        }

        //服务商名称
        $service_providers_name = $this->_request('service_providers_name');
        if ($service_providers_name) {
            $where .= ' AND service_providers_name LIKE "%' . $service_providers_name . '%"';
        }

        //导师/联系人名称
        $user_name = $this->_request('user_name');
        if ($user_name) {
            $where .= ' AND user_name LIKE "%' . $user_name . '%"';
        }

        //服务商类型
        $service_type_id = $this->_request('service_type_id');
        if ($service_type_id) {
                $where .= ' AND service_type_id = ' . $service_type_id;
        }

        // 服务商状态
        $service_providers_status = $this->_request('service_providers_status');
        $service_providers_status = ($service_providers_status == '' || $service_providers_status == -1) ? -1 : intval($service_providers_status);
        if ($service_providers_status!= -1) {
            $where .= ' AND service_providers_status = ' . $service_providers_status;
        }

        //是否推荐
        $is_recommend = $this->_request('is_recommend');
        $is_recommend = ($is_recommend == '' || $is_recommend == -1) ? -1 : intval($is_recommend);
        if ($is_recommend!= -1) {
            $where .= ' AND is_recommend = ' . $is_recommend;
        }

        //添加时间范围起始时间
        $start_date = $this->_request('start_date');
        $start_date = str_replace('+', ' ', $start_date);
        $start_date = strtotime($start_date);
        if ($start_date) {
            $where .= ' AND addtime >= ' . $start_date;
        }

        //添加时间范围结束时间
        $end_date = $this->_request('end_date');
        $end_date = str_replace('+', ' ', $end_date);
        $end_date = strtotime($end_date);
        if ($end_date) {
            $where .= ' AND addtime <= ' . $end_date;
        }

        //重新赋值到表单
        $this->assign('service_providers_name', $service_providers_name);
        $this->assign('is_recommend', $is_recommend);
        $this->assign('start_date', $start_date ? $start_date : '');
        $this->assign('end_date', $end_date ? $end_date : '');
        $this->assign('service_type_id', $service_type_id);
        $this->assign('user_name', $user_name);
        $this->assign('service_providers_status', $service_providers_status);

        return $where;
    }

    /**
     * 获取服务商列表，公共方法
     * @author 姜伟
     * @param string $where
     * @param string $head_title
     * @return void
     * @todo 获取服务商列表，公共方法
     */
    public function service_providers_list($where, $head_title)
    {
        $where .= $this->get_search_condition();
        $service_providers_obj = new ServiceProvidersModel();

        //分页处理
        import('ORG.Util.Pagelist');
        $count = $service_providers_obj->getServiceProvidersNum($where);
        $Page  = new Pagelist($count, C('PER_PAGE_NUM'));
        $service_providers_obj->setStart($Page->firstRow);
        $service_providers_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('page', $Page);
        $this->assign('show', $show);

        $service_providers_list = $service_providers_obj->getServiceProvidersList($where, ' addtime DESC');
        $service_providers_list = $service_providers_obj->getListData($service_providers_list);
//        dump($service_providers_obj->getLastSql());die;
        $this->assign('service_providers_list', $service_providers_list);
        #echo "<pre>";
        #print_r($service_providers_list);
        #echo "</pre>";
        #echo $service_providers_obj->getLastSql();

        //服务类型
        $service_type_obj = new ServiceTypeModel();
        $service_type_list = $service_type_obj->getServiceTypeList();
        $this->assign('service_type_list',$service_type_list);

        // 服务商状态
        $this->assign('providers_status', $service_providers_obj->arr);
        $this->assign('head_title', $head_title);
        $this->display(APP_PATH . 'Tpl/AcpServiceProviders/get_service_providers_list.html');
    }


    /**
     * 全服的服务商列表
     * @author 姜伟
     * @param void
     * @return void
     * @todo 获取审核通过的服务商列表
     */
    public function get_all_service_providers_list()
    {
        $this->service_providers_list('service_providers_status !=4 AND service_providers_type=0', '全部的服务商列表');
    }

    /**
     * 审核通过的服务商列表
     * @author 姜伟
     * @param void
     * @return void
     * @todo 获取审核通过的服务商列表
     */
    public function get_approved_service_providers_list()
    {
        $this->service_providers_list('service_providers_status = 1 AND service_providers_type=0 ', '审核通过的服务商列表');
    }

    /**
     * 待审核的服务商列表
     * @author 姜伟
     * @param void
     * @return void
     * @todo 获取仓库中的服务商列表
     */
    public function get_to_audit_service_providers_list()
    {
        $this->service_providers_list('service_providers_status = 0 AND service_providers_type=0', '待审核的服务商列表');
    }

    /**
     * 审核未通过的服务商列表
     * @author 姜伟
     * @param void
     * @return void
     * @todo 获取仓库中的服务商列表
     */
    public function get_not_pass_service_providers_list()
    {
        $this->service_providers_list('service_providers_status = 2 AND service_providers_type=0', '未通过审核的服务商列表');
    }

    /**
     * 下架的服务商列表
     * @author 姜伟
     * @param void
     * @return void
     * @todo 获取仓库中的服务商列表
     */
    public function get_sold_out_service_providers_list()
    {
        $this->service_providers_list('service_providers_status = 3 AND service_providers_type=0', '下架的服务商列表');
    }


    /**
     * 添加服务商
     * @author 姜伟
     * @return void
     * @todo 上传新服务商
     */
    public function add_service_providers()
    {
        $action = I('post.action');

        $service_providers_obj = D('ServiceProviders');
        // 添加服务商
        if ($action == 'add') {
            $_post =$this->_post();
            $username = $_post['username'];
            $password = MD5($_post['password']);

            $link = U('/AcpServiceProviders/add_service_providers');
            if($service_providers_obj->create()){

                $user_obj = new UserModel();
                $info = $user_obj->getUserInfo('user_id',"username = '" . $username . "'");
                if($info)
                {
                    $user_id = $info['user_id'];
                }else{
                    if(!$username)
                    {
                        $this->error('对不起，请指定一个登录名');
                    }
                    if(!$password){
                        $this->error('密码不能为空');
                    }
                    $data_user=array(
                        'username'=>$username,
                        'password'=>$password,
                        'reg_time'=>time(),
                        'role_type' => 3,
                        'is_enable' => 1,
                    );
                    $user_id = $user_obj->addUser($data_user);
                }

//                dump($service_providers_obj->create());die;
                $service_providers_obj->user_id = $user_id;
                $service_providers_obj->addtime = time();
                $service_providers_obj->service_providers_type = ServiceProvidersModel::GENERAL;
                if ($service_providers_obj->add()) {
                    $this->success("添加服务商成功",$link);
                }else {
                    $this->error("添加服务商失败",$link);
                }
            }else{
                $this->error($service_providers_obj->getError(),$link);
            }
        }

        //所有省
        $address_obj = new AddressProvinceModel();
        $province_list = $address_obj->getProvinceList();
        $this->assign('province_list',$province_list);

        //获取所有服务类型
        $service_type_obj = new ServiceTypeModel();
        $service_type_list = $service_type_obj->getServiceTypeList();
        $this->assign('service_type_list',$service_type_list);

        //服务商状态
        $this->assign('providers_status', $service_providers_obj->arr);

        $this->assign('logo_pic', array(
            'name'  => 'service_providers_logo',
            'title' => '服务商logo',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('charter_pic', array(
//            'batch' => true,
            'name'  => 'charter',
            'title' => '营业执照',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('accounts_code_pic', array(
//            'batch' => true,
            'name'  => 'accounts_code',
            'title' => '二维码',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('poster_pic', array(
//            'batch' => true,
            'name'  => 'poster',
            'title' => '海报',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));


        $this->assign('head_title', '添加服务商');
        $this->display();
    }

    /**
     * 修改服务商
     * @author 姜伟
     * @return void
     * @todo 修改服务商
     */
    public function edit_service_providers()
    {
        $redirect = U('/AcpServiceProviders/get_service_providers');
        $service_providers_id  = intval($this->_get('service_providers_id'));
        if (!$service_providers_id) {
            $this->error('对不起，非法访问！', $redirect);
        }
        $service_providers_obj  = new ServiceProvidersModel($service_providers_id);
        $service_providers_info = $service_providers_obj->getServiceProvidersInfo('service_providers_id = ' . $service_providers_id);
        if (!$service_providers_info) {
            $this->error('对不起，不存在相关服务商！', $redirect);
        }

        $act = $this->_post('action');
        $service_providers_obj = D('ServiceProviders');
        $link =  U('/AcpServiceProviders/edit_service_providers/service_providers_id/'.$service_providers_id);
        // 修改服务商
        if ($act == 'edit') {
            if ($service_providers_obj->create()) {
                if ($service_providers_obj->where('service_providers_id='.$service_providers_id)->save()) {
                    $this->success("恭喜你,修改成功",$link);
                } else {
                    $this->error("抱歉,修改失败", $link);
                }
            }else{
                $this->error($service_providers_obj->getError(),$link);
//                $this->error("抱歉,系统错误", $link);
            };
        }

//        dump($service_providers_info);die;
        //获取所有服务类型
        $service_type_obj = new ServiceTypeModel();
        $service_type_list = $service_type_obj->getServiceTypeList();
        $this->assign('service_type_list',$service_type_list);

        //服务商状态
        $this->assign('providers_status', $service_providers_obj->arr);

        $this->assign('logo_pic', array(
            'name'  => 'service_providers_logo',
            'title' => '服务商logo',
            'url'=>$service_providers_info['service_providers_logo'],
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('charter_pic', array(
//            'batch' => true,
            'name'  => 'charter',
            'title' => '营业执照',
            'url'=>$service_providers_info['charter'],
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('accounts_code_pic', array(
//            'batch' => true,
            'name'  => 'accounts_code',
            'title' => '二维码',
            'url'=>$service_providers_info['accounts_code'],
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('poster_pic', array(
//            'batch' => true,
            'name'  => 'poster',
            'title' => '海报',
            'url'=>$service_providers_info['poster'],
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));
//        dump($service_providers_info);die;
        //所有省
        $address_obj = new AddressProvinceModel();
        $province_list = $address_obj->getProvinceList();
        $this->assign('province_list',$province_list);

        //省对应的所有市
        $address_city_obj = new AddressCityModel();
        $city_list=$address_city_obj->getCity($service_providers_info['province_id']);
        $this->assign('city_list',$city_list);

        //市对应的所有区
        $area_list = M('AddressArea')->where('city_id='.$service_providers_info['city_id'])->select();
        $this->assign('area_list',$area_list);

        $this->assign('service_providers_info', $service_providers_info);
        $this->assign('head_title', '修改服务商');
        $this->display();
    }

    /**
     * 服务商状态批量编辑(删除等)
     * @author 姜伟
     */
    public function batch_set_status(){
        $service_providers_ids= $this->_post('obj_ids');
        $service_providers_status= intval($this->_post('status'));
        if($service_providers_ids && $service_providers_status){
            $service_providers_arr = explode(',', $service_providers_ids);
            $success_num     = 0;
            foreach($service_providers_arr as $service_providers_id){
                $service_providers_obj = new ServiceProvidersModel();
                $success_num +=$service_providers_obj->setServiceProvidersStatus($service_providers_id,array('service_providers_status'=>$service_providers_status));
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }
        exit;
    }

    /**
     * 服务商状态编辑(删除等)
     * @author 姜伟
     */
    public function set_status(){
        $service_providers_id= intval($this->_post('service_providers_id'));
        $service_providers_status= intval($this->_post('service_providers_status'));

        if($service_providers_id && $service_providers_status){
            $service_providers_obj = new ServiceProvidersModel();
            $service_providers_obj->setServiceProvidersStatus($service_providers_id,array('service_providers_status'=>$service_providers_status));
            $title='';
            if($service_providers_status == 1){
                $title='申请服务商审核通过';
            }elseif($service_providers_status == 2){
                $title='申请服务商审核失败';
            }
            $providers_obj = new ServiceProvidersModel();
            $providers_info = $providers_obj->getServiceProvidersInfo('service_providers_id ='.$service_providers_id,'user_id');
            $service_providers_list = $service_providers_obj->getServiceProvidersList('service_providers_id ='.$service_providers_id);
            $service_providers_list = $service_providers_obj->getListData($service_providers_list);
            $contents = serialize($service_providers_list[0]);

            $message_obj = new MessageModel();
            $message_obj -> addMessage($service_providers_id,MessageModel::PROVIDER,0,$providers_info['user_id'],$title,$contents);
            $this->ajaxReturn('success');
        }
        exit;
    }

    /**
     * 编写拒绝理由
     * @author 姜伟
     */
    public function write_reason(){
        $service_providers_id = intval($this->_post('service_providers_id'));

        if ($service_providers_id) {

            $act = $this->_post('act');
            if ($act == 'add') {
                $status_cause = $_POST['status_cause'];
                if (!$status_cause) {
                    $this->error('对不起，请填写理由！');
                }
                $service_providers_obj = new ServiceProvidersModel();
                $success = $service_providers_obj ->setServiceProviders($service_providers_id,array('status_cause'=>$status_cause));
            if ($success) {
                $this->ajaxReturn('success');
            } else {
                exit;
            }
            }
        }
        exit;
    }


    /**
     * 添加导师
     * @author 姜伟
     */
    public function add_mentor(){
        $action = I('post.action');

        $service_providers_obj = D('ServiceProviders');
        // 添加导师
        if ($action == 'add') {
            $_post =$this->_post();
            $username = $_post['username'];
            $real_name = $_post['user_name'];
            $password = MD5($_post['password']);
            $position = $_post['position'];
            $sex = $_post['sex'];

            $service_type_obj = new ServiceTypeModel();
            $service_type_id = $service_type_obj->getServiceTypeFieldId('service_label="chuangyedaoshi"','service_type_id');

            if($service_type_id){
                $_post=$this->_post();
                $user_name = $_post['user_name'];
                $domain_arr = $_post['domain'];
                $province_id = $_post['province_id'];
                $city_id = $_post['city_id'];
                $area_id = $_post['area_id'];
                $address = $_post['address'];
                $is_recommend = $_post['is_recommend'];
                $serial = $_post['serial'];
                $service_providers_describe = $_POST['service_providers_describe'];
                $service_providers_logo = $_POST['service_providers_logo'];
                $poster = $_POST['poster'];

                $domain_str = implode(',',$domain_arr);

                if(!$user_name){
                    $this->error('请输入导师姓名');
                }
                if(!$domain_arr){
                    $this->error('请选择领域');
                }
                if(!$province_id){
                    $this->error('请选择省份');
                }
                if(!$city_id){
                    $this->error('请选择城市');
                }
                if(!$area_id){
                    $this->error('请选择区域');
                }
                if(!$address){
                    $this->error('请输入地址');
                }
                if(!$service_providers_describe){
                    $this->error('请输入导师简介');
                }
                if(!$service_providers_logo){
                    $this->error('请添加导师头像照片');
                }
                if(!$poster){
                    $this->error('请添加导师海报');
                }

                $user_obj = new UserModel();
                $info = $user_obj->getUserInfo('user_id',"username = '" . $username . "'");

                if($info)
                {
                    $user_id = $info['user_id'];
                }else{
                    if(!$username)
                    {
                        $this->error('对不起，请指定一个登录名');
                    }
                    if(!$password){
                        $this->error('密码不能为空');
                    }
                    if(!$position){
                        $this->error('职位不能为空');
                    }
                    $data_user=[
                        'username'=>$username,
                        'realname'=>$real_name,
                        'password'=>$password,
                        'position'=>$position,
                        'sex'=>$sex,
                        'reg_time'=>time(),
                        'role_type' => 3,
                        'is_enable' => 1,
                    ];
                    $user_id = $user_obj->addUser($data_user);
                }


                $data=[
                    'user_name'=>$user_name,
                    'domain'=>$domain_str,
                    'province_id'=>$province_id,
                    'city_id'=>$city_id,
                    'area_id'=>$area_id,
                    'address'=>$address,
                    'is_recommend'=>$is_recommend,
                    'service_providers_logo'=>$service_providers_logo,
                    'poster'=>$poster,
                    'service_providers_describe'=>$service_providers_describe,
                    'service_providers_type'=>ServiceProvidersModel::MENTOR,
                    'service_providers_status'=>ServiceProvidersModel::APPROVED,
                    'user_id'=>$user_id,
                    'serial'=>$serial,
                    'service_type_id'=>$service_type_id,
                    'addtime'=>time(),
                ];
//                dump($data);
                $r=$service_providers_obj->addServiceProviders($data);
//                log_file($service_providers_obj->getLastSql());
                $link = U('/AcpServiceProviders/add_mentor');
                if($r){
                    $this->success("恭喜你,添加导师成功",$link);
                } else {
                    $this->error("抱歉,添加导师失败", $link);
                }
            }
        }

        //所有省
        $address_obj = new AddressProvinceModel();
        $province_list = $address_obj->getProvinceList();
        $this->assign('province_list',$province_list);

        //领域
        $type_obj = new TypeModel();
        $domain_list = $type_obj->getTypeList('type_type ='.TypeModel::DOMAIN);
        $this->assign('domain_list', $domain_list);

        $this->assign('poster_pic', array(
//            'batch' => true,
            'name'  => 'poster',
            'title' => '导师海报',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));


        $this->assign('logo_pic', array(
            'name'  => 'service_providers_logo',
            'title' => '导师头像',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('head_title', '添加导师');
        $this->display();
    }


    /**
     * 导师列表
     * @author 姜伟
     */
    public function get_mentor_list(){
        $where = 'service_providers_status !=4 AND service_providers_type ='.ServiceProvidersModel::MENTOR;
        $where.= $this->get_search_condition();
        $service_providers_obj = new ServiceProvidersModel();

        //分页处理
        import('ORG.Util.Pagelist');
        $count = $service_providers_obj->getServiceProvidersNum($where);
        $Page  = new Pagelist($count, C('PER_PAGE_NUM'));
        $service_providers_obj->setStart($Page->firstRow);
        $service_providers_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('page', $Page);
        $this->assign('show', $show);

        $service_providers_list = $service_providers_obj->getServiceProvidersList($where, ' addtime DESC');
        $service_providers_list = $service_providers_obj->getListData($service_providers_list);
//        dump($service_providers_obj->getLastSql());die;
        $this->assign('service_providers_list', $service_providers_list);
        #echo "<pre>";
        #print_r($service_providers_list);
        #echo "</pre>";
        #echo $service_providers_obj->getLastSql();

        $this->assign('head_title', '导师列表');
        $this->display();


    }
    /**
     * 修改导师
     * @author 姜伟
     */
    public function edit_mentor()
    {
        $redirect = U('/AcpServiceProviders/get_service_providers');
        $service_providers_id  = intval($this->_get('service_providers_id'));
        if (!$service_providers_id) {
            $this->error('对不起，非法访问！', $redirect);
        }
        $service_providers_obj  = new ServiceProvidersModel($service_providers_id);
        $service_providers_info = $service_providers_obj->getServiceProvidersInfo('service_providers_id = ' . $service_providers_id);
        if (!$service_providers_info) {
            $this->error('对不起，不存在相关导师！', $redirect);
        }

        $action = $this->_post('action');
        $service_providers_obj = D('ServiceProviders');
        $link =  U('/AcpServiceProviders/edit_mentor/service_providers_id/'.$service_providers_id);

        if ($action == 'edit') {
            $_post = $this->_post();
            $user_name = $_post['user_name'];
            $domain_arr = $_post['domain'];
            $province_id = $_post['province_id'];
            $city_id = $_post['city_id'];
            $area_id = $_post['area_id'];
            $address = $_post['address'];
            $serial = $_post['serial'];
            $is_recommend = $_post['is_recommend'];
            $service_providers_describe = $_POST['service_providers_describe'];
            $service_providers_logo = $_POST['service_providers_logo'];
            $poster = $_POST['poster'];

            $domain_str = implode(',', $domain_arr);

            if(!$user_name){
                $this->error('请输入导师姓名');
            }
            if(!$domain_arr){
                $this->error('请选择领域');
            }
            if(!$province_id){
                $this->error('请选择省份');
            }
            if(!$city_id){
                $this->error('请选择城市');
            }
            if(!$area_id){
                $this->error('请选择区域');
            }
            if(!$address){
                $this->error('请输入地址');
            }
            if(!$service_providers_describe){
                $this->error('请输入导师简介');
            }
            if(!$service_providers_logo){
                $this->error('请添加导师头像照片');
            }
            if(!$poster){
                $this->error('请添加导师海报');
            }
            $data = [
                'user_name' => $user_name,
                'domain' => $domain_str,
                'province_id' => $province_id,
                'city_id' => $city_id,
                'area_id' => $area_id,
                'address' => $address,
                'serial'=>$serial,
                'is_recommend' => $is_recommend,
                #'service_providers_status' => 1,
                'service_providers_logo' => $service_providers_logo,
                'service_providers_describe' => $service_providers_describe,
                'poster'=>$poster,
            ];

            $r = $service_providers_obj->setServiceProviders($service_providers_id,$data);

            if ($r) {
                $this->success("恭喜你,修改导师成功", $link);
            } else {
                $this->error("抱歉,修改导师失败", $link);
            }

        }

        //所有省
        $address_obj = new AddressProvinceModel();
        $province_list = $address_obj->getProvinceList();
        $this->assign('province_list', $province_list);

        //省对应的所有市
        $address_city_obj = new AddressCityModel();
        $city_list=$address_city_obj->getCity($service_providers_info['province_id']);
        $this->assign('city_list',$city_list);

        //市对应的所有区
        $area_list = M('AddressArea')->where('city_id='.$service_providers_info['city_id'])->select();
        $this->assign('area_list',$area_list);

        //所有领域
        $type_obj = new TypeModel();
        $domain_list = $type_obj->getTypeList('type_type =' . TypeModel::DOMAIN);
        $this->assign('domain_list', $domain_list);

        //已有领域
        $domain_array= explode(',',$service_providers_info['domain']);

        $this->assign('domain_array', $domain_array);

        $this->assign('logo_pic', array(
            'name' => 'service_providers_logo',
            'title' => '导师头像',
            'url'=>$service_providers_info['service_providers_logo'],
            'help' => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('poster_pic', array(
//            'batch' => true,
            'name'  => 'poster',
            'title' => '导师海报',
            'url'=>$service_providers_info['poster'],
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('service_providers_info', $service_providers_info);
        $this->assign('head_title', '修改导师');
        $this->display();
    }


}
