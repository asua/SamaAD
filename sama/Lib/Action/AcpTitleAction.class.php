<?php
/**
 * acp头衔后台
 */
class AcpTitleAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
//        $this->assign('action_title', '头衔');
//        $this->assign('action_src', U('/AcpClass/list_class'));
    }

    //头衔列表
    public function get_title_list($where,$opt,$title)
    {
        $title_obj = new TitleModel();
        //数据总量
        $total = $title_obj->getTitleNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $title_obj->setStart($Page->firstRow);
        $title_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $title_list = $title_obj->getTitleList($where);
        $title_list = $title_obj->getListData($title_list);
        $this->assign('opt',$opt);
        $this->assign('title_list', $title_list);
        $this->assign('head_title',$title);
        $this->display('get_title_list');

    }
    //普通头衔列表
    public function get_ordinary_title_list(){
        $this->get_title_list('type ='.TitleModel::ORDINARY,'ordinary','普通头衔列表');
    }
    //特殊头衔列表
    public function get_special_title_list(){
        $this->get_title_list('type ='.TitleModel::SPECIAL,'special','特殊头衔列表');
    }
    //鉴赏师头衔列表
    public function get_appraiser_title_list(){
        $this->get_title_list('type ='.TitleModel::APPRAISER,'appraiser','鉴赏师头衔列表');
    }


    //添加修改头衔
    public function add_edit_title()
    {
        $title_id = I('title_id',0,'int');
        $act = I('act');
        $title_post_cash      = I('title_post_cash', 0, 'intval');

        $title_obj = new TitleModel();
        if($title_id){
            //头衔类型
            $title_type_list = $title_obj ->getEditTitleType();
            $this->assign('title_type_list',$title_type_list);
            $title_info = $title_obj->getTitle($title_id);
            $this->assign('act','edit');
            $this->assign('info',$title_info);
            $this->assign('head_title','修改头衔');
        }else{
            //头衔类型
            $title_type_list = $title_obj ->getAddTitleType();
            $this->assign('title_type_list',$title_type_list);
            $this->assign('head_title','添加头衔');
            $this->assign('act','add');
        }

        if(IS_POST){

            $_post = $this->_post();
            $title_name = $_post['title_name'];
            $color = $_post['color'];
            $type = $_post['type'];
            $tag_id = $_post['tag_id'];
            $rank_up_num = $_post['rank_up_num'];
            $channel_id = $_post['channel_id'];
            $isuse = $_post['isuse'];
            $title_rates = intval($_POST['title_rates']);
            $translate = $_POST['translate'];
            //表单验证
            if (!$title_name) $this->error('请填写头衔名称！');
            if(!$color) $this->error('请选择颜色！');
            if (!$type) $this->error('请选择头衔类型！');
            if(!is_numeric($title_rates)) $this->error('请输入正确的分成额度！');
            if($title_rates == 0) $title_rates = 0;
            $arr = array(
                'title_name' => $title_name,
                'color'     => $color,
                'type'      => $type,
                'isuse'      => 1,
                'title_rates' => $title_rates,
                'title_post_cash' => $title_post_cash,
                'translate'  => $translate,
            );

            $url = '/AcpTitle/get_special_title_list';
            if($type == TitleModel::ORDINARY){
                if (!$tag_id) $this->error('请选择普通头衔标签！');
                if (!$rank_up_num) $this->error('请填写升级所需次数！');
                if ($rank_up_num && !is_numeric($rank_up_num)) $this->error('请正确填写升级所需次数！');
                $arr['tag_id'] = $tag_id;
                $arr['rank_up_num'] = $rank_up_num;
                $url = '/AcpTitle/get_ordinary_title_list';
            }
            if($type == TitleModel::APPRAISER){
                if (!$channel_id) $this->error('请选择该鉴定师频道权限！');
                $arr['channel_id'] = $channel_id;
                $url = '/AcpTitle/get_appraiser_title_list';
            }

            if ($act == 'add') {
                $success   = $title_obj->addTitle($arr);
                if ($success) {
                    $this->success('恭喜您，头衔添加成功！', $url);
                } else {
                    $this->error('抱歉，头衔添加失败！', $url);
                }
             }

             if($act == 'edit'){
                 $success = $title_obj->setTitle($title_id,$arr);;
                 if ($success !== false) {
                     $this->success('恭喜您，头衔修改成功！',$url);
                 } else {
                     $this->error('抱歉，头衔修改失败！', $url);
                 }
             }
        }


        //普通头衔标签
        $ordinary_tag_list = $title_obj ->getOrdinaryTitleTag();
        $this->assign('ordinary_tag_list',$ordinary_tag_list);

        //所有频道
        $channel_obj  = new ChannelModel();
        $channel_list = $channel_obj->getValidChannelList('channel_type ='.ChannelModel::PRAISE);
        $this->assign('channel_list',$channel_list);
        $this->display();
    }


    /**
     * 删除头衔
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据问题ID删除头衔
     */
    public function batch_delete_title()
    {
        $question_ids = $this->_post('obj_ids');
        if ($question_ids) {
            $question_id_ary = explode(',', $question_ids);
            $success_num     = 0;
            foreach ($question_id_ary as $question_id) {
                $user_title_obj        = new UserTitleModel();
                $user_title_obj->delUserTitleByTitleId($question_id);

                $question_obj = new TitleModel($question_id);
                $success_num += $question_obj->delTitle();
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }
        exit('failure');
    }
}
