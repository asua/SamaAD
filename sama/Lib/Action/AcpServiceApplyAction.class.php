<?php
/**
 * Acp后台服务预约
 */
class AcpServiceApplyAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
        $this->assign('action_title', '服务预约列表');
        $this->assign('action_src', U('/AcpServiceApply/get_service_apply_list'));
    }

    /**
     * 接收搜索表单数据，组织返回where子句
     * @author 姜伟
     * @param void
     * @return void
     * @todo 接收表单提交的参数，过滤合法性，组织成where子句并返回
     */
    public function get_search_condition()
    {
        //初始化查询条件
        $where = '';


        //id
        $service_providers_id = $this->_request('service_providers_id');
        if ($service_providers_id) {
            $where .= ' AND service_providers_id = ' . $service_providers_id;
        }

        //导师/联系人名称
        $user_name = $this->_request('user_name');
        if ($user_name) {
            $where .= ' AND user_name LIKE "%' . $user_name . '%"';
        }

        //手机号
        $mobile = $this->_request('mobile');
        if ($mobile) {
            $where .= ' AND mobile LIKE "%' . $mobile . '%"';
        }


        //添加时间范围起始时间
        $start_date = $this->_request('start_date');
        $start_date = str_replace('+', ' ', $start_date);
        $start_date = strtotime($start_date);
        if ($start_date) {
            $where .= ' AND addtime >= ' . $start_date;
        }

        //添加时间范围结束时间
        $end_date = $this->_request('end_date');
        $end_date = str_replace('+', ' ', $end_date);
        $end_date = strtotime($end_date);
        if ($end_date) {
            $where .= ' AND addtime <= ' . $end_date;
        }

        //重新赋值到表单
        $this->assign('start_date', $start_date ? $start_date : '');
        $this->assign('end_date', $end_date ? $end_date : '');
        $this->assign('user_name', $user_name);
        $this->assign('mobile', $mobile);

        return $where;
    }

    public function service_apply_list($where, $head_title, $opt){
        $service_apply_obj = new ServiceApplyModel();
        
        $where .=$this->get_search_condition();

        //数据总量
        $total = $service_apply_obj->getServiceApplyNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $service_apply_obj->setStart($Page->firstRow);
        $service_apply_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $service_apply_list = $service_apply_obj->getServiceApplyList($where,'addtime DESC');
        $service_apply_list = $service_apply_obj->getListData($service_apply_list);
//        dump($service_apply_list);

        $apply_status_arr = array(
            '0'=>'待服务',
            '1'=>'服务中',
            '2'=>'已服务',
            '3'=>'已取消',
        );

        $this->assign('apply_status_arr', $apply_status_arr);
        $this->assign('service_apply_list', $service_apply_list);
        $this->assign('head_title', $head_title);
        $this->assign('opt', $opt);
        $this->display('get_service_apply_list');
    }

    public function get_service_apply_list()
    {
        $this->service_apply_list('true', '全部服务', 'all');
    }

    public function get_mentor_apply_list(){
        $this->service_apply_list('service_type_id = 2', '创业导师服务', 'mentor');
    }

    public function get_project_apply_list(){
        $this->service_apply_list('service_type_id = 1', '项目申报服务', 'project');
    }

    public function get_rz_apply_list(){
        $this->service_apply_list('service_type_id = 4', '申请融资服务', 'rz');
    }

    public function get_other_apply_list(){
        $this->service_apply_list('service_type_id not in(1,2,4)', '其他服务', 'other');
    }


    //修改状态
    public function set_apply_status(){
        $obj_id = I('obj_id',0,'int');
        $apply_status = I('apply_status');
        if($obj_id){
            $order_apply_obj = new ServiceApplyModel();
            if($order_apply_obj->setServiceApplyStatus($obj_id,array('apply_status'=>$apply_status))){

                $apply_info = $order_apply_obj->getServiceApplyInfo('service_apply_id ='.$obj_id);

                if($apply_info['service_type_id'] == 1){
                    $project_report_obj = new ProjectReportModel();
                    //项目申报
                    $project_report_name = $project_report_obj -> getProjectReportField($apply_info['service_providers_id'],'project_report_name');
                    $their_name  = '项目申报'.'-'.$project_report_name;
                }else{
                    $service_providers_obj = new ServiceProvidersModel();
                    //所属服务商
                    $providers_name = $service_providers_obj->getServiceProvidersField($apply_info['service_providers_id'],'service_providers_name');
                    //所属导师
                    $mentor_name = $service_providers_obj->getServiceProvidersField($apply_info['service_providers_id'],'user_name');
                    //如果有服务商显示服务商名称,不然显示导师名称
                    $their_name  = $providers_name ? $providers_name : $mentor_name;
                }

                $contents = serialize($apply_info);

                $title='';
                if($apply_status == 1){
                    $title = '申请'.$their_name.'服务成功';
                }elseif($apply_status == 3){
                    $title = '申请'.$their_name.'服务失败';
                }
                $message_obj = new MessageModel();
                $message_obj ->addMessage($obj_id,MessageModel::SERVICE_AUDIT,0,$apply_info['user_id'],$title,$contents);


                $this->ajaxReturn(array('code'=>1,'msg'=>'修改状态成功!'));
            }else{
                $this->ajaxReturn(array('code'=>0,'msg'=>'修改状态失败!'));
            }
        }
        $this->ajaxReturn(array('code'=>0,'msg'=>'操作失败!'));
    }

    /**
     * 删除服务预约
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据服务类型ID删除服务类型
     */
    public function delete_service_apply()
    {
        $service_apply_id = intval($this->_post('obj_id'));
        if ($service_apply_id) {
            $service_apply_obj = new ServiceApplyModel();
            $success = $service_apply_obj->delServiceApply($service_apply_id);
            exit($success ? 'success' : 'failure');
        }
        exit('failure');
    }

    public function batch_delete_service_apply()
    {

        $service_apply_ids = $this->_post('obj_ids');
        if ($service_apply_ids) {
            $service_apply_ary = explode(',', $service_apply_ids);
            $success_num     = 0;
            foreach ($service_apply_ary as $service_apply_id) {
                $service_apply_obj = new ServiceApplyModel();
                $success_num += $service_apply_obj->delServiceApply($service_apply_id);
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }

        exit('failure');
    }



}
