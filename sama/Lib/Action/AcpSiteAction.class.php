<?php
/**
 * Acp后台场地
 */
class AcpSiteAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
        $this->assign('action_title', '场地列表');
        $this->assign('action_src', U('/AcpSite/get_site_list'));
    }
    /**
     * 接收搜索表单数据，组织返回where子句
     * @author 姜伟
     * @param void
     * @return void
     * @todo 接收表单提交的参数，过滤合法性，组织成where子句并返回
     */
    public function get_search_condition()
    {
        //初始化查询条件
        $where = '';


        //id
        $site_id = $this->_request('site_id');
        if ($site_id) {
            $where .= ' AND site_id = ' . $site_id;
        }

        //联系人名称
        $user_name = $this->_request('user_name');
        if ($user_name) {
            $where .= ' AND user_name LIKE "%' . $user_name . '%"';
        }

        //手机号
        $mobile = $this->_request('mobile');
        if ($mobile) {
            $where .= ' AND mobile LIKE "%' . $mobile . '%"';
        }


        //添加时间范围起始时间
        $start_date = $this->_request('start_date');
        $start_date = str_replace('+', ' ', $start_date);
        $start_date = strtotime($start_date);
        if ($start_date) {
            $where .= ' AND addtime >= ' . $start_date;
        }

        //添加时间范围结束时间
        $end_date = $this->_request('end_date');
        $end_date = str_replace('+', ' ', $end_date);
        $end_date = strtotime($end_date);
        if ($end_date) {
            $where .= ' AND addtime <= ' . $end_date;
        }

        //重新赋值到表单
        $this->assign('start_date', $start_date ? $start_date : '');
        $this->assign('end_date', $end_date ? $end_date : '');
        $this->assign('user_name', $user_name);
        $this->assign('mobile', $mobile);

        return $where;
    }


    public function get_site_list()
    {
        $site_obj = new SiteModel();
        $where     = 'status !=2';
        $where.=$this->get_search_condition();
        //数据总量
        $total = $site_obj->getSiteNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $site_obj->setStart($Page->firstRow);
        $site_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);



        $site_list = $site_obj->getSiteList($where,'serial');
        $site_list = $site_obj->getListData($site_list);
        $this->assign('site_list', $site_list);
        $this->assign('head_title', '场地列表');
        $this->display();

    }

    //添加场地
    public function add_site()
    {

        $act = $this->_post('act');
        if ($act == 'add') {
            $_post      = $this->_post();
            $site_name = $_post['site_name'];
            $incubator_id     = $_post['incubator_id'];
            $site_type_id     = $_post['site_type_id'];
            $price     = $_post['price'];
            $address     = $_post['address'];
            $people      = $_post['people'];
            $user_name  = $_post['user_name'];
            $mobile  = $_post['mobile'];
            $start_time  = $_post['start_time'];
            $end_time  = $_post['end_time'];
            $serial  = $_post['serial'];
            $site_sketch  = $_post['site_sketch'];
            $pic_arr  = $_post['pic'];
            $site_facility_ids = $_post['site_facility_ids'];
            $site_activity_ids = $_post['site_activity_ids'];

            //表单验证
            if (!$site_name) {
                $this->error('请填写场地名称！');
            }
            if(!$incubator_id){
                $this->error('请选择孵化器！');
            }
            if(!$site_type_id){
                $this->error('请选择场地类型！');
            }
            if(!is_numeric($price)){
                $this->error('请输入价格！');
            }
            if(!$site_type_id){
                $this->error('请输入场地地址！');
            }
            if(!ctype_digit($people)){
                $this->error('请输入人数！');
            }
            if(!$user_name){
                $this->error('请输入联系人名称！');
            }
            if(!$mobile){
                $this->error('请输入联系人方式！');
            }
            if(!$site_sketch){
                $this->error('请填写场地简介！');
            }
            if(!$pic_arr){
                $this->error('请添加场地图片！');
            }
            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }
            if(!$site_facility_ids){
                $this->error('请选择配套设施！');
            }
            if(!$site_activity_ids){
                $this->error('请选择支持活动！');
            }
            if(!$start_time){
                $this->error('请选择开始时间！');
            }
            if(!$end_time){
                $this->error('请选择结束时间！');
            }
            if($end_time<$start_time){
                $this->error('结束时间不能小于开始时间！');
            }

            $pic_str = implode(',',$pic_arr);
            $site_facility_str = implode(',',$site_facility_ids);
            $site_activity_str = implode(',',$site_activity_ids);
            //转换成小时
            $start_time=strtotime($start_time)-strtotime(date('Y-m-d'),time());
            $end_time=strtotime($end_time)-strtotime(date('Y-m-d'),time());

            $arr = array(
                'site_name' => $site_name,
                'incubator_id'     => $incubator_id,
                'site_type_id'      => $site_type_id,
                'price'   => $price,
                'address'   => $address,
                'people'   => $people,
                'user_name'   => $user_name,
                'mobile'   => $mobile,
                'start_time'   => $start_time,
                'end_time'   => $end_time,
                'serial'   => $serial,
                'site_sketch'   => $site_sketch,
                'pic'   => $pic_str,
                'site_facility_ids'   => $site_facility_str,
                'site_activity_ids'   => $site_activity_str,
            );
            $site_obj = new SiteModel();
            $success   = $site_obj->addSite($arr);

            if ($success) {
                $this->success('恭喜您，场地添加成功！', '/AcpSite/add_site');
            } else {
                $this->error('抱歉，场地添加失败！', '/AcpSite/add_site');
            }
        }

        //图片
        $this->assign('pic_arr', array(
            'batch' => true,
            'name'  => 'pic',
            'title' => '图片',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));


        //配套设施
        $site_deploy_obj = new SiteDeployModel();
        $site_facility_list = $site_deploy_obj->getSiteDeployList('site_deploy_type ='.SiteDeployModel::FACILITY);
        $this->assign('site_facility_list', $site_facility_list);

        //支持活动
        $site_deploy_obj = new SiteDeployModel();
        $site_activity_list = $site_deploy_obj->getSiteDeployList('site_deploy_type ='.SiteDeployModel::ACTIVITY);
        $this->assign('site_activity_list', $site_activity_list);

        //孵化器
        $incubator_obj = new IncubatorModel();
        $incubator_list = $incubator_obj->getIncubatorList();
        $this->assign('incubator_list', $incubator_list);

        //场地类型
        $site_type_obj = new TypeModel();
        $site_type_list = $site_type_obj->getTypeList('type_type ='.TypeModel::SITE);
        $this->assign('site_type_list', $site_type_list);

        $this->assign('head_title', '添加场地');
        $this->display();
    }



    //修改场地
    public function edit_site()
    {
        $redirect = U('/AcpSite/get_site_list');
        $site_id = intval($this->_get('site_id'));
        if (!$site_id) {
            $this->error('对不起，非法访问！', $redirect);
        }

        $site_obj  = new SiteModel();
        $site_info = $site_obj->getSiteInfo('site_id ='.$site_id);

        if (!$site_info) {
            $this->error('对不起，不存在相关场地！', $redirect);
        }

        $act = $this->_post('act');

        if ($act == 'edit') {
            $_post      = $this->_post();
            $site_name = $_post['site_name'];
            $incubator_id     = $_post['incubator_id'];
            $site_type_id     = $_post['site_type_id'];
            $price     = $_post['price'];
            $address     = $_post['address'];
            $people      = $_post['people'];
            $user_name  = $_post['user_name'];
            $mobile  = $_post['mobile'];
            $start_time  = $_post['start_time'];
            $end_time  = $_post['end_time'];
            $serial  = $_post['serial'];
            $site_sketch  = $_post['site_sketch'];
            $pic_arr  = $_post['pic'];
            $site_facility_ids = $_post['site_facility_ids'];
            $site_activity_ids = $_post['site_activity_ids'];

            if (!$site_name) {
                $this->error('请填写场地名称！');
            }
            if(!$incubator_id){
                $this->error('请选择孵化器！');
            }
            if(!$site_type_id){
                $this->error('请选择场地类型！');
            }
            if(!is_numeric($price)){
                $this->error('请输入价格！');
            }
            if(!$site_type_id){
                $this->error('请输入场地地址！');
            }
            if(!ctype_digit($people)){
                $this->error('请输入人数！');
            }
            if(!$user_name){
                $this->error('请输入联系人名称！');
            }
            if(!$mobile){
                $this->error('请输入联系人方式！');
            }
            if(!$site_sketch){
                $this->error('请填写场地简介！');
            }
            if(!$pic_arr){
                $this->error('请添加场地图片！');
            }
            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }
            if(!$site_facility_ids){
                $this->error('请选择配套设施！');
            }
            if(!$site_activity_ids){
                $this->error('请选择支持活动！');
            }
            if(!$start_time){
                $this->error('请选择开始时间！');
            }
            if(!$end_time){
                $this->error('请选择结束时间！');
            }
            if($end_time<$start_time){
                $this->error('结束时间不能小于开始时间！');
            }

            //数组转换成字符串
            $pic_str = implode(',',$pic_arr);
            $site_facility_str = implode(',',$site_facility_ids);
            $site_activity_str = implode(',',$site_activity_ids);

            //转换成小时
            $start_time=strtotime($start_time)-strtotime(date('Y-m-d'),time());
            $end_time=strtotime($end_time)-strtotime(date('Y-m-d'),time());

            $arr = array(
                'site_name' => $site_name,
                'incubator_id'     => $incubator_id,
                'site_type_id'      => $site_type_id,
                'price'   => $price,
                'address'   => $address,
                'people'   => $people,
                'user_name'   => $user_name,
                'mobile'   => $mobile,
                'start_time'   => $start_time,
                'end_time'   => $end_time,
                'serial'   => $serial,
                'site_sketch'   => $site_sketch,
                'pic'   => $pic_str,
                'site_facility_ids'   => $site_facility_str,
                'site_activity_ids'   => $site_activity_str,
            );

            $url       = '/AcpSite/edit_site/site_id/' . $site_id;

            if ($site_obj->setSite($site_id, $arr)) {
                $this->success('恭喜您，场地修改成功！', $url);
            } else {
                $this->error('抱歉，场地修改失败！', $url);
            }
        }
        $pic_arr = explode(',',$site_info['pic']);

        //图片
        $this->assign('pic_arr', array(
            'batch' => true,
            'name'  => 'pic',
            'title' => '图片',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
            'pic_arr'=>$pic_arr,
        ));

        //配套设施
        $site_deploy_obj = new SiteDeployModel();
        $site_facility_list = $site_deploy_obj->getSiteDeployList('site_deploy_type ='.SiteDeployModel::FACILITY);
        $this->assign('site_facility_list', $site_facility_list);

        //支持活动
        $site_deploy_obj = new SiteDeployModel();
        $site_activity_list = $site_deploy_obj->getSiteDeployList('site_deploy_type ='.SiteDeployModel::ACTIVITY);
        $this->assign('site_activity_list', $site_activity_list);

        //现有的设施和活动
        $facility_arr = explode(',',$site_info['site_facility_ids']);
        $activity_arr = explode(',',$site_info['site_activity_ids']);
        $this->assign('facility_arr', $facility_arr);
        $this->assign('activity_arr', $activity_arr);

        //时间转换
//        dump($site_info['start_time']);
//        dump(date('H:i',$site_info['start_time']));
//        dump(8*3600);
        $site_info['start_hour'] = $site_info['start_time']-8*3600;
        $site_info['end_hour'] = $site_info['end_time']-8*3600;

        //孵化器
        $incubator_obj = new IncubatorModel();
        $incubator_list = $incubator_obj->getIncubatorList();
        $this->assign('incubator_list', $incubator_list);

        //场地类型
        $site_type_obj = new TypeModel();
        $site_type_list = $site_type_obj->getTypeList('type_type ='.TypeModel::SITE);
        $this->assign('site_type_list', $site_type_list);

        $this->assign('site_info', $site_info);
        $this->assign('head_title', '修改场地');
        $this->display();

    }
    /**
     * 场地详情
     * @author 姜伟
     * @param void
     * @return void
     */
    public function detail_site(){





    }

    /**
     * 删除场地
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据场地ID删除场地
     */
    public function delete_site()
    {
        $site_id = intval($this->_post('obj_id'));
        if ($site_id) {
            $site_obj = new SiteModel($site_id);

            $success = $site_obj->setSite($site_id,array('status'=> 2));
            exit($success ? 'success' : 'failure');
        }

        exit('failure');
    }

    public function batch_delete_site()
    {

        $site_ids = $this->_post('obj_ids');
        if ($site_ids) {
            $site_ary = explode(',', $site_ids);
            $success_num     = 0;
            foreach ($site_ary as $site_id) {
                $site_obj = new SiteModel($site_id);
                $success_num += $site_obj->setSite($site_id,array('status'=> 2));
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }

        exit('failure');
    }


}
