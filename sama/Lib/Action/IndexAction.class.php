<?php
class IndexAction extends FrontAction
{
	function _initialize() 
	{
		if (ACTION_NAME != 'js')
		{
			parent::_initialize();
		}
	}

	//wx-js
	function js()
	{
		//获取jsapi-ticket
		Vendor('Wxin.WeiXin');
		$appid = C('appid');
		$secret = C('appsecret');
		$wx_obj = new WxApi();
		$access_token = WxApi::getAccessToken($appid, $secret);
		$result = $wx_obj->getJsapiTicket($access_token);
		$ticket = $result['ticket'];
        $user_id = intval(session('user_id'));
        $user_obj = new UserModel($user_id);
        $arr = array(
            'ticket'	=> $ticket
        );
        $user_obj->setUserInfo($arr);
        $user_obj->saveUserInfo();
        $url = 'http://' . $_SERVER['HTTP_HOST'] . '/Index/js';
        vendor('wxpay.WxPayPubHelper');
        $address_obj = new Address();
        $params = $address_obj->getParametersAll($ticket, $url, $appid);
        #echo "<pre>";
        #print_r($params);
        #die;
		$this->assign('params', $params);
        log_file($params['signature']);
		$this->assign('head_title', '微信JS-SDK测试');
		$this->display();
	}

	//unserialize
	function unserialize()
	{
		if (isset($_POST['submit']))
		{
			echo "<pre>反序列化值：";
			print_r(unserialize($_POST['str']));
		}
		$this->display();
	}

	//解析json
	function parse_json()
	{
		print_r(json_decode('{"code":42037,"error_msg":"\u62a2\u5355\u5931\u8d25"}', true));
		if (isset($_POST['submit']))
		{
			echo "<pre>json值：";
			print_r(json_decode($_POST['str']), true);
		}
		$this->display();
	}

	//MD5测试
	function getmd5()
	{
		if (isset($_POST['submit']))
		{
			echo "<h1>MD5值：" . md5($_POST['str']) . "</h1>";
		}
		$this->display();
	}

	//静态页跳转方法
	function redirect($url)
	{
		if (!empty($_POST))
		{
			redirect($url);
		}
	}

	//首页
	public function index()
	{
		//if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger'))
	//	{
			redirect('/FrontIndex/sama_index');
		//}
$this->assign('head_title', '盈软科技');
$this->display();
	}

	/**
     * 企业付款测试
     */
    public function rebate()
    {
	    vendor("wxpay.WxPayPubHelper");
        $mchPay = new WxMchPay();
        // 用户openid
	$user_id = cur_user_id();
	$user_obj = new UserModel($user_id);
	$user_info = $user_obj->getUserInfo('openid');
	$mchPay->setParameter('openid', 'o7bmws_dlX6MYz1W_zguVlRsUOYA');
        // 商户订单号
        $mchPay->setParameter('partner_trade_no', 'test-'.time());
        // 校验用户姓名选项
        $mchPay->setParameter('check_name', 'NO_CHECK');
        // 企业付款金额  单位为分
        $mchPay->setParameter('amount', 100);
        // 企业付款描述信息
        $mchPay->setParameter('desc', '开发测试');
        // 调用接口的机器IP地址  自定义
        $mchPay->setParameter('spbill_create_ip', '127.0.0.1'); # getClientIp()
        // 收款用户姓名
        // $mchPay->setParameter('re_user_name', 'Max wen');
        // 设备信息
        // $mchPay->setParameter('device_info', 'dev_server');

        $data = $mchPay->getResult();
log_file('pay_result = ' . json_encode($data), 'qiye_pay', true);
        if( !empty($data) )
	{
		if ($data['result_code'] == 'FAIL')
		{
			echo $data['err_code_des'];
		}
		dump($data);
		return $data;
	}
    }


    /*********************************\
    *             SAMA                *
    \*********************************/
    //首页
    public function sama_index(){
        $this->display();
    }
    //消息中心
    public function message(){
        $this->display();
    }
    //回复我的
    public function reply_me(){
        $this->display();
    }
    //@我
    public function at_me(){
        $this->display();
    }
    //用户私信
    public function personal_letter(){
        $this->display();
    }
    //官方消息
    public function official_information(){
        $this->display();
    }

    //官方消息详情
    public function information_detail(){
         $this->display();
    }

    //频道介绍是通用的
    public function channel_info(){
        $this->display();
    }
    //提交审核是通用的
    public function submit_review(){
        $this->display();
    }
    //发帖子
    public function publish_post(){
        $this->display();
    }
    //心愿首页
    public function zhonchou_home(){
        $this->display();
    }
    //心愿详情
    public function zhonchou_detail(){
        $this->display();
    }
    //楼主页面-楼主回帖
    public function louzhu_huitie(){
       $this->display();
    }
    // 楼主看到的页面
    public function louzhu(){
        $this->display();
    }
    // 心愿-楼主回帖
    public function louzhou_reply(){
        $this->display();
    }
    //发布心愿帖
    public function post_zc(){
        $this->display();
    }
   


    //频道首页
    public function channel_home(){
        $this->display();
    }
    //频道中心
    public function channel_center(){
        $this->display();
    }
    //搜索
    public function channel_search(){
        $this->display();
    }



    //赞赏频道首页
    public function zanshang_home(){
        $this->display();
    }
    //赞赏频道介绍
    public function zanshang_info(){
        $this->display();
    }
    //赞赏帖详情-普通用户看到的
    public function zanshang_detail(){
        $this->display();
    }
    //赞赏贴 - 鉴定师看到的
    public function jiandin_detail(){
        $this->display();
    }
    // 赞赏贴 -楼主自己看到的
    public function zanshang_lz(){
        $this->display();
    }
    // 赞赏贴 -发布赞赏帖子
    public function post_zs(){
        $this->display();
    }
    //赞赏贴 -我要评分
    public function jiandin_df(){
        $this->display();
    }


    //悬赏频道
    public function xuanshang_home(){
        $this->display();
    }
    //发悬赏帖
    public function post_xs(){
        $this->display();
    } 
    //我关注的人
    public function my_follow(){
        $this->display();
    }
    //悬赏详情
     public function xuanshang_detail(){
        $this->display();
    }
    //我的回答
    public function my_answer(){
        $this->display();
    }
    //悬赏贴----楼主
    public function xuanshang_lz(){
        $this->display();
    }

    //通用频道模板主页
    public function common_channel(){
        $this->display();
    }
    //通用频道帖子
    public function common_tiezi(){
        $this->display();
    }
    //通用频道发帖
    public function post_tiezi(){
        $this->display();
    }

    //
    public function test(){
        $this->display();
    }







    // 他人个人中心主页
    public function others_personal_center()
    {
        $this->display();
    }
    //他的关注
    public function his_concern()
    {
        $this->display();
    }
    //他的粉丝
    public function his_fans()
    {
        $this->display();
    }
    //他的关注频道
    public function his_concernChannel()
    {
        $this->display();
    }
    //我的关注
    public function my_concern()
    {
        $this->display();
    }
    //我的粉丝
    public function my_fans()
    {
        $this->display();
    }
    //我参与的悬赏
    public function my_joined_reward()
    {
        $this->display();
    }
    //我赞赏的
    public function my_appreciated()
    {
        $this->display();
    }
    //我的心愿
    public function my_Crowd_funding()
    {
        $this->display();
    }
    //我的浏览记录
    public function my_scan_record()
    {
        $this->display();
    }
    //我的赞赏帖子
    public function my_post_zanshang()
    {
        $this->display();
    }
    //我的心愿帖子
    public function my_post_zhongchou()
    {
        $this->display();
    }
    //我的悬赏帖子
    public function my_post_xuanshang()
    {
        $this->display();
    }
    //我的普通帖子
    public function my_post_common()
    {
        $this->display();
    }
    //我的收藏
    public function my_collection()
    {
        $this->display();
    }
    //我的钱包
    public function my_wallet()
    {
        $this->display();
    }
    //余额充值
    public function recharge()
    {
        $this->display();
    }
    //提现
    public function withdraw()
    {
        $this->display();
    }
    //设置提现账户
    public function set_withdraw_account()
    {
        $this->display();
    }
    //输入提现账户
    public function input_withdraw_account()
    {
        $this->display();
    }
    //小额免密支付
    public function no_psw_pay()
    {
        $this->display();
    }
    //消费记录-支出
    public function consume_record_expenditrue()
    {
        $this->display();
    }
    //消费记录-收入
    public function consume_record_income()
    {
        $this->display();
    }
    //消费记录-充值
    public function consume_record_recharge()
    {
        $this->display();
    }
    //消费记录-提现
    public function consume_record_withdraw()
    {
        $this->display();
    }
    //我的个人中心
    public function my_personal_center()
    {
        $this->display();
    }
    //关于我们
    public function aboutus()
    {
        $this->display();
    }
    //头衔首页
    public function title_index()
    {
        $this->display();
    }
    //领取头衔
    public function get_title()
    {
        $this->display();
    }
    //我的资料
    public function my_data()
    {
        $this->display();
    }
    //系统设置
    public function system_set()
    {
        $this->display();
    }
    //账号安全
     public function account_safe()
    {
        $this->display();
    }
    //修改账号密码
     public function modify_account_psw()
    {
        $this->display();
    }
    //修改支付密码
     public function set_pay_psw()
    {
        $this->display();
    }
    //确认支付密码
     public function confirm_pay_psw()
    {
        $this->display();
    }
    //修改支付密码
     public function modify_pay_psw()
    {
        $this->display();
    }
    //设置支付密码成功
     public function set_success_pay_psw()
    {
        $this->display();
    }
    //忘记支付密码
     public function forget_pay_psw()
    {
        $this->display();
    }
    //隐私设置
     public function privacy_set()
    {
        $this->display();
    }
    //视频
     public function live()
    {
        $this->display();
    }
    //视频频道
     public function live_channel()
    {
        $this->display();
    }
    //申请特殊头衔
     public function apply_special_title()
    {
        $this->display();
    }
    //我的消费记录
     public function my_consume_record()
    {
        $this->display();
    }
    //意见反馈
     public function feedback()
    {
        $this->display();
    }
    //分享
     public function share()
    {
        $this->display();
    }
}
