<?php
/**
 * ProjectReport类
 */
class AcpProjectReportAction extends AcpAction
{
	public function _initialize()
	{
		parent::_initialize();
	}

	//搜索
	public function get_search(){
		//初始化搜索条件
		$where = '';

		$title = $this->_request('title');
		if($title){
			$where.=' AND title LIKE "%'.$title.'%"';
		}


		$begin_time =$this->_request('begin_time');
		$begin_time = str_replace('+','',$begin_time);
		$begin_time = strtotime($begin_time);
		if($begin_time){
			$where .= ' AND addtime >= ' . $begin_time;
		}

		$end_time =$this->_request('end_time');
		$end_time = str_replace('+','',$end_time);
		$end_time = strtotime($end_time);
		if($end_time){
			$where .= ' AND addtime <= ' . $end_time;
		}

		$this->assign('title',$title);
		$this->assign('begin_time',$begin_time);
		$this->assign('end_time',$end_time);

		return $where;
	}


	//项目申报列表
	public function get_project_report_list(){
//		$where = true;
		$where = $this->get_search();
		$project_report_obj = new ProjectReportModel();

		//分页处理
		import('ORG.Util.Pagelist');
		$count = $project_report_obj->getProjectReportNum($where);
		$Page  = new Pagelist($count, C('PER_PAGE_NUM'));
		$project_report_obj->setStart($Page->firstRow);
		$project_report_obj->setLimit($Page->listRows);
		$show = $Page->show();
		$this->assign('show', $show);

		$project_report_list = $project_report_obj->getProjectReportList($where);
		$project_report_list = $project_report_obj->getListData($project_report_list);
		$this->assign('project_report_list',$project_report_list);
		$this->assign('head_title','项目申报列表');
		$this->display();
	}

	//添加项目申报
	public function add_project_report(){

		$act = I('act');
		if($act == 'add'){
			$_post = $this->_post();
			$project_report_name = $_post['project_report_name'];
			$project_report_type_id = $_post['project_report_type_id'];
			$serial = $_post['serial'];
			$isuse = $_post['isuse'];
			$path_img = $_post['pic'];
			$province_id = $_post['province_id'];
			$city_id = $_post['city_id'];
			$area_id = $_post['area_id'];
			$contents = $_post['contents'];
			if(!$project_report_name){
				$this->error('请输入项目申报标题');
			}
			if(!$project_report_type_id){
				$this->error('请选择项目申报类型');
			}
			if(!$province_id){
				$this->error('请选择省份');
			}
			if(!$city_id){
				$this->error('请选择城市');
			}
			if(!$area_id){
				$this->error('请选择区域');
			}
			if(!$path_img){
				$this->error('请添加封面图');
			}

			$data = [
				'project_report_name' =>$project_report_name,
				'project_report_type_id' =>$project_report_type_id,
				'serial' =>$serial,
				'isuse' =>$isuse,
				'pic' =>$path_img,
				'province_id' =>$province_id,
				'city_id' =>$city_id,
				'area_id' =>$area_id,
				'contents' =>$contents,
				'addtime' =>time(),
			];

			$project_report_obj = new ProjectReportModel();
			$r = $project_report_obj ->addProjectReport($data);
			if($r){
				$this->success('项目申报添加成功!');
			}else{
				$this->error('项目申报添加成功!');
			}
		}
		//项目申报类型
		$project_report_type_obj = new TypeModel();
		$project_report_type_list = $project_report_type_obj->getTypeList('type_type ='.TypeModel::PROJECTREPORT);

		//省份
		$province_obj = new AddressProvinceModel();
		$province_list = $province_obj->getProvinceList();


		$this->assign('pic', array(
            'batch' => false,
			'name'  => 'pic',
			'title' => '封面图',
			'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
		));

		$this->assign('project_report_type_list',$project_report_type_list);
		$this->assign('province_list',$province_list);
		$this->assign('act','add');
		$this->assign('head_title','添加项目申报');
		$this->display();
	}

	//修改项目申报
	public function edit_project_report(){

		$id = I('id',0,'int');
		$redirect = U('/AcpProjectReport/get_project_report_list');
		if(!$id){
			$this->error('非法访问',$redirect);
		}
		$project_report_obj = new ProjectReportModel();
		$info = $project_report_obj->getProjectReportInfo('project_report_id ='.$id);
		if(!$info){
			$this->error('没有该项目申报,抱歉!',$redirect);
		}

		$act = I('act');
		if($act == 'edit') {
			$_post = $this->_post();
			$project_report_name = $_post['project_report_name'];
			$project_report_type_id = $_post['project_report_type_id'];
			$serial = $_post['serial'];
			$isuse = $_post['isuse'];
			$path_img = $_post['pic'];
			$province_id = $_post['province_id'];
			$city_id = $_post['city_id'];
			$area_id = $_post['area_id'];
			$contents = $_post['contents'];
			if (!$project_report_name) {
				$this->error('请输入项目申报标题');
			}
			if (!$project_report_type_id) {
				$this->error('请选择项目申报类型');
			}
			if (!$province_id) {
				$this->error('请选择省份');
			}
			if (!$city_id) {
				$this->error('请选择城市');
			}
			if (!$area_id) {
				$this->error('请选择区域');
			}
			if (!$path_img) {
				$this->error('请添加封面图');
			}

			$data = [
				'project_report_name' => $project_report_name,
				'project_report_type_id' => $project_report_type_id,
				'serial' => $serial,
				'isuse' => $isuse,
				'pic' => $path_img,
				'province_id' => $province_id,
				'city_id' => $city_id,
				'area_id' => $area_id,
				'contents' => $contents,
			];

			if($project_report_obj->setProjectReport($id,$data)){
				$this->success('项目申报修改成功!',$redirect);
			}else{
				$this->error('项目申报修改失败!',$redirect);
			}
		}

		//项目申报类型
		$project_report_type_obj = new TypeModel();
		$project_report_type_list = $project_report_type_obj->getTypeList('type_type ='.TypeModel::PROJECTREPORT);

		//省份
		$province_obj = new AddressProvinceModel();
		$province_list = $province_obj->getProvinceList();
		//城市
		$city_obj  = new AddressCityModel();
		$city_list = $city_obj->getCityList();
		//区域
		$area_list = M('AddressArea')->select();


		$this->assign('pic', array(
			'batch' => false,
			'name' => 'pic',
			'title' => '封面图',
			'url' => $info['pic'],
			'help' => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
		));

		$this->assign('project_report_type_list',$project_report_type_list);
		$this->assign('province_list',$province_list);
		$this->assign('city_list',$city_list);
		$this->assign('area_list',$area_list);
		$this->assign('act','edit');
		$this->assign('info',$info);
		$this->assign('head_title','修改项目申报');
		$this->display('add_project_report');
	}

	/**
	 * 删除项目申报
	 * @author 姜伟
	 * @param void
	 * @return void
	 * @todo 异步方法，根据ID删除
	 */
	public function delete_obj()
	{
		$obj_id = intval($this->_post('obj_id'));
		if ($obj_id) {
			$project_report_obj  = new ProjectReportModel();
			$success=$project_report_obj->delProjectReport($obj_id);
			exit($success ? 'success' : 'failure');
		}
		exit('failure');
	}

	public function batch_delete_obj()
	{

		$obj_ids = $this->_post('obj_ids');

		if ($obj_ids) {
			$obj_ids_ary = explode(',', $obj_ids);
			$success_num     = 0;
			foreach ($obj_ids_ary as $obj_id) {
				$project_report_obj = new ProjectReportModel();
				$success_num += $project_report_obj->delProjectReport($obj_id);
			}
			echo $success_num ? 'success' : 'failure';
			exit;
		}
		exit('failure');
	}

}
?>
