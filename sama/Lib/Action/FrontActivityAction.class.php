<?php

class FrontActivityAction extends FrontAction
{
    public $where;
    function _initialize()
    {
        parent::_initialize();
        $this->page_num = 3;
    }

    //活动列表
    function activity_list()
    {
       
        $sign = I('sign', 0, 'intval');
        $key_words= I('key_words');
        if($sign == 1){
            $user_id = session('user_id');
            $activity_ids = M('ActivitySign')->where('user_id ='.$user_id.' and is_check = 1')->getField('activity_id', true);
            $activity_ids[] = 0;
            $this->where = 'activity_id in('.implode(',', $activity_ids).')';
        }
        if($key_words){
            $this->where = 'activity_title LIKE "%'.$key_words.'%"';
        }
        $activity_obj = new ActivityModel();
        $total=$activity_obj->getActivityNum($this->where);
        $firstRow = intval($this->_request('firstRow'));
        $activity_obj->setStart($firstRow);
        $activity_obj->setLimit($this->page_num);
        $activity_list = $activity_obj->getActivityList('',$this->where,'sort');
        /*dump($activity_list);die;*/
        /*echo time();
        exit;*/
        foreach ($activity_list as $k=>$v){
            if($v['notice_date'] <= time() &&  time() < $v['start_date']){
                $status='预告中';
            }else if($v['start_date'] <= time() &&  time() < $v['end_date']){
                $status='进行中';
            }else{
                $status='已结束';
            }
            $activity_list[$k]['status']=$status;
        }
        //dump($activity_list);die;
        $activity_obj->setLimit($total);
        $activity_list_for_city=$activity_obj->getActivityList('',$this->where,'sort');

        foreach ($activity_list as $k => $v) {
            $picarr = explode(',', $v['detail_pic']);
            $activity_list[$k]['first_pic'] = $picarr[0];

        }
        foreach($activity_list_for_city as $k=>$v){
            $city_list[] = $v['city_id'];//1取活动的城市id
        }

        $city_list = array_unique($city_list);//去重
        $city_obj = new AddressCityModel();
        foreach ($city_list as $k => $v) {
            $city_list_name[$v] = $city_obj->getCityName($v);
        }
        $this->assign('sign', $sign);
        $this->assign('head_title', '活动');
        $this->assign('city_list_name', $city_list_name);
        $this->assign('activity_list', $activity_list);
        $this->assign('key_words',$key_words);
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $this->display();
    }

    //筛选活动列表
    function filter_activity_list()
    {
        $status = $this->_post('status');
        $city_id = $this->_post('city_id');
        $sign = I('sign', 0, 'intval');
        $key_words= I('key_words');
        /*echo $status;*/
        $activity_obj = new ActivityModel();
        $activity_obj->setLimit($this->page_num);
        $where = 1;
        if ($status) {
            //$where .= ' AND status =' . $status;
            if ($status == -1) {//-1代表全部，表里有个状态是0,-1是为了方便区分
                $where = 1;
            }
            if($status==1){//预告
                $where .= ' AND  notice_date <=' .time().' AND start_date >'.time();
            }
            if($status==3){//进行
                $where .= ' AND  start_date <=' .time().' AND end_date >'.time();
            }
            if($status==4){//结束
                $where .=' AND end_date <='.time();
            }

        }
        if ($city_id) {
            $where .= ' AND city_id =' . $city_id;
            if ($city_id == -1) {
                $where = 1;
            }
        }
        if($sign == 1){
            $user_id = session('user_id');
            $activity_ids = M('ActivitySign')->where('user_id ='.$user_id)->getField('activity_id', true);
            $activity_ids[] = 0;
            $where2 = 'activity_id in('.implode(',', $activity_ids).')';
            $where .= ' and '.$where2;
        }
        if($key_words){
          $where .= ' AND activity_title LIKE "%'.$key_words.'%"';
        }
        $total=$activity_obj->getActivityNum($where);
        $activity_list = $activity_obj->getActivityList('', $where,'sort');


        foreach ($activity_list as $k => $v) {
            $picarr = explode(',', $v['detail_pic']);
            $activity_list[$k]['first_pic'] = $picarr[0];
            if($v['notice_date'] <= time() &&  time() < $v['start_date']){
                $status='预告中';
            }else if($v['start_date'] <= time() &&  time() < $v['end_date']){
                $status='进行中';
            }else{
                $status='已结束';
            }
            $activity_list[$k]['status']=$status;
        }
        $return_data=array(
            'total'=>$total,
            'data'=>$activity_list,
        );
        echo json_encode($return_data);
        exit;
    }

    //活动列表的列表的下拉加载
    function activity_drop_loading(){
        $status = $this->_post('status');
        $city_id = $this->_post('city_id');
        $firstRow = $this->_post('firstRow');
        $sign = I('sign', 0, 'intval');
        $key_words= I('key_words');
        $where = 1;
        if ($status) {
           // $where .= ' AND status =' . $status;
            if ($status == -1) {//-1代表全部，表里有个状态是0,-1是为了方便区分
                $where = 1;
            }
            if($status==1){//预告
                $where .= ' AND  notice_date <=' .time().' AND start_date >'.time();
            }
            if($status==3){//进行
                $where .= ' AND  start_date <=' .time().' AND end_date >'.time();
            }
            if($status==4){//结束
                $where .=' AND end_date <='.time();
            }
        }
        if ($city_id) {
            $where .= ' AND city_id =' . $city_id;
            if ($city_id == -1) {
                $where = 1;
            }
        }
        if($sign == 1){
            $user_id = session('user_id');
            $activity_ids = M('ActivitySign')->where('user_id ='.$user_id)->getField('activity_id', true);
            $activity_ids[] = 0;
            $where2 = 'activity_id in('.implode(',', $activity_ids).')';
            $where .= ' and '.$where2;
        }

        if($key_words){
            $where .= ' AND activity_title LIKE "%'.$key_words.'%"';
        }

        $activity_obj = new ActivityModel();
        $total=$activity_obj->getActivityNum($where);

        $activity_obj->setStart($firstRow);
        $activity_obj->setLimit($this->page_num);
        $activity_list=$activity_obj->getActivityList('',$where,'sort');
        foreach ($activity_list as $k => $v) {
            $picarr = explode(',', $v['detail_pic']);
            $activity_list[$k]['first_pic'] = $picarr[0];
            if($v['notice_date'] <= time() &&  time() < $v['start_date']){
                $status='预告中';
            }else if($v['start_date'] <= time() &&  time() < $v['end_date']){
                $status='进行中';
            }else{
                $status='已结束';
            }
           // $v['status']=$status;
            $activity_list[$k]['status']=$status;
        }
        if($activity_list){
            $return_data=array(
                'code'=>1,
                'total'=>$total,
                'data'=>$activity_list,
            );
        }else{
            $return_data=array(
                'code'=>-1,
                'total'=>$total,
                'data'=>'',
            );
        }
        echo json_encode($return_data);
        exit;
    }

    //活动详情
    function activity_detail()
    {
        $activity_id = $this->_get('activity_id');
        
        $from = $this->_get('from');
        $activity_obj = new ActivityModel();
        $activity_info = $activity_obj->getActivityInfo('activity_id = ' . $activity_id);
        $pic_arr = explode(',', $activity_info['detail_pic']);//轮播图

        $comment_obj = new CommentModel();
        $where = 'reply_comment_id =0 and comment_type=3 AND type_id = ' . $activity_id;
        $total = $comment_obj->getCommentNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $comment_obj->setStart($firstRow);
        $comment_obj->setLimit($this->page_num);

        $comment_list = $comment_obj->getCommentList('', $where,'addtime desc');

        $comment_obj = new CommentModel();//因为上面有限制limit,这里获取回复内容的时候重新申请一个对象
        $user_obj = new UserModel();
        foreach ($comment_list as $k => $v) {
            $user_info = $user_obj->where('user_id = ' . $v['user_id'])->find();
            $handle_comment_list[$k]['comment']=$v;
            $handle_comment_list[$k]['comment']['portrait'] = $user_info['headimgurl'];
            $handle_comment_list[$k]['comment']['nickname'] = $user_info['realname'];
            $count=$comment_obj->getCommentNum('reply_comment_id = '.$v['comment_id']);
            $comment_obj->setLimit($count);
            $replay_list=$comment_obj->getCommentList('','reply_comment_id = '.$v['comment_id'],'addtime');
            $handle_comment_list[$k]['replay']=$replay_list;
        }

        /*echo json_encode($handle_comment_list);
        exit;*/
        //是否点赞
        $praise_obj = D('Praise');
        $praise_num = $praise_obj->getPraiseNum('user_id =' . session('user_id') . ' and id =' . $activity_id);
        if ($praise_num) {
            $is_praise = 1;
        } else {
            $is_praise = 0;
        }
        $this->assign('is_praise', $is_praise);

        //是否报名
        $activity_sign_obj = new ActivitySignModel();
        $activity_sign_info=$activity_sign_obj->getActivitySignInfo('activity_id = '.$activity_id.' AND user_id = '.intval(session('user_id')).' AND is_check != 2');
        if($activity_sign_info){
           if($activity_sign_info['pay_status'] == 0 && $activity_sign_info['is_check'] == 0){
               $is_sign = 0; // 待支付
           }elseif($activity_sign_info['pay_status'] == 1 && $activity_sign_info['is_check'] == 0){
               $is_sign = 1; // 待审核
           }elseif($activity_sign_info['pay_status'] == 1 && $activity_sign_info['is_check'] == 1){
               $is_sign = 2; // 已报名
           }
        }

        $activity_info['is_end'] = $activity_info['end_date'] < time() ? 1 : 0;
        
        $user_obj=new UserModel();
        $user_id=session('user_id');
        $user_info=$user_obj->getUserInfo('','user_id = '.$user_id);
        $cur_nickname=$user_info['realname'];
        $this->assign('is_sign', $is_sign);
        $this->assign('head_title', '活动详情');
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $this->assign('comment_list', $handle_comment_list);
        $this->assign('activity_info', $activity_info);
        $this->assign('activity_id', $activity_id);
        $this->assign('pic_arr', $pic_arr);
        $this->assign('from', $from?$from:0);
        $this->assign('cur_nickname',$cur_nickname);
        $this->display();
    }

    //异步获取活动的评论
    function async_get_activity()
    {
        $firstRow = $this->_request('firstRow');
        $activity_id = $this->_request('activity_id');
        $comment_obj = new CommentModel();
        $comment_obj->setStart($firstRow);
        $comment_obj->setLimit($this->page_num);

        $where = 'comment_type=3 AND type_id = ' . $activity_id;

        $comment_list = $comment_obj->getCommentList('', $where,'addtime desc');
        $user_obj = new UserModel();
        $comment_obj = new CommentModel();//因为上面有限制limit,这里获取回复内容的时候重新申请一个对象
        foreach ($comment_list as $k => $v) {
            $user_info = $user_obj->where('user_id = ' . $v['user_id'])->find();
            $handle_comment_list[$k]['comment']=$v;
            $handle_comment_list[$k]['comment']['portrait'] = $user_info['headimgurl'];
            $handle_comment_list[$k]['comment']['nickname'] = $user_info['realname'];
            $replay_list=$comment_obj->getCommentList('','reply_comment_id = '.$v['comment_id'],'addtime');
            $handle_comment_list[$k]['replay']=$replay_list;
        }
        echo json_encode($comment_list);
        exit;
    }


    //活动评论
    public function activity_comment(){

        $activity_id = I('activity_id', 0, 'intval');
        $this->assign('activity_id', $activity_id);
        $this->assign('head_title', '活动评论');
        $this->display();
    }

    //评论
    function comment()
    {
        $user_id = session('user_id');
        // $user_id=39609;
        $comment = I('comment');
        $activity_id = I('activity_id');
        if(!$user_id){
            $result_data=array(
                'code'=>-1,
                'msg'=>'请先登录',
            );
            $this->ajaxReturn($result_data);
            // echo json_encode($result_data);
            // exit;
        }

        if(!$comment){
            $result_data=array(
                'code'=>-1,
                'msg'=>'内容不能为空',
            );
            $this->ajaxReturn($result_data);
            // echo json_encode($result_data);
            // exit;
        }
        if(!$activity_id){
            $result_data=array(
                'code'=>-1,
                'msg'=>'活动id不能为空',
            );
            $this->ajaxReturn($result_data);
            // echo json_encode($result_data);
            // exit;
        }
        if($comment){
            $arr=array(
                'comment_type'=>3,
                'type_id'=>$activity_id,
                'addtime'=>time(),
                'contents'=>$comment,
                'user_id'=>$user_id,
            );
            $comment_obj=new CommentModel();
            $result=$comment_obj->addComment($arr);
            if($result){
                $result_data=array(
                    'code'=>1,
                    'msg'=>'评论成功',
                );
            }else{
                $result_data=array(
                    'code'=>-1,
                    'msg'=>'评论失败',
                );
            }
            $this->ajaxReturn($result_data);
            // echo json_encode($result_data);
            // exit;
        }
    }

    //活动的回复
    //to_nickname=被回复人名字,activity_id活动id,reply_comment_id被评论的评论id,contents内容
    function replay()
    {
        $to_nickname = trim($this->_post('to_nickname'));
        $activity_id = trim($this->_post('activity_id'));
        $contents = trim($this->_post('contents'));
        $reply_comment_id = trim($this->_post('reply_comment_id'));

        $user_id = intval(session('user_id'));
        
        if (!$user_id) {
            $result_data = array(
                'code' => -1,
                'msg' => '请先登录',
            );
            $this->ajaxReturn($result_data);
        }

        if ($contents) {
            $user_obj = new UserModel();
            $user_info = $user_obj->where('user_id = ' . $user_id)->find();
            $arr = array(
                'comment_type' => 3,
                'type_id' => $activity_id,
                'addtime' => time(),
                'contents' => $contents,
                'user_id' => $user_id,
                'to_nickname' => $to_nickname,
                'from_nickname' => $user_info['realname']?$user_info['realname']:'游客',
                'reply_comment_id'=>$reply_comment_id,
            );
            $comment_obj = new CommentModel();
            $result = $comment_obj->addComment($arr);
            if ($result) {
                $result_data = array(
                    'code' => 1,
                    'msg' => '回复成功',
                );
            } else {
                $result_data = array(
                    'code' => -1,
                    'msg' => '回复失败',
                );
            }
            $this->ajaxReturn($result_data);


        }
    }

    //报名
    function sign_up()
    {
        $activity_id = I('activity_id',0,'int');
        $data = $this->_post();
        $user_id = session('user_id');
        $activity_sign_obj = new ActivitySignModel();
//        $activity_sign_info=$activity_sign_obj->getActivitySignInfo('activity_id = '.$activity_id.' AND user_id = '.$user_id);

        $activity_obj = new ActivityModel();
        $activity_info = $activity_obj->getActivityInfo('activity_id ='.$activity_id);
            if ($data['opt'] == 'add') {
                $arr = array(
                    'user_id' => $user_id,
                    'order_sn' => 'HD'.$activity_sign_obj->generateOrderSn(),
                    'position' => $data['position'],
                    'real_name' => $data['real_name'],
                    'phone' => $data['phone'],
                    'company_name' => $data['company_name'],
                    'activity_id' => $data['activity_id'],
                    'unit_price' => $activity_info['sign_money'],
                    'pay_status'=> $activity_info['sign_money']== 0.00 ? 1 : 0,
                    'addtime' => time(),
                    'total_amount'=> $activity_info['sign_money'],
                );
                $result = $activity_sign_obj->addActivitySign($arr);
                if ($result) {

                    if($activity_info['sign_money'] == 0.00){
                        $return_data = array(
                            'code' => 1,
                            'msg' => '报名成功'
                        );
                    }else{
                        //有费用的活动,跳转订单页
                        $return_data = array(
                            'code' => 3,
                            'order_id' => $result,
                            'msg' => '报名成功,请前往支付报名费用!'
                        );
                    }
                } else {
                    $return_data = array(
                        'code' => -1,
                        'msg' => '报名失败'
                    );
                }
                echo json_encode($return_data);
                exit;
            }
        $this->assign('activity_id', $activity_id);
        $this->assign('head_title', '活动报名');
        $this->display();
    }

    //支付活动费用
    public function activity_order_detail(){
        // $activity_id = I('activity_id',0,'int');
        // $activity_obj = new ActivityModel();
        // $activity_info = $activity_obj ->getActivityInfo('activity_id ='.$activity_id);

        // $sign_obj = new ActivitySignModel();
        // $sign_info = $sign_obj->getActivitySignInfo('activity_id ='.$activity_id);
        $order_id = I('order_id', 0, 'intval');
        $sign_obj = new ActivitySignModel();
        $sign_info = $sign_obj->getActivitySignInfo('activity_sign_id ='.$order_id);

        $activity_obj = new ActivityModel();
        $activity_info = $activity_obj ->getActivityInfo('activity_id ='.$sign_info['activity_id']);

        $title = $sign_info['pay_status'] == 0 ? '确定订单' : '订单详情';
        $this->assign('activity_info',$activity_info);
        $this->assign('sign_info',$sign_info);
        $this->assign('order_id', $order_id);
        $this->assign('head_title', $title);
        $this->display();
    }

    //活动的签到
    function activity_sign(){
        $user_id=session('user_id');
       // $user_id=39454;
        $activity_id=$this->_request('activity_id');
        if(!$user_id){
            $return_data=array(
                'code'=>-1,
                'msg'=>'请先登录!'
            );
            echo json_encode($return_data);
            exit;
        }

        $activity_sign_obj = new ActivitySignModel();
        $activity_sign_info=$activity_sign_obj->getActivitySignInfo('activity_id = '.$activity_id.' AND user_id = '.$user_id);
        if($activity_sign_info){
            if($activity_sign_info['is_sign']==1){
                $return_data=array(
                    'code'=>-1,
                    'msg'=>'您已经签到了!'
                );
                echo json_encode($return_data);
                exit;
            }
            $result=$activity_sign_obj->editActivitySign($activity_sign_info['activity_sign_id'],array('is_sign'=>1));
            if($result){
                $return_data=array(
                    'code'=>1,
                    'msg'=>'签到成功!'
                );
            }else{
                $return_data=array(
                    'code'=>-1,
                    'msg'=>'签到失败!'
                );
            }

        }else{
            $return_data=array(
                'code'=>-1,
                'msg'=>'签到失败!您没有报名哦!'
            );
        }
        echo json_encode($return_data);
        exit;
    }


    //支付
    public function payment(){

        if(IS_POST && IS_AJAX){
            $order_id = I('order_id', 0, 'intval');
            $payway = I('payway');
            if(!$order_id || !$payway) $this->ajaxReturn(array('code'=>1));

            $order_obj = new ActivitySignModel();
            $order = $order_obj->getActivitySignInfo('activity_sign_id ='.$order_id);
            
            if($order['pay_status'] != 0 || $order['total_amount'] <= 0) $this->ajaxReturn(array('code'=>1));
            if($payway == 'wx_pay'){
                $wxpay_obj = new WXPayModel();
                $result = $wxpay_obj->mobile_pay_code($order_id, 0, 0, false, 'activity');
                $this->ajaxReturn(array('code'=>0,'parameter'=> $result));
            }elseif($payway == 'ali_pay'){
                $alipay_obj = new AlipayModel();
                $result = $alipay_obj->mobile_pay_code($order_id, 0, 'activity');
                $this->ajaxReturn(array('code'=> 0,'parameter'=> $result));
            }
            $this->ajaxReturn(array('code'=> 1));
        }
    }
}
