<?php
/**
 * Acp楼层
 */
class AcpStoreyAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
//        $this->assign('action_title', '楼层');
//        $this->assign('action_src', U('/AcpStorey/get_storey_list'));
    }

    //楼层列表
    public function get_storey_list($where,$title,$opt)
    {
        $incubator_id = I('incubator_id',0,'int');
        $storey_obj = new StoreyModel();
        //数据总量
        $total = $storey_obj->getStoreyNum('incubator_id ='.$incubator_id);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $storey_obj->setStart($Page->firstRow);
        $storey_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $storey_list = $storey_obj->getStoreyJoinList($where);

        $this->assign('storey_list', $storey_list);
        $this->assign('incubator_id', $incubator_id);
        $this->assign('head_title', $title);
        $this->assign('opt', $opt);
        $this->display('get_storey_list');
    }

    //园区楼层
    public function get_park_storey_list(){
        $incubator_id = I('incubator_id',0,'int');
        $this->get_storey_list('a.incubator_type = 1 and b.incubator_id ='.$incubator_id,'园区楼层列表','park_storey');
    }

    //众创空间楼层
    public function get_zhongchuang_storey_list(){
        $incubator_id = I('incubator_id',0,'int');
        $this->get_storey_list('a.incubator_type = 2 and b.incubator_id ='.$incubator_id,'众创空间楼层列表','zhongchuang_storey');
    }


    //添加楼层
    public function add_edit_storey($title)
    {
        $incubator_id = I('incubator_id',0,'int');
        $incubator_obj = new IncubatorModel();
        if($incubator_id){
            //1为园区2为众创空间
            $incubator_type = $incubator_obj->getIncubatorField('incubator_type','incubator_id ='.$incubator_id);
            $this->assign('incubator_type', $incubator_type);
            $this->assign('incubator_id', $incubator_id);
        }

        $act  = I('act');
        $storey_obj =new StoreyModel();
        $storey_id = I('storey_id',0,'int');
        if(!$storey_id){
            $this->assign('act', 'add');
        }else{
            $storey_info = $storey_obj->getStoreyInfo('storey_id ='.$storey_id);
            $incubator_type = $incubator_obj->getIncubatorField('incubator_type','incubator_id ='.$storey_info['incubator_id']);
            $this->assign('incubator_type', $incubator_type);
            $this->assign('storey_id', $storey_id);
            $this->assign('info', $storey_info);
            $this->assign('act', 'edit');
        }

        if(IS_POST){
            $_post = $this->_post();
            $storey_name  = $_post['storey_name'];
            $covered_area = floatval($_post['covered_area']);
            $usable_area  = floatval($_post['usable_area']);
            $rooms_num    = intval($_post['rooms_num']);
            $serial       = $_post['serial'];
            $storey_remark= $_POST['storey_remark'];
            $incubator_type=$_post['incubator_type'];

            if(!$storey_name) $this->error('请填写楼层名称');
            if(!$rooms_num) $this->error('请填写总数');

            if($rooms_num && !is_numeric($rooms_num)) $this->error('请填写正确总数');

            if($incubator_type == 1){
                if(!$covered_area) $this->error('请填写建筑面积');
                if(!$usable_area) $this->error('请填写实用面积');
                if($covered_area && !is_float($covered_area)) $this->error('请填写正确建筑面积');
                if($usable_area && !is_float($usable_area)) $this->error('请填写正确实用面积');
            }
            $data = array(
                'incubator_id' => $incubator_id,
                'storey_name' => $storey_name,
                'rooms_num' => $rooms_num,
                'serial' => $serial,
            );
            if($incubator_type == 1){
                $data['covered_area'] = $covered_area;
                $data['usable_area'] = $usable_area;
                $data['storey_remark'] = $storey_remark;
            }

            if($incubator_type == 1){
                $url = 'get_park_storey_list';
            }else{
                $url = 'get_zhongchuang_storey_list';
            }

            $add_url ='/AcpStorey/'.$url.'/incubator_id/'.$incubator_id;
            $edit_url ='/AcpStorey/'.$url.'/incubator_id/'.$storey_info['incubator_id'];
            if($act == 'add'){
                if($storey_obj->addStorey($data)){
                    $this->success('添加成功',$add_url);
                }
                    $this->error('添加失败');
            }
            if($act == 'edit'){
                unset($data['incubator_id']);
                if($storey_obj->setStorey($storey_id,$data)){
                    $this->success('修改成功',$edit_url);
                }
                    $this->error('修改失败');
            }
        }

        $this->assign('head_title', $title);
        $this->display('add_edit_storey');
    }

    //添加修改园区楼层
    public function add_edit_park_storey(){
        $storey_id = I('storey_id',0,'int');
        $title = '添加园区楼层';
        if($storey_id) $title = '修改园区楼层';
        $this->add_edit_storey($title);
    }

    //添加修改众创空间楼层
    public function add_edit_zhongchuang_storey(){
        $storey_id = I('storey_id',0,'int');
        $title = '添加园区楼层';
        if($storey_id) $title = '修改园区楼层';
        $this->add_edit_storey($title);
    }

    /**
     * 删除楼层
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据楼层ID删除楼层
     */
    public function batch_delete_storey()
    {

        $storey_ids = $this->_post('storey_ids');
        if ($storey_ids) {
            $storey_ary = explode(',', $storey_ids);
            $success_num     = 0;
            $station_obj  =  new  StationModel();
            foreach ($storey_ary as $storey_id) {
                $num = $station_obj->getStationNum('storey_id = ' . $storey_id);
                if ($num) {
                    continue;
                }

                $storey_obj = new StoreyModel($storey_id);
                $success_num += $storey_obj->delStorey();
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }

        exit('failure');
    }

    /**
     * 房间和工位列表
     */
    public function get_station_list($title,$opt){

        $storey_id = I('storey_id',0,'int');
        $station_obj = new StationModel();
        $where = 'storey_id ='.$storey_id;
        //数据总量
        $total = $station_obj->getStationNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, 10);
        $station_obj->setStart($Page->firstRow);
        $station_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $station_list = $station_obj->getStationList('',$where);
        if($opt == 'zhongchuang_station'){
            $station_list = $station_obj->getListData($station_list);
        }
        $this->assign('station_list', $station_list);
        $this->assign('storey_id', $storey_id);
        $this->assign('head_title', $title);
        $this->assign('opt', $opt);
        $this->display('get_station_list');

    }

    //园区房间
    public function park_station(){
        $this->get_station_list('园区房间列表','park_station');
    }

    //众创工位
    public function zhongchuang_station(){
        $this->get_station_list('众创空间工位列表','zhongchuang_station');
    }


    //添加房间和工位
    public function add_edit_station($title,$opt){

        $act = I('act');
        $storey_id =I('storey_id',0,'int');//楼层id
        $storey_obj = new StoreyModel();
        $incubator_id = $storey_obj->getStoreyField($storey_id,'incubator_id');//孵化器id

        $this->assign('storey_id',$storey_id);
        $this->assign('incubator_id',$incubator_id);

        


        $station_type_id = I('station_type_id',0,'int');// 房间工位id
        $station_obj  = new StationModel($station_type_id);
        if(!$station_type_id){
            $this->assign('act','add');
            if($opt == 'zhongchuang_station'){
                //根据孵化器获取工位类型
                $class_obj = D('StationClass');
                $class_list = D('StationClass')->getStationClassList('class_name, station_class_id', 'incubator_id ='.$incubator_id);
                $this->assign('class_list', $class_list);
            }
        }else{
            $station_info = $station_obj ->getStationInfo('station_type_id ='.$station_type_id);
            if($opt == 'zhongchuang_station'){
                //根据孵化器获取工位类型
                $class_obj = D('StationClass');
                $class_list = D('StationClass')->getStationClassList('class_name, station_class_id', 'incubator_id ='.$station_info['incubator_id']);
                $this->assign('class_list', $class_list);
            }
            $this->assign('station_type_id',$station_type_id);
            $this->assign('info',$station_info);
            $this->assign('act','edit');
        }

        if(IS_POST){
            $_post = $this->_post();

            $station_type_name = $_post['station_type_name'];//名称
            $price = floatval($_post['price']); //价格
            $covered_area = floatval($_post['covered_area']);//建筑面积
            $usable_area = floatval($_post['usable_area']);//实用面积
            $station_num = $_post['station_num'];//工位号
            $station_total_num = intval($_post['station_total_num']);//工位数
            $sort = $_post['sort'];//排序号
            $station_desc = $_post['station_desc'];//备注
            $storey_id = $_post['storey_id'];//楼层id
            $incubator_id = $_post['incubator_id'];//孵化器id
            $pic = $_post['pic'];//孵化器id

            
            

            $data =array(
                'station_type_name'=>$station_type_name,
                'price'=>$price,
                'sort'=>$sort,
                'station_desc'=>$station_desc,
                'storey_id'=>$storey_id,
                'incubator_id'=>$incubator_id,
            );

            if($opt == 'park_station'){
                if(!$price) $this->error('请输入价格');
                if($price && !is_float($price)) $this->error('请输入正确价格');
                if(!$station_type_name) $this->error('请输入名称');
                if(!$covered_area) $this->error('请输入建筑面积');
                if($covered_area && !is_float($covered_area)) $this->error('请输入正确建筑面积');
                if(!$usable_area) $this->error('请输入实用面积');
                if($usable_area && !is_float($usable_area)) $this->error('请输入正确实用面积');
                $data['covered_area'] =$covered_area;
                $data['usable_area'] =$usable_area;
                $data['types'] = 1;
            }

            if($opt == 'zhongchuang_station'){
                unset($data['station_type_name']);
                unset($data['price']);
                $station_class_id = I('station_class_id', 0, 'intval');
                if(!$station_class_id) $this->error('请选择工位类型');                

                if(!$station_num) $this->error('请输入工位号');
                // if(!$station_total_num) $this->error('请输入工位数量');
                // if($station_total_num && !is_numeric($station_total_num)) $this->error('请输入正确工位数量');
                // if(!$pic) $this->error('请添加工位图片');
                // $data['station_num'] =$station_num;
                // $data['station_total_num'] =$station_total_num;
                // $data['pic'] =$pic;
                $data['station_class_id'] =$station_class_id;
                $data['types'] = 2;
            }


            if($opt == 'park_station'){
                $url = 'park_station';
            }else{
                $url = 'zhongchuang_station';
            }

            $add_url ='/AcpStorey/'.$url.'/storey_id/'.$storey_id;
            $edit_url ='/AcpStorey/'.$url.'/storey_id/'.$station_info['storey_id'];

            if($act =='add'){
                if($station_obj->addStation($data)){
                    $this->success('添加成功',$add_url);
                }
                $this->error('添加失败');
            }

            if($act == 'edit'){
                unset($data['incubator_id']);
                unset($data['storey_id']);
                if($station_obj->editStation($data)){
                    $this->success('修改成功',$edit_url);
                }
                $this->error('修改失败');
            }
        }

        // 商品图片
        $this->assign('pic', array(
            'batch'   => false,
            'name'    => 'pic',
            'title'    => '图片',
            'url' => $station_info['pic'] ? $station_info['pic'] : '',
            'help'    => '点击查看大图',
        ));

        $this->assign('head_title',$title);
        $this->assign('opt',$opt);
        $this->display('add_edit_station');
    }


    //添加房间
    public function add_edit_park_station(){
        $station_type_id = I('station_type_id',0,'int');// 房间工位id
        $title = '添加房间';
        if($station_type_id) $title ='修改房间';
            $this->add_edit_station($title,'park_station');
    }

    //添加工位
    public function add_edit_zhongchuang_station(){
        $station_type_id = I('station_type_id',0,'int');// 房间工位id
        $title = '添加工位';
        if($station_type_id) $title ='修改工位';
        $this->add_edit_station($title,'zhongchuang_station');
    }


    /**
     * 删除工位
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据工位ID删除工位
     */
    public function batch_delete_station()
    {

        $station_type_ids = $this->_post('station_type_ids');
        if ($station_type_ids) {
            $station_ary = explode(',', $station_type_ids);
            $success_num     = 0;
            foreach ($station_ary as $station_type_id) {

                $station_obj  =  new  StationModel();
                $success_num += $station_obj->delStation('station_type_id ='.$station_type_id);
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }

        exit('failure');
    }


    /**
     * 获取孵化器下的楼层
     */
    public function get_ajax_storey(){
        $incubator_id =I('incubator_id',0,'int');
        $storey_obj = new StoreyModel();
        $storey_obj->setLimit(10000);
        $storey_list = $storey_obj->getStoreyList('incubator_id ='.$incubator_id);
        $this->ajaxReturn($storey_list);
    }

    /**
     * 获取楼层下的工位or房间
     */
    public function get_ajax_station(){
        $storey_id = I('storey_id',0,'int');
        // $select_station_ids = I('select_station_ids', '', 'strval');
        // $select_station_ids = trim($select_station_ids, ',');
        $station_obj = new StationModel();
        $station_obj->setLimit(10000);
        
        $opt = I('opt');
        $contract_obj = new ContractModel();
        $where = 'storey_id ='.$storey_id;
        if($opt == 'zhongchuang_contract'){
            //$station_list = $station_obj->getStationList('station_type_name, station_type_id, covered_area, price',$where, '', '', 'station_type_name');
            $storey = D('Storey')->where($where)->find();
            $station_list = D('StationClass')->getStationClassList('class_name, station_class_id, price', 'incubator_id ='.$storey['incubator_id']);
        }else{
            $station_list = $station_obj->getStationList('',$where);
        }
        
        // if($select_station_ids){
        //     $where .= ' and station_type_id not in('.$select_station_ids.')';
        // }

        
        // echo $station_obj->getLastSql();
        $this->ajaxReturn($station_list);
    }
}
