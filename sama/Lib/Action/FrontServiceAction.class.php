<?php

/**
 * Created by PhpStorm.
 * User: 辰
 * Date: 2017/3/13
 * Time: 9:39
 */
class FrontServiceAction extends FrontAction
{
    function _initialize()
    {
        parent::_initialize();
        $this->assign('nav','service');
        $this->page_num = 5;



    }

    //首页
    public function service_index(){
        //服务类型
        $service_type_obj = new ServiceTypeModel();
        $service_type_obj->setLimit(7);
        $service_type_list=$service_type_obj->getServiceTypeList('isuse = 1');
		foreach ($service_type_list AS $k => $v)
		{
			if ($v['service_label'] == 'changdi')
			{
				$link = '/FrontSite/get_site_list';
			}
			elseif ($v['service_label'] == 'chuangyedaoshi')
			{
				$link = '/FrontService/get_mentor_list';
			}elseif($v['service_label'] == 'xiangmushenbao'){

                $link = '/FrontService/get_project_list';
			}
			else
			{
				$link = '/FrontService/get_providers_list/service_type_id/' . $v['service_type_id'];
			}
			$service_type_list[$k]['link'] = $link;
		}
        $this->assign('service_type_list',$service_type_list);

        $where = 'is_recommend = 1 AND service_providers_status='.ServiceProvidersModel::APPROVED;

        $city_id = intval(session('city_id'));

        if(!$city_id){
            $address = getIPSource(get_client_ip());
            $city_id = $address['city_id'];
            session('city_id', $city_id);
        }


        $city_obj = new AddressCityModel();
        $city_name = $city_obj ->getCityName($city_id);
        $this ->assign('city',$city_name);

        if($city_id) {
            $where .= ' AND city_id =' . $city_id;
        }
//        dump($city_id);
        //推荐的创业导师
        $service_mentor_obj = new ServiceProvidersModel();
        $service_mentor_list=$service_mentor_obj->getServiceProvidersList($where.' AND service_providers_type = 1');
		log_file($service_mentor_obj->getLastSql(), 'ce', true);
        $service_mentor_list=$service_mentor_obj->getListData($service_mentor_list);
//        dump($service_mentor_list);die;
//        dump($service_mentor_obj->getLastSql());die;
        $this->assign('service_mentor_list',$service_mentor_list);

        //推荐的服务商
        $service_providers_obj = new ServiceProvidersModel();
        $service_providers_list=$service_providers_obj->getServiceProvidersList($where.' AND service_providers_type = 0');
        $service_providers_list=$service_providers_obj->getListData($service_providers_list);
//        dump($service_mentor_list);die;
//        dump($service_providers_obj->getLastSql());die;
        $this->assign('service_providers_list',$service_providers_list);


        $cust_flash_obj = new CustFlashModel();
        $cust_flash_list = $cust_flash_obj->getCustFlashList('', 'isuse = 1 and position = 3', 'serial asc');
        $this->assign('cust_flash_list', $cust_flash_list); 

        $this->assign('head_title','服务');

        $this->display();
    }


    //服务类型更多
    public function service_more(){
        $service_type_obj = new ServiceTypeModel();
        $service_type_list=$service_type_obj->getServiceTypeList('isuse = 1');
        foreach ($service_type_list AS $k => $v)
        {
            if ($v['service_label'] == 'changdi')
            {
                $link = '/FrontSite/get_site_list';
            }
            elseif ($v['service_label'] == 'chuangyedaoshi')
            {
                $link = '/FrontService/get_mentor_list';
            }elseif($v['service_label'] == 'xiangmushenbao'){

                $link = '/FrontService/get_project_list';
            }
            else
            {
                $link = '/FrontService/get_providers_list/service_type_id/' . $v['service_type_id'];
            }
            $service_type_list[$k]['link'] = $link;
        }
        $this->assign('service_type_list',$service_type_list);


        $this->assign('head_title','更多服务');
        $this->display();
    }


    //项目申报列表
    public function get_project_list(){
        $project_obj = new ProjectReportModel();
        $where = 'isuse = 1';

        $city_id = intval(session('city_id'));
        if(IS_AJAX && IS_POST){
            $city_id = I('city_id', 0, 'intval');
        }

        $project_type_id = I('project_type_id', 0, 'intval');

        if($project_type_id){
            $where .= ' and project_report_type_id='.$project_type_id;
        }

        if($city_id){
            $where .= ' and city_id ='.$city_id;
        }

        $total = $project_obj ->getProjectReportNum($where);
        $firstRow = I('firstRow',0,'int');
        $project_obj -> setStart($firstRow);
        $project_obj -> setLimit($this->page_num);
        $project_list = $project_obj ->getProjectReportList($where,'serial');
//        dump($project_obj->getLastSql());die;
        foreach($project_list as $k => $v){
            $project_list[$k]['add_time'] = date('Y-m-d',$v['addtime']);
        }

        if(IS_POST && IS_AJAX){
            $this->ajaxReturn(['project_list'=>$project_list,'total'=>$total]);
        }

        //类型
        $project_type_obj = new TypeModel();
        $project_type_list = $project_type_obj->getTypeList('type_type = '.TypeModel::PROJECTREPORT);

        //城市列表
        $city_ids_arr =  $project_obj->getGroupIdFields('city_id','isuse = 1','city_id');
        $city_str = implode(',',$city_ids_arr);
        $city_obj = new AddressCityModel();
        $city_list = $city_obj->getCityList('city_id IN('.$city_str.')');

        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('project_list',$project_list);
        $this->assign('city_list',$city_list);
        $this->assign('city_id',$city_id);
        $this->assign('project_type_list',$project_type_list);
        $this->assign('head_title','项目申报');
        $this->display();
    }


    //项目申报详情
    public function get_project_detail(){
        $redirect = U('/FrontService/get_project_list');

        $id  = intval($this->_get('id'));
        if (!$id) {
            $this->alert('对不起，非法访问！', $redirect);
        }

        $project_obj  = new ProjectReportModel();
        $info = $project_obj->getProjectReportInfo('project_report_id = ' . $id);
        if (!$info) {
            $this->alert('对不起，不存在相关项目申报！', $redirect);
        }

        $service_type_obj = new ServiceTypeModel();
        $service_type_id = $service_type_obj->where('service_label ="xiangmushenbao"')->getField('service_type_id');

        $this->assign('info',$info);
        $this->assign('service_type_id',$service_type_id);
        $this->assign('head_title','项目详情');
        $this->display();
    }


    //导师列表
    public function get_mentor_list(){

        //导师领域
        $domain_obj = new TypeModel();
        $domain_list = $domain_obj->getTypeList('type_type ='.TypeModel::DOMAIN);
        $this->assign('domain_list',$domain_list);
        //接收数值筛选查询
        $where = 'service_providers_type = 1 AND service_providers_status ='.ServiceProvidersModel::APPROVED;

        $key_words = I('key_words');
        if($key_words){
            $where.=' AND user_name LIKE "%'.$key_words.'%" ';
            $this->assign('key_words',$key_words);
        }

//        $city_id = intval(session('city_id'));
//
//        if(IS_AJAX && IS_POST){
//            $city_id = I('city_id', 0, 'intval');
//        }
//
//        if($city_id){
//            $where .= ' and city_id ='.$city_id;
//        }

        $domain_id = I('domain_id', 0, 'intval');

        if($domain_id){
            $where .= ' and find_in_set('.$domain_id.', domain)';
        }


        //创业导师列表
        $service_mentor_obj = new ServiceProvidersModel();

        $total = $service_mentor_obj->getServiceProvidersNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $service_mentor_obj ->setStart($firstRow);
        $service_mentor_obj ->setLimit($this->page_num);

        $service_mentor_list=$service_mentor_obj->getServiceProvidersList($where,'serial');
        $service_mentor_list=$service_mentor_obj->getListData($service_mentor_list);
        
        #dump($service_mentor_list);
        //城市列表
//        $city_ids_arr =  $service_mentor_obj->getServiceProvidersGroupFields('city_id','service_providers_type = 1 AND service_providers_status ='.ServiceProvidersModel::APPROVED,'city_id');
//        $city_str = implode(',',$city_ids_arr);
//        $city_obj = new AddressCityModel();
//        $city_list = $city_obj->getCityList('city_id IN('.$city_str.')');

        if(IS_POST && IS_AJAX){
            $this->ajaxReturn(array('mentor_list'=>$service_mentor_list,'total'=>$total));
        }

        $this->assign('firstRow',$this->page_num);
        $this->assign('total',$total);
//        $this->assign('city_list',$city_list);
//        $this->assign('city_id',$city_id);
        $this->assign('service_mentor_list',$service_mentor_list);
        $this->assign('head_title','创业导师');

        $this->display();

    }

    //导师详情
    public function mentor_detail(){
        $redirect = U('/FrontService/get_mentor_list');
        $service_providers_id  = intval($this->_get('service_providers_id'));
        if (!$service_providers_id) {
            $this->alert('对不起，非法访问！', $redirect);
        }
        $service_providers_obj  = new ServiceProvidersModel($service_providers_id);
        $service_providers_info = $service_providers_obj->getServiceProvidersInfo('service_providers_id = ' . $service_providers_id);
        if (!$service_providers_info) {
            $this->alert('对不起，不存在相关导师！', $redirect);
        }
        //所在城市
        $service_providers_info['city_name'] = D('AddressCity')->getCityName($service_providers_info['city_id']);
        //查询用用户
        $user_obj = new UserModel();
        $user_info=$user_obj->getUserInfo('position,sex','user_id='.$service_providers_info['user_id']);
        //职位
        $service_providers_info['position_name']=$user_info['position'];
        //性别
        $service_providers_info['sex']=$user_info['sex'];

        $service_providers_info['share_desc'] = strip_tags($service_providers_info['service_providers_describe']);
        $service_providers_info['share_desc'] = str_replace(array("\r\n", "\r", "\n", "&nbsp;", "\t", " "), "", $service_providers_info['share_desc']);
        $this->assign('service_providers_info',$service_providers_info);
        $this->assign('head_title','导师详情');
        $this->display();
    }

    //服务商列表
    public function get_providers_list(){

        $service_type_id = I('service_type_id',0,'int');
        $this->assign('service_type_id',$service_type_id);

        //存入历史搜索
        $where = 'service_providers_status = 1 AND service_providers_type='.ServiceProvidersModel::GENERAL;
        //接收搜索内容
        $search_name = I('search_name');
        if($search_name){
            $where  .= ' AND service_providers_name LIKE "%'.$search_name.'%"';
            $this->assign('search_name',$search_name);
        }
        //接收服务类型id
        $service_type_id = I('service_type_id',0,'int');

        //所有的服务类型列表
        $service_type_obj = new ServiceTypeModel();
        $service_type_list = $service_type_obj->getServiceTypeList('isuse = 1');
        $this->assign('service_type_list',$service_type_list);


        //接收数值筛选查询
        if($service_type_id != 0) {
            $where  .= ' AND service_type_id='.$service_type_id;
            $service_type_name = $service_type_obj->getServiceTypeField($service_type_id,'service_type_name');
        }

        $service_type_name = $service_type_name ? $service_type_name : '更多服务商';

        $city_id = intval(session('city_id'));
//        if(!$city_id){
//            $address = getIPSource(get_client_ip());
//            $city_id = $address['city_id'];
//            session('city_id', $city_id);
//        }

        if(IS_AJAX && IS_POST){
            $city_id = I('city_id', 0, 'intval');
        }

        $type_id = I('type_id', 0, 'intval');
        if($type_id){
            $where .= ' and service_type_id ='.$type_id;
        }

        if($city_id){
            $where .= ' and city_id ='.$city_id;
        }
        //服务商列表
        $service_providers_obj = new ServiceProvidersModel();
        $total = $service_providers_obj->getServiceProvidersNum($where);
        $firstRow  = intval($this->_request('firstRow'));
        $service_providers_obj->setStart($firstRow);
        $service_providers_obj->setLimit($this->page_num);
        $service_providers_list=$service_providers_obj->getServiceProvidersList($where,'serial');
        $service_providers_list=$service_providers_obj->getListData($service_providers_list);
//        var_dump($where);
//        var_dump($service_providers_obj->getLastSql());
//        var_dump($service_providers_list);die;

        if(IS_POST && IS_AJAX){
//            var_dump($service_providers_obj->getLastSql());die;
//            var_dump($service_providers_list);die;
            $this->ajaxReturn(array('providers_list'=>$service_providers_list,'total'=>$total));
        }

        //城市列表
        $city_ids_arr =  $service_providers_obj->getServiceProvidersGroupFields('city_id', 'service_providers_status = 1 AND service_providers_type='.ServiceProvidersModel::GENERAL,'city_id');
        $city_str = implode(',',$city_ids_arr);
        $city_obj = new AddressCityModel();
        $city_list = $city_obj->getCityList('city_id IN('.$city_str.')');

        $this->assign('city_id',$city_id);
        $this->assign('service_type_id',$service_type_id);
        $this->assign('total',$total);
        $this->assign('city_list',$city_list);
        $this->assign('firstRow',$this->page_num);
        $this->assign('service_providers_list',$service_providers_list);
        $this->assign('head_title',$service_type_name);
        $this->display();
    }

    //服务商详情
    public function providers_detail(){

        $redirect = U('/FrontService/get_providers_list');
        $service_providers_id  = intval($this->_get('service_providers_id'));
        if (!$service_providers_id) {
            $this->alert('对不起，非法访问！', $redirect);
        }
        $service_providers_obj  = new ServiceProvidersModel($service_providers_id);
        $service_providers_info = $service_providers_obj->getServiceProvidersInfo('service_providers_id = ' . $service_providers_id);
        if (!$service_providers_info) {
            $this->alert('对不起，不存在相关服务商！', $redirect);
        }

        $service_providers_info['sahre_desc'] = strip_tags($service_providers_info['service_providers_describe']);
        $service_providers_info['sahre_desc'] = str_replace(array("\r\n", "\r", "\n", "&nbsp;", "\t", " "), "", $service_providers_info['sahre_desc']);

        $this->assign('service_providers_info',$service_providers_info);
        $this->assign('head_title','服务商详情');
        $this->display();
    }

    //申请成为服务商
    public function apply_service_providers(){

        $user_id = intval(session('user_id'));
        $service_providers_obj = D('ServiceProviders');
        if(IS_AJAX && IS_POST){
//            var_dump($_POST) ;
            if($service_providers_obj->create()){
                $service_providers_obj->user_id = $user_id;
                $service_providers_obj->service_providers_status =ServiceProvidersModel::GENERAL;
                $service_providers_obj->addtime=time();
                if($service_providers_obj->add()){
                    $this->ajaxReturn(['status'=>0,'msg'=>'申请成功!']);
                }else{
                    $this->ajaxReturn(['status'=>1,'msg'=>'申请失败!']);
                }
            }else{
                $this->ajaxReturn(['status'=>1,'msg'=>$service_providers_obj->getError()]);
            }
        }

        //所有省
        $address_obj = new AddressProvinceModel();
        $province_list = $address_obj->getProvinceList();
        $this->assign('province_list',$province_list);

        $service_type_obj = new ServiceTypeModel();
        $service_type_list = $service_type_obj->getServiceTypeList();
        $this->assign('service_type_list',$service_type_list);
        $this->assign('head_title','申请服务商');
        $this->display();
    }

    //申请服务
    public function apply_service(){
        //服务商ID
        $service_providers_id  = I('service_providers_id',0,'int');
        //用户ID
        $user_id = intval(session('user_id'));
        //项目申报ID
        $service_type_id = I('get.service_type_id',0,'int');//值为1;

        if(!$service_type_id){
            $service_providers_obj  = new ServiceProvidersModel($service_providers_id);
            $service_providers_info = $service_providers_obj->getServiceProvidersInfo('service_providers_id = ' . $service_providers_id);
            $service_type_id = $service_providers_info['service_type_id'];
        }

        if(IS_AJAX && IS_POST) {

            if (!$user_id) {
                $this->ajaxReturn(['status'=>1,'msg'=>'请先登录!']);
            }
            $_post = $this->_post();
            $user_name = $_post['user_name'];
            $mobile = $_post['mobile'];
            $company_name = $_post['company_name'];
            $service_sketch = $_post['service_sketch'];
            $remark = $_post['remark'];
            $service_type_id = $_post['service_type_id'];
            if(!$user_name){
                $this->ajaxReturn(['status'=>1,'msg'=>'请输入用户名!']);
            }
            if(!$mobile || !checkMobile($mobile))
            {
                $this->ajaxReturn(['status'=>1,'msg'=>'对不起，手机号格式不正确!']);
            }
            if(!$company_name){
                $this->ajaxReturn(['status'=>1,'msg'=>'请输入公司名称!']);
            }
            if(!$service_sketch){
                $this->ajaxReturn(['status'=>1,'msg'=>'请输入申请服务描述!']);
            }
            $data=[
                'user_name'=>$user_name,
                'user_id'=>$user_id,
                'service_providers_id'=>$service_providers_id,
                'mobile'=>$mobile,
                'company_name'=>$company_name,
                'service_sketch'=>$service_sketch,
                'remark'=>$remark,
                'addtime'=>time(),
//                'service_type_id'=>$service_providers_info['service_type_id'] ? $service_providers_info['service_type_id'] : $service_type_id
                'service_type_id'=>$service_type_id
            ];

            $service_apply_obj = new ServiceApplyModel();
            $success = $service_apply_obj->addServiceApply($data);
            if($success){

                if($service_type_id == 1){
                    $project_report_obj = new ProjectReportModel();
                    //项目申报
                    $project_report_name = $project_report_obj -> getProjectReportField($service_providers_id,'project_report_name');
                    $their_name  = '项目申报'.'-'.$project_report_name;
                }else{
                    //如果有服务商显示服务商名称,不然显示导师名称
                    $their_name  = $service_providers_info['service_providers_name'] ? $service_providers_info['service_providers_name'] : $service_providers_info['user_name'];
                }

                $user_obj = new UserModel();
                $user_info = $user_obj->getUserInfo('realname','user_id ='.$user_id);
//                dump($user_info);die;
                $message_obj = new MessageModel();

                $title = $user_info['realname'].'申请了你的'.$their_name.'服务';
                $message_obj->addMessage($service_providers_id,MessageModel::SERVICE,$user_id,$service_providers_info['user_id'],$title,$title);

                $msg = '申请'.$their_name.'服务成功耐心等待';
                $message_obj->addMessage($service_providers_id,MessageModel::SERVICE,$user_id,$user_id,$msg,$msg);
//                dump($message_obj->getLastSql());die;

                $this->ajaxReturn(['status'=>0,'msg'=>'申请服务成功!']);
            }else{
                $this->ajaxReturn(['status'=>1,'msg'=>'申请服务失败!']);
            }
        }

        $this->assign('head_title','申请服务');
        $this->assign('service_providers_id',$service_providers_id);
        $this->assign('service_type_id',$service_type_id);
        $this->display();
    }


    public function search(){
        $hot_str=$GLOBALS['config_info']['HOT_SERVICE_NAME'];
        $hot_arr = explode(',',$hot_str);

        $user_id = intval(session('user_id'));

        if(IS_POST){
            $search_name=$this->_post('search_name');
            redirect('/FrontService/get_providers_list/search_name/' . $search_name);
        }
        //历史搜索
        $search_obj = new SearchHistoryModel();
        $search_list = $search_obj->getSearchHistoryList('user_id='.$user_id.' AND is_del = 0','search_history_id DESC','search_value');
//        DUMP($search_obj->getLastSql());

        $this->assign('search_list',$search_list);

        $this->assign('hot_arr',$hot_arr);
        $this->assign('head_title','搜索');
        $this->display();
    }






}
