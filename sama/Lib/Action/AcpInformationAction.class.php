<?php
/**
 * 资讯类
 */

class AcpInformationAction extends AcpAction
{
    public function AcpActivityAction()
    {
        parent::_initialize();
    }
    //资讯列表
    public function information_list(){
        import('ORG.Util.Pagelist');
        $where='1';
        $information_obj=new InformationArticleModel();

        $data=$this->_request();
        if($data['opt']=='select'){
            if($data['information_name']){
                $information_name=$data['information_name'];
                $where.=' AND title LIKE "%'.$information_name.'%"';
            }
            if($data['incubator_type']!=-1){
                $information_type=$data['incubator_type'];
                $where.=' AND isuse ='.$information_type;
            }
        }
        $count =  $information_obj->getInformationNum($where);
        $Page = new Pagelist($count, C('PER_PAGE_NUM'));

        $information_obj->setStart($Page->firstRow);
        $information_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('page', $Page);
        $this->assign('show', $show);

        $information_list=$information_obj->getInformationList('',$where,'serial asc,addtime DESC');
        foreach($information_list as $k=>$v){
            $information_list[$k]['time']=date('Y-m-d',$v['addtime']);
        }
        $this->assign('information_name',$information_name?$information_name:'');
        $this->assign('information_type',$information_type==''||$information_type==-1?-1:$information_type);
        $this->assign('information_list',$information_list);
        $this->assign('head_title', '资讯列表');
        $this->display();
    }
    //增加资讯
    public function add_information(){
        $data=$this->_post();
        if($data['opt']=='add'){
            if(!$data['contents']||!$data['information_name']){
                $this->error('添加失败,请填写完整内容');
            }
            if(!$data['article_sort_id']){
                $this->error('添加失败,请选择类型');
            }
            $arr=array(
                'title'=>$data['information_name'],
                'isuse'=>$data['is_hot']?$data['is_hot']:0,
                'path_img'=>$data['pic'],
                'serial'=>$data['sort'],
                'addtime'=>$data['addtime'] ? strtotime($data['addtime']) : time(),
                'article_sort_id'=>$data['article_sort_id'],
            );
            $information_obj=new InformationArticleModel();
            $result=$information_obj->addInformation($arr);//返回的是增加的文章id
            if($result){
            $article_txt_obj=new ArticleTxtModel();
            $article_txt_arr=array(
                    'article_id'=>$result,
                    'contents'=>$data['contents'],
                );
                $result= $article_txt_obj->addArticleTxt($article_txt_arr);
                if($result){
                    $this->success('添加成功',U('/AcpInformation/information_list'));
                }else{
                    $this->error('添加失败！');
                }
            }else{
                $this->error('添加失败！');
            }
        }
        $this->assign('pic_data', array(
            'batch' => false,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳',
            'url' => '',
        ));
         $information_type_obj=new ArticleSortModel();
         $information_type_list=$information_type_obj->getArticleSortList();
         $this->assign('information_type_list',$information_type_list);
         $this->assign('head_title', '添加资讯');
         $this->display();
    }
    //删除资讯
    public function del_information(){

        $information_id=$this->_post('information_id');
        $information_obj=new InformationArticleModel();
        $result=$information_obj->delInformation($information_id);
        if($result){
            echo 'success';
            exit;
        }else{
            echo 'fail';
            exit;
        }
    }
    //批量删除资讯
    public function del_information_batch(){
        $ids=$this->_post('id');

        $information_obj=new InformationArticleModel();
        $idarr=explode(',',$ids);
        array_pop($idarr);//删除元素最后一个元素
        foreach($idarr as $id){
            $r=$information_obj->delInformation($id);
            if(!$r){
                echo 'fail';
                exit;
            }
        }
        echo 'success';
        exit;
    }
    //编辑资讯
    public function edit_information(){
        $information_id=$this->_get('information_id');
        //资讯资料
        $information_obj=new InformationArticleModel();
        $information_info=$information_obj->getInformationInfo('article_id = '.$information_id);
        //资讯内容
        $information_txt_obj=new ArticleTxtModel();
        $information_txt_info=$information_txt_obj->getArticleTxtInfo('article_id = '.$information_id);

        $data=$this->_post();

        if($data['opt']=='edit') {
            if(!$data['contents']||!$data['information_name']){
                $this->error('添加失败,请填写完整内容');
            }
            if(!$data['article_sort_id']){
                $this->error('添加失败,请选择类型');
            }
            $arr = array(
                'title' => $data['information_name'],
                'isuse' => $data['is_hot'] ? $data['is_hot'] : 0,
                'path_img'=>$data['pic'],
                'serial'=>$data['sort'],
                'addtime' => $data['addtime'] ? strtotime($data['addtime']) : time(),
                'article_sort_id'=>$data['article_sort_id'],
            );
            $information_obj = new InformationArticleModel();
            $result1 = $information_obj->editInformation($arr,$data['information_id']);

            $article_txt_obj = new ArticleTxtModel();
            $article_txt_arr = array(
                'contents' => $data['contents'],
            );
            $result2 = $article_txt_obj->editArticleTxt($article_txt_arr,$data['information_id']);
            if($result2||$result1){
                $this->success('编辑成功', U('/AcpInformation/information_list'));
            }else{
                $this->error('编辑失败！');
            }
        }
        $this->assign('pic_data', array(
            'batch' => false,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳',
            'url' => $information_info['path_img'],
        ));
        $information_type_obj=new ArticleSortModel();
        $information_type_list=$information_type_obj->getArticleSortList();
        $this->assign('information_type_list',$information_type_list);
        $this->assign('information_info',$information_info);
        $this->assign('information_txt_info',$information_txt_info);
        $this->assign('head_title', '编辑资讯');
        $this->display();

    }
    //添加资讯分类
    function add_information_type(){
        $data=$this->_post();

        if($data['opt']=='add'){
            $information_name=$data['information_name'];
           $arr=array(
               'article_sort_name'=>$information_name,
               'serial'=>$data['serial'],
           );
            $information_type_obj=new ArticleSortModel();
            $result=$information_type_obj->addArticleSort($arr);
            if($result){
                $this->success('添加成功','/AcpInformation/information_type_list');
            }else{
                $this->error('添加失败');
            }
        }
        $this->assign('head_title', '添加资讯分类');
        $this->display();
    }
    function information_type_list(){

        import('ORG.Util.Pagelist');
        $where='1';
        $information_type_obj=new ArticleSortModel();

        $data=$this->_request();
        if($data['opt']=='select'){
            if($data['information_type_name']){
                $article_sort_name=$data['information_type_name'];
                $where.=' AND article_sort_name LIKE "%'.$article_sort_name.'%"';
            }

        }
        $count =  $information_type_obj->getArticleSortNum($where);
        $Page = new Pagelist($count, C('PER_PAGE_NUM'));

        $information_type_obj->setStart($Page->firstRow);
        $information_type_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('page', $Page);
        $this->assign('show', $show);

        $information_type_list=$information_type_obj->getArticleSortList('',$where);
        $this->assign('information_type_list',$information_type_list);
       /* echo json_encode($information_type_list);
        exit;*/
        $this->assign('article_sort_name',$article_sort_name);
        $this->assign('head_title', '资讯分类列表');
        $this->display();
    }
    function edit_information_type(){
        $information_type_id=$this->_get('information_type_id');
        $information_type_obj=new ArticleSortModel();
        $data=$this->_post();
        if($data['opt']=='edit'){
            $arr['article_sort_name']=$data['information_name'];
            $arr['serial']=$data['serial'];
            $result=$information_type_obj->editArticleSort($arr,$data['information_type_id']);
            if($result){
                $this->success('修改成功','/AcpInformation/information_type_list');
            }else{
                $this->error('修改失败');
            }
        }
        $information_type_info=$information_type_obj->getArticleSortInfo('article_sort_id = '.$information_type_id);
        $this->assign('information_type_id',$information_type_id);
        $this->assign('information_type_info',$information_type_info);
        $this->display();
    }
    function del_information_type(){
        $information_type_id=$this->_post('information_type_id');
        $information_type_obj=new ArticleSortModel();
        $result=$information_type_obj->delArticleSort($information_type_id);
        if($result){
            echo 'success';
            exit;
        }else{
            echo 'fail';
            exit;
        }
    }
    function delInformationTypeBatch(){
        $ids=$this->_post('id');

        $information_type_obj=new ArticleSortModel();
        $idarr=explode(',',$ids);
        array_pop($idarr);//删除元素最后一个元素
        foreach($idarr as $id){
            $r=$information_type_obj->delArticleSort($id);
            if(!$r){
                echo 'fail';
                exit;
            }
        }
        echo 'success';
        exit;
    }

}