<?php
/**
 * Acp后台场地支持活动活动
 */
class AcpSiteDeployAction extends AcpAction
{
    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
        $this->assign('action_title', '场地列表');
        $this->assign('action_src', U('/AcpSite/get_site_list'));
    }

    /**
     * 接收搜索表单数据，组织返回where子句
     * @author 姜伟
     * @param void
     * @return void
     * @todo 接收表单提交的参数，过滤合法性，组织成where子句并返回
     */
    public function get_search_condition()
    {
        //初始化查询条件
        $where = '';

        //服务商名称
        $site_deploy_name = $this->_request('site_deploy_name');
        if ($site_deploy_name) {
            $where .= ' AND site_deploy_name LIKE "%' . $site_deploy_name . '%"';
        }

        $this->assign('site_deploy_name', $site_deploy_name);

        return $where;
    }

    //配套设施列表
    public function get_site_deploy_facility_list()
    {
        $site_deploy_obj = new SiteDeployModel();
        $where     = ' site_deploy_type ='.SiteDeployModel::FACILITY;
        $where.=$this->get_search_condition();
        //数据总量
        $total = $site_deploy_obj->getSiteDeployNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $site_deploy_obj->setStart($Page->firstRow);
        $site_deploy_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $site_deploy_list = $site_deploy_obj->getSiteDeployList($where,'site_deploy_id');
//        dump($site_deploy_list);die;

        $this->assign('site_deploy_list', $site_deploy_list);
        $this->assign('head_title', '场地配套设施列表');
        $this->display();
    }




    //添加场地配套设施
    public function add_site_deploy_facility()
    {
        $act = $this->_post('act');
        if ($act == 'add') {
            $_post      = $this->_post();
            $site_deploy_name = $_post['site_deploy_name'];
            $site_deploy_pic     = $_post['site_deploy_pic'];
            $sort=$_post['sort'];

            //表单验证
            if (!$site_deploy_name) {
                $this->error('请填写场地配套设施活动名称！');
            }

            if (!$site_deploy_pic) {
                $this->error('请添加设施图标！');
            }

            $arr = array(
                'site_deploy_name' => $site_deploy_name,
                'site_deploy_pic'      => $site_deploy_pic,
                'site_deploy_type'      => SiteDeployModel::FACILITY,
                'sort'=>$sort
            );

            $site_deploy_obj = new SiteDeployModel();
            $success   = $site_deploy_obj->addSiteDeploy($arr);

            if ($success) {
                $this->success('恭喜您，场地配套设施添加成功！', '/AcpSiteDeploy/get_site_deploy_facility_list');
            } else {
                $this->error('抱歉，场地配套设施添加失败！', '/AcpSiteDeploy/get_site_deploy_facility_list');
            }
        }


        $this->assign('is_pic', array(
            'name'  => 'site_deploy_pic',
            'title' => '设施图标',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('head_title', '添加场地配套设施');
        $this->display();
    }

    //修改场地配套设施
    public function edit_site_deploy_facility()
    {
        $site_deploy_id = intval($this->_get('site_deploy_id'));

        $redirect = U('/AcpSiteDeploy/get_site_deploy_facility_list/');
        if (!$site_deploy_id) {
            $this->error('对不起，非法访问！', $redirect);
        }

        $site_deploy_obj  = new SiteDeployModel();
        $site_deploy_info = $site_deploy_obj->getSiteDeployInfo('site_deploy_id ='.$site_deploy_id);

        if (!$site_deploy_info) {
            $this->error('对不起，不存在相关场地配套设施！', $redirect);
        }

        $act = $this->_post('act');

        if ($act == 'edit') {
            $_post      = $this->_post();
            $site_deploy_name = $_post['site_deploy_name'];
            $site_deploy_pic     = $_post['site_deploy_pic'];
            $sort=$_post['sort'];
            //表单验证
            if (!$site_deploy_name) {
                $this->error('请填写场地配套设施活动名称！');
            }

            if (!$site_deploy_pic) {
                $this->error('请添加设施图标！');
            }

            $arr = array(
                'site_deploy_name' => $site_deploy_name,
                'site_deploy_pic'      => $site_deploy_pic,
                'sort'=>$sort
            );

            if ($site_deploy_obj->setSiteDeploy($site_deploy_id, $arr)) {
                $this->success('恭喜您，场地配套设施修改成功！', $redirect);
            } else {
                $this->error('抱歉，场地配套设施修改失败！', $redirect);
            }
        }

        $this->assign('is_pic', array(
            'name'  => 'site_deploy_pic',
            'title' => '设施图标',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
            'url'   => $site_deploy_info['site_deploy_pic'],
        ));


        $this->assign('site_deploy_info', $site_deploy_info);
        $this->assign('head_title', '修改场地配套设施');
        $this->display();

    }

    /**
     * 删除场地配套设施
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据场地配套设施活动ID删除场地配套设施活动
     */
    public function delete_deploy_facility()
    {
        $site_deploy_id = intval($this->_post('obj_id'));
        if ($site_deploy_id) {
            $site_deploy_obj = new SiteDeployModel($site_deploy_id);
            $success = $site_deploy_obj->delSiteDeploy($site_deploy_id);
            exit($success ? 'success' : 'failure');
        }
        exit('failure');
    }

    public function batch_delete_deploy_facility()
    {

        $site_deploy_ids = $this->_post('obj_ids');
        if ($site_deploy_ids) {
            $site_deploy_ary = explode(',', $site_deploy_ids);
            $success_num     = 0;
            foreach ($site_deploy_ary as $site_deploy_id) {
                $site_deploy_obj = new SiteDeployModel();
                $success_num += $site_deploy_obj->delSiteDeploy($site_deploy_id);
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }
        exit('failure');
    }


    //支持活动列表
    public function get_site_deploy_activity_list()
    {
        $site_deploy_obj = new SiteDeployModel();
        $where     = ' site_deploy_type ='.SiteDeployModel::ACTIVITY;
        $where.=$this->get_search_condition();
        //数据总量
        $total = $site_deploy_obj->getSiteDeployNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $site_deploy_obj->setStart($Page->firstRow);
        $site_deploy_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $site_deploy_list = $site_deploy_obj->getSiteDeployList($where);
//        dump($site_deploy_obj->getLastSql());
//        dump($site_deploy_list);

        $this->assign('site_deploy_list', $site_deploy_list);
        $this->assign('head_title', '场地支持活动列表');
        $this->display();
    }




    //添加场地支持活动
    public function add_site_deploy_activity()
    {
        $site_id = $this->_get('site_id');
        $act = $this->_post('act');
        if ($act == 'add') {
            $_post      = $this->_post();
            $site_deploy_name = $_post['site_deploy_name'];
            $site_deploy_pic     = $_post['site_deploy_pic'];

            //表单验证
            if (!$site_deploy_name) {
                $this->error('请填写场地支持活动活动名称！');
            }

            if (!$site_deploy_pic) {
                $this->error('请添加设施图标！');
            }

            $arr = array(
                'site_deploy_name' => $site_deploy_name,
                #'site_id'     => $site_id,
                'site_deploy_pic'      => $site_deploy_pic,
                'site_deploy_type'      => SiteDeployModel::ACTIVITY,
            );

            $site_deploy_obj = new SiteDeployModel();
            $success   = $site_deploy_obj->addSiteDeploy($arr);

            if ($success) {
                $this->success('恭喜您，场地支持活动添加成功！', '/AcpSiteDeploy/get_site_deploy_activity_list/site_id/'.$site_id);
            } else {
                $this->error('抱歉，场地支持活动添加失败！', '/AcpSiteDeploy/get_site_deploy_activity_list/site_id/'.$site_id);
            }
        }


        $this->assign('is_pic', array(
            'name'  => 'site_deploy_pic',
            'title' => '活动图标',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('head_title', '添加场地支持活动');
        $this->display();
    }



    //修改场地支持活动活动
    public function edit_site_deploy_activity()
    {
        $site_deploy_id = intval($this->_get('site_deploy_id'));
        $site_id = intval($this->_get('site_id'));
        $redirect = U('/AcpSiteDeploy/get_site_deploy_activity_list/site_id/'.$site_id);
        if (!$site_deploy_id) {
            $this->error('对不起，非法访问！', $redirect);
        }

        $site_deploy_obj  = new SiteDeployModel();
        $site_deploy_info = $site_deploy_obj->getSiteDeployInfo('site_deploy_id ='.$site_deploy_id);

        if (!$site_deploy_info) {
            $this->error('对不起，不存在相关场地支持活动！', $redirect);
        }

        $act = $this->_post('act');

        if ($act == 'edit') {
            $_post      = $this->_post();
            $site_deploy_name = $_post['site_deploy_name'];
            $site_deploy_pic     = $_post['site_deploy_pic'];

            //表单验证
            if (!$site_deploy_name) {
                $this->error('请填写场地支持活动活动名称！');
            }

            if (!$site_deploy_pic) {
                $this->error('请添加设施图标！');
            }

            $arr = array(
                'site_deploy_name' => $site_deploy_name,
                'site_deploy_pic'      => $site_deploy_pic,
            );

            if ($site_deploy_obj->setSiteDeploy($site_deploy_id, $arr)) {
                $this->success('恭喜您，场地支持活动修改成功！', $redirect);
            } else {
                $this->error('抱歉，场地支持活动修改失败！', $redirect);
            }
        }
        $this->assign('is_pic', array(
            'name'  => 'site_deploy_pic',
            'title' => '活动图标',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
            'url'   => $site_deploy_info['site_deploy_pic'],
        ));
        $this->assign('site_deploy_info', $site_deploy_info);
        $this->assign('head_title', '修改场地支持活动');
        $this->display();

    }

    /**
     * 删除场地支持活动
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据场地支持活动活动ID删除场地支持活动活动
     */
    public function delete_deploy_activity()
    {
        $site_deploy_id = intval($this->_post('obj_id'));
        if ($site_deploy_id) {
            $site_deploy_obj = new SiteDeployModel($site_deploy_id);
            $success = $site_deploy_obj->delSiteDeploy($site_deploy_id);
            exit($success ? 'success' : 'failure');
        }
        exit('failure');
    }

    public function batch_delete_deploy_activity()
    {

        $site_deploy_ids = $this->_post('obj_ids');
        if ($site_deploy_ids) {
            $site_deploy_ary = explode(',', $site_deploy_ids);
            $success_num     = 0;
            foreach ($site_deploy_ary as $site_deploy_id) {
                $site_deploy_obj = new SiteDeployModel();
                $success_num += $site_deploy_obj->delSiteDeploy($site_deploy_id);
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }
        exit('failure');
    }

}
