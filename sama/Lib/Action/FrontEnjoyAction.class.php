<?php 
class FrontEnjoyAction extends FrontAction{
	function _initialize() 
	{
		parent::_initialize();
		$this->page_num = 5;
		$this->assign('nav', 'enjoy');
	}


	public function enjoy_index(){
		//热门话题
		$topic_obj = D('Topic');
		$topic_obj->setLimit(100);
		$topic_list = $topic_obj->getTopicList('topic_id, pic, title', 'is_del = 0 and is_hot = 1');
		log_file($topic_obj->getLastSql(), 'cc', true);
		$topic_list = $topic_obj->getListData($topic_list);
		$topic_num = count($topic_list);
		$swiper_len = ceil($topic_num/3);
		$hot_topic_list = array();
		for ($i=0; $i < $swiper_len; $i++) {
			$hot_topic_list[$i][] = $topic_list[$i*3];
			$hot_topic_list[$i][] = $topic_list[$i*3+1];
			$hot_topic_list[$i][] = $topic_list[$i*3+2];
		}
		#dump($hot_topic_list);die;
		
		//动态
		$view_obj = D('View');
		$where = 'is_del = 0 and view_type ='.ViewModel::DYNAMIC;
		$total = $view_obj->getViewNum($where);
		$firstRow = intval($this->_request('firstRow'));
		$view_obj->setStart($firstRow);
        $view_obj->setLimit($this->page_num);
		$dynamic_list = $view_obj->getViewList('', $where, 'addtime desc');
		$dynamic_list = $view_obj->getListData($dynamic_list);
		
		if(IS_AJAX && IS_POST){
			$this->ajaxReturn($dynamic_list);
		}
		#dump($dynamic_list[0]);die;
		
		$cust_flash_obj = new CustFlashModel();
		$cust_flash_list = $cust_flash_obj->getCustFlashList('', 'isuse = 1 and position = 2', 'serial asc');
		$this->assign('cust_flash_list', $cust_flash_list);	
		
		$this->assign('dynamic_list', $dynamic_list);
		$this->assign('total', $total);
		$this->assign('firstRow', $this->page_num);
		$this->assign('hot_topic_list', $hot_topic_list);
		$this->assign('head_title', '创享圈');
		$this->display();
	}

	//话题详情
	public function topic_details(){
		$topic_id = I('topic_id', 0, 'intval');
		if(!$topic_id){
			redirect('/FrontEnjoy/enjoy_index');
		}
		session('topic_id', $topic_id);
		$topic_obj = D('Topic');
		$topic = $topic_obj->getTopicList('','topic_id ='.$topic_id);
		$topic = $topic_obj->getListData($topic);
		$this->assign('topic', $topic[0]);
		#dump($topic);die;
		//观点
		$view_obj = D('View');
        $where ='is_del = 0 and view_type ='.ViewModel::TOPIC.' and topic_id ='.$topic_id;

        $total = $view_obj->getViewNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $view_obj->setStart($firstRow);
        $view_obj->setLimit($this->page_num);


		$view_list = $view_obj->getViewList('', $where, 'addtime desc');
		$view_list = $view_obj->getListData($view_list);
		
        if(IS_AJAX && IS_POST){
            $this->ajaxReturn($view_list);
        }

		$this->assign('topic_id', $topic_id);
		$this->assign('view_list', $view_list);
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
		#dump($view_list);die;
		$this->assign('head_title', '话题详情');
		$this->display();
	}

	//发布观点
	public function add_view(){
		$topic_id = I('topic_id', 0, 'intval');
		#log_file(IS_AJAX . IS_POST, 'a', true);
		if(IS_AJAX && IS_POST){
			
			$user_id = session('user_id');

			$user_obj = new UserModel($user_id);
			$user = $user_obj->getUserInfo('nickname, position, headimgurl');
			$contents = I('contents');
			if(!$contents){
				$this->ajaxReturn(array('code'=>1,'msg'=>'请填写观点内容'));
			}
			$imgs = I('imgs');
			$imgs = trim($imgs, ',');
			/*$imgs = '';
			if(count($pic)){
				$imgs = implode(',', $pic);
			}*/
			$data = array(
				'view_type' => ViewModel::TOPIC,
				'user_id' => $user_id,
				'contents' => $contents,
				'imgs' => $imgs,
				'addtiem' => time(),
				'position' => $user['position'],
				'nickname' => $user['nickname'],
				'headimgurl' => $user['headimgurl'],
				'topic_id' => $topic_id,
				);
			$view_obj = D('View');
			if($view_obj->addView($data)){
				$this->ajaxReturn(array('code'=>0,'msg'=>'观点发表成功'));
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'观点发表失败'));
			}
		}
		if(!$topic_id){
			$this->alert('话题不存在', '/FrontEnjoy/enjoy_index');
		}
		$this->assign('topic_id', $topic_id);
		$this->assign('head_title', '发表观点');
		$this->display();
	}	


	//发布动态
	public function add_dynamic(){
			#log_file(json_encode($_REQUEST), 'd', true);
		if(IS_AJAX && IS_POST){
			$user_id = session('user_id');

			$user_obj = new UserModel($user_id);
			$user = $user_obj->getUserInfo('nickname, position, headimgurl,is_investment');
			if(!$user['is_investment']){
				$this->ajaxReturn(array('code'=>1,'msg'=>'只有投资人才能发表动态'));
			}
			$contents = I('contents');
			if(!$contents){
				$this->ajaxReturn(array('code'=>1,'msg'=>'请填写动态内容'));
			}
			#log_file('a', 'd', true);
			$imgs = I('imgs');
			$imgs = trim($imgs, ',');
			/*if(count($pic)){
				$imgs = implode(',', $pic);
			}*/
			$data = array(
				'view_type' => ViewModel::DYNAMIC,
				'user_id' => $user_id,
				'contents' => $contents,
				'imgs' => $imgs,
				'addtiem' => time(),
				'position' => $user['position'],
				'nickname' => $user['nickname'],
				'headimgurl' => $user['headimgurl'],
				'nickname' => $user['nickname'],
				);
			$view_obj = D('View');
			#log_file(json_encode($data), 'd', true);
			if($view_obj->addView($data)){
				$this->ajaxReturn(array('code'=>0,'msg'=>'动态发表成功'));
			}else{
				$this->ajaxReturn(array('code'=>1,'msg'=>'动态发表失败'));
			}
		}

		$this->assign('head_title', '发表动态');
		$this->display();
	}


	//项目列表
	public function project_list(){
		$project_obj = D('Project');
		$where = 'is_del = 0';
		$field = I('field', 0, 'intval');
		$phase = I('phase', 0, 'intval');
		if($field){
			$where .= ' and find_in_set('.$field.', project_field)';
			$this->assign('field', $field);
		}

		if($phase){
			$where .= ' and investment_phase ='.$phase;
			$this->assign('phase', $phase);
		}
			
		$total = $project_obj->getProjectNum($where);
		$firstRow = intval($this->_request('firstRow'));
		$project_obj->setStart($firstRow);
        $project_obj->setLimit($this->page_num);
		$project_list = $project_obj->getProjectList('',$where, 'addtime desc');
		// echo $project_obj->getLastSql();die;
		log_file($project_obj->getLastSql(), 'cc', true);
		$project_list = $project_obj->getListData($project_list);
		if(IS_POST && IS_AJAX){
			$this->ajaxReturn(array('project_list'=>$project_list, 'total'=>$total));
		}
		//阶段
		$type_obj = D('Type');
		$phase_list = $type_obj->getTypeList('type_type ='.TypeModel::PHASE);
		$this->assign('phase_list', $phase_list);
		//领域
		$field_list = $type_obj->getTypeList('type_type ='.TypeModel::DOMAIN);
		$this->assign('field_list', $field_list);

		#dump($project_list);die;
		$this->assign('total', $total);
		$this->assign('firstRow', $this->page_num);
		$this->assign('project_list', $project_list);

		$cust_flash_obj = new CustFlashModel();
		$cust_flash_list = $cust_flash_obj->getCustFlashList('', 'isuse = 1 and position = 2', 'serial asc');
		$this->assign('cust_flash_list', $cust_flash_list);	

		$this->assign('head_title', '找项目');
		$this->display();
	}

	//项目详情
	public function project_details(){
		$project_id = I('project_id', 0, 'intval');
		if(!$project_id){
			redirect('/FrontEnjoy/project_list');
		}
		$project_obj = D('Project');
		$project_list = $project_obj->getProjectList('', 'project_id ='.$project_id);
		$project_list = $project_obj->getListData($project_list);
		
		//评论
		$comment_obj = D('Comment');
		$comment_obj->setLimit($this->page_num);
		$total = $comment_obj->getCommentNum('comment_type ='.CommentModel::PROJECT .' AND type_id ='.$project_id);
		$comment_list = $comment_obj->getCommentList('', 'comment_type ='.CommentModel::PROJECT .' AND type_id ='.$project_id, 'addtime desc');
		$comment_list = $comment_obj->getListData($comment_list);
		#dump($comment_list);
		#dump($project_list[0]);die;
		$this->assign('comment_list', $comment_list);
		$this->assign('project', $project_list[0]);
		$this->assign('total', $total);
		$this->assign('firstRow', $this->page_num);
		$this->assign('head_title', '项目详情');
		$this->display();
	}

	public function get_project_comment(){
		
		$firstRow = $this->_request('firstRow');
		$project_id = I('project_id', 0, 'intval');
		$comment_obj = new CommentModel();
		$comment_obj->setStart($firstRow);
		$comment_obj->setLimit($this->page_num);
		$comment_list = $comment_obj->getCommentList('', 'comment_type ='.CommentModel::PROJECT .' AND type_id ='.$project_id, 'addtime DESC');
		$comment_list = $comment_obj->getListData($comment_list);
		#dump($comment_list);die;
		$this->json_exit($comment_list);
	}

	//项目评论
	public function add_project_comment(){
		$project_id = I('project_id', 0, 'intval');
		if(!$project_id){
			$this->alert('项目不存在');
		}
        $project_obj = new ProjectModel();
        $project_info = $project_obj->getProjectInfo('project_id ='.$project_id,'user_id,project_name');
		$user_id = intval(session('user_id'));
        $user_obj = new UserModel();
        $user_info = $user_obj->getUserInfo('realname','user_id ='.$user_id);
		if(IS_AJAX && IS_POST){
			$contents = I('contents');
			$data = array(
				'type_id' => $project_id,
				'contents' => $contents,
				'comment_type' => CommentModel::PROJECT,
				'user_id' => $user_id,
				);
			$comment_obj = D('Comment');
			if($comment_obj->addComment($data)){

                $message_obj = new MessageModel();
                $title = $user_info['realname'].'对你的'.$project_info['project_name'].'项目进行了评论';
                $message_obj->addMessage($project_id,MessageModel::PROJECT_COMMENT,$user_id,$project_info['user_id'],$title,$title);

				$this->ajaxReturn(['code'=>0, 'msg'=>'评论发表成功']);
			}
			$this->ajaxReturn(['code'=>1, 'msg'=>'评论发表失败']);
		}
		$this->assign('project_id', $project_id);
		$this->assign('head_title', '项目评论');
		$this->display();
	}


	//BP列表
	public function bp_list(){
		$user_id = session('user_id');
		$project_obj = D('Project');
		$project_list = $project_obj->getProjectList('project_id, user_id, project_name, project_desc, pic', 'is_del = 0 and user_id ='.$user_id, 'addtime desc');
		$project_list = $project_obj->getListData($project_list);
		/*echo json_encode($project_list);
		exit;*/
		$this->assign('project_list', $project_list);
		$this->assign('head_title', '提交BP');
		$this->display();
	}

	//提交BP
	public function add_bp(){
		$user_id = session('user_id');
		if(IS_AJAX && IS_POST){
			#dump($_POST);die;
			$project_name = I('project_name');
			$financing_money = I('financing_money', 0.0, 'floatval');
			$project_field = I('field');
			$investment_phase = I('phase', 0, 'intval');
			$project_highlight = I('project_highlight');
			$project_desc = I('project_desc');
			$pic = I('pic');
			$plan_book = I('plan_book');
			$cover = I('cover');
			$is_ios = I('is_ios', 0, 'intval');
			if(!$project_name){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请填写项目名称'));
			}
			// if(!$financing_money){
			// 	$this->ajaxReturn(array('code'=>1, 'msg'=>'请填写融资金额'));
			// }
			if(!$project_field){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请选择项目领域'));
			}
			if(!$investment_phase){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请选择项目阶段'));
			}
			if(!$project_highlight){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请填写项目亮点'));
			}
			if(!$project_desc){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请填写项目简介'));
			}
			if(!$pic){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请上传项目封面图'));
			}
			if(!$is_ios){
				if(!$plan_book){
					$this->ajaxReturn(array('code'=>1, 'msg'=>'请上传商业计划书'));
				}
				if(!$cover){
					$this->ajaxReturn(array('code'=>1, 'msg'=>'请上传自荐材料'));
				}
			}
			
			$data = array(
				'project_name' => $project_name,
				'financing_money' => $financing_money,
				'project_field' => $project_field,
				'investment_phase' => $investment_phase,
				'project_highlight' => $project_highlight,
				'project_desc' => $project_desc,
				'plan_book' => $plan_book,
				'cover' => $cover,
				'pic' => $pic,
				'user_id' => $user_id,
				);
			$project_obj = D('Project');
			if($project_obj->addProject($data)){
				$this->ajaxReturn(array('code'=>0,'msg'=>'BP添加成功'));
			}
			$this->ajaxReturn(array('code'=>1,'msg'=>'BP添加失败'));
		}

		//阶段
		$type_obj = D('Type');
		$type_obj->setLimit(1000);
		$phase_list = $type_obj->getTypeList('type_type ='.TypeModel::PHASE);
		$this->assign('phase_list', $phase_list);
		//领域
		$field_list = $type_obj->getTypeList('type_type ='.TypeModel::DOMAIN);
		$this->assign('field_list', $field_list);
		
		$this->assign('service_email', $GLOBALS['config_info']['CUSTOMER_SERVICE_EMAIL']);
		$this->assign('head_title', '提交BP');
		$this->display();
	}


	//找投资
	public function touzi_list(){
		
		$where = 'is_enable = 1 and is_investment = 1';
		$field = I('field', 0, 'intval');
		$city_id = I('city_id', 0, 'intval');
		$phase = I('phase', 0, 'intval');
		
		if($city_id){
			$where .= ' and city_id ='.$city_id;
			//$this->assign('field', $field);
		}

		if($field){
			$where .= ' and find_in_set('.$field.',follow_field)';
			//$this->assign('phase', $phase);
		}

		if($phase){
			$where .= ' and find_in_set('.$phase.',follow_phase)';
			//$this->assign('phase', $phase);
		}

		$user_obj = D('User');		
		$total = $user_obj->getUserNum($where);
		$firstRow = intval($this->_request('firstRow'));
		$user_obj->setStart($firstRow);
        $user_obj->setLimit($this->page_num);
		$user_list = $user_obj->getUserList('realname, user_id, company_address, introduction, follow_field, follow_phase, headimgurl',$where, 'user_id desc');
		$user_list = $user_obj->getAuthListData($user_list);
		#echo $user_obj->getLastSql();
		//$user_list = $user_obj->getListData($project_list);
		if(IS_POST && IS_AJAX){
			$this->ajaxReturn(array('user_list'=>$user_list, 'total'=>$total));
		}


		//领域
		$type_obj = D('Type');
		$field_list = $type_obj->getTypeList('type_type ='.TypeModel::DOMAIN);
		$this->assign('field_list', $field_list);
		//阶段
		$phase_list = $type_obj->getTypeList('type_type ='.TypeModel::PHASE);
		$this->assign('phase_list', $phase_list);
		//地域
		$user_obj2 = D('User');
		$area_list = $user_obj2->getUserGroupField('city_id', 'is_enable = 1 and is_investment = 1');
		$area_list[] = 0;
		$area_list = implode(',', $area_list);
		$city_list = M('AddressCity')->where('city_id in('.$area_list.')')->select();
		#dump($city_list);die;
		$this->assign('city_list', $city_list);

		$this->assign('total', $total);
		$this->assign('firstRow', $this->page_num);
		$this->assign('user_list', $user_list);
		#dump($user_list);die;
		#
		$cust_flash_obj = new CustFlashModel();
		$cust_flash_list = $cust_flash_obj->getCustFlashList('', 'isuse = 1 and position = 2', 'serial asc');
		$this->assign('cust_flash_list', $cust_flash_list);	

		$this->assign('head_title', '找投资');
		$this->display();
	}

	//投资人详情
	public function touzi_details(){
		$user_id = I('user_id', 0, 'intval');

		$user_obj = new UserModel();
		$user = $user_obj->getUserList('', 'is_enable = 1 and user_id ='.$user_id);
		$user = $user_obj->getAuthListData($user);
		if(!$user){
			$this->alert('投资人不存在', '/FrontEnjoy/touzi_list');
		}

		//是否收藏
		$collect_obj = D('Collect');
		$collect_num = $collect_obj->getCollectNum('user_id ='.session('user_id').' and type_id ='.$user_id);
		if($collect_num){
			$is_collect = 1;
		}else{
			$is_collect = 0;
		}
		$this->assign('is_collect', $is_collect);

		$this->assign('user', $user[0]);
		#dump($user[0]);die;
		$this->assign('head_title', '投资人详情');
		$this->display();
	}


	//表达意向
	public function express_will(){
		$user_id = session('user_id');
		$user_obj = new UserModel($user_id);
		$user = $user_obj->getUserInfo('is_investment');

		//是否为投资人
		if($user['is_investment'] != 1){
			$this->ajaxReturn(array('code'=>1, 'msg'=>'您还不是投资人，请先认证'));
		}

		$project_id = I('project_id', 0, 'intval');
		if(!$project_id){
			$this->ajaxReturn(array('code'=>1, 'msg'=>'意向表达失败，请重试'));
		}

		$project_obj = D('Project');
		$project = $project_obj->getProjectInfo('project_id ='.$project_id. ' and is_del = 0', 'user_id,project_name');

		//是投资人,投递名片
		$express_will_obj = D('ExpressWill');
		$data = array(
			'send_user' => $user_id,
			'to_user' => $project['user_id'],
			'project_id' => $project_id,
			);
		if($express_will_obj->addExpressWill($data)){
            $user_obj = new UserModel();
            $user_info = $user_obj->getUserInfo('','user_id ='.$user_id);
		    $message_obj = new MessageModel();
            $title =$user_info['realname'].'对你的'.$project['project_name'].'项目表达了意向';

            $type_obj = D('Type');
            $field_list = $type_obj->getTypeField('type_name','type_id in('.$user_info['follow_field'].') and type_type ='.TypeModel::DOMAIN);
            $user_info['follow_field'] = $field_list[0];
            $contents = serialize($user_info);

            $message_obj->addMessage($project_id,MessageModel::EXPRESS_WILL,$user_id,$project['user_id'],$title,$contents);
			$this->ajaxReturn(array('code'=>0, 'msg'=> '意向表达成功'));
		}
		$this->ajaxReturn(array('code'=>1, 'msg'=> '意向表达失败，请重试'));
	}

	//认证投资人
	public function check_investor(){
		$project_id = I('project_id', 0, 'intval');
		$this->assign('project_id', $project_id);
		$user_id = session('user_id');
		$user_obj = new UserModel($user_id);
		$user = $user_obj->getUserInfo('is_investment, realname');
		$this->assign('user', $user);
		if($user['is_investment']){
			//redirect('/FrontUserCenter/investor_card');
			
		}
		//是否有在申请的认证
		$auth_investment_obj = D('AuthInvestment');
		$have_auth = $auth_investment_obj->getAuthInvestmentInfo('user_id ='.$user_id .' and status = 0');
		if(IS_AJAX && IS_POST){
			if($have_auth){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'认证申请已提交，请勿重复申请！'));
			}
			$realname = I('realname');
			$contact_phone = I('contact_phone');
			$company_address = I('company_address');
			$position = I('position');
			$field = I('field');
			$phase = I('phase');
			$main_currency = I('main_currency');
			$overseas = I('overseas', 0, 'intval');
			$introduction = I('introduction');
			$mechanism_desc = I('mechanism_desc');
			$province_id = I('province_id', 0, 'intval');
			$city_id = I('city_id', 0, 'intval');
			$touzi_pic = I('pic');
			if(!$realname){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请填写真实姓名'));
			}
			if(!$contact_phone){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请填写联系电话'));
			}
			if(!$company_address){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请填写所在机构'));
			}
			if(!$position){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请填写职位'));
			}
			if(!$field){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请选择关注领域'));
			}
			if(!$phase){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请选择关注阶段'));
			}
			if(!$main_currency){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请选择主投币种'));
			}
			if(!$introduction){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请填写个人简介'));
			}
			if(!$mechanism_desc){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请填写机构信息'));
			}
			if(!$province_id){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请选择所在省份'));	
			}
			if(!$city_id){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请选择所在城市'));	
			}
			if(!$touzi_pic){
				$this->ajaxReturn(array('code'=>1, 'msg'=>'请上传投资人照片或机构logo'));
			}
			$data = array(
				'realname' => $realname,
				'contact_phone' => $contact_phone,
				'company_address' => $company_address,
				'position' => $position,
				'follow_field' => $field,
				'follow_phase' => $phase,
				'main_currency' => $main_currency,
				'introduction' => $introduction,
				'mechanism_desc' => $mechanism_desc,
                'overseas'=>$overseas,
                'province_id'=> $province_id,
                'city_id'=>$city_id,
                'touzi_pic' => $touzi_pic,
				);
			$auth_investment_obj = D('AuthInvestment');
			if($auth_investment_obj->addAuthInvestment(array('user_id'=> $user_id))){
				$user_obj->setUserInfo($data);
				$user_obj->saveUserInfo();
				$this->ajaxReturn(array('code'=>0, 'msg'=>'我们将在3~5个工作日内对您的申请做出答复！'));
			}
		}

		//阶段
		$type_obj = D('Type');
		$phase_list = $type_obj->getTypeList('type_type ='.TypeModel::PHASE);
		$this->assign('phase_list', $phase_list);
		//领域
		$field_list = $type_obj->getTypeList('type_type ='.TypeModel::DOMAIN);
		$this->assign('field_list', $field_list);
		//币种
		$currency_list = $type_obj->getTypeList('type_type ='.TypeModel::CURRENCY);
		$this->assign('currency_list', $currency_list);

		 //所有省
        $address_obj = new AddressProvinceModel();
        $province_list = $address_obj->getProvinceList();
        $this->assign('province_list',$province_list);

		$this->assign('head_title', '认证投资人');
		$this->display();
	}

	//话题收藏
	public function collect_topic(){
		$user_id = session('user_id');
		$topic_id = I('topic_id', 0, 'intval');
		$type = I('type', 0, 'intval');
		if(!$topic_id){
			$this->ajaxReturn(array('code'=>1, 'msg'=>'操作失败'));
		}
		$collect_obj = D('Collect');
		if($type == 1){
			$data = array(
				'type_id' => $topic_id,
				'user_id' => $user_id,
				'collect_type' => CollectModel::TOPIC,
				);
			if($collect_obj->addCollect($data)){
				$this->ajaxReturn(array('code'=>0, 'msg'=>'收藏成功'));
			}else{
				$this->ajaxReturn(array('code'=>1, 'msg'=>'收藏失败'));
			}
		}else{
			$where = 'user_id ='.$user_id.' and type_id ='.$topic_id;
			if($collect_obj->delCollect($where)){
				$this->ajaxReturn(array('code'=>0, 'msg'=>'取消收藏'));
			}else{
				$this->ajaxReturn(array('code'=>1, 'msg'=>'取消收藏失败'));
			}
		}
	} 

	//项目收藏
	public function collect_project(){
		$user_id = session('user_id');
		$project_id = I('project_id', 0, 'intval');
		$type = I('type', 0, 'intval');
		if(!$project_id){
			$this->ajaxReturn(array('code'=>1, 'msg'=>'操作失败'));
		}
		$collect_obj = D('Collect');
		if($type == 1){
			$data = array(
				'type_id' => $project_id,
				'user_id' => $user_id,
				'collect_type' => CollectModel::PROJECT,
				);
			if($collect_obj->addCollect($data)){
				$this->ajaxReturn(array('code'=>0, 'msg'=>'收藏成功'));
			}else{
				$this->ajaxReturn(array('code'=>1, 'msg'=>'收藏失败'));
			}
		}else{
			$where = 'user_id ='.$user_id.' and type_id ='.$project_id;
			if($collect_obj->delCollect($where)){
				$this->ajaxReturn(array('code'=>0, 'msg'=>'取消收藏'));
			}else{echo $collect_obj->getLastSql();die;
				$this->ajaxReturn(array('code'=>1, 'msg'=>'取消收藏失败'));
			}
		}
	} 

	//投资人收藏
	public function collect_touzi(){
		$user_id = session('user_id');
		$touzi_id = I('touzi_id', 0, 'intval');
		$type = I('type', 0, 'intval');
		if(!$touzi_id){
			$this->ajaxReturn(array('code'=>1, 'msg'=>'操作失败'));
		}
		$collect_obj = D('Collect');
		if($type == 1){
			$data = array(
				'type_id' => $touzi_id,
				'user_id' => $user_id,
				'collect_type' => CollectModel::TOUZI,
				);
			if($collect_obj->addCollect($data)){
				$this->ajaxReturn(array('code'=>0, 'msg'=>'收藏成功'));
			}else{
				$this->ajaxReturn(array('code'=>1, 'msg'=>'收藏失败'));
			}
		}else{
			$where = 'user_id ='.$user_id.' and type_id ='.$touzi_id;
			if($collect_obj->delCollect($where)){
				$this->ajaxReturn(array('code'=>0, 'msg'=>'取消收藏'));
			}else{
				$this->ajaxReturn(array('code'=>1, 'msg'=>'取消收藏失败'));
			}
		}
	} 


	//动态、观点评论
	public function view_comment(){
		if(IS_AJAX && IS_POST){
			$user_id = intval(session('user_id'));
			$user = M('Users')->where('user_id ='.$user_id)->find();
			$nickname = $user['realname'];
			$view_id = I('view_id', 0, 'intval');
			$reply_user_id = I('reply_user_id', 0, 'intval');
			$comment_type = I('comment_type');
			$reply_comment_id = I('reply_comment_id', 0, 'intval');
			$reply_nickname = '';
			if($reply_user_id){
				$reply_user = M('Users')->where('user_id ='.$reply_user_id)->find();
				$reply_nickname = $reply_user['realname'];
			}
			$contents = I('contents');
			if(!$user_id || !$view_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'评论失败'));
			if(!$contents) $this->ajaxReturn(array('code'=>1, 'msg'=>'请输入评论内容'));
			if($comment_type != 2) $this->ajaxReturn(array('code'=>1, 'msg'=>'评论失败'));

			$comment_obj = new CommentModel();
			$data = array(
				'comment_type' => $comment_type,
				'contents' => $contents,
				'type_id' => $view_id,
				'addtime' => time(),
				'user_id' => $user_id,
				'from_nickname' => $nickname,
				'to_nickname' => $reply_nickname,
				'reply_user_id' => $reply_user_id,
				'reply_comment_id' => $reply_comment_id,
				);
			if($id = $comment_obj->addComment($data)){
				$data['comment_id'] = $id;
				// if($reply_user_id){
				// 	$view_obj = new ViewModel();
				// 	$view = $view_obj->getViewInfo('view_id ='.$view_id);
				// 	if($view['view_type'] == ViewModel::TOPIC){
				// 		$topic_obj = new TopicModel();
				// 		$topic = $topic_obj->getTopicInfo('topic_id ='.$view['topic_id']);
				// 		$msg = $nickname.'对你在《'.$topic['title'].'》热门话题的回答下进行了评论';
				// 	}elseif($view['view_type'] == ViewModel::DYNAMIC){
				// 		$msg = $nickname.'对你的动态进行了评论';
				// 	}	
					
				// 	D('Message')->addMessage($id, MessageModel::VIEW_COMMENT, $user_id, $reply_user_id, $msg, $msg);
				// }
				$view_obj = new ViewModel();
					$view = $view_obj->getViewInfo('view_id ='.$view_id);
					if($view['view_type'] == ViewModel::DYNAMIC){
						$msg = $nickname.'对你的动态进行了评论';
						$message_type =  MessageModel::DYNAMIC_COMMENT;
					}elseif($view['view_type'] == ViewModel::TOPIC){
						$topic_obj = new TopicModel();
				 		$topic = $topic_obj->getTopicInfo('topic_id ='.$view['topic_id']);
						$msg = $nickname.'对你在《'.$topic['title'].'》热门话题的回答下进行了评论';
						$message_type =  MessageModel::VIEW_COMMENT;
					}	
					D('Message')->addMessage($id, $message_type, $user_id, $view['user_id'], $msg, $msg);
				$this->ajaxReturn(array('code'=>0, 'data'=> $data));
			}

			$this->ajaxReturn(array('code'=>1, 'msg'=>'评论失败'));
		}

	}
}

