<?php

class AcpCustomerAction extends AcpAction
{
    public function AcpUserAction()
    {
        parent::_initialize();

    }

    private function get_search_condition()
    {
        //初始化SQL查询的where子句
        $where = 'true';

        //客户名称
        $customer_name =I('customer_name');
        if($customer_name){
            $where .= ' AND customer_name LIKE "%'.$customer_name.'%"';
        }

        //企业名称
        $company_name =I('company_name');
        if($company_name){
            $where .= ' AND company_name LIKE "%'.$company_name.'%"';
        }


        //业务员
        $salesman = I('salesman');
        if($salesman){
            $user_obj = new UserModel();
            $user_ids = $user_obj->getUserField('user_id','realname LIKE "%'.$salesman.'%" AND role_type = 1');
            $user_str = implode(',',$user_ids);
            if($user_str){
                $where .=' AND salesman IN ('.$user_str.')';
            }else{
                $where .=' AND false';
            }
        }

        //客户性质
        $customer_type = I('customer_type',0,'int');
        if($customer_type){
            $where .=' AND customer_type ='.$customer_type;
        }

        //跟进状态
        $follow_state =I('follow_state');
//        $follow_state = $this->_request('follow_state');
        $follow_state = ($follow_state == '' || $follow_state == -1) ?  -1 : intval($follow_state);
        if($follow_state != -1){
            $where .= ' AND follow_state ='.$follow_state;
        }

        #echo $where;
        //重新赋值到表单
        $this->assign('customer_name', $customer_name);
        $this->assign('company_name', $company_name);
        $this->assign('salesman', $salesman);
        $this->assign('customer_type', $customer_type);
        $this->assign('follow_state', $follow_state);
//        $this->assign('start_time', $start_time ? $start_time : '');
//        $this->assign('end_time', $end_time ? $end_time : '');

        return $where;
    }

    //客户列表
    public function get_customer_list(){
        $user_id = intval(session('user_id'));
        $where=$this->get_search_condition();
        if($user_id != 1){
            $where .= ' AND salesman ='.$user_id;
        }
        $customer_obj = new CustomerModel();

        //数据总量
        $total = $customer_obj->getCustomerNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $customer_obj->setStart($Page->firstRow);
        $customer_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $customer_list = $customer_obj->getCustomerList('',$where);
        $customer_list = $customer_obj->getListData($customer_list);
        $this->assign('customer_list',$customer_list);

        //业务员
        $user_obj = new UserModel();
        $user_obj->setLimit(100000);
        $user_list = $user_obj->getUserList('user_id,realname','role_type = 1 AND user_id <> 1');
        $this->assign('user_list',$user_list);
        $this->assign('user_id',$user_id);

        //客户性质
        $customer_type_list = $customer_obj->customerType();
        $this->assign('customer_type_list',$customer_type_list);

        //跟进状态
        $follow_state_list = $customer_obj->followState();
        $this->assign('follow_state_list',$follow_state_list);


        $this->assign('head_title', '客户列表');
        $this->display();
    }


    //添加修改客户
    public function add_edit_customer(){

        $act = I('act');
        $user_id = intval(session('user_id'));
        $customer_id =I('customer_id',0,'int');
        $customer_obj = new CustomerModel($customer_id);
        if(!$customer_id){
            $this->assign('head_title', '添加客户');
            $this->assign('act', 'add');
        }else{
            $customer_info = $customer_obj ->getCustomerInfo('customer_id ='.$customer_id);
            //附件
            if($customer_info['file']){
                $files = explode(',', $customer_info['file']);
            }
            $this->assign('files', $files);
            $this->assign('file_val', $customer_info['file']);

            $this->assign('customer_id', $customer_id);
            $this->assign('info', $customer_info);
            $this->assign('head_title', '修改客户');
            $this->assign('act', 'edit');
        }

        if(IS_POST){
            $_post= $this->_post();
            $customer_type =$_post['customer_type']; //客户性质
            $project_type =$_post['project_type']; //项目性质
            $customer_name =$_post['customer_name']; //客户名称
            $likeman =$_post['likeman']; //联系人
            $post =$_post['post']; //职务
            $mobile =$_post['mobile']; //手机号
            $other_link =$_post['other_link']; //其他联系方式
            $field =$_post['field']; //行业领域
            $source =$_post['source']; //客户来源
            $level =$_post['level']; //客户等级
            $first_talk_time = strtotime($_post['first_talk_time']); //初步洽谈时间
            $lease_area =floatval($_post['lease_area']); //租凭面积
            $lease_station =intval($_post['lease_station']); //租凭工位
            $intent =$_post['intent']; //合作意向
            $other_contents =$_post['other_contents']; //其他
            $remark =$_POST['remark']; //需求备注
            $file = $_post['files'];//附件
            $file = trim($file, ',');


            if(!$customer_type) $this->error('请选择客户性质');
            if(!$project_type) $this->error('请选择项目性质');
            if(!$customer_name) $this->error('请填写客户名称');
            if(!$likeman) $this->error('请填写联系人');
            if(!$post) $this->error('请填写职务');
            if(!$mobile) $this->error('请填写手机号');
            if($mobile && !check_mobile($mobile)) $this->error('请填写正确手机号');
            if(!$field) $this->error('请选择行业领域');
            if(!$source) $this->error('请选择客户来源');
            if(!$level) $this->error('请选择客户等级');
            if($lease_area && !is_float($lease_area)) $this->error('请输入正确租凭面积');
            if($lease_station && !is_numeric($lease_station)) $this->error('请输入正确租凭工位');

            $data =array(
                'customer_type'=>$customer_type,
                'project_type'=>$project_type,
                'customer_name'=>$customer_name,
                'likeman'=>$likeman,
                'post'=>$post,
                'mobile'=>$mobile,
                'other_link'=>$other_link,
                'field'=>$field,
                'source'=>$source,
                'level'=>$level,
                'first_talk_time'=>$first_talk_time,
                'lease_area'=>$lease_area,
                'lease_station'=>$lease_station,
                'intent'=>$intent,
                'other_contents'=>$other_contents,
                'remark'=>$remark,
                'file' => $file,
            );

            $url = '/AcpCustomer/get_customer_list';
            if($act == 'add'){
                $data['salesman']=$user_id;
                $data['addtime']=time();
                $data['follow_state']= 1 ;//初步洽谈
                if($customer_obj->addCustomer($data)) $this->success('添加成功',$url);
                $this->error('添加失败');
            }

            if($act == 'edit'){
                if($customer_obj->editCustomer($data)) $this->success('修改成功',$url);
                $this->error('修改失败');
            }

        }

        //客户来源
        $type_obj = new TypeModel();
        $source_list = $type_obj->getTypeList('type_type ='.TypeModel::SOURCE.' AND is_del = 0');
        $this->assign('source_list',$source_list);
        //行业领域
        $domain_list = $type_obj->getTypeList('type_type ='.TypeModel::DOMAIN.' AND is_del = 0');
        $this->assign('domain_list',$domain_list);
        //客户性质
        $customer_type_list = $type_obj->getTypeList('type_type ='.TypeModel::CUSTOMER_NATURE.' AND is_del = 0');
        $this->assign('customer_type_list',$customer_type_list);

        //项目性质
        $project_type_list = $type_obj->getTypeList('type_type ='.TypeModel::PROJECT_NATURE.' AND is_del = 0');
        $this->assign('project_type_list',$project_type_list);

        //其他联系方式
        $other_link_list = $customer_obj->otherLink();
        $this->assign('other_link_list',$other_link_list);
        //客户等级
        $level_list = $customer_obj->level();
        $this->assign('level_list',$level_list);

        $this->display();
    }


    public function detail_customer(){

        $customer_id = I('customer_id',0,'int');

        $customer_obj = new CustomerModel();

        $customer_info = $customer_obj->getCustomerInfo('customer_id ='.$customer_id);

        //客户性质
        $customer_type_list = $customer_obj->customerType();
        $customer_info['customer_type_name'] =$customer_type_list[$customer_info['customer_type']];

        //项目性质
        $project_type_list = $customer_obj->projectType();
        $customer_info['project_type_name'] =$project_type_list[$customer_info['project_type']];

        //其他联系方式
        $other_link_list = $customer_obj->otherLink();
        $customer_info['other_link_name'] =$other_link_list[$customer_info['other_link']];

        //客户来源
        $type_obj = new TypeModel();
        $source_list = $type_obj->getTypeList('type_type ='.TypeModel::SOURCE . ' and type_id ='.$customer_info['source']);
        $customer_info['source_name'] = $source_list[0]['type_name'];
        //行业领域
        $domain_list = $type_obj->getTypeList('type_type ='.TypeModel::DOMAIN. ' and type_id ='.$customer_info['field']);
        $customer_info['field_name'] = $domain_list[0]['type_name'];
        //等级
        $level_list = $customer_obj->level();
        $customer_info['level_name'] =$level_list[$customer_info['level']];

        //附件
        if($customer_info['file']){
            $files = explode(',', $customer_info['file']);
        }
        $this->assign('files', $files);
        $this->assign('file_val', $customer_info['file']);

        $follow_state_list = $customer_obj->followState();
        $customer_info['follow_state_name'] =$follow_state_list[$customer_info['follow_state']];

        $user_obj = new UserModel();
        $user_info =$user_obj->getUserInfo('realname','user_id ='.$customer_info['salesman']);
        $customer_info['user_name'] =$user_info['realname'];



        $this->assign('head_title', '客户详情');
        $this->assign('info',$customer_info);
        $this->display();
    }


    public function  set_salesman(){
        $user_id = intval(session('user_id'));
        if($user_id != 1) $this->ajaxReturn('false');
        $salesman = I('salesman',0,'int');
        $obj_id = I('obj_id',0,'int');
        if($salesman && $obj_id){
            $customer_obj = new CustomerModel($obj_id);
            if($customer_obj->editCustomer(array('salesman'=>$salesman))) $this->ajaxReturn('success');
            $this->ajaxReturn('false');
        }
        $this->ajaxReturn('false');
    }

    /**
     * 查看沟通纪要．
     */
    public function get_talk_summary_list(){

        $customer_id =I('customer_id');
        $where =$this->get_search_condition();
        $where .= ' AND customer_id ='.$customer_id;
        $talk_summary_obj = new TalkSummaryModel();
        $total = $talk_summary_obj->getTalkSummaryNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $talk_summary_obj->setStart($Page->firstRow);
        $talk_summary_obj->setLimit($Page->listRows);
        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $talk_summary_list = $talk_summary_obj->getTalkSummaryList('',$where,'addtime desc');
        $talk_summary_list =  $talk_summary_obj->getListData($talk_summary_list);

        //跟进状态
        $customer_obj = new CustomerModel();
        $follow_state_list = $customer_obj->followState();
        $this->assign('follow_state_list',$follow_state_list);

        //是否流失
        $drain = $customer_obj->getCustomerNum('follow_state = 5 AND customer_id='.$customer_id);
        $this->assign('drain',$drain);

        $this->assign('talk_summary_list',$talk_summary_list);
        $this->assign('customer_id',$customer_id);
        $this->assign('head_title','沟通纪要列表');
        $this->display();

    }


    /**
     * 添加修改沟通纪要．
     */
    public function add_edit_talk_summary(){
        $act = I('act');
        $talk_summary_id = I('talk_summary_id',0,'int');

        $customer_id = I('customer_id',0,'int');
        $this->assign('customer_id',$customer_id);
        $customer_obj = new CustomerModel($customer_id);

        $talk_summary_obj = new TalkSummaryModel($talk_summary_id);
        if(!$talk_summary_id){
            $this->assign('head_title','添加沟通纪要');
            $this->assign('act','add');
        }else{
            $talk_summary_info =$talk_summary_obj->getTalkSummaryInfo('talk_summary_id ='.$talk_summary_id);
            $this->assign('info',$talk_summary_info);
            $this->assign('talk_summary_id',$talk_summary_id);
            $this->assign('act','edit');
            $this->assign('head_title','修改沟通纪要');
        }

        if(IS_POST) {
            $_post = $this->_post();
            $addtime = strtotime($_post['addtime']);
            $address = $_post['address'];
            $our_staff = $_post['our_staff'];
            $company_name = $_post['company_name'];
            $their_staff = $_post['their_staff'];
            $link_way = $_post['link_way'];
            $follow_state = $_post['follow_state'];
            $project_detail = $_post['project_detail'];
            $enter_need = $_post['enter_need'];
            $remark = $_post['remark'];
            $drain_why =$_post['drain_why'];

            if(!$addtime) $this->error('请选择沟通时间');
            if(!$address) $this->error('请输入地址');
            if(!$our_staff) $this->error('请输入我方人员');
            if(!$company_name) $this->error('请输入企业名称');
            if(!$their_staff) $this->error('请输入对方人员');
            if(!$link_way) $this->error('请输入联系方式');
            if($follow_state == -1) $this->error('请选择跟进状态');

            if($follow_state == 5){
                if(!$drain_why) $this->error('请选择流失原因');
            }

            $data=array(
                'addtime' => $addtime,
                'address' => $address,
                'our_staff' => $our_staff,
                'company_name' => $company_name,
                'their_staff' => $their_staff,
                'link_way' => $link_way,
                'follow_state' => $follow_state,
                'drain_why' => $drain_why,
                'project_detail' => $project_detail,
                'enter_need' => $enter_need,
                'remark' => $remark,
            );

            $customer_id = $customer_id ? $customer_id : $talk_summary_info['customer_id'];
            $url = '/AcpCustomer/get_talk_summary_list/customer_id/'.$customer_id;

            if($act =='add'){
                $data['customer_id']=$customer_id;
                if($talk_summary_obj->addTalkSummary($data)){
                    //添加成功后,修改客户档案中的follow_state字段
                    $customer_obj->editCustomer(array('follow_state'=>$follow_state));
                    $this->success('添加沟通纪要成功!',$url);
                }
                $this->error('添加沟通纪要失败');
            }

            if($act =='edit'){
                if($talk_summary_obj->editTalkSummary($data)) $this->success('修改沟通纪要成功!',$url);
                $this->error('修改沟通纪要失败');
            }

        }

        //跟进状态
        $follow_state_list = $customer_obj->followState();
        $this->assign('follow_state_list',$follow_state_list);

        //流失原因
        $type_obj = new TypeModel();
        $drain_list = $type_obj->getTypeList(' type_type ='.TypeModel::DRAIN);
        $this->assign('drain_list',$drain_list);

        $this->display();
    }

    //沟通纪要详情
    public function detail_talk_summary(){
        $talk_summary_id = I('talk_summary_id',0,'int');
        $talk_summary_obj = new TalkSummaryModel();

        $talk_summary_info = $talk_summary_obj->getTalkSummaryInfo('talk_summary_id ='.$talk_summary_id);

        $customer_obj = new CustomerModel();
        $follow_state_list = $customer_obj->followState();
        $talk_summary_info['follow_state_name'] =$follow_state_list[$talk_summary_info['follow_state']];

        //流失原因
        $type_obj = new TypeModel();
        $talk_summary_info['drain_name'] = $type_obj->getTypeSiteField('type_name','type_id ='.$talk_summary_info['drain_why']);

        $this->assign('info',$talk_summary_info);
        $this->assign('head_title','沟通纪要详情');
        $this->display();
    }






}
?>
