<?php
/**
 * acp视频后台
 */
class AcpLiveAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
//        $this->assign('action_title', '视频视频');
//        $this->assign('action_src', U('/AcpClass/list_class'));
    }

    //视频视频列表
    public function get_live_list()
    {
        $live_obj = new LiveModel();
        $where     = '';
        //数据总量
        $total = $live_obj->getLiveNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $live_obj->setStart($Page->firstRow);
        $live_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $list = $live_obj->getLiveList();

        $list = $live_obj->getListData($list);
        $this->assign('live_list', $list);
        $this->assign('head_title', '视频视频列表');
        $this->display();

    }

    //添加视频
    public function add_edit_live()
    {
        $live_id = I('live_id',0,'int');
        $act = I('act');
        $live_obj = new LiveModel();
        if($live_id){
            $live_info = $live_obj->getLive($live_id);
            $this->assign('act','edit');
            $this->assign('info',$live_info);
            $this->assign('head_title','修改视频');
        }else{
            $this->assign('head_title','添加视频');
            $this->assign('act','add');
        }

        if(IS_POST){
            $_post      = $this->_post();
            $live_name = $_post['live_name'];
            $live_class_id = $_post['live_class_id'];
            $url = $_post['url'];
            $price = $_post['price'];
            $serial     = $_post['serial'];
            $isuse      = $_post['isuse'];
            $pic  = $_post['pic'];

            //表单验证
            if (!$live_name) $this->error('请填写视频名称！');
            if (!$live_class_id) $this->error('请选择所属频道！');
            if (!$pic) $this->error('请添加视频图片！');
            if (!$url) $this->error('请添加url！');
            if (!$price) $this->error('请输入观看价格！');
            if ($price && !is_numeric($price)) $this->error('请输入正确观看价格！');
            if (!ctype_digit($serial)) $this->error('请填写排序号！');
            if (!ctype_digit($isuse)) $this->error('请选择是否显示！');

            $arr = array(
                'live_name' => $live_name,
                'live_class_id' => $live_class_id,
                'url' => $url,
                'price' => $price,
                'serial' => $serial,
                'isuse' => $isuse,
                'pic' => $pic,
            );

            if ($act == 'add') {
                $success   = $live_obj->addLive($arr);
                if ($success) {
                    $this->success('恭喜您，视频添加成功！', '/AcpLive/get_live_list');
                } else {
                    $this->error('抱歉，视频修改失败！', '/AcpLive/get_live_list');
                }
             }

             if($act == 'edit'){
                 $success = $live_obj->setLive($live_id,$arr);;
                 if ($success !== false) {
                     $this->success('恭喜您，视频修改成功！', '/AcpLive/get_live_list');
                 } else {
                     $this->error('抱歉，视频修改失败！', '/AcpLive/get_live_list');
                 }
             }
        }
        
        //频道列表
        $live_class_obj  = new LiveClassModel();
        $live_class_list = $live_class_obj -> getValidLiveClassList();
        $this->assign('live_class_list',$live_class_list);

        // 视频图标
        $this->assign('pic_data', array(
            'name'  => 'pic',
            'title' => '封面图',
            'help'  => '点击查看大图',
            'url'   => $live_info['pic'],
            'dir'   => 'class',
        ));
        $this->display();
    }


    /**
     * 删除视频视频
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据问题ID删除视频视频
     */
    public function batch_delete_live()
    {
        $question_ids = $this->_post('obj_ids');
        if ($question_ids) {
            $question_id_ary = explode(',', $question_ids);
            $success_num     = 0;
            foreach ($question_id_ary as $question_id) {
                $question_obj = new LiveModel($question_id);
                $success_num += $question_obj->delLive();
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }
        exit('failure');
    }
}
