<?php
/**
 * Acp后台服务类型
 */
class AcpServiceTypeAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
        $this->assign('action_title', '服务类型');
        $this->assign('action_src', U('/AcpServiceType/get_service_type_list'));
    }

    public function get_service_type_list()
    {
        $service_type_obj = new ServiceTypeModel();
        $where     = '';
        //数据总量
        $total = $service_type_obj->getServiceTypeNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $service_type_obj->setStart($Page->firstRow);
        $service_type_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $service_type_list = $service_type_obj->getServiceTypeList();

        $this->assign('service_type_list', $service_type_list);
        $this->assign('head_title', '服务类型列表');
        $this->display();

    }

    //添加服务类型
    public function add_service_type()
    {

        $act = $this->_post('act');
        if ($act == 'add') {
            $_post      = $this->_post();
            $service_type_name = $_post['service_type_name'];
            $serial     = $_post['serial'];
            $isuse      = $_post['isuse'];
            $is_service  = $_post['is_service'];
            $icon   = $_post['icon'];

            //表单验证
            if (!$service_type_name) {
                $this->error('请填写服务类型名称！');
            }

            if (!$icon) {
                $this->error('请填写图标！');
            }

            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }

            if (!ctype_digit($isuse)) {
                $this->error('请选择是否有效！');
            }

            if (!ctype_digit($is_service)) {
                $this->error('请选择是否自有服务！');
            }

            $arr = array(
                'service_type_name' => $service_type_name,
                'serial'     => $serial,
                'isuse'      => $isuse,
                'is_service'   => $is_service,
                'icon'=>$icon,
            );

            $service_type_obj = new ServiceTypeModel();
            $success   = $service_type_obj->addServiceType($arr);

            if ($success) {
                $this->success('恭喜您，服务类型添加成功！', '/AcpServiceType/add_service_type');
            } else {
                $this->error('抱歉，服务类型添加失败！', '/AcpServiceType/add_service_type');
            }
        }

        $this->assign('icon_pic', array(
            'name'  => 'icon',
            'title' => '图标',
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('act', 'add');
        $this->assign('head_title', '添加服务类型');
        $this->display();
    }



    //修改服务类型
    public function edit_service_type()
    {
        $redirect = U('/AcpServiceType/get_service_type');
        $service_type_id = intval($this->_get('service_type_id'));
        if (!$service_type_id) {
            $this->error('对不起，非法访问！', $redirect);
        }

        $service_type_obj  = new ServiceTypeModel();
        $service_type_info = $service_type_obj->getServiceTypeInfo('service_type_id ='.$service_type_id);

        if (!$service_type_info) {
            $this->error('对不起，不存在相关服务类型！', $redirect);
        }

        $act = $this->_post('act');

        if ($act == 'edit') {
            $_post      = $this->_post();
            $service_type_name = $_post['service_type_name'];
            $serial     = $_post['serial'];
            $isuse      = $_post['isuse'];
            $is_service   = $_post['is_service'];
            $icon   = $_post['icon'];

            //表单验证
            if (!$service_type_name) {
                $this->error('请填写服务类型名称！');
            }

            if (!$icon) {
                $this->error('请填写图标！');
            }

            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }

            if (!ctype_digit($isuse)) {
                $this->error('请选择是否有效！');
            }

            if (!ctype_digit($is_service)) {
                $this->error('请选择是否自有服务！');
            }

            $arr = array(
                'service_type_name' => $service_type_name,
                'serial'     => $serial,
                'isuse'      => $isuse,
                'is_service'   => $is_service,
                'icon'=>$icon,
            );

            $url       = '/AcpServiceType/edit_service_type/service_type_id/' . $service_type_id;

            if ($service_type_obj->setServiceType($service_type_id, $arr)) {
                $this->success('恭喜您，服务类型修改成功！', $url);
            } else {
                $this->error('抱歉，服务类型修改失败！', $url);
            }
        }

        $this->assign('icon_pic', array(
            'name'  => 'icon',
            'title' => '图标',
            'url'=>$service_type_info['icon'],
            'help'  => '暂时只支持上传单张2M以内JPEG,PNG,GIF格式图片',
        ));

        $this->assign('service_type_info', $service_type_info);
        $this->assign('act', 'edit');
        $this->assign('head_title', '修改服务类型');
        $this->display();

    }

    /**
     * 删除服务类型
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据服务类型ID删除服务类型
     */
    public function delete_service_type()
    {
        $service_type_id = intval($this->_post('service_type_id'));
        if ($service_type_id) {
            $service_type_obj = new ServiceTypeModel($service_type_id);
            $service_provides_obj           = new ServiceProvidersModel();
            $num                = $service_provides_obj->getServiceProvidersNum('service_type_id = ' . $service_type_id);

            //存在服务商则删除失败；
            if ($num) {
                exit('failure');
            }

            $success = $service_type_obj->delServiceType();
            exit($success ? 'success' : 'failure');
        }

        exit('failure');
    }

    public function batch_delete_service_type()
    {

        $service_type_ids = $this->_post('service_type_ids');
        if ($service_type_ids) {
            $service_type_ary = explode(',', $service_type_ids);
            $success_num     = 0;
            $service_provides_obj           = new ServiceProvidersModel();
            foreach ($service_type_ary as $service_type_id) {
                $num = $service_provides_obj->getServiceProvidersNum('service_type_id = ' . $service_type_id);
                if ($num) {
                    continue;
                }

                $service_type_obj = new ServiceTypeModel($service_type_id);
                $success_num += $service_type_obj->delServiceType();
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }

        exit('failure');
    }


}
