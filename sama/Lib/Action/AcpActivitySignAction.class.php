<?php
/**
 * 活动报名类
 */

class AcpActivitySignAction extends AcpAction
{
    public function AcpActivitySignAction()
    {
        parent::_initialize();
    }


    //获取报名列表
    public function get_activity_sign_list($where,$opt){
        import('ORG.Util.Pagelist');
        $activity_sign_obj=new ActivitySignModel();
        $activity_obj=new ActivityModel();
        $data=$this->_post();
        $is_select_name=0;
        if($data['opt']=='select'){
            if($data['activity_name']){
                $is_select_name=1;
                $activity_name=$data['activity_name'];
                $activity=$activity_obj->getActivityList('activity_id','activity_title LIKE "%'.$activity_name.'%"');
                foreach($activity as $k=>$v){
                    $activity_id_arr[]=$v['activity_id'];
                }
            }
            if($data['real_name']){
                $real_name=$data['real_name'];
                $where.=' AND real_name LIKE "%'.$real_name.'%"';
            }
            if($data['is_sign']!="-1"){
                $is_sign=$data['is_sign'];
                $where.=' AND is_sign = '.$is_sign;
            }
        }

        $count =  $activity_sign_obj->getActivitySignNum($where);
        $Page = new Pagelist($count, C('PER_PAGE_NUM'));
        $activity_sign_obj->setStart($Page->firstRow);
        $activity_sign_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $activity_sign_list=$activity_sign_obj->getActivitySignList('',$where);

        $user_obj=new UserModel();
        foreach($activity_sign_list as $k=>$v){
            $user_info=$user_obj->getUserInfo('nickname','user_id = '.$v['user_id']);
            $activity_sign_list[$k]['nickname']=$user_info['nickname'];
            $activity_info=$activity_obj->getActivityInfo('activity_id = '.$v['activity_id'],'activity_title');
            $activity_sign_list[$k]['activity_name']=$activity_info['activity_title'];
            if($is_select_name&&in_array($v['activity_id'],$activity_id_arr)){
                $filter_arr[$k]=$activity_sign_list[$k];
            }
            $status = $activity_sign_obj->payStatus();
            $activity_sign_list[$k]['state'] = $status[$v['pay_status']];
        }
        $this->assign('real_name',$real_name?$real_name:'');
        $this->assign('is_sign',$is_sign==-1||$is_sign==''?-1:$is_sign);
        if($is_select_name){
            $this->assign('activity_sign_list',$filter_arr);
        }else{
            $this->assign('activity_sign_list',$activity_sign_list);
        }
        $this->assign('activity_name',$activity_name);
        $this->assign('page', $Page);
        $this->assign('show', $show);
        $this->assign('opt', $opt);
        $this->display('get_activity_sign_list');
    }

    //通过审核
    public function activity_passed_list(){
        $this->get_activity_sign_list('is_check = 1',$opt = 'passed');
    }

    //未通过审核
    public function activity_not_passed_list(){
        $this->get_activity_sign_list('is_check = 0',$opt = 'not_passed');
    }

    //审核操作．
    public function set_check(){
        $obj_id = I('obj_id',0,'int');
        $is_type =I('is_type',0,'int');
        $content = I('content');


        $activity_sign_obj=new ActivitySignModel();
        $sign_info = $activity_sign_obj ->getActivitySignInfo('activity_sign_id ='.$obj_id);
        $activity_obj =new ActivityModel();
        $activity_info = $activity_obj->getActivityInfo('activity_id ='.$sign_info['activity_id'],'activity_title');

        $contents = serialize($sign_info);

        if($is_type == 1){
            if($activity_sign_obj->editActivitySign($obj_id,array('is_check' => 1))){

                $message_obj = new MessageModel();
                $title = '活动'.$activity_info['activity_title'].'报名成功';
                $message_obj ->addMessage($obj_id,MessageModel::ACTIVITY_AUDIT,0,$sign_info['user_id'],$title,$contents);

                $this->ajaxReturn(array('code'=>1,'msg'=>'审核已通过!'));
            }else{
                $this->ajaxReturn(array('code'=>0,'msg'=>'审核通过失败!'));
            }
        }elseif($is_type == 2){
            if($activity_sign_obj->editActivitySign($obj_id,array('is_check' => 2,'content'=>$content))){

                $message_obj = new MessageModel();
                $title = '活动'.$activity_info['activity_title'].'报名失败';
                $message_obj ->addMessage($obj_id,MessageModel::ACTIVITY_AUDIT,0,$sign_info['user_id'],$title,$contents);

                $this->ajaxReturn(array('code'=>1,'msg'=>'审核已拒绝!'));
            }else{
                $this->ajaxReturn(array('code'=>0,'msg'=>'审核拒绝失败!'));
            }
        }else{
            $this->ajaxReturn(array('code'=>0,'msg'=>'操作失败!'));
        }
    }


    public function del_activity_sign(){
        $activity_sign_id=$this->_post('activity_sign_id');
        $activity_sign_obj=new ActivitySignModel();
        $result=$activity_sign_obj->delActivitySign($activity_sign_id);
        /* echo $activity_sign_obj->getLastSql();
         exit;*/
        if($result){
            echo 'success';
            exit;
        }else{
            echo 'fail';
            exit;
        }
    }
    public function del_activity_sign_batch(){
        $ids=$this->_post('id');

        $activity_obj=new ActivitySignModel();
        $idarr=explode(',',$ids);
        array_pop($idarr);//删除元素最后一个元素
        foreach($idarr as $id){
            $r=$activity_obj->delActivitySign($id);
            if(!$r){
                echo 'fail';
                exit;
            }
        }
        echo 'success';
        exit;
    }
    public function change_activity_sign(){
        $activity_sign_id=$this->_post('activity_sign_id');
        $activity_sign_obj=new ActivitySignModel();
        $activity_sign_info=$activity_sign_obj->getActivitySignInfo('activity_sign_id = '.$activity_sign_id);

        $arr['is_sign']=!$activity_sign_info['is_sign'];
        $result=$activity_sign_obj->editActivitySign($activity_sign_id,$arr);
        if($result){
            echo 'success';
            exit;
        }else{
            echo 'fail';
            exit;
        }
    }

    public function detail_activity_order($title){
        $activity_sign_id = I('activity_sign_id',0,'int');
        $activity_sign_obj = new ActivitySignModel();
        $activity_sign_info = $activity_sign_obj->getActivitySignInfo('activity_sign_id ='.$activity_sign_id);

        $activity_obj = new ActivityModel();
        $activity_info =$activity_obj->getActivityInfo('activity_id ='.$activity_sign_info['activity_id']);
        $activity_sign_info['activity_name'] =$activity_info['activity_title'];

        $pay_status = $activity_sign_obj->payStatus();
        $activity_sign_info['state'] = $pay_status[$activity_sign_info['pay_status']];

        $this->assign('info',$activity_sign_info);
        $this->assign('head_title',$title);
        $this->display('detail_activity_order');
    }


    public function detail_passed_activity_order(){

        $this->detail_activity_order('已通过活动申请详情');
    }
    public function detail_bypass_activity_order(){

        $this->detail_activity_order('待通过活动申请详情');
    }


    //退款
    public function refund(){
        $order_id = I('order_id', 0, 'intval');
        if(!$order_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，操作失败'));

        $order_obj = D('ActivitySign');
        $order = $order_obj->getActivitySignInfo('activity_sign_id ='.$order_id);
        if(!$order || $order['pay_status'] != 1 || !$order_id) $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，操作失败'));
        // dump($site_order);die;
        $payway = $order['payway'];
        $pay_code = $order['pay_code'];
        $refund_fee = $order['pay_amount'];
        if($payway == 'wxpay'){
            $wxpay_obj = new WXPayModel();
            $r = $wxpay_obj->wx_refund($pay_code, $refund_fee);
            if($r){
                $order_obj->where('activity_sign_id ='.$order_id)->save(array('pay_status'=>3, 'refund_time'=>time()));

                $this->ajaxReturn(array('code'=>0, 'msg'=>'退款成功'));
            }else{
                $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，退款失败'));
            }
            // dump($r);die;
        }elseif($payway == 'mobile_alipay'){
            $alipay_obj = new AlipayModel();
            // $r = $alipay_obj->ali_refund($pay_code, $refund_fee, 'site');
            $r = $alipay_obj->ali_refund($order_id, 'activity');
            $this->ajaxReturn(array('code'=>2, 'link'=> $r));
            // die;
            // redirect($r);
        }
        $this->ajaxReturn(array('code'=>1, 'msg'=>'对不起，退款失败'));
    }
}