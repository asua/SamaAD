<?php
/**
 * Acp后台场地类型
 */
class AcpSiteTypeAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
        $this->assign('action_title', '场地类型');
        $this->assign('action_src', U('/AcpSiteType/get_site_type_list'));
    }

    public function get_site_type_list()
    {
        $site_type_obj = new TypeModel();
        $where     = 'type_type ='.TypeModel::SITE;
        //数据总量
        $total = $site_type_obj->getTypeNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $site_type_obj->setStart($Page->firstRow);
        $site_type_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $site_type_list = $site_type_obj->getTypeList($where);
        $this->assign('site_type_list', $site_type_list);
        $this->assign('head_title', '场地类型列表');
        $this->display();
    }

    //添加场地类型
    public function add_site_type()
    {

        $act = $this->_post('act');
        if ($act == 'add') {
            $_post      = $this->_post();
            $type_name = $_post['type_name'];
            $serial     = $_post['serial'];

            //表单验证
            if (!$type_name) {
                $this->error('请填写场地类型名称！');
            }

            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }


            $arr = array(
                'type_name' => $type_name,
                'serial'     => $serial,
                'type_type' => TypeModel::SITE,
            );

            $site_type_obj = new TypeModel();
            $success   = $site_type_obj->addType($arr);

            if ($success) {
                $this->success('恭喜您，场地类型添加成功！', '/AcpSiteType/add_site_type');
            } else {
                $this->error('抱歉，场地类型添加失败！', '/AcpSiteType/add_site_type');
            }
        }

        $this->assign('act', 'add');
        $this->assign('head_title', '添加场地类型');
        $this->display();
    }



    //修改场地类型
    public function edit_site_type()
    {
        $redirect = U('/AcpSiteType/get_site_type');
        $site_type_id = intval($this->_get('type_id'));
        if (!$site_type_id) {
            $this->error('对不起，非法访问！', $redirect);
        }

        $site_type_obj  = new TypeModel();
        $site_type_info = $site_type_obj->getTypeInfo('type_id ='.$site_type_id);

        if (!$site_type_info) {
            $this->error('对不起，不存在相关场地类型！', $redirect);
        }

        $act = $this->_post('act');

        if ($act == 'edit') {
            $_post      = $this->_post();
            $site_type_name = $_post['type_name'];
            $serial     = $_post['serial'];

            //表单验证
            if (!$site_type_name) {
                $this->error('请填写场地类型名称！');
            }

            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }


            $arr = array(
                'type_name' => $site_type_name,
                'serial'     => $serial,
            );

            $url       = '/AcpSiteType/edit_site_type/type_id/' . $site_type_id;

            if ($site_type_obj->setType($site_type_id, $arr)) {
                $this->success('恭喜您，场地类型修改成功！', $url);
            } else {
                $this->error('抱歉，场地类型修改失败！', $url);
            }
        }
        $this->assign('site_type_info', $site_type_info);
        $this->assign('act', 'edit');
        $this->assign('head_title', '修改场地类型');
        $this->display();

    }

    /**
     * 删除场地类型
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据场地类型ID删除场地类型
     */
    public function delete_site_type()
    {
        $site_type_id = intval($this->_post('type_id'));
        if ($site_type_id) {
            $site_type_obj = new TypeModel($site_type_id);
            $site_obj = new SiteModel();
//            $num                = $site_obj->getSiteNum('FIND_IN_SET('.$site_type_id.',site_type)');
            $num                = $site_obj->getSiteNum('site_type_id ='.$site_type_id);
            //存在服务商则删除失败；
            if ($num) {
                exit('failure');
            }
            $success = $site_type_obj->delType();
            exit($success ? 'success' : 'failure');
        }

        exit('failure');
    }

    public function batch_delete_site_type()
    {

        $site_type_ids = $this->_post('type_ids');
        if ($site_type_ids) {
            $site_type_ary = explode(',', $site_type_ids);
            $success_num     = 0;
            $site_obj = new SiteModel();
            foreach ($site_type_ary as $site_type_id) {
//                $num = $service_provides_obj->getServiceProvidersNum('FIND_IN_SET('.$site_type_id.',site_type)');
                $num                = $site_obj->getSiteNum('site_type_id ='.$site_type_id);
                if ($num) {
                    continue;
                }

                $site_type_obj = new TypeModel($site_type_id);
                $success_num += $site_type_obj->delType();
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }

        exit('failure');
    }


}
