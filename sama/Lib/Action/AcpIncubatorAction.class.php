<?php
/**
 * 孵化器类
 */

class AcpIncubatorAction extends AcpAction
{
    public function AcpIncubatorAction()
    {
        parent::_initialize();
    }

/*    public function add_incubator()
    {

        $province_obj = new AddressProvinceModel();//获取省信息
        $province_list = $province_obj->getProvinceList();
        $city_obj = new AddressCityModel();
        $city_list = $city_obj->getCityList();//市信息
        $equipment_obj=new SiteDeployModel();//获取配套设施相关列表
        $equipment_list=$equipment_obj->getSiteDeployList('site_deploy_type = 1','sort');
        $data = $this->_post();

        if ($data['opt'] == 'add') {
            $this->data_is_correct($data);//检查数据是否正确
            $data['pic_url'] = implode(',', $data['pic']);
            $arr = array(
                'incubator_type' => $data['incubator_type'],//孵化器类型
                'incubator_name' => $data['incubator_name'],
                'is_index'=>$data['is_index'],
                'park_type' => $data['park_type'],//园区类型
                'tel' => $data['mobile'],
                'province_id' => $data['province'],
                'city_id' => $data['city'],
                'address' => $data['address'],
                'longitude' => $data['center_lng'],
                'latitude' => $data['center_lat'],
                'pic_url' => $data['pic_url'],
                'describe' => $data['contents'],
                'equipment_id'=>implode(',',$data['equipment']),
                'sort'=>$data['sort'],
            );
            $incubator_obj = new IncubatorModel();
            $result = $incubator_obj->addIncubator($arr);
            if ($result) {
                $this->success('添加成功！', U('/AcpIncubator/get_incubator_list'));
            } else {
                $this->error('添加失败', U('/AcpIncubator/add_incubator'));
            }
        }
        $this->assign('city_list', $city_list);
        $this->assign('province_list', $province_list);
        $this->assign('head_title', '添加孵化器');
        $this->assign('equipment_list',$equipment_list);
        $this->assign('pic_data', array(
            'batch' => true,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳，建议使用4张以内图片',
        ));
        $this->display();
    }*/

    //添加园区
    public function add_park(){
        $province_obj = new AddressProvinceModel();//获取省信息
        $province_list = $province_obj->getProvinceList();
        $city_obj = new AddressCityModel();
        $city_list = $city_obj->getCityList();//市信息
        $equipment_obj=new SiteDeployModel();//获取配套设施相关列表
        $equipment_list=$equipment_obj->getSiteDeployList('site_deploy_type = 1','sort');
        $data = $this->_post();

        if ($data['opt'] == 'add') {
            $this->data_is_correct($data);//检查数据是否正确
            $data['pic_url'] = implode(',', $data['pic']);
            $arr = array(
                'incubator_type' => $data['incubator_type'],//孵化器类型
                'incubator_name' => $data['incubator_name'],
                'is_index'=>$data['is_index'],
                'park_type' => $data['park_type'],//园区类型
                'tel' => $data['mobile'],
                'province_id' => $data['province'],
                'city_id' => $data['city'],
                'address' => $data['address'],
                'longitude' => $data['center_lng'],
                'latitude' => $data['center_lat'],
                'pic_url' => $data['pic_url'],
                'describe' => $data['contents'],
                'equipment_id'=>implode(',',$data['equipment']),
                'sort'=>$data['sort'],
            );
            $incubator_obj = new IncubatorModel();
            $result = $incubator_obj->addIncubator($arr);
            if ($result) {
                $this->success('添加成功！', U('AcpIncubator/get_park_list'));
            } else {
                $this->error('添加失败');
            }
        }
        $this->assign('city_list', $city_list);
        $this->assign('province_list', $province_list);
        $this->assign('head_title', '添加孵化器');
        $this->assign('equipment_list',$equipment_list);
        $this->assign('pic_data', array(
            'batch' => true,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳，建议使用4张以内图片',
        ));
        $this->display();

    }
    //添加众创空间
    public function add_zhongchuang(){
        $province_obj = new AddressProvinceModel();//获取省信息
        $province_list = $province_obj->getProvinceList();
        $city_obj = new AddressCityModel();
        $city_list = $city_obj->getCityList();//市信息
        $equipment_obj=new SiteDeployModel();//获取配套设施相关列表
        $equipment_list=$equipment_obj->getSiteDeployList('site_deploy_type = 1','sort');
        $data = $this->_post();

        if ($data['opt'] == 'add') {
            $this->data_is_correct($data);//检查数据是否正确
            $data['pic_url'] = implode(',', $data['pic']);
            $arr = array(
                'incubator_type' => $data['incubator_type'],//孵化器类型
                'incubator_name' => $data['incubator_name'],
                'is_index'=>$data['is_index'],
                'park_type' => $data['park_type'],//园区类型
                'tel' => $data['mobile'],
                'province_id' => $data['province'],
                'city_id' => $data['city'],
                'address' => $data['address'],
                'longitude' => $data['center_lng'],
                'latitude' => $data['center_lat'],
                'pic_url' => $data['pic_url'],
                'describe' => $data['contents'],
                'equipment_id'=>implode(',',$data['equipment']),
                'sort'=>$data['sort'],
            );
            $incubator_obj = new IncubatorModel();
            $result = $incubator_obj->addIncubator($arr);
            if ($result) {
                $this->success('添加成功！', U('/AcpIncubator/get_zhongchuang_list'));
            } else {
                $this->error('添加失败');
            }
        }
        $this->assign('city_list', $city_list);
        $this->assign('province_list', $province_list);
        $this->assign('head_title', '添加孵化器');
        $this->assign('equipment_list',$equipment_list);
        $this->assign('pic_data', array(
            'batch' => true,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳，建议使用4张以内图片',
        ));
        $this->display();
    }





    public function get_incubator_list()
    {//孵化器列表
        $incubator_obj = new IncubatorModel();
        $where = '1';
        if ($this->_request('incubator_id')) {
            $incubator_id = $this->_request('incubator_id');
            $where .= ' AND incubator_id =' . $incubator_id;
        }
        if ($this->_request('incubator_name')) {
            $incubator_name = $this->_request('incubator_name');
            $where .= ' AND incubator_name LIKE "%' . $incubator_name . '%"';
        }
        if ($this->_request('incubator_type')) {
            $incubator_type = $this->_request('incubator_type');
            $where .= ' AND incubator_type = ' . $incubator_type;
        }
        if ($this->_request('park_type')) {
            $park_type = $this->_request('park_type');
            $where .= ' AND park_type= ' . $park_type;
        }
        if ($this->_request('is_index')!=-1&&$this->_request('is_index')!=''){
            $is_index = $this->_request('is_index');
            $where .= ' AND is_index= ' . $is_index;
        }
        import('ORG.Util.Pagelist');
        $count = $incubator_obj->getIncubatorNum($where);

        $Page = new Pagelist($count, C('PER_PAGE_NUM'));
        $incubator_obj->setStart($Page->firstRow);
        $incubator_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('page', $Page);
        $this->assign('show', $show);

        $incubator_list = $incubator_obj->getIncubatorList('', $where, 'incubator_id DESC');
        $incubator_list = $incubator_obj->getIncubatorData($incubator_list);
        if ($incubator_list == false) {
            $incubator_list = array();
        }
        $this->assign('is_index',$is_index==-1||$is_index==''?-1:$is_index);
        $this->assign('incubator_name', $incubator_name);
        $this->assign('incubator_type', $incubator_type);
        $this->assign('park_type', $park_type);
        $this->assign('incubator_list', $incubator_list);
        $this->assign('head_title', '孵化器列表');
        $this->display();
    }

    //孵化器判断post过来的数据是否缺失、正确
    public function data_is_correct($data){
        if(!$data['incubator_name']){
            $this->error('请填写孵化器名');
        }
     /*   if(!$data['incubator_type']||!$data['park_type']){
            $this->error('请选择类型');
        }*/
        if(!$data['mobile']){
            $this->error('请填写联系电话');
        }
        
        
        if(!$data['province']||!$data['city']){
            $this->error('请选择省市');
        }
        if(!$data['center_lng']||!$data['center_lat']||!$data['address']){
            $this->error('请选择地区并且填写经纬度(经纬度可控制地图标签获取)');
        }
        if(!$data['pic']){
            $this->error('请上传图片');
        }
        if(!$data['contents']){
            $this->error('请写孵化器简介');
        }
        if(!$data['equipment']){
            $this->error('请写选择配套设施');
        }

    }
    //园区列表
    function get_park_list(){
        $incubator_obj = new IncubatorModel();
        $where = 'incubator_type = 1';
        if ($this->_request('incubator_id')) {
            $incubator_id = $this->_request('incubator_id');
            $where .= ' AND incubator_id =' . $incubator_id;
        }
        if ($this->_request('incubator_name')) {
            $incubator_name = $this->_request('incubator_name');
            $where .= ' AND incubator_name LIKE "%' . $incubator_name . '%"';
        }

        if ($this->_request('park_type')) {
            $park_type = $this->_request('park_type');
            $where .= ' AND park_type= ' . $park_type;
        }
        if ($this->_request('is_index')!=-1&&$this->_request('is_index')!=''){
            $is_index = $this->_request('is_index');
            $where .= ' AND is_index= ' . $is_index;
        }
        import('ORG.Util.Pagelist');
        $count = $incubator_obj->getIncubatorNum($where);

        $Page = new Pagelist($count, C('PER_PAGE_NUM'));
        $incubator_obj->setStart($Page->firstRow);
        $incubator_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('page', $Page);
        $this->assign('show', $show);

        $incubator_list = $incubator_obj->getIncubatorList('', $where, 'incubator_id DESC');
        $incubator_list = $incubator_obj->getIncubatorData($incubator_list);
        if ($incubator_list == false) {
            $incubator_list = array();
        }
        $this->assign('is_index',$is_index==-1||$is_index==''?-1:$is_index);
        $this->assign('incubator_name', $incubator_name);
        $this->assign('incubator_type', $incubator_type);
        $this->assign('park_type', $park_type);
        $this->assign('incubator_list', $incubator_list);
        $this->assign('head_title', '园区列表');
        $this->display('get_incubator_list');
    }
    //众创空间列表
    function get_zhongchuang_list(){
        $incubator_obj = new IncubatorModel();
        $where = 'incubator_type = 2';
        if ($this->_request('incubator_id')) {
            $incubator_id = $this->_request('incubator_id');
            $where .= ' AND incubator_id =' . $incubator_id;
        }
        if ($this->_request('incubator_name')) {
            $incubator_name = $this->_request('incubator_name');
            $where .= ' AND incubator_name LIKE "%' . $incubator_name . '%"';
        }

        if ($this->_request('park_type')) {
            $park_type = $this->_request('park_type');
            $where .= ' AND park_type= ' . $park_type;
        }
        if ($this->_request('is_index')!=-1&&$this->_request('is_index')!=''){
            $is_index = $this->_request('is_index');
            $where .= ' AND is_index= ' . $is_index;
        }
        import('ORG.Util.Pagelist');
        $count = $incubator_obj->getIncubatorNum($where);

        $Page = new Pagelist($count, C('PER_PAGE_NUM'));
        $incubator_obj->setStart($Page->firstRow);
        $incubator_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('page', $Page);
        $this->assign('show', $show);

        $incubator_list = $incubator_obj->getIncubatorList('', $where, 'incubator_id DESC');
        $incubator_list = $incubator_obj->getIncubatorData($incubator_list);
        if ($incubator_list == false) {
            $incubator_list = array();
        }
        $this->assign('is_index',$is_index==-1||$is_index==''?-1:$is_index);
        $this->assign('incubator_name', $incubator_name);
        $this->assign('incubator_type', $incubator_type);
        $this->assign('park_type', $park_type);
        $this->assign('incubator_list', $incubator_list);
        $this->assign('head_title', '众创空间列表');
        $this->display('get_incubator_list');
    }

    public function getcity()
    {
        $province_id = $this->_post('province_id');
        $city_obj = new AddressCityModel();
        $city_list = $city_obj->getCity($province_id);//市信息
        echo json_encode($city_list);
        exit;
    }

    // 删除孵化器
    public function del_incubator()
    {
        $incubator_id = $this->_post('incubator_id');
        $incubator_obj = new IncubatorModel();
        $result = $incubator_obj->delIncubator($incubator_id);
        if ($result) {
            echo 'success';
            exit;
        } else {
            echo 'fail';
            exit;
        }
    }
    //批量删除孵化器
    public function del_incubator_batch(){
        $ids=$this->_post('id');

        $incubator_obj=new IncubatorModel();
        $idarr=explode(',',$ids);
        array_pop($idarr);//删除元素最后一个元素
        foreach($idarr as $id){
            $r=$incubator_obj->delIncubator($id);
            if(!$r){
                echo 'fail';
                exit;
            }
        }
        echo 'success';
        exit;
    }

    //园区详情
    function park_detail(){
        $this->incubator_detail();
    }
    //众创空间详情
    function zhongchuang_detail(){
        $this->incubator_detail();
    }

    //孵化器详情
    public function incubator_detail()
    {
        $incubator_id = $this->_get('incubator_id');
        $incubator_obj = new IncubatorModel();
        $incubator_info = $incubator_obj->getIncubatorInfo('incubator_id = ' . $incubator_id);

        //获取相关省市名称
        $province_obj = new AddressProvinceModel();
        $city_obj = new AddressCityModel();
        $incubator_info['province'] = $province_obj->getProvinceName($incubator_info['province_id']);
        $incubator_info['city'] = $city_obj->getCityName($incubator_info['city_id']);

        //处理照片
        $picarr = explode(',', $incubator_info['pic_url']);

        $this->assign('picarr', $picarr);
        $this->assign('incubator_info', $incubator_info);
        $this->assign('head_title', '孵化器详情');
        $this->display('incubator_detail');
    }
    //编辑园区
    public function edit_park(){
        $incubator_id = $this->_get('incubator_id');
        $this->edit_incubator($incubator_id);
    }
    //编辑众创空间
    public function edit_zhongchuang(){
        $incubator_id = $this->_get('incubator_id');
        $this->edit_incubator($incubator_id);
    }

    //编辑孵化器
    public function edit_incubator($incubator_id)
    {
        //$incubator_id = $this->_get('incubator_id');
        $incubator_obj = new IncubatorModel();
        $incubator_info = $incubator_obj->getIncubatorInfo('incubator_id = ' . $incubator_id);

        $province_obj = new AddressProvinceModel();//获取省信息
        $province_list = $province_obj->getProvinceList();
        $city_obj = new AddressCityModel();
        $city_list = $city_obj->getCity($incubator_info['province_id']);//市信息

        $equipment_obj=new SiteDeployModel();//获取配套设施相关列表
        $equipment_list=$equipment_obj->getSiteDeployList('site_deploy_type = 1','sort');

        $data = $this->_post();

        if ($data['opt'] == 'edit') {
            $data['pic_url'] = implode(',', $data['pic']);
            $this->data_is_correct($data);//检查数据是否正确
            $arr = array(
                'incubator_type' => $data['incubator_type'],//孵化器类型
                'incubator_name' => $data['incubator_name'],
                'park_type' => $data['park_type'],//园区类型
                'is_index'=>$data['is_index'],
                'tel' => $data['mobile'],
                'province_id' => $data['province'],
                'city_id' => $data['city'],
                'address' => $data['address'],
                'longitude' => $data['center_lng'],
                'latitude' => $data['center_lat'],
                'pic_url' => $data['pic_url'],
                'describe' => $data['contents'],
                'equipment_id'=>implode(',',$data['equipment']),
                'sort'=>$data['sort']
            );
            $incubator_obj = new IncubatorModel();
            $result = $incubator_obj->editIncubator($arr, $data['incubator_id']);
            if ($result) {
                if( $data['incubator_type']==1){
                    $this->success('修改成功！', U('/AcpIncubator/get_park_list'));
                }else{
                    $this->success('修改成功！', U('/AcpIncubator/get_zhongchuang_list'));
                }

            } else {
                $this->error('修改失败,请填写完整信息', U('/AcpIncubator/get_incubator_list'));
            }
        }


        //获取相关省市名称
        $province_obj = new AddressProvinceModel();
        $city_obj = new AddressCityModel();
        $incubator_info['province'] = $province_obj->getProvinceName($incubator_info['province_id']);
        $incubator_info['city'] = $city_obj->getCityName($incubator_info['city_id']);

        //相关配套处理成数组
        $incubator_equipment=explode(',',$incubator_info['equipment_id']);
        $pic_arr = explode(',', $incubator_info['pic_url']);

        $this->assign('pic_data', array(
            'batch' => true,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳，建议使用4张以内图片',
            'pic_arr' => $pic_arr,
        ));
        $this->assign('incubator_equipment',$incubator_equipment);
        $this->assign('equipment_list',$equipment_list);
        $this->assign('province_list', $province_list);
        $this->assign('city_list', $city_list);
        $this->assign('incubator_info', $incubator_info);
        $this->assign('head_title', '编辑孵化器');
        $this->display('edit_incubator');
    }
    //配套设备列表
    public function get_equipment_list(){

        $incubator_id=$this->_get('incubator_id');
        $equipment_obj=new EquipmentModel();
        $where='1';
        if($this->_request('opt')=='select'){
            $equipment_name=$this->_request('equipment_name');
            $where.=' AND equipment  LIKE "%' . $equipment_name . '%"';
        }
        import('ORG.Util.Pagelist');
        $where.= $incubator_id ? ' AND incubator_id = '.$incubator_id : '';
        $count = $equipment_obj->getEquipmentNum($where);
        /*echo $count;
        exit;*/
        $Page = new Pagelist($count, C('PER_PAGE_NUM'));//分页
        $equipment_obj->setStart($Page->firstRow);
        $equipment_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('page', $Page);
        $this->assign('show', $show);

        $equipment_list=$equipment_obj->getEquipmentlist($where);

        $incubator_obj = new IncubatorModel();
        foreach($equipment_list as $k=>$v){
            $incubator_info=$incubator_obj->getIncubatorInfo('incubator_id = '.$v['incubator_id']);
            $equipment_list[$k]['incubator_name']=$incubator_info['incubator_name'];
        }
        $this->assign('equipment_name',$equipment_name);
        $this->assign('equipment_list',$equipment_list);
        $this->assign('incubator_id',$incubator_id);
        $this->assign('head_title', '配套设备列表');
        $this->display();

    }
    //增加配套设施
    public function add_equipment(){
        $incubator_id=$this->_get('incubator_id');
        $data=$this->_post();
        if($data['opt']=='add'){
            $arr=array(
                'incubator_id'=>$data['incubator_id'],
                'equipment'=>$data['equipment_name'],
                'pic_url'=>$data['pic'],
                'sort'=>$data['sort'],
            );
            $equipment_obj=new EquipmentModel();
            $result=$equipment_obj->addEquipment($arr);
            if($result){
                $url='/AcpIncubator/get_equipment_list/incubator_id/'.$data['incubator_id'];
                $this->success('添加成功！',U($url));
            }else{
                $this->error('添加失败！');
        }
        }
        $this->assign('pic_data', array(
            'batch' => false,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳',
        ));
        $this->assign('incubator_id',$incubator_id);
        $this->assign('head_title', '增加设备列表');
        $this->display();
    }
    public function edit_equipment(){
        $equipment_id=$this->_get('equipment_id');
        $incubator_id=$this->_get('incubator_id');
        $equipment_obj=new EquipmentModel();
        $equipment_info= $equipment_obj->getEquipmentInfo($equipment_id);
       /* echo json_encode($equipment_info);
        exit;*/
        $this->assign('pic_data', array(
            'batch' => false,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳，建议使用4张以内图片',
            'url' => $equipment_info['pic_url'],
        ));
        $data=$this->_post();
        if($data['opt']=='edit'){
            $arr=array(
                'incubator_id'=>$data['incubator_id'],
                'equipment'=>$data['equipment_name'],
                'pic_url'=>$data['pic'],
                'sort'=>$data['sort'],
            );
            $result=$equipment_obj->editEquipment($arr,$data['equipment_id']);
            if($result){
                $url='/AcpIncubator/get_equipment_list/incubator_id/'.$data['incubator_id'];
                $this->success('修改成功！',U($url));
            }else{
                $this->error('修改失败');
            }
        }
        $url='/AcpIncubator/get_equipment_list/incubator_id/'.$incubator_id;
        $this->assign('action_title', '配套设备');
        $this->assign('action_src', U($url));
        $this->assign('equipment_id',$equipment_id);
        $this->assign('incubator_id',$incubator_id);
        $this->assign('equipment_info',$equipment_info);
        $this->assign('head_title','编辑配套设备');
        $this->display();
    }
    //删除设备
    public function del_equipment(){
        $equipment_id=$this->_post('equipment_id');
        $equipment_obj=new EquipmentModel();
        $result=$equipment_obj->delEquipment($equipment_id);
        if($result){
            echo 'success';
            exit;
        }else{
            echo 'fail';
            exit;
        }

    }
    //批量删除设备
    public function del_equipment_batch(){
        $ids=$this->_post('id');

        $equipment_obj=new EquipmentModel();
        $idarr=explode(',',$ids);
        array_pop($idarr);//删除元素最后一个元素
        foreach($idarr as $id){
            $r=$equipment_obj->delEquipment($id);
            if(!$r){
                echo 'fail';
                exit;
            }
        }
        echo 'success';
        exit;
    }
    //周边配套列表
    public function get_supporting_list(){
        $incubator_id=$this->_get('incubator_id');
        $supporting_obj=new SupportingModel();
        $where='1';
        if($this->_request('opt')=='select'){
            $equipment_name=$this->_request('equipment_name');
            $where.=' AND equipment  LIKE "%' . $equipment_name . '%"';
        }
        import('ORG.Util.Pagelist');
        $where.=' AND incubator_id = '.$incubator_id;
        $count = $supporting_obj->getSupportingNum($where);

        $Page = new Pagelist($count, C('PER_PAGE_NUM'));//分页
        $supporting_obj->setStart($Page->firstRow);
        $supporting_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('page', $Page);
        $this->assign('show', $show);


        $supporting_list=$supporting_obj->getSupportinglist($where);

        $incubator_obj = new IncubatorModel();
        foreach($supporting_list as $k=>$v){
            $incubator_info=$incubator_obj->getIncubatorInfo('incubator_id = '.$v['incubator_id']);
            $supporting_list[$k]['incubator_name']=$incubator_info['incubator_name'];
        }
        $this->assign('supporting_list',$supporting_list);
        $this->assign('head_title', '周边配套列表');
        $this->assign('incubator_id',$incubator_id);
        $this->display();
    }
    public function add_supporting(){
        $incubator_id=$this->_get('incubator_id');
        $data=$this->_post();
        if($data['opt']=='add'){

            $arr=array(
                'supporting_name'=>$data['supporting_name'],
                'incubator_id'=>$data['incubator_id'],
                'num'=>$data['supporting_num'],
                'describe'=>$data['describe'],
                'pic_url'=>$data['pic'],
                'sort'=>$data['sort']
            );
            $supporting_obj=new SupportingModel();
            $result=$supporting_obj->addSupporting($arr);
            if($result){
                $url='/AcpIncubator/get_supporting_list/incubator_id/'.$data['incubator_id'];
                $this->success('添加成功！',U($url));
            }else{
                $this->error('添加失败！');
            }
        }

        $this->assign('pic_data', array(
            'batch' => false,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳',
        ));
        $this->assign('incubator_id',$incubator_id);
        $this->assign('head_title','添加周边配套');
        $this->display();
    }
    //删除周边配套
    public function del_supporting(){
        $supporting_id=$this->_post('supporting_id');
        $supporting_obj=new SupportingModel();
        $result=$supporting_obj->delSupporting($supporting_id);
        if($result){
            echo 'success';
            exit;
        }else{
            echo 'fail';
            exit;
        }

    }
    //批量删除周边配套
    public function del_supporting_batch(){
        $ids=$this->_post('id');

        $supporting_obj=new SupportingModel();
        $idarr=explode(',',$ids);
        array_pop($idarr);//删除元素最后一个元素
        foreach($idarr as $id){
            $r=$supporting_obj->delSupporting($id);
            if(!$r){
                echo 'fail';
                exit;
            }
        }
        echo 'success';
        exit;
    }
    public function edit_supporting(){
        $supporting_id=$this->_get('supporting_id');
        $incubator_id=$this->_get('incubator_id');
        $supporting_obj=new SupportingModel();
        $supporting_info= $supporting_obj->getSupportingInfo($supporting_id);
        $this->assign('pic_data', array(
            'batch' => false,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳，建议使用4张以内图片',
            'url' => $supporting_info['pic_url'],
        ));
        $data=$this->_post();
        if($data['opt']=='edit'){
            $arr=array(
                'supporting_name'=>$data['supporting_name'],
                'incubator_id'=>$data['incubator_id'],
                'num'=>$data['supporting_num'],
                'describe'=>$data['describe'],
                'sort'=>$data['sort'],
                'pic_url'=>$data['pic'],
            );
            $result=$supporting_obj->editSupporting($arr,$data['supporting_id']);
           /* echo $supporting_obj->getLastSql();
            exit;*/
            if($result){
                $url='/AcpIncubator/get_supporting_list/incubator_id/'.$data['incubator_id'];
                $this->success('修改成功！',U($url));
            }else{
                $this->error('修改失败');
            }
        }
        $url='/AcpIncubator/get_supporting_list/incubator_id/'.$incubator_id;
        $this->assign('action_title', '周边配套');
        $this->assign('action_src', U($url));
        $this->assign('supporting_id',$supporting_id);
        $this->assign('incubator_id',$incubator_id);
        $this->assign('supporting_info',$supporting_info);
        $this->assign('head_title','编辑周边配套');
        $this->display();
    }

    //工位列表
    public function station_list(){
        $incubator_id = I('incubator_id', 0, 'intval');
        if(!$incubator_id){
            $this->error('孵化器不存在');
        }
        $where = 'incubator_id ='.$incubator_id;
        $station_obj = D('Station');
        $show = getPageList($station_obj, 'getStationNum', $where);
        $this->assign('show', $show);
        $station_list = $station_obj->getStationList('',$where);
        $station_list = $station_obj->getListData($station_list);
        $this->assign('station_list', $station_list);
        $this->assign('incubator_id', $incubator_id);
        $this->assign('head_title', '工位列表');
        $this->display();
    }

    //添加工位
    public function add_station(){
        $incubator_id = I('incubator_id', 0, 'intval');
        if(!$incubator_id){
            $this->error('孵化器不存在');
        }
        $act = I('act');
        if($act == 'add' && IS_POST){
            $station_type_name = I('station_type_name');
            $price = I('price', 0.0, 'floatval');
            $station_total_num = I('station_total_num', 0, 'intval');
            $pic = I('pic');
            $desc = I('desc');
            $sort=I('sort');
            if(!$station_type_name){
                $this->error('请填写工位类型名称');
            }
            if(!$price){
                $this->error('请填写工位价格');
            }
            if(!$station_total_num || $station_total_num <= 0){
                $this->error('工位数量必须大于0');
            }
            if(!$pic){
                $this->error('请上传工位图片');
            }
            if(!$desc){
                $this->error('请填写简介');
            }
            if(!$sort){
                $this->error('请填写排序号');
            }
            $data = array(
                'station_type_name' => $station_type_name,
                'price' => $price,
                'station_total_num' => $station_total_num,
                'station_left_num' => $station_total_num,
                'pic' => $pic,
                'incubator_id' => $incubator_id,
                'station_desc' =>$desc,
                'sort'=>$sort,
                );
            $station_obj = D('Station');
            if($station_obj->addStation($data)){
                $this->success('工位添加成功');
            }else{
                $this->error('工位添加失败');
            }

        }

        $this->assign('pic_data', array(
            'batch' => false,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳',
            'url' => '',
        ));
        $this->assign('incubator_id', $incubator_id);
        $this->assign('act', 'add');
        $this->assign('head_title', '添加工位');
        $this->display();
    }

    //修改工位
    public function edit_station(){
        $station_type_id = I('station_type_id', 0, 'intval');
        if(!$station_type_id){
            $this->error('工位不存在');
        }
        $station_obj = D('Station');
        $station = $station_obj->getStationInfo('station_type_id ='.$station_type_id);
        $this->assign('station', $station);
        $act = I('act');
        if($act == 'edit' && IS_POST){
            $station_type_name = I('station_type_name');
            $price = I('price', 0.0, 'floatval');
            $station_total_num = I('station_total_num', 0, 'intval');
            $pic = I('pic');
            $desc= I('desc');
            $sort= I('sort');
            if(!$station_type_name){
                $this->error('请填写工位类型名称');
            }
            if(!$price){
                $this->error('请填写工位价格');
            }
            if(!$pic){
                $this->error('请上传工位图片');
            }
            if($sort){
                $this->error('请填写排序号');
            }
            $data = array(
                'station_type_name' => $station_type_name,
                'price' => $price,
                'pic' => $pic,
                'station_desc'=>$desc,
                'sort'=>$sort,
                );
            $station_obj = new StationModel($station_type_id);
            if($station_obj->editStation($data)){
                $this->success('工位修改成功');
            }else{
                $this->error('工位修改失败');
            }

        }

        $this->assign('pic_data', array(
            'batch' => false,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳',
            'url' => $station['pic'],
        ));
        $this->assign('station_type_id', $station_type_id);
        $this->assign('act', 'edit');
        $this->assign('head_title', '修改工位');
        $this->display('add_station');
    }

    //删除工位
    public function del_station(){
        $station_ids = I('ids');
        $station_obj = new StationModel();
        if($station_obj->delStation('station_type_id in('.$station_ids.')')){
            $this->ajaxReturn('success');
        }else{
            $this->ajaxReturn('failure');
        }
    }


    //入驻列表
    public function get_settled_list(){
        $settled_obj=new EnterTeamModel();
        $incubator_id=$this->_get('incubator_id');
        $where='incubator_id ='.$incubator_id;
        $data=$this->_request();
        if($data['opt']=='select'){
            $settled_name=$data['settled_name'];
            $where .= ' AND team_name LIKE "%'.$settled_name.'%"';
        }
        import('ORG.Util.Pagelist');
        $count = $settled_obj->getEnterTeamNum($where);
        $Page = new Pagelist($count, C('PER_PAGE_NUM'));//分页
        $settled_obj->setStart($Page->firstRow);
        $settled_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $settled_list=$settled_obj->getEnterTeamList('',$where);//获取入驻列表信息
        $domain_obj=new TypeModel();//把数据中的领域id转化为名字
        foreach ($settled_list as $k=>$v){
            $name_arr=$domain_obj->getTypeNameData($v['domain']);
            $settled_list[$k]['domain_name']=implode(',',$name_arr);
        }
        $this->assign('settled_list',$settled_list);
        $this->assign('incubator_id',$incubator_id);
        $this->assign('settled_name',$settled_name?$settled_name:'');
        $this->assign('page', $Page);
        $this->assign('show', $show);
        $this->assign('head_title', '入驻列表');
        $this->display();
    }
    //添加入驻
    public function add_settled(){
        $incubator_id=$this->_get('incubator_id');
        $domain_obj=new TypeModel();
        $domain_list=$domain_obj->getTypeList('type_type = 1');
        $this->assign('pic_data', array(
            'batch' => false,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳',
            'url' => '',
        ));
        $data=$this->_post();
        if($data['opt']=='add'){
            $enter_team_obj=new EnterTeamModel();
            $arr=array(
                'team_name'=>$data['settled_name'],
                'pic'=>$data['pic'],
                'domain'=>implode(',',$data['domain']),
                'incubator_id'=>$data['incubator_id'],
                'sort'=>$data['sort'],
            );
            $return=$enter_team_obj->addEnterTeam($arr);
            if($return){
                $this->success('添加成功！','/AcpIncubator/get_settled_list/incubator_id/'.$data['incubator_id']);
            }else{
                $this->error('添加失败');
            }
        }
        $url='/AcpIncubator/get_settled_list/incubator_id/'.$incubator_id;
        $this->assign('action_title', '入驻列表');
        $this->assign('action_src', U($url));
        $this->assign('head_title', '增加入驻');
        $this->assign('incubator_id',$incubator_id);
        $this->assign('domain_list', $domain_list);
        $this->display();
    }
    //编辑入驻
    public  function edit_settled(){
        $incubator_id=$this->_get('incubator_id');
        $settled_id=$this->_get('settled_id');
        $enter_team_obj=new EnterTeamModel();
        $settled_info=$enter_team_obj->getEnterTeamInfo('enter_team_id = '.$settled_id);
       /* echo json_encode($settled_info);
        exit;*/
        $current_domain=explode(',',$settled_info['domain']);
        $data=$this->_post();
        if($data['opt']=='edit'){
            $arr=array(
                'team_name'=>$data['settled_name'],
                'pic'=>$data['pic'],
                'domain'=>implode(',',$data['domain']),
                'incubator_id'=>$data['incubator_id'],
                'sort'=>$data['sort'],
            );
            $result=$enter_team_obj->editEnterTeam($arr,$data['settled_id']);
            if($result){
                $this->success('修改成功！','/AcpIncubator/get_settled_list/incubator_id/'.$data['incubator_id']);
            }else{
                $this->error('修改失败');
            }
        }
        $this->assign('pic_data', array(
            'batch' => false,
            'name' => 'pic',
            'help' => '图片800x800像素的效果最佳，建议使用4张以内图片',
            'url' => $settled_info['pic'],
        ));//图片信息
        $domain_obj=new TypeModel();
        $domain_list=$domain_obj->getTypeList('type_type = 1');//领域信息
        $url='/AcpIncubator/get_settled_list/incubator_id/'.$incubator_id;
        $this->assign('action_title', '入驻列表');
        $this->assign('action_src', U($url));
        $this->assign('head_title', '编辑入驻');
        $this->assign('incubator_id',$incubator_id);
        $this->assign('domain_list', $domain_list);//所有领域
        $this->assign('current_domain', $current_domain);//当前领域
        $this->assign('settled_info', $settled_info);
        $this->display();
    }
    //删除入驻
    public function del_settled(){
        $settled_id=$this->_post('settled_id');
        $enter_team_obj=new EnterTeamModel();
        $result=$enter_team_obj->delEnterTeam($settled_id);
        if($result){
            echo 'success';
            exit;
        }else{
            echo 'fail';
            exit;
        }
    }
    //批量删除
    public function del_settled_batch(){
        $ids=$this->_post('id');

        $enter_team_obj=new EnterTeamModel();
        $idarr=explode(',',$ids);
        array_pop($idarr);//删除元素最后一个元素
        foreach($idarr as $id){
            $r=$enter_team_obj->delEnterTeam($id);
            if(!$r){
                echo 'fail';
                exit;
            }
        }
        echo 'success';
        exit;
    }



    //工位类型列表
    public function station_class_list(){
        $incubator_id = I('incubator_id', 0, 'intval');
        $class_obj = new StationClassModel();

        $show = getPageList($class_obj, 'getStationClassNum', 'is_del = 0 and incubator_id ='.$incubator_id);
        $class_list = $class_obj->getStationClassList('','incubator_id ='.$incubator_id, 'serial asc');
        $this->assign('class_list', $class_list);

        $this->assign('incubator_id', $incubator_id);
        $this->assign('head_title', '工位类型列表');
        $this->display();
    }

    //添加/修改工位类型
    public function set_station_class(){
        $incubator_id = I('incubator_id', 0, 'intval');
        $class_id = I('class_id', 0, 'intval');
        $class_obj = D('StationClass');
        if(!$class_id){
            $this->assign('head_title', '添加工位类型');
            $this->assign('act', 'add');
        }else{
            $this->assign('head_title', '修改工位类型');
            $class = $class_obj->getStationClassInfo('station_class_id ='.$class_id);
            $this->assign('info', $class);
            $this->assign('act', 'edit');
            $this->assign('class_id', $class_id);
        }    
        $this->assign('pic', array(
            'batch'   => false,
            'name'    => 'pic',
            'title'    => '图片',
            'url' => $class['pic'] ? $class['pic'] : '',
            'help'    => '点击查看大图',
        ));
        $act = I('act');
        if(IS_POST){
            $class_name = I('class_name');
            $serial = I('serial', 0, 'intval');
            $pic = I('pic');
            $price = I('price', 0.0 ,'floatval' );
            if(!$class_name) $this->error('请输入类型名称');
            if(!$pic) $this->error('请上传分类图片');
            if(!$price) $this->error('请填写工位价格');
            $data = array(
                'class_name' => $class_name,
                'serial' => $serial,
                'pic' => $pic,
                'price' => $price,
                );
            if($act == 'add'){
                if(!$incubator_id) $this->error('工位类型添加失败');
                $data['incubator_id'] = $incubator_id;
                if($class_obj->addStationClass($data)){
                    $this->success('工位类型添加成功');
                }
                $this->error('工位类型添加失败');
            }elseif($act == 'edit'){
                if($class_obj->editStationClass('station_class_id ='.$class_id, $data)){
                    $this->success('工位类型修改成功');
                }
                $this->error('工位类型修改失败');
            }
        }
        

        $this->assign('incubator_id', $incubator_id);
        $this->display();   
    }
}
?>
