<?php
class AcpPostAction extends AcpAction
{

    public function _initialize()
    {
        parent::_initialize();

//        $this->assign('action_title', '帖子列表');
//        $this->assign('action_src', U('/AcpSite/get_site_list'));
    }


    private function get_search_condition(){
        $where = '';

        $post_name = I('post_name');
        if($post_name){
            $where .= ' and title like "%'.$post_name.'%"';
            $this->assign('post_name', $post_name);
        }

        $user_name = I('user_name');
        if($user_name){
            $user_obj = new UserModel();
            $user_ids = $user_obj->getUserFields('user_id','nickname like "%'.$user_name.'%"');
            if($user_ids){
                $where .= ' and user_id IN('.implode(',', $user_ids).')';
            }else{
                $where = false;
            }
            $this->assign('user_name', $user_name);
        }

        $channel_id = I('channel_id');
        if($channel_id){
            $where .= ' and channel_id ='.$channel_id;
            $this->assign('channel_id', $channel_id);
        }

        //心愿悬赏搜索．
        $reward_post_name = I('reward_post_name');
        if($reward_post_name){
            $post_obj =new PostModel();
            $post_ids = $post_obj->getPostFields('post_type ='.PostModel::REWARD.' AND title like "%'.$reward_post_name.'%"','post_id');
            $post_id_str = implode(',',$post_ids);
            if($post_id_str){
                $where .= ' and post_id IN ('.$post_id_str.')';
            }else{
                $where .= ' and false';
            }
            $this->assign('reward_post_name', $reward_post_name);
        }

        $reward_channel_id =I('reward_channel_id',0,'int');
        if($reward_channel_id){
            #dump($reward_channel_id);
            $post_obj =new PostModel();
            $post_ids = $post_obj->getPostFields('post_type ='.PostModel::REWARD.' AND channel_id ='.$reward_channel_id,'post_id');
            #dump($post_obj->getLastSql());
            $post_id_str = implode(',',$post_ids);
            #dump($post_id_str);
            if($post_id_str){
                $where .= ' and post_id IN ('.$post_id_str.')';
            }else{
                $where .= ' and false';
            }
            $this->assign('reward_channel_id', $reward_channel_id);
        }

        $cf_post_name = I('cf_post_name');
        if($cf_post_name){
            $post_obj =new PostModel();
            $post_ids = $post_obj->getPostFields('post_type ='.PostModel::CROWDFUNDING.' AND title like "%'.$cf_post_name.'%"','post_id');
            $post_id_str = implode(',',$post_ids);
            if($post_id_str){
                $where .= ' and post_id IN ('.$post_id_str.')';
            }else{
                $where .= ' and false';
            }
            $this->assign('cf_post_name', $cf_post_name);
        }

        $cf_channel_id =I('cf_channel_id',0,'int');
        if($cf_channel_id){
            #dump($reward_channel_id);
            $post_obj =new PostModel();
            $post_ids = $post_obj->getPostFields('post_type ='.PostModel::CROWDFUNDING.' AND channel_id ='.$cf_channel_id,'post_id');
            #dump($post_obj->getLastSql());
            $post_id_str = implode(',',$post_ids);
            #dump($post_id_str);
            if($post_id_str){
                $where .= ' and post_id IN ('.$post_id_str.')';
            }else{
                $where .= ' and false';
            }
            $this->assign('cf_channel_id', $cf_channel_id);
        }

        $nick_name = I('nick_name');
        if($nick_name){
            $user_obj =new UserModel();
            $user_ids = $user_obj->getUserFields('user_id','nickname like "%'.$nick_name.'%"');
            $user_id_str = implode(',',$user_ids);
            if($user_id_str){
                $where .= ' and user_id IN ('.$user_id_str.')';
            }else{
                $where .= ' and false';
            }
            $this->assign('nick_name', $nick_name);
        }

        return $where;
    }

    public function get_all_post_cash(){
        $this->display();
    }

    //普通贴列表
    public function get_normal_list($where){

        $post_obj = new PostModel();
        $where .= ' AND post_type ='.PostModel::NORMAL;
        $where .= $this->get_search_condition();
        //数据总量
        $total = $post_obj->getPostNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $post_obj->setStart($Page->firstRow);
        $post_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('show', $show);
        $post_list = $post_obj->getPostList('',$where);
        $post_list = $post_obj->getListData($post_list);
        #dump($post_list);die;

        //普通所有频道
        $channel_obj = new ChannelModel();
        $channel_list = $channel_obj->getValidChannelList('channel_type ='.ChannelModel::NORMAL);

        $this->assign('channel_list',$channel_list);

        $this->assign('post_list', $post_list);
        $this->assign('head_title', '普通贴列表');
        $this->display('get_normal_list');
    }

    //已删除普通贴列表．
    public function get_no_del_normal_list(){
        $this->get_normal_list('is_del = 0');
    }

    //已删除普通贴列表．
    public function get_del_normal_list(){
        $this->get_normal_list('is_del = 1');
    }

    //赞赏贴列表
    public function get_praise_list($where){

        $post_obj = new PostModel();
        $where .= ' AND post_type ='.PostModel::PRAISE;
        $where .= $this->get_search_condition();
        //数据总量
        $total = $post_obj->getPostNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $post_obj->setStart($Page->firstRow);
        $post_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('show', $show);
        $post_list = $post_obj->getPostList('',$where);
        $post_list = $post_obj->getListData($post_list);
        #dump($post_list);die;
        //赞赏所有频道
        $channel_obj = new ChannelModel();
        $channel_list = $channel_obj->getValidChannelList('channel_type ='.ChannelModel::PRAISE);

//        var_dump($post_list);
        $this->assign('channel_list', $channel_list);
        $this->assign('post_list', $post_list);
        $this->assign('head_title', '赞赏贴列表');
        $this->display('get_praise_list');

    }


    //wangtao 2017.9.15 -strat
    /**
     * 将帖子设置成精选
     */
    public function set_featured(){
        if(IS_POST && IS_AJAX){
            $post_id = I('post_id', 0, 'intval');
            if(!$post_id) $this->ajaxReturn('failure');
            $post_obj = new PostModel($post_id);
            if($post_obj->setFeatured($post_id)){
                $this->ajaxReturn('success');
            }
            $this->ajaxReturn('failure');
        }

    }

    /**
     * 将帖子删除精选
     */
    public function del_featured(){
        if(IS_POST && IS_AJAX){
            $post_id = I('post_id', 0, 'intval');
            if(!$post_id) $this->ajaxReturn('failure');
            $post_obj = new PostModel($post_id);
            if($post_obj->delFeatured($post_id)){
                $this->ajaxReturn('success');
            }
            $this->ajaxReturn('failure');
        }
    }

    //wangtao 2017.9.15 -end

    //未删除的悬赏贴列表
    public function get_no_del_praise_list(){
        $this->get_praise_list('is_del = 0');
    }

    //已删除的悬赏贴列表
    public function get_del_praise_list(){
        $this->get_praise_list('is_del = 1');
    }

    //悬赏贴公用列表
    public function get_reward_list($where,$title,$opt){

        $reward_obj = new RewardModel();
        $where .= $this->get_search_condition();
        //数据总量
        $total = $reward_obj->getRewardNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $reward_obj->setStart($Page->firstRow);
        $reward_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('show', $show);
        $reward_list = $reward_obj->getRewardList('',$where);
        $reward_list = $reward_obj->getListData($reward_list);
        #dump($post_list);die;
        //悬赏所有频道
        $channel_obj = new ChannelModel();
        $channel_list = $channel_obj->getValidChannelList('channel_type ='.ChannelModel::REWARD);
        $this->assign('channel_list', $channel_list);
        $this->assign('opt', $opt);
        $this->assign('reward_list', $reward_list);
        $this->assign('head_title', $title);
        $this->display('get_reward_list');
    }

    //悬赏中的列表
    public function get_rewarding_list(){
        $this->get_reward_list('is_del = 0 and status ='.RewardModel::REWARDING,'悬赏中的列表','rewarding');
    }

    //已采纳的悬赏
    public function get_adopted_list(){
        $this->get_reward_list('is_del = 0 and status ='.RewardModel::ADOPTED,'已采纳的悬赏','adopted');
    }

    //未采纳的悬赏
    public function get_not_adopted_list(){
        $this->get_reward_list('is_del = 0 and status ='.RewardModel::NOT_ADOPTED,'未采纳的悬赏','not_adopted');
    }

    //退款中的悬赏
    public function get_refunding_list(){
        $this->get_reward_list('is_del = 0 and status ='.RewardModel::REFUNDING,'退款中的悬赏','refunding');
    }

    //已退款的悬赏
    public function get_refunded_list(){
        $this->get_reward_list('is_del = 0 and status ='.RewardModel::REFUNDED,'已退款的悬赏','refunded');
    }

    //已删除的悬赏
    public function get_del_refunded_list(){
        $this->get_reward_list('is_del = 1','已删除的悬赏','del');
    }


    //悬赏退款．
    public function reward_refunded(){
        if (IS_POST && IS_AJAX){
            $reward_id = I('reward_id',0,'int');
            $type = I('type',0,'int');
            if(!$reward_id && !$type) $this->ajaxReturn(array('code'=> 0 ,'msg'=> '缺少参数'));
            $reward_obj = new RewardModel();
            $reward_info =$reward_obj ->getRewardInfo('reward_id ='.$reward_id);
            if($reward_info['status'] == RewardModel::REFUNDED) $this->ajaxReturn(array('code'=> 0 ,'msg'=> '已退款'));
            $success = $reward_obj->rewardIsRefunded($reward_id,$type);
            if(!$success) $this->ajaxReturn(array('code'=> 0 ,'msg'=> '操作失败!'));

            $this->ajaxReturn(array('code'=> 0 ,'msg'=> '操作成功!'));
        }
    }




    //答案列表
    public function get_reward_answer_list($title){
        $reward_id =I('reward_id',0,'int');
        if(!$reward_id) $this->error('没有该悬赏!');
        $reward_answer_obj = new RewardAnswerModel();
        //数据总量
        $total = $reward_answer_obj->getRewardAnswerNumByReward($reward_id);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $reward_answer_obj->setStart($Page->firstRow);
        $reward_answer_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('show', $show);
        $reward_answer_list = $reward_answer_obj->getRewardAnswerList('','reward_id ='.$reward_id);
        $reward_answer_list = $reward_answer_obj->getListData($reward_answer_list);
        $this->assign('reward_answer_list', $reward_answer_list);
        $this->assign('head_title', $title);
        $this->display('get_reward_answer_list');
    }

    //悬赏中答案的列表
    public function get_rewarding_answer_list(){
        $this->get_reward_answer_list('悬赏中的答案列表');
    }

    //已采纳的答案列表
    public function get_adopted_answer_list(){
        $this->get_reward_answer_list('已采纳的答案列表');
    }

    //未采纳的答案列表
    public function get_not_adopted_answer_list(){
        $this->get_reward_answer_list('未采纳的答案列表');
    }

    //退款中的答案列表
    public function get_refunding_answer_list(){
        $this->get_reward_answer_list('退款中的答案列表');
    }

    //已退款的答案列表
    public function get_refunded_answer_list(){
        $this->get_reward_answer_list('已退款的答案列表');
    }

    //已删除的答案列表
    public function get_del_answer_list(){
        $this->get_reward_answer_list('已删除的答案列表');
    }

    //删除答案
    public function del_answer(){
        if(IS_POST && IS_AJAX){
            $reward_answer_id = I('reward_answer_id', 0, 'intval');
            if(!$reward_answer_id) $this->ajaxReturn('failure');
            $reward_answer_obj = new RewardAnswerModel($reward_answer_id);
            if($reward_answer_obj->delRewardAnswer($reward_answer_id)){
                $this->ajaxReturn('success');
            }
            $this->ajaxReturn('failure');
        }
    }


    //查询围观列表
    public function get_reward_look_list(){

        $reward_id = I('reward_id',0,'int');
        if(!$reward_id) $this->error('没有该悬赏!');
        $reward_look_obj = new RewardLookModel();
        //数据总量
        $total = $reward_look_obj->getRewardLookNum('reward_id ='.$reward_id);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $reward_look_obj->setStart($Page->firstRow);
        $reward_look_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('show', $show);
        $reward_look_list = $reward_look_obj->getRewardLookist('','reward_id ='.$reward_id);
        $reward_look_list = $reward_look_obj->getListData($reward_look_list);


        $this->assign('reward_look_list',$reward_look_list);
        $this->assign('head_title','围观用户列表');
        $this->display();
    }

    //心愿贴列表
    public function get_crowdfunding_list($where,$title,$opt){

        $cf_obj = new CrowdfundingModel();
        $where .= $this->get_search_condition();
        //数据总量
        $total = $cf_obj->getCrowdfundingNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $cf_obj->setStart($Page->firstRow);
        $cf_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('show', $show);
        $cf_list = $cf_obj->getCrowdfundingList('',$where);
        #dump($cf_obj->getLastSql());
        $cf_list = $cf_obj->getListData($cf_list);
        #$dump($cf_list);die;
        //心愿所有频道
        $channel_obj = new ChannelModel();
        $channel_list = $channel_obj->getValidChannelList('channel_type ='.ChannelModel::CROWDFUNDING);
        $this->assign('channel_list', $channel_list);
        $this->assign('opt', $opt);
        $this->assign('head_title', $title);
        $this->assign('cf_list', $cf_list);
        $this->display('get_crowdfunding_list');
    }

    //待审核的心愿
    public function get_review_cf_list(){
        $where = 'is_del = 0 and status ='.CrowdfundingModel::REVIEW;
        $this->get_crowdfunding_list($where, '待审核心愿列表', 'review');
    }
    //进行中的心愿
    public function get_passed_cf_list(){
        $where = 'is_del = 0 and status ='.CrowdfundingModel::PASSED;
        $this->get_crowdfunding_list($where, '进行中心愿列表', 'passed');
    }

    //成功的心愿
    public function get_success_cf_list(){
        $where = ' is_del = 0 and  status ='.CrowdfundingModel::SUCCESS;
        $this->get_crowdfunding_list($where, '成功的心愿列表', 'success');
    }

    //失败的心愿
    public function get_failure_cf_list(){
        $where = ' is_del = 0 and status ='.CrowdfundingModel::FAILURE;
        $this->get_crowdfunding_list($where, '失败的心愿列表', 'failure');
    }

    //删除的心愿
    public function get_del_cf_list(){
        $where = 'is_del = 1';
        $this->get_crowdfunding_list($where, '删除的心愿列表', 'del');
    }


    //心愿贴审核．
    public function set_status(){
        if(IS_AJAX && IS_POST) {
            $cf_id = I('cf_id', 0, 'int');
            $state = I('state', 0, 'int');
            if (!$cf_id && !$state) $this->ajaxReturn(array('code' => 0, 'msg' => '参数错误!'));

            //通过
            $cf_obj = new CrowdfundingModel();
            $cf = $cf_obj->getCrowdfundingInfo('crowdfunding_id ='.$cf_id);
            
            if ($state == 1) {
                $end_days = intval($GLOBALS['config_info']['ZHONGCHOU_DAYS']);
                $data = array(
                    'status' => $state,
                    'pass_time' => date('Ymd',time()),
                    'end_time' => date('Ymd',(time() + ($end_days * 24*60*60))),
                );
                #dump($end_days);
                #dump($data);die;
                $success = $cf_obj->setStatus($cf_id, $data);
                if ($success){
                    
                    $this->ajaxReturn(array('code' => 1, 'msg' => '通过审核成功!'));  
                } 
                $this->ajaxReturn(array('code' => 0, 'msg' => '通过审核失败!'));
            }
            //拒绝
            if ($state == 2) {
                $success = $cf_obj->setStatus($cf_id, array('status' => $state));
                if ($success){
                    // D('Message')->addMessage($cf['post_id'], 4, 0, $cf['user_id'], '对不起，您的心愿《'.$title.'》未通过审核', '对不起，您的心愿《'.$title.'》未通过审核', '');
                    $this->ajaxReturn(array('code' => 1, 'msg' => '拒绝审核成功!'));
                } 
                $this->ajaxReturn(array('code' => 0, 'msg' => '拒绝审核失败!'));
            }
        }
    }

    //心愿支持列表．
    public function get_cf_support_list(){
        $crowdfunding_id = I('crowdfunding_id',0,'int');
        $cf_support_obj = new CfSupportModel();
        $total = $cf_support_obj->getCfSupportNum('crowdfunding_id ='.$crowdfunding_id);
        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $cf_support_obj->setStart($Page->firstRow);
        $cf_support_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('show', $show);

        $support_list = $cf_support_obj->getCfSupportList('','crowdfunding_id ='.$crowdfunding_id);
        $support_list = $cf_support_obj->getListData($support_list);
        $this->assign('support_list', $support_list);
        $this->display('get_cf_support_list');
    }

    //进行中的支持
    public function passed_cf_support(){
        $this->get_cf_support_list();
    }

    //成功的支持
    public function success_cf_support(){
        $this->get_cf_support_list();
    }

    //失败的支持
    public function failure_cf_support(){
        $this->get_cf_support_list();
    }

    //失败的支持
    public function del_cf_support(){
        $this->get_cf_support_list();
    }

    //失败的心愿退款
    public function failure_cf_refund(){
        $cf_id = I('cf_id',0,'int');
        $cf_obj = new CrowdfundingModel();
        $cf_info = $cf_obj->getCrowdfundingInfo('crowdfunding_id ='.$cf_id);
        if($cf_info['status'] != CrowdfundingModel::FAILURE) $this->ajaxReturn(array('code' => 0,'msg'=>'该心愿未失败!'));
        if($cf_info['is_refund'] == 1 ) $this->ajaxReturn(array('code' => 0,'msg'=>'该心愿已退款!'));

        $success = $cf_obj ->cf_refund($cf_id);
        if(!$success) $this->ajaxReturn(array('code' => 0,'msg'=>'退款失败!'));
        $cf_obj->setStatus($cf_id,array('is_refund' => 1));
        $this->ajaxReturn(array('code' => 0,'msg'=>'退款成功!'));
    }

    //帖子详情
    public function post_detail($title){
        $post_id = I('post_id',0,'int');
        if(!$post_id) $this->error('没有该帖!');
        //获取帖子详情
        $post_obj = new PostModel();
        $post_info = $post_obj->getPostInfo('post_id ='.$post_id);
        //获取用户昵称
        $user_obj = new UserModel();
        $post_info['nick_name'] = $user_obj->getUserField('nickname','user_id ='.$post_info['user_id']);
        //频道．
        $channel_obj = new ChannelModel();
        $post_info['channel_name'] = $channel_obj->getChannelField($post_info['channel_id'],'channel_name');
        //图片
        $post_info['imgs'] = D('PostImg')->getPostImgList($post_info['post_id']);

        //时间
        $post_info['acp_time'] = date('Y-m-d H:i:s', $post_info['addtime']);

        //普通帖子
        if($post_info['post_type'] == PostModel::NORMAL) {
            $post_info = $post_obj->getPostNormalInfo($post_info);
        }
        //赞赏帖子
        if($post_info['post_type'] == PostModel::PRAISE) {
            $post_info = $post_obj->getPostPraiseInfo($post_info);
        }
        //悬赏帖子
        if($post_info['post_type'] == PostModel::REWARD) {
            $post_info = $post_obj->getPostRewardInfo($post_info);
        }
        //心愿帖子
        if($post_info['post_type'] == PostModel::CROWDFUNDING) {
            $post_info = $post_obj->getPostCrowdFundingInfo($post_info);
        }

        #dump($post_info);die;

        $this->assign('post_info',$post_info);
        $this->assign('head_title',$title);
        $this->display('post_detail');
    }

    //普通贴详情
    public function post_normal_detail(){
        $this->post_detail('普通贴详情');
    }

    //已删除的普通贴详情
    public function post_del_normal_detail(){
        $this->post_detail('已删除的普通贴详情');
    }

    //赞赏贴详情
    public function post_praise_detail(){
        $this->post_detail('赞赏贴详情');
    }
    //已删除赞赏贴详情
    public function post_del_praise_detail(){
        $this->post_detail('已删除的赞赏贴详情');
    }

    //待审核心愿详情
    public function post_review_cf_detail(){
        $this->post_detail('待审核心愿详情');
    }

    //进行中的心愿详情
    public function post_passed_cf_detail(){
        $this->post_detail('进行中的心愿详情');
    }

    //成功的心愿详情
    public function post_success_cf_detail(){
        $this->post_detail('成功的心愿详情');
    }

    //失败的心愿详情
    public function post_failure_cf_detail(){
        $this->post_detail('失败的心愿详情');
    }

    //失败的心愿详情
    public function post_del_cf_detail(){
        $this->post_detail('删除的心愿详情');
    }

    //进行中的悬赏详情
    public function get_rewarding_detail(){
        $this->post_detail('进行中的悬赏详情');
    }

    //已采纳的悬赏详情
    public function get_adopted_detail(){
        $this->post_detail('已采纳的悬赏详情');
    }

    //未采纳的悬赏详情
    public function get_not_adopted_detail(){
        $this->post_detail('未采纳的悬赏详情');
    }

    //退款中的悬赏详情
    public function get_refunding_detail(){
        $this->post_detail('退款中的悬赏详情');
    }

    //已退款的悬赏详情
    public function get_refunded_detail(){
        $this->post_detail('已退款的悬赏详情');
    }

    //已删除的悬赏详情
    public function get_del_reward_detail(){
        $this->post_detail('已删除的悬赏详情');
    }

    //修改帖子
    public function sele_post_type(){
        if(IS_POST && IS_AJAX){
            $post_type = I('post_type', 0, 'intval');
            $post_ordinary = I('post_ordinary');
            var_dump($post_type,$post_ordinary);
            if(!$post_type) $this->ajaxReturn('failure');
//            $post_obj = new PostModel($post_id);
//            if($post_obj->delPost($post_id)){
                $this->ajaxReturn('success');
//            }
            $this->ajaxReturn('failure');
        }
    }

    //删除帖子
    public function del_post(){
        if(IS_POST && IS_AJAX){
            $post_id = I('post_id', 0, 'intval');
            if(!$post_id) $this->ajaxReturn('failure');
            $post_obj = new PostModel($post_id);
            if($post_obj->delPost($post_id)){
                $this->ajaxReturn('success');
            }
            $this->ajaxReturn('failure');
        }
    }

    //wangtao 2017.9.15 -strat

    //恢复帖子
    public function recover_post(){
        if(IS_POST && IS_AJAX){
            $post_id = I('post_id', 0, 'intval');
            if(!$post_id) $this->ajaxReturn('failure');
            $post_obj = new PostModel($post_id);
            if($post_obj->recoverPost($post_id)){
                $this->ajaxReturn('success');
            }
            $this->ajaxReturn('failure');
        }
    }

    //wangtao 2017.9.15 -end


    //删除图片
    public function del_img(){
        if(IS_POST && IS_AJAX){
            $img_id = I('img_id',0,'int');
            $img_type = I('img_type');
            if(!$img_id && !$img_type) $this->ajaxReturn(array('code'=>0,'msg'=>'条件缺失,删除失败'));

            if($img_type == 'normal'){ //普通图片删除
                $img_obj = new PostImgModel();
                $success = $img_obj->delImg($img_id);
                if(!$success)  $this->ajaxReturn(array('code'=>0,'msg'=>'删除失败'));
                $this->ajaxReturn(array('code'=>1,'msg'=>'删除成功'));
            }

            if($img_type == 'praise'){ //赞赏隐藏图片
                $img_obj = new PraiseHideImgModel();
                $success = $img_obj->delImg($img_id);
                if(!$success)  $this->ajaxReturn(array('code'=>0,'msg'=>'删除失败'));
                $this->ajaxReturn(array('code'=>1,'msg'=>'删除成功'));
            }

            if($img_type == 'cf_reply'){ //心愿回帖删除图片
                $img_obj = new CfReplyImgModel();
                $success = $img_obj->delImg($img_id);
                if(!$success)  $this->ajaxReturn(array('code'=>0,'msg'=>'删除失败'));
                $this->ajaxReturn(array('code'=>1,'msg'=>'删除成功'));
            }

            if($img_type == 'answer_img'){ //悬赏答案删除图片
                $img_obj = new RewardAnswerImgModel();
                $success = $img_obj->delImg($img_id);
                if(!$success)  $this->ajaxReturn(array('code'=>0,'msg'=>'删除失败'));
                $this->ajaxReturn(array('code'=>1,'msg'=>'删除成功'));
            }

            if($img_type == 'comment'){ //评论图片删除
                $img_obj = new CommentImgModel();
                $success = $img_obj->delImg($img_id);
                if(!$success)  $this->ajaxReturn(array('code'=>0,'msg'=>'删除失败'));
                $this->ajaxReturn(array('code'=>1,'msg'=>'删除成功'));
            }

        }
    }


    public function edit_post_content(){
        if(IS_POST && IS_AJAX){
            $post_id = I('post_id',0,'int');
            $content = I('content');
            $content_type = I('content_type');
            if(!$post_id && !$content) $this->ajaxReturn(array('code'=> 0 ,'msg'=>'条件缺失,修改失败'));
            $success = 0;
            //wangtao 2017.9.19 -strat
            if($content_type == 'title'){
                $post_obj = new PostModel();
                $success = $post_obj->setPost($post_id,array('title' => $content));
            }
            //wangtao 2017.9.19 -strat
            if($content_type == 'normal'){
                $post_obj  = new PostModel();
                $success = $post_obj -> setPost($post_id,array('content' => $content));
            }

            if($content_type == 'praise'){
                $praise_hide_obj  = new PraiseHideModel();
                $success = $praise_hide_obj ->settPraiseHide($post_id,array('content' => $content));
            }

            if($content_type == 'cf_reply'){
                $cf_reply_obj  = new CfReplyModel();
                $success = $cf_reply_obj ->setCfReply($post_id,array('content' => $content));
            }

            if($content_type == 'answer'){
                $answer_obj  = new RewardAnswerModel();
                $success = $answer_obj ->setRewardAnswer($post_id,array('content' => $content));
            }


            if($content_type == 'comment'){
                $post_comment_obj  = new PostCommentModel();
                $success = $post_comment_obj ->setPostComment($post_id,array('content' => $content));
            }

            if($success !==false) $this->ajaxReturn(array('code'=>1,'msg'=>'修改成功'));
            $this->ajaxReturn(array('code'=>1,'msg'=>'修改失败'));
        }
    }


    //公共评论
    public function get_comment_list($title,$opt){

        $post_id = I('post_id',0,'int');
        $where = 'post_id ='.$post_id.' AND reply_comment_id = 0';
        $post_comment_obj =new PostCommentModel();
        $total = $post_comment_obj->getPostCommentNum($where);
        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $post_comment_obj->setStart($Page->firstRow);
        $post_comment_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('show', $show);
        $post_comment_list = $post_comment_obj->getPostCommentList('',$where);
        $post_comment_list = $post_comment_obj->getListData($post_comment_list);

        #dump($post_comment_list);die;
        $this->assign('post_comment_list', $post_comment_list);
        $this->assign('opt', $opt);
        $this->assign('head_title', $title);
        $this->display('get_comment_list');

    }

    public function get_normal_comment_list(){
        $this->get_comment_list('普通帖评论列表','normal_comment');
    }

    public function get_del_normal_comment_list(){
        $this->get_comment_list('已删除的普通帖评论列表','del_normal_comment');
    }

    public function get_praise_comment_list(){
        $this->get_comment_list('赞赏贴评论列表','praise_comment');
    }

    public function get_del_praise_comment_list(){
        $this->get_comment_list('已删除的赞赏贴评论列表','del_praise_comment');
    }

    //悬赏评论列表
    public function get_rewarding_comment_list(){
        $this->get_comment_list('进行的悬赏评论列表','rewarding_comment');
    }

    public function get_adopted_comment_list(){
        $this->get_comment_list('已采纳悬赏评论列表','adopted_comment');
    }

    public function get_not_adopted_comment_list(){
        $this->get_comment_list('未采纳悬赏评论列表','not_adopted_comment');
    }

    public function get_refunding_comment_list(){
        $this->get_comment_list('退款中的悬赏评论列表','refunding_comment');
    }

    public function get_refunded_comment_list(){
        $this->get_comment_list('已退款的悬赏评论列表','refunded_comment');
    }

    public function get_del_reward_comment_list(){
        $this->get_comment_list('已删除的悬赏评论列表','del_reward_comment');
    }

    //心愿评论列表
    public function get_passed_cf_comment_list(){
        $this->get_comment_list('进行中的心愿评论列表','passed_cf_comment');
    }

    public function get_success_cf_comment_list(){
        $this->get_comment_list('成功的心愿评论列表','success_cf_comment');
    }

    public function get_failure_cf_comment_list(){
        $this->get_comment_list('失败的心愿评论列表','failure_cf_comment');
    }

    public function get_del_cf_comment_list(){
        $this->get_comment_list('删除的心愿评论列表','del_cf_comment');
    }


    public function del_comment()
    {
        if (IS_POST && IS_AJAX) {
            $comment_id = I('comment_id', 0, 'intval');
            if (!$comment_id) $this->ajaxReturn('failure');
            $comment_obj = new PostCommentModel();
            if ($comment_obj->delPostComment($comment_id)) {
                $this->ajaxReturn('success');
            }
            $this->ajaxReturn('failure');
        }
    }

    public function comment_detail($title){

        $post_comment_id = I('post_comment_id',0,'int');
        if(!$post_comment_id) $this->error('没有该评论');
        $post_comment_obj = new PostCommentModel();
        $where = 'post_comment_id ='.$post_comment_id;
        $post_comment_info = $post_comment_obj->getPostCommentInfo($where);

        //查询评论用户
        $user_obj = new UserModel();
        $post_comment_info['nick_name'] = $user_obj->getUserField('nickname','user_id ='.$post_comment_info['user_id']);
        $post_comment_info['acp_time'] = date('Y-m-d H:i:s',$post_comment_info['addtime']);
        //查询楼层图片
        $comment_img_obj = new CommentImgModel();
        $comment_img_list = $comment_img_obj->getCommentList($post_comment_id);
        $post_comment_info['img_list'] = $comment_img_list;

        //查询出所有回复该楼层的回复
        $post_comment_obj->setLimit(10000);
        $reply_comment_list = $post_comment_obj->getPostCommentList('', 'post_id = '.$post_comment_info['post_id'].' and reply_comment_id ='.$post_comment_info['post_comment_id']);
        #dump($post_comment_obj->getLastSql());
        foreach ($reply_comment_list as $k => $v) {
            $reply_comment_list[$k]['reply_nick_name'] = $user_obj->getUserField('nickname','user_id ='.$v['user_id']);
        }
        $post_comment_info['reply_comment_list'] = $reply_comment_list;

        //赞赏隐藏内容
        $post_comment_info['comment_hide'] = D('PraiseHide')->getCommentHide($post_comment_id);

        #dump($post_comment_info);die;
        $this->assign('info', $post_comment_info);
        $this->assign('head_title', $title);
        $this->display('comment_detail');
    }

    public function normal_comment_detail(){
        $this->comment_detail('普通帖评论详情');
    }

    public function del_normal_comment_detail(){
        $this->comment_detail('已删除的普通帖评论详情');
    }

    public function praise_comment_detail(){
        $this->comment_detail('赞赏贴评论详情');
    }

    public function del_praise_comment_detail(){
        $this->comment_detail('已删除的赞赏贴评论详情');
    }

    //悬赏评论详情
    public function rewarding_comment_detail(){
        $this->comment_detail('进行的悬赏评论详情');
    }

    public function adopted_comment_detail(){
        $this->comment_detail('已采纳悬赏评论详情');
    }

    public function not_adopted_comment_detail(){
        $this->comment_detail('未采纳悬赏评论详情');
    }

    public function refunding_comment_detail(){
        $this->comment_detail('退款中悬赏评论详情');
    }

    public function refunded_comment_detail(){
        $this->comment_detail('已退款悬赏评论详情');
    }

    public function del_reward_comment_detail(){
        $this->comment_detail('已删除悬赏评论详情');
    }

    //心愿评论详情
    public function passed_cf_comment_detail(){
        $this->comment_detail('进行中的心愿评论详情');
    }

    public function success_cf_comment_detail(){
        $this->comment_detail('成功的心愿评论详情');
    }

    public function failure_cf_comment_detail(){
        $this->comment_detail('失败的心愿评论详情');
    }

    public function del_cf_comment_detail(){
        $this->comment_detail('删除的心愿评论详情');
    }


}
