<?php

class FrontLiveAction extends FrontAction
{
    function _initialize()
    {
        parent::_initialize();
        $this->page_num = 10;
    }

    //视频首页．
    public function live_index(){

        $live_class_obj  = new LiveClassModel();
        $where = 'isuse = 1';
        $total = $live_class_obj->getLiveClassNum($where);
        $firstRow = I('firstRow',0,'int');
        $live_class_obj->setStart($firstRow);
        $live_class_obj->setLimit($this->page_num);
        $live_class_list = $live_class_obj->getLiveClassList($where);
        $live_class_list = $live_class_obj->getListData($live_class_list);
        if(IS_POST && IS_AJAX){
            $this->ajaxReturn($live_class_list);
        }

        $this->assign('live_class_list',$live_class_list);
        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('head_title','视频');
        $this->display();
    }

    public function live_channel(){


        $live_class_id = I('live_class_id',0,'int');
//        if($this->is_hidden){
//            redirect('/FrontLive/live_channel_check/live_class_id/'.$live_class_id);
//        }
        $live_class_obj = new LiveClassModel();
        $title = $live_class_obj->getLiveClassField($live_class_id,'live_class_name');

        $where = 'isuse = 1 and live_class_id ='.$live_class_id;
        $live_obj = new LiveModel();
        $total = $live_obj->getLiveNum($where);
        $firstRow = I('firstRow',0,'int');
        $live_obj->setStart($firstRow);
        $live_obj->setLimit($this->page_num);
        $live_list = $live_obj->getLiveList($where);
        if(IS_POST && IS_AJAX){
            $this->ajaxReturn($live_list);
        }

        $this->assign('live_list',$live_list);
        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('head_title',$title);
        $this->display();
    }

    public function live_channel_check(){

        $live_class_id = I('live_class_id',0,'int');
        $live_class_obj = new LiveClassModel();
        $title = $live_class_obj->getLiveClassField($live_class_id,'live_class_name');

        $where = 'isuse = 1 and live_class_id ='.$live_class_id.' and price <= 0';
        $live_obj = new LiveModel();
        $total = $live_obj->getLiveNum($where);
        $firstRow = I('firstRow',0,'int');
        $live_obj->setStart($firstRow);
        $live_obj->setLimit($this->page_num);
        $live_list = $live_obj->getLiveList($where);
        if(IS_POST && IS_AJAX){
            $this->ajaxReturn($live_list);
        }

        $this->assign('live_list',$live_list);
        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('head_title',$title);
        $this->display();
    }


}
