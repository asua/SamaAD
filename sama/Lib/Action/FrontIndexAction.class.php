<?php

class FrontIndexAction extends FrontAction
{
    function _initialize()
    {
        parent::_initialize();
        $this->page_num = 10;
        $this->assign('nav', 'index');
    }


    //wangtao 2017.9.15 -start
//
//    /**
//     * 記錄用戶所選的頻道類別
//     * @param null $channel_type_id 用戶所選的頻道類別ID
//     */
//    public function channenlt_type($channel_type_id =  null){
//
//        //判斷用戶是否登錄與用戶類別是否爲空
//        if (session('user_id') != null && $channel_type_id != null) {
//
//            $user_id = intval(session('user_id'));
//
//            //修改用戶所選的類別
//            $users_obj = new  UserModel();
//            $requrs = $users_obj->setChannelTypeId($user_id,$channel_type_id);
//
//            if($requrs){
//                $this->sama_index();
//            }
//        }
//
//        $this->sama_index();
//
//    }
    //wangtao 2017.9.15 -end



    public function sama_index()
    {

        //判斷用戶是否為第一次注冊
//        $logins_noce = 0;
//        if (session('user_id') != null){
//            $user_id = intval(session('user_id'));
//            $users_obj = new UserModel();
//            $login_once = $users_obj->getUserFieldsById($user_id,'login_once');//根據用戶ID獲取用戶是否是第一次登陸
//            if($login_once == 1){
//                //儅用戶為第一次注冊時彈出選擇框
//                $logins_noce = 1;
//            }
//            //根據用戶ID獲取用戶所選的頻道類型
////            $user_channel_type_id = $users_obj->getUserFieldsById($user_id,'channel_type_id');
//            //根據頻道類型獲取所有的
////            $channel_ids = M('Channel')->where('channel_type_id='.$user_channel_type_id)->getField('channel_id', true);
////            if()
//        }




        //wangtao 2017.9.18 -start

        //轮播图
        $cust_flash_obj = new CustFlashModel();

        if($this->is_hidden) {

            $cust_flash_list = $cust_flash_obj->getCustFlashList('', ' serial < 20 and isuse = 1 and cust_type = 0', ' serial ASC');

            $information = null;

        }else{

            $cust_flash_list = $cust_flash_obj->getCustFlashList('', 'serial < 50 and cust_type = 0', ' serial ASC');

            $information = $cust_flash_obj->getCustFlashList('',' isuse=1 and cust_type = 1',' serial ASC');

        }

        //获取推荐用户


        $where = ' and post_type = '.PostModel::NORMAL;

        //符合條件的頻道集合
        if($this->is_hidden){

            $where_s = 'and serial < 20';
            $where_c = ' and channel_type = '.ChannelModel::NORMAL;
            $user_info = null;

        }else{

            $user_obj = new UserModel();
            $user_info = $user_obj->getUserSelected();
            $where_s = 'and serial < 50';
            $where_c = '';

        }

        $channel_ids = M('Channel')->where('isuse = 1 and is_del = 0 '. $where_s)->getField('channel_id', true);
        //帖子
        $post_obj = new PostModel();

        //獲取可現實頻道的數量

        $total = $post_obj->getPostNum('is_del = 0 and channel_id in('.implode(',', $channel_ids).') '.$where);
        $firstRow = intval($this->_request('firstRow'));
        $post_obj->setStart($firstRow);
        $post_obj->setLimit($this->page_num);
        $post_list = $post_obj->getPostList('','is_del = 0 and channel_id in('.implode(',', $channel_ids).') '.$where,'updatetime DESC');
        #dump($post_obj->getLastSql());
        #dump($post_list);die;
        $post_list = $post_obj->getListData($post_list,false,$information);
        #dump($post_list);die;
        if(IS_POST && IS_AJAX){
            #dump('ajax = '.json_encode($post_list));
            #dump($post_obj->getLastSql());die;
            $this->ajaxReturn($post_list);
        }

        $this->assign('post_list',$post_list);

        //wangtao 2017.9.18 -end

         //精选频道

        $this->assign('user_info',$user_info);
        $this->assign('cust_flash_list', $cust_flash_list);
        $channel_obj = new ChannelModel();
        $channel_list = $channel_obj->getChannelList('','is_featured = 1 and isuse = 1'.$where_c);
        $this->assign('channel_list',$channel_list);

        $this->assign('total',$total);
        $this->assign('firstRow',$this->page_num);
        $this->assign('head_title', 'Sama字母圈');
        $this->display();
    }


    //搜索
    public function search(){
        // dump($_GET);die;
        $kw = I('kw', '');
        $this->assign('kw', $kw);

        $type = I('type', 'post');
        if($this->is_hidden){
            $where = ' and post_type = '.PostModel::NORMAL;
            $where_c = ' and channel_type = '.ChannelModel::NORMAL;
        }

        $user_id = intval(session('user_id'));
        if($kw){
            if($type == 'pindao'){


                $channel_obj = new ChannelModel();
                $where = 'isuse = 1 and is_del = 0 and channel_name like "%'.$kw.'%"'.$where_c;
                $total = $channel_obj->getChannelNum($where);
                $firstRow = intval($this->_request('firstRow'));
                $channel_obj->setStart($firstRow);
                $channel_obj->setLimit($this->page_num);
                $channel_list = $channel_obj->getChannelList('',$where);

                foreach ($channel_list as $k => $v) {
                    //用户等级
                    $user_channel_rank = D('UserChannelRank')->getUserChannelRankInfo('user_id ='.$user_id. ' and channel_id ='.$v['channel_id']);
                    $channel_rank = D('ChannelRank')->getChannelRankInfo('channel_rank_id ='.$user_channel_rank['channel_rank_id']);
                    $channel_list[$k]['user_channel_rank'] = strval($channel_rank['rank_name']);
                }
                $search_list = $channel_obj->getListData($channel_list);
                if(IS_AJAX){
                    $this->ajaxReturn(array('data'=>$search_list, 'total'=>$total));
                }
            }elseif($type == 'people'){

                $user_obj = new UserModel();
                $where = 'nickname like "%'.$kw.'%" and role_type = 3';
                $total = $user_obj->getUserNum($where);
                $firstRow = intval($this->_request('firstRow'));
                $user_obj->setStart($firstRow);
                $user_obj->setLimit($this->page_num);
                $search_list = $user_obj->getUserList('user_id, headimgurl, nickname',$where);
                

                if(IS_AJAX){
                    $this->ajaxReturn(array('data'=>$search_list, 'total'=>$total));
                }
            }else{
                $post_obj = new PostModel();
                //帖子列表
                $where = 'is_del = 0 and review = 1 and ( title like "%'.$kw.'%" or content like "%'.$kw.'%")'.$where;
                $total = $post_obj->getPostNum($where);
                $firstRow = intval($this->_request('firstRow'));
                $post_obj->setStart($firstRow);
                $post_obj->setLimit($this->page_num);
                $post_list = $post_obj->getPostList('',$where);
                $search_list = $post_obj->getListData($post_list);

                foreach ($search_list as $k => $v) {
                    $search_list[$k]['title'] = str_replace($kw, '<span style="color:#FF6600;">'.$kw.'</span>', $v['title']);
                    $search_list[$k]['content'] = str_replace($kw, '<span style="color:#FF6600;">'.$kw.'</span>', $v['content']);
                }

                if(IS_AJAX){
                    $this->ajaxReturn(array('data'=>$search_list, 'total'=>$total));
                }

            }
        }else{
            $search_list = null;
            if(IS_AJAX){
                $this->ajaxReturn(array('data'=>$search_list, 'total'=>0));
            }
            
        }
        // dump($search_list);die;
        $this->assign('search_list', $search_list);
        $this->assign('firstRow', $this->page_num);
        $this->assign('total', $total);
        $this->assign('head_title', '搜索');
        $this->display();
    }



    //消息中心
    public function message_center(){
        $user_id = intval(session('user_id'));

//        $user_id = '62701';
        $message_obj = new MessageModel();
        //回复我的
        $reply_me_num = $message_obj->getMessageNum('reply_user_id ='.$user_id . ' and message_type ='.MessageModel::REPLY .' and is_read = 0');
        $this->assign('reply_me_num', $reply_me_num);

        //@我的
        $at_me_num = $message_obj->getMessageNum('reply_user_id ='.$user_id . ' and message_type ='.MessageModel::AT .' and is_read = 0');
        $this->assign('at_me_num', $at_me_num);

        // $sx_num = M('Users')->where('user_id ='.$user_id)->getField('sx_num');
        
        //私信数
//         Vendor('Hx.hx');
//         $hx_account = M('Users')->where('user_id ='.$user_id)->getField('hx_account');
//         $rs = new Hxcall();
//         $sx_num = $rs->hx_msg_count('hx_account');
        
        //私信权限
        $check_chat = D('User')->checkChat($user_id);

        $this->assign('check_chat', $check_chat);
        
        $this->assign('sx_num', $sx_num);
        $this->assign('head_title', '消息中心');
        $this->display();
    }


    //官方消息列表
    public function official_news(){

        $official_news_obj = new OfficialNewsModel();
        $where = 'isuse = 1';
        $total = $official_news_obj->getOfficialNewsNum($where);
        $firstRow = I('firstRow',0,'int');
        $official_news_obj->setStart($firstRow);
        $official_news_obj->setLimit($this->page_num);
        $news_list = $official_news_obj->getOfficialNewsList($where);
        #echo $official_news_obj->getLastSql();die;
        $news_list = $official_news_obj->getListData($news_list);

        if(IS_AJAX && IS_POST){
            $this->ajaxReturn($news_list);
        }

        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $this->assign('news_list', $news_list);
        $this->assign('head_title', '官方消息');
        $this->display();
    }

    //官方消息详情
    public function official_news_detail(){
        $article_id = I('article_id',0,'int');

        $official_news_obj = new OfficialNewsModel();

        $news_info = $official_news_obj->getOfficialNewsInfo('article_id ='.$article_id);
        $txt_obj =new ArticleTxtModel();
        $txt_info = $txt_obj ->getArticleTxtInfo('article_id ='.$article_id);
        $news_info['contents'] = $txt_info['contents'];
        $news_info['add_time'] = get_time($news_info['addtime']);
        $this->assign('news_info',$news_info);
        $this->display();
    }

    //回复我的
    public function reply_me(){
        $user_id = intval(session('user_id'));
        $message_obj = new MessageModel();

        $where = 'reply_user_id = '.$user_id . ' and message_type ='.MessageModel::REPLY;
        $total = $message_obj->getMessageNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $message_obj->setStart($firstRow);
        $message_obj->setLimit($this->page_num);
        $message_list = $message_obj->getMessageList('',$where, 'addtime desc');
        
        foreach ($message_list as $k => $v) {
            $send_user = M('Users')->where('user_id ='.$v['send_user_id'])->find();
            $message_list[$k]['send_nickname'] = $send_user['nickname'];
            $message_list[$k]['send_headimg'] = $send_user['headimgurl'];
            
            $content = M('PostComment')->where('post_comment_id ='.$v['id'])->getField('content');
            $message_list[$k]['content'] = strip_tags($content);
            $message_list[$k]['content'] = mb_substr($content, 0, 25, 'utf-8');
            $message_list[$k]['addtime'] = get_time($v['addtime']);
        }

        if(IS_AJAX && IS_POST){
            $this->ajaxReturn($message_list);
        }

        // dump($message_list);die;
        $this->assign('message_list', $message_list);
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $message_obj->setRead($user_id, MessageModel::REPLY);
        $this->assign('head_title', '回复我的');
        $this->display();
    }

    //@我的
    public function at_me(){
        $user_id = intval(session('user_id'));
        $message_obj = new MessageModel();

        $where = 'reply_user_id = '.$user_id . ' and message_type ='.MessageModel::AT;
        $total = $message_obj->getMessageNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $message_obj->setStart($firstRow);
        $message_obj->setLimit($this->page_num);
        $message_list = $message_obj->getMessageList('',$where, 'addtime desc');
        
        foreach ($message_list as $k => $v) {
            $send_user = M('Users')->where('user_id ='.$v['send_user_id'])->find();
            $message_list[$k]['send_nickname'] = $send_user['nickname'];
            $message_list[$k]['send_headimg'] = $send_user['headimgurl'];
            $message_list[$k]['addtime'] = get_time($v['addtime']);
        }

        if(IS_AJAX && IS_POST){
            $this->ajaxReturn($message_list);
        }

        // dump($message_list);die;
        $this->assign('message_list', $message_list);
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $message_obj->setRead($user_id, MessageModel::AT);

        $this->assign('head_title', '@我的');
        $this->display();
    }


    // 官方消息
    public function system_messages(){
        $user_id = intval(session('user_id'));

        $message_obj = new MessageModel();
        $where = 'reply_user_id = '.$user_id;
        $total = $message_obj->getSystemMessageNum($where);
        $firstRow = intval($this->_request('firstRow'));
        $message_obj->setStart($firstRow);
        $message_obj->setLimit($this->page_num);
        $message_list = $message_obj->getSystemMessageList($where);
        
        foreach ($message_list as $k => $v) {
            $message_list[$k]['addtime'] = get_time($v['addtime']);
        }

        if(IS_AJAX && IS_POST){
            $this->ajaxReturn($message_list);
        }

        // dump($message_list);die;
        $this->assign('message_list', $message_list);
        $this->assign('total', $total);
        $this->assign('firstRow', $this->page_num);
        $message_obj->setReadSystem($user_id);

        $this->assign('head_title', '系统消息');
        $this->display();
    }

}
