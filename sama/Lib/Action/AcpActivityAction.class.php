<?php
/**
 * 活动类
 */

class AcpActivityAction extends AcpAction
{
    public function AcpActivityAction()
    {
        parent::_initialize();
    }
    public function add_activity(){

        $province_obj = new AddressProvinceModel();//获取省信息
        $province_list = $province_obj->getProvinceList();
        $city_obj = new AddressCityModel();
        $city_list = $city_obj->getCityList();//市信息


        $arr=$this->handle_post_data();
        if(is_array($arr)){
            $activity_obj=new ActivityModel();
            $result=$activity_obj->addActivity($arr);
            if($result){
                $this->success('添加成功！',U('/AcpActivity/get_activity_list'));
            }else{
                $this->error('添加失败');
            }
        }
        $this->assign('pic_index', array(
            'title'=>'海报图',
            'batch' => false,
            'name' => 'pic_index',
            'help' => '请上传首页海报图',
        ));
        $this->assign('detail_pic', array(
            'title'=>'详情轮播图',
            'batch' => true,
            'name' => 'detail_pic',
            'height' => 175,
            'help' => '请上传活动详情页图片，第一张将作为列表显示图片,<front style="color: red">高度不要超过175px</front>',

        ));
        $this->assign('province_list',$province_list);
        $this->assign('city_list',$city_list);
        $this->assign('head_title', '添加活动');
        $this->display();
    }
    //处理 post过来的信息，返回存数据的数组
    public function handle_post_data(){
        $data=$this->_post();
        if($data['opt']=='add'||$data['opt']=='edit'){

            if(!$data['province']||!$data['city']){
                $this->error('请选择省市');
            }else{
                $arr['province_id']=$data['province'];
                $arr['city_id']=$data['city'];
            }
            $arr['is_index']=$data['is_index'];
            if($data['activity_name']){
                $arr['activity_title']=$data['activity_name'];
            }
//            if($data['activity_type']!=-1){
//                $arr['status']=$data['activity_type'];
//            }
            if($data['mobile']){
                $arr['phone']=$data['mobile'];
            }
            if($data['sponsor']){
                $arr['sponsor']=$data['sponsor'];
            }
            if($data['contractor']){
                $arr['contractor']=$data['contractor'];
            }
            if($data['sign_money']){
                $arr['sign_money']=$data['sign_money'];
            }
            if($data['content']){
               $arr['contents']=$data['content'];
            }
            if($data['notice_date']){
                $arr['notice_date']=strtotime($data['notice_date']);
            }
            if($data['start_date']){
                $arr['start_date']=strtotime($data['start_date']);
            }
            if($data['end_date']){
                $arr['end_date']=strtotime($data['end_date']);
            }
            if($data['address']){
                $arr['activity_address']=$data['address'];
            }
            if($data['pic_index']){
                $arr['pic_index']=$data['pic_index'];
            }
            if($data['detail_pic']){
                $arr['detail_pic']=implode(',',$data['detail_pic']);
            }
            if($data['center_lng']){
                $arr['lng']=$data['center_lng'];
            }
            if($data['center_lat']){
                $arr['lat']=$data['center_lat'];
            }
            if($data['sort']){
                $arr['sort']=$data['sort'];
            }
            return $arr;
        }
    }

    public function get_activity_list(){

        import('ORG.Util.Pagelist');
        $where='1';
        $activity_obj=new ActivityModel();
        $data=$this->_request();
        if($data['opt']=='select'){
            if($data['activity_name']){
                $activity_name=$data['activity_name'];
                $where.=' AND activity_title LIKE "%'.$data['activity_name'].'%"';
            }
            if($data['activity_status']!=-1){
                $activity_status=$data['activity_status'];
                if($activity_status == 1){//预告
                    $where.=' AND notice_date <='.time() .' AND start_date >'.time();
                }elseif($activity_status == 2){//进行中
                    $where.=' AND start_date <='.time().' AND end_date >'.time();
                }elseif($activity_status == 3){//已结束
                    $where.=' AND end_date <='.time();
                }
            }
            if($data['start_date']){
                $start_date=$data['start_date'];
                $where.=' AND start_date ='.strtotime($data['start_date']);
            }
            if($data['is_index']!=-1&&$data['is_index']!=''){
                $is_index=$data['is_index'];
                $where.=' AND is_index ='.$is_index;
            }
        }
        $count =  $activity_obj->getActivityNum($where);

        $Page = new Pagelist($count, C('PER_PAGE_NUM'));

        $activity_obj->setStart($Page->firstRow);
        $activity_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('page', $Page);
        $this->assign('show', $show);




        $activity_sign_obj=new ActivitySignModel();
        $activity_list=$activity_obj->getActivityList('',$where);
        foreach($activity_list as $k => $v){
            if($v['notice_date'] <= time() &&  time() < $v['start_date']){
                $status='预告中';
            }else if($v['start_date'] <= time() &&  time() < $v['end_date']){
                $status='进行中';
            }else{
                $status='已结束';
            }
            $activity_list[$k]['status']=$status;
            $start_time=$v['start_date'];
            $activity_list[$k]['start_time']=date('Y-m-d H:i',$start_time);
            $activity_list[$k]['notice_date']=date('Y-m-d H:i',$v['notice_date']);
            $activity_list[$k]['end_date']=date('Y-m-d H:i',$v['end_date']);
            $activity_list[$k]['sign_num']=$activity_sign_obj->getActivitySignNum('activity_id = '.$v['activity_id']);
         /*  $picarr=explode(',',$v['pic_url']);
           $activity_list[$k]['first_pic']=$picarr[0];*/
        }

        $this->assign('is_index',$is_index==''||$is_index==-1?-1:$is_index);
        $this->assign('activity_name',$activity_name?$activity_name:'');
        $this->assign('activity_status',$activity_status==''||$activity_status==-1?-1:$activity_status);
        $this->assign('start_date',$start_date?$start_date:'');
        $this->assign('activity_list',$activity_list);
        $this->assign('head_title', '活动列表');
        $this->display();
    }
    //编辑活动
    public function edit_activity(){

        $province_obj = new AddressProvinceModel();//获取省信息
        $province_list = $province_obj->getProvinceList();
        $city_obj = new AddressCityModel();
        $city_list = $city_obj->getCityList();//市信息

        $activity_id=$this->_get('activity_id');
        $activity_obj=new ActivityModel();
        $activity_info=$activity_obj->getActivityInfo('activity_id = '.$activity_id);
        $arr=$this->handle_post_data();

        if(is_array($arr)){
            $activity_id=$this->_post('activity_id');
            $result=$activity_obj->editActivity($activity_id,$arr);
//            dump($activity_obj->getLastSql());die;
            if($result){
                $this->success('编辑成功！',U('/AcpActivity/get_activity_list'));
            }else{
                $this->error('编辑失败');

            }
        }
        $this->assign('pic_index', array(
            'title'=>'海报图',
            'batch' => false,
            'name' => 'pic_index',
            'help' => '请上传首页海报图',
            'url'=>$activity_info['pic_index'],
        ));
        $this->assign('detail_pic', array(
            'title'=>'详情轮播图',
            'batch' => true,
            'name' => 'detail_pic',
            'height' => 175,
            'help' => '请上传活动详情页图片，第一张将作为列表显示图片,<front style="color: red">不要超过175px</front>',
            'pic_arr'  =>explode(',',$activity_info['detail_pic']),
        ));

        $this->assign('province_list',$province_list);
        $this->assign('city_list',$city_list);
        $this->assign('activity_info',$activity_info);
        $this->display();
    }
    public function del_activity(){
        $activity_id=$this->_post('activity_id');
        $activity_obj=new ActivityModel();
        $result=$activity_obj->delActivity($activity_id);
        if($result){
            echo 'success';
            exit;
        }else{
            echo 'fail';
            exit;
        }
    }
    //批量删除活动
    public function del_activity_batch(){
        $ids=$this->_post('id');

        $activity_obj=new ActivityModel();
        $idarr=explode(',',$ids);
        array_pop($idarr);//删除元素最后一个元素
        foreach($idarr as $id){
            $r=$activity_obj->delActivity($id);
            if(!$r){
                echo 'fail';
                exit;
            }
        }
        echo 'success';
        exit;
    }
    //获取评论列表
    public function get_comment_list(){
        import('ORG.Util.Pagelist');
        $activity_id=$this->_get('activity_id');
        $comment_obj=new CommentModel();
        $data=$this->_request();
        $where1='comment_type = 1 and type_id = '.$activity_id;
        if($data['opt']=='select'){
            $key_words=$data['key_words'];
            $where1.=' AND contents LIKE "%'.$key_words.'%"';
        }
        $Model = new Model(); // 实例化一个model对象 没有对应任何数据表
        $count =  $comment_obj->getCommentNum($where1);//分页操作
        $Page = new Pagelist($count, C('PER_PAGE_NUM'));
        $comment_obj->setStart($Page->firstRow);
        $comment_obj->setLimit($Page->listRows);
        $show = $Page->show();
        $this->assign('page', $Page);
        $this->assign('show', $show);

        $Model = new Model(); // 实例化一个model对象 没有对应任何数据表
        $where="select *,tp_comment.contents as activity_comment from tp_comment inner join tp_activity on tp_comment.type_id=tp_activity.activity_id where  comment_type = 1 AND type_id = ".$activity_id;
        if($data['opt']=='select'){
            $where.=' AND tp_comment.contents LIKE "%'.$key_words.'%"';
        }
        $where.=" order by tp_comment.addtime DESC";
        $comment_list=$Model->query($where);
        $this->assign('activity_id',$activity_id);
        $this->assign('key_words',$key_words);
        $this->assign('comment_list',$comment_list);
        $this->display();
    }
    //删除评论
    function del_comment(){
        $comment_id=$this->_post('comment_id');
        $comment_obj = new CommentModel();
        $result = $comment_obj->delComment('comment_id ='.$comment_id);
        if ($result) {
            echo 'success';
            exit;
        } else {
            echo 'fail';
            exit;
        }
    }
    //批量删除评论
    function del_comment_batch(){
        $ids=$this->_post('id');

        $comment_obj = new CommentModel();
        $idarr=explode(',',$ids);
        array_pop($idarr);//删除元素最后一个元素
        foreach($idarr as $id){
            $r=$comment_obj->delComment('comment_id ='.$id);
            if(!$r){
                echo 'fail';
                exit;
            }
        }
        echo 'success';
        exit;
    }
}