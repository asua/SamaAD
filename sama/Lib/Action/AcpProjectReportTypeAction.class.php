<?php
/**
 * Acp后台项目申报类型
 */
class AcpProjectReportTypeAction extends AcpAction
{

    /**
     * 初始化
     * @author 张勇
     * @return void
     * @todo 初始化方法
     */
    public function _initialize()
    {
        parent::_initialize();
        $this->assign('action_title', '项目申报类型');
        $this->assign('action_src', U('/AcpProjectReportType/get_project_report_type_list'));
    }

    public function get_project_report_type_list()
    {
        $project_report_type_obj = new TypeModel();
        $where     = 'type_type ='.TypeModel::PROJECTREPORT;
        //数据总量
        $total = $project_report_type_obj->getTypeNum($where);

        //处理分页
        import('ORG.Util.Pagelist');
        $per_page_num = C('PER_PAGE_NUM');
        $Page         = new Pagelist($total, $per_page_num);
        $project_report_type_obj->setStart($Page->firstRow);
        $project_report_type_obj->setLimit($Page->listRows);

        $page_str = $Page->show();
        $this->assign('page_str', $page_str);

        $project_report_type_list = $project_report_type_obj->getTypeListAcp($where);

        $this->assign('project_report_type_list', $project_report_type_list);
        $this->assign('head_title', '项目申报类型列表');
        $this->display();

    }

    //添加项目申报类型
    public function add_project_report_type()
    {

        $act = $this->_post('act');
        if ($act == 'add') {
            $_post      = $this->_post();
            $type_name = $_post['type_name'];
            $serial     = $_post['serial'];

            //表单验证
            if (!$type_name) {
                $this->error('请填写项目申报类型名称！');
            }

            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }


            $arr = array(
                'type_name' => $type_name,
                'serial'     => $serial,
                'type_type' => TypeModel::PROJECTREPORT,
            );

            $project_report_type_obj = new TypeModel();
            $success   = $project_report_type_obj->addType($arr);

            if ($success) {
                $this->success('恭喜您，项目申报类型添加成功！', '/AcpProjectReportType/add_project_report_type');
            } else {
                $this->error('抱歉，项目申报类型添加失败！', '/AcpProjectReportType/add_project_report_type');
            }
        }

        $this->assign('act', 'add');
        $this->assign('head_title', '添加项目申报类型');
        $this->display();
    }



    //修改项目申报类型
    public function edit_project_report_type()
    {
        $redirect = U('/AcpProjectReportType/get_project_report_type');
        $project_report_type_id = intval($this->_get('type_id'));
        if (!$project_report_type_id) {
            $this->error('对不起，非法访问！', $redirect);
        }

        $project_report_type_obj  = new TypeModel();
        $project_report_type_info = $project_report_type_obj->getTypeInfo('type_id ='.$project_report_type_id);

        if (!$project_report_type_info) {
            $this->error('对不起，不存在相关项目申报类型！', $redirect);
        }

        $act = $this->_post('act');

        if ($act == 'edit') {
            $_post      = $this->_post();
            $project_report_type_name = $_post['type_name'];
            $serial     = $_post['serial'];

            //表单验证
            if (!$project_report_type_name) {
                $this->error('请填写项目申报类型名称！');
            }

            if (!ctype_digit($serial)) {
                $this->error('请填写排序号！');
            }


            $arr = array(
                'type_name' => $project_report_type_name,
                'serial'     => $serial,
            );

            $url       = '/AcpProjectReportType/edit_project_report_type/type_id/' . $project_report_type_id;

            if ($project_report_type_obj->setType($project_report_type_id, $arr)) {
                $this->success('恭喜您，项目申报类型修改成功！', $url);
            } else {
                $this->error('抱歉，项目申报类型修改失败！', $url);
            }
        }
        $this->assign('project_report_type_info', $project_report_type_info);
        $this->assign('act', 'edit');
        $this->assign('head_title', '修改项目申报类型');
        $this->display('add_project_report_type');

    }

    /**
     * 删除项目申报类型
     * @author 姜伟
     * @param void
     * @return void
     * @todo 异步方法，根据项目申报类型ID删除项目申报类型
     */
    public function delete_project_report_type()
    {
        $project_report_type_id = intval($this->_post('type_id'));
        if ($project_report_type_id) {
            $project_report_type_obj = new TypeModel($project_report_type_id);
            $project_report_obj           = new ProjectReportModel();
//            $num                = $project_report_obj->getServiceProvidersNum('FIND_IN_SET('.$project_report_type_id.',project_report_type)');
            $num                = $project_report_obj->getProjectReportNum('project_report_type_id ='.$project_report_type_id);
            //存在项目申报则删除失败；
            if ($num) {
                exit('failure');
            }
            $success = $project_report_type_obj->delType('type_id ='.$project_report_type_id);
            exit($success ? 'success' : 'failure');
        }

        exit('failure');
    }

    public function batch_delete_project_report_type()
    {

        $project_report_type_ids = $this->_post('type_ids');
        if ($project_report_type_ids) {
            $project_report_type_ary = explode(',', $project_report_type_ids);
            $success_num     = 0;
            $project_report_obj           = new ProjectReportModel();
            foreach ($project_report_type_ary as $project_report_type_id) {
//                $num = $service_provides_obj->getServiceProvidersNum('FIND_IN_SET('.$project_report_type_id.',project_report_type)');
                $num = $project_report_obj->getProjectReportNum('project_report_type_id ='.$project_report_type_id );
//                dump($service_provides_obj->getLastSql());die;
                if ($num) {
                    continue;
                }
                $project_report_type_obj = new TypeModel($project_report_type_id);
                $success_num += $project_report_type_obj->delType('type_id ='.$project_report_type_id);
            }
            echo $success_num ? 'success' : 'failure';
            exit;
        }

        exit('failure');
    }


}
