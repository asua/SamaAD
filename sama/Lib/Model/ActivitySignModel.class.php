<?php

class ActivitySignModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    //获取数量
    public function getActivitySignNum($where='')
    {
        $count = $this->where($where)->count();
        return $count;
    }
    //单条信息
    public function getActivitySignInfo($where, $fields = '')
    {
        return $this->field($fields)->where($where)->find();
    }
    //获取列表
    public function getActivitySignList($fields = '', $where = '', $orderby = 'addtime desc', $groupby = '')
    {
        return $this->field($fields)->where($where)->group($groupby)->order($orderby)->limit()->select();
    }
    //增加报名
    public function addActivitySign($arr){
        return $this->add($arr);
    }
    //编辑报名
    public function editActivitySign($activity_sign_id,$arr){
        return $this->where('activity_sign_id = '.$activity_sign_id)->save($arr);
    }
    //删除报名
    public function delActivitySign($activity_sign_id){
        return $this->where('activity_sign_id = '.$activity_sign_id)->delete();
    }

    /**
     * 生成订单号
     * @author 姜伟
     * @param void
     * @return string $order_sn
     * @todo 查询当天数据库中的订单量$count，订单号即为日期 . ($count + 1)，如201403010099，2014年3月1日的第99个订单
     */
    public function generateOrderSn()
    {
        //当天0点的时间戳
        $timestamp = strtotime(date('Y-m-d', time()));
        //当天订单总量
        $count = $this->where('addtime >=' . $timestamp)->count();
        //后缀位数
        $len = strlen((string) $count);
        //少于4位取4位，否则不变
        $len = $len < 4 ? 4 : $len;
        $suffix = sprintf("%0" . $len . "d", $count);
        $order_sn = date('Ymd') . $suffix;

        return $order_sn;
    }

    public function payStatus(){
        return array(
          '0'=>'待付款',
          '1'=>'已完成',
          '2'=>'已取消',
          '3'=>'已退款',
        );
    }


    public function getListData($sign_list){

        foreach($sign_list as $k => $v){
            $activity_obj = new ActivityModel();
            $activity_info = $activity_obj->getActivityInfo('activity_id ='.$v['activity_id']);
            $sign_list[$k]['activity_name'] = $activity_info['activity_title'];
            $sign_list[$k]['address'] = $activity_info['activity_address'];
            $status = self::payStatus();
            $sign_list[$k]['state'] = $status[$v['pay_status']];
            $sign_list[$k]['time'] = date('Y-m-d',$activity_info['start_date']);
            $sign_list[$k]['pic'] = $activity_info['pic_index'];
        }
        return $sign_list;
    }

}