<?php
/**
 * 赞赏帖隐藏图片模型类
 */

class PraiseHideImgModel extends Model
{
    private $praise_hide_id;


    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($praise_hide_id=0)
    {
        parent::__construct();
        $this->praise_hide_id = $praise_hide_id;
    }


    public function addPraiseHideImg($img_arr){
        return $this->addAll($img_arr);
    }

    public function getPraiseHideImg($praise_hide_id){
        return $this->where('praise_hide_id ='.$praise_hide_id)->getField('img_url', true);
    }

    public function getPraiseHideImgList($praise_hide_id){
        return $this->where('praise_hide_id ='.$praise_hide_id)->limit(100000)->select();
    }


    //删除照片．
    public function delImg($praise_hide_img_id){
        return $this->where('praise_hide_img_id ='.$praise_hide_img_id)->delete();
    }
}
