<?php
/**
 * 悬赏答案图片模型类
 */

class RewardAnswerImgModel extends Model
{
    private $reward_answer_id;


    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($reward_answer_img_id=0)
    {
        parent::__construct();
        $this->reward_answer_img_id = $reward_answer_img_id;
    }


    public function addRewardAnswerImg($img_arr){
        return $this->addAll($img_arr);
    }

    public function getRewardAnswerImg($reward_answer_id){
        return $this->where('reward_answer_id ='.$reward_answer_id)->getField('img_url', true);
    }

    public function getRewardAnswerImgList($reward_answer_id){
        return $this->where('reward_answer_id ='.$reward_answer_id)->limit(100000)->select();
    }

    //删除照片．
    public function delImg($reward_answer_img_id){
        return $this->where('reward_answer_img_id ='.$reward_answer_img_id)->delete();
    }

}
