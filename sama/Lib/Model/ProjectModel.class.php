<?php
/**
 * 项目模型类
 */

class ProjectModel extends Model
{
    // 项目id
    public $project_id;
   
    /**
     * 构造函数
     * @author 姜伟
     * @param $project_id 项目ID
     * @return void
     * @todo 初始化项目id
     */
    public function ProjectModel($project_id)
    {
        parent::__construct('project');

        if ($project_id = intval($project_id))
		{
            $this->project_id = $project_id;
		}
    }

    /**
     * 获取项目信息
     * @author 姜伟
     * @param int $project_id 项目id
     * @param string $fields 要获取的字段名
     * @return array 项目基本信息
     * @todo 根据where查询条件查找项目表中的相关数据并返回
     */
    public function getProjectInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改项目信息
     * @author 姜伟
     * @param array $arr 项目信息数组
     * @return boolean 操作结果
     * @todo 修改项目信息
     */
    public function editProject($arr)
    {
        return $this->where('project_id = ' . $this->project_id)->save($arr);
    }

    /**
     * 添加项目
     * @author 姜伟
     * @param array $arr 项目信息数组
     * @return boolean 操作结果
     * @todo 添加项目
     */
    public function addProject($arr)
    {
        if (!is_array($arr)) return false;

		$arr['addtime'] = time();

        return $this->add($arr);
    }

    /**
     * 删除项目
     * @author 姜伟
     * @param int $project_id 项目ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delProject($where)
    {
        if (!$where) return false;
        return $this->where($where)->save(array('is_del' => 1));
    }

    /**
     * 根据where子句获取项目数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的项目数量
     * @todo 根据where子句获取项目数量
     */
    public function getProjectNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询项目信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 项目基本信息
     * @todo 根据SQL查询字句查询项目信息
     */
    public function getProjectList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 获取项目列表页数据信息列表
     * @author 姜伟
     * @param array $project_list
     * @return array $project_list
     * @todo 根据传入的$project_list获取更详细的项目列表页数据信息列表
     */
    public function getListData($project_list)
    {
		foreach ($project_list AS $k => $v)
		{
            $user_obj = new UserModel($v['user_id']);
            $user = $user_obj->getUserInfo('nickname');
            $project_list[$k]['nickname'] = $user['nickname'];
            $project_list[$k]['addtime'] = date('Y-m-d H:i:s', $v['addtime']);
            $field = M('Type')->where('type_id in('.$v['project_field']. ') and type_type ='.TypeModel::DOMAIN)->getField('type_name',true);
            $project_list[$k]['field_name'] = implode(',', $field);

            $phase = M('Type')->where('type_id ='.$v['investment_phase']. ' and type_type ='.TypeModel::PHASE)->getField('type_name');
            $project_list[$k]['phase_name'] = $phase; 

            //是否收藏
            $collect_obj = D('Collect');
            $collect_num = $collect_obj->getCollectNum('user_id ='.session('user_id').' and type_id ='.$v['project_id'].' and collect_type ='.CollectModel::PROJECT);
            if($collect_num){
                $project_list[$k]['is_collect'] = 1; 
            }else{
                $project_list[$k]['is_collect'] = 0; 
            }
		}

		return $project_list;
    }


    public function getProjectField($field, $where = ''){
        return $this->where($where)->getField($field, true);
    }

    public function setFieldInc($field, $where = ''){
        return $this->where($where)->setDec($field);
    }
    public function setFieldDec($field, $where = ''){
        return $this->where($where)->setDec($field);
    }
}
