<?php
class SupportingModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    //获取总数量
    public function getSupportingNum($where){
        return $this->where($where)->count();
    }
    //获取周边配套
    public function getSupportinglist($where,$orderby=''){
        return $this->where($where)->order($orderby)->select();
    }
    //增加周边配套
    public function addSupporting($arr){
        return $this->add($arr);
    }
    //删除周边配套
    public function delSupporting($supporting_id){
        return $this->where('supporting_id = '.$supporting_id)->delete();
    }
    //获取周边配套详情
    public function getSupportingInfo($supporting_id){
        return $this->where('supporting_id = '.$supporting_id)->find();
    }
    //修改周边配套
    public function editSupporting($arr,$supporting_id){
        return $this->where('supporting_id = '.$supporting_id)->save($arr);
    }
}
