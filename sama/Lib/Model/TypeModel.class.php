<?php
/**
 * 类型模型
 */

Class TypeModel extends Model
{
    private $type_id;

    const DOMAIN = 1; //领域
    const PHASE = 2; //阶段
    const SITE = 3; //场地
    const CURRENCY = 4; //币种
    const PROJECTREPORT = 5; //项目申报

    const SOURCE =7 ; //客户来源
    const DRAIN =8 ; //流失原因
    const SURRENDER =9 ; //退租原因

    const CUSTOMER_NATURE = 10; //客户性质
    const PROJECT_NATURE = 11; //项目性质




    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($type_id=0)
    {
        parent::__construct();
        $this->type_id = $type_id;
    }

    public function getTypeNum($where='') {
      return $this->where($where)->count();
    }

    /**
     * 添加类型
     * @author 姜伟
     * @param array $arr_Type 类型数组
     * @return boolean 操作结果
     * @todo 添加类型
     */
    public function addType($arr_Type)
    {
        if (!is_array($arr_Type)) return false;
        return $this->add($arr_Type);
    }

    /**
     * 删除类型
     * @author 姜伟
     * @param string $type_id 类型ID
     * @return boolean 操作结果
     * @todo 删除类型
     */
    public function delType($where)
    {
        if (!$where) return false;
        return $this->where($where)->delete();
    }

    /**
     * 更改类型
     * @author 姜伟
     * @param int $type_id 类型ID
     * @param array $arr_Type 类型数组
     * @return boolean 操作结果
     * @todo 更改类型
     */
    public function setType($type_id, $arr_Type)
    {
        if (!is_numeric($type_id) || !is_array($arr_Type)) return false;
        return $this->where('type_id = ' . $type_id)->save($arr_Type);
    }

    /**
     * 获取类型
     * @author 姜伟
     * @param int $type_id 类型ID
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 类型
     * @todo 获取类型
     */
    public function getType($type_id, $fields = null)
    {
        if (!is_numeric($type_id))   return false;
        return $this->field($fields)->where('type_id = ' . $type_id)->find();
    }

    /**
     * 获取类型某个字段的信息
     * @author 姜伟
     * @param int $type_id 类型ID
     * @param string $field 查询的字段名
     * @return array 类型
     * @todo 获取类型某个字段的信息
     */
    /*public function getTypeField($type_id, $field)
    {
        if (!is_numeric($type_id))   return false;
        return $this->where('type_id = ' . $type_id)->getField($field);
    }*/

    /**
     * 前台获取所有类型列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 类型列表
     * @todo 获取所有类型列表
     */
    public function getTypeList($where = null)
    {   
        return $this->where($where)->order('serial')->limit(10000)->select();
    }

    /**
     * 后台获取所有类型列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 类型列表
     * @todo 获取所有类型列表
     */
    public function getTypeListAcp($where = null)
    {
        return $this->where($where)->order('serial')->limit()->select();
    }
 
    /**
     * 获取分类信息
     * @author 姜伟
     * @param string $where where子句
     * @param string $fields 要获取的字段名
     * @return array 商品基本信息
     * @todo 根据where查询条件查找商品表中的相关数据并返回
     */
    public function getTypeInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }


    public function getTypeField($field,$where = ''){
        return $this->where($where)->getField($field, true);
    }
    public function getTypeSiteField($field,$where = ''){
        return $this->where($where)->getField($field);
    }

    public function getTypeNameData($id_str){
        $arr=explode(',',$id_str);
        foreach($arr as $type_id){
            $name=$this->getTypeField('type_name','type_id = '.$type_id);
            $type_name_arr[]=$name[0];
        }
        return $type_name_arr;

    }
}
