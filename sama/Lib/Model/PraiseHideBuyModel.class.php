<?php
/**
 * 赞赏帖隐藏内容购买记录模型类
 */

class PraiseHideBuyModel extends Model
{
    private $praise_hide_buy_id;


    /**
     * 判断用户是否有查看隐藏内容的权限
     * @param  [type] $user_id   [description]
     * @param  [type] $hide_type 类型，1帖子，2评论
     * @param  [type] $id        类型对应的id
     * @return [type]            [description]
     */
    public function checkSeeHidePriv($user_id, $hide_id){

        $r = $this->where('user_id ='.$user_id . ' and praise_hide_id ='.$hide_id)->find();

        return $r ? true : false;
    }


    public function getPraiseHideBuyNum($where){
        return $this->where($where)->count();
    }


    public function addPraiseHideBuy($arr){
        if(!is_array($arr)) return false;
        $arr['addtime'] = time();
        return $this->add($arr);
    }


    //查询某个字段
    public function getPraiseHideBuyField($where,$field){
        return $this->where($where)->getField($field);
    }

    public function getPraiseHideBuyFields($where,$field,$group=''){
        return $this->where($where)->group($group)->getField($field,true);
    }
}
