<?php
/**
 * 服务类型模型
 */

Class ServiceTypeModel extends Model
{
    private $service_type_id;
    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($service_type_id=0)
    {
        parent::__construct();
        $this->service_type_id = $service_type_id;
    }

    public function getServiceTypeNum($where='') {
      return $this->where($where)->count();
    }

    /**
     * 添加服务类型
     * @author 姜伟
     * @param array $arr_ServiceType 服务类型数组
     * @return boolean 操作结果
     * @todo 添加服务类型
     */
    public function addServiceType($arr_ServiceType)
    {
        if (!is_array($arr_ServiceType)) return false;
        return $this->add($arr_ServiceType);
    }

    /**
     * 删除服务类型
     * @author 姜伟
     * @param string $service_type_id 服务类型ID
     * @return boolean 操作结果
     * @todo 删除服务类型
     */
    public function delServiceType()
    {
        if (!is_numeric($this->service_type_id)) return false;
        return $this->where('service_type_id = ' . $this->service_type_id)->delete();
    }

    /**
     * 更改服务类型
     * @author 姜伟
     * @param int $service_type_id 服务类型ID
     * @param array $arr_ServiceType 服务类型数组
     * @return boolean 操作结果
     * @todo 更改服务类型
     */
    public function setServiceType($service_type_id, $arr_ServiceType)
    {
        if (!is_numeric($service_type_id) || !is_array($arr_ServiceType)) return false;
        return $this->where('service_type_id = ' . $service_type_id)->save($arr_ServiceType);
    }

    /**
     * 获取服务类型
     * @author 姜伟
     * @param int $service_type_id 服务类型ID
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 服务类型
     * @todo 获取服务类型
     */
    public function getServiceType($service_type_id, $fields = null)
    {
        if (!is_numeric($service_type_id))   return false;
        return $this->field($fields)->where('service_type_id = ' . $service_type_id)->find();
    }

    /**
     * 获取服务类型
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 服务类型
     * @todo 获取服务类型
     */
    public function getServiceTypeFieldId($where, $fields = null)
    {
        return $this->where($where)->getField($fields);
    }

    /**
     * 获取服务类型某个字段的信息
     * @author 姜伟
     * @param int $service_type_id 服务类型ID
     * @param string $field 查询的字段名
     * @return array 服务类型
     * @todo 获取服务类型某个字段的信息
     */
    public function getServiceTypeField($service_type_id, $field)
    {
        if (!is_numeric($service_type_id))   return false;
        return $this->where('service_type_id = ' . $service_type_id)->getField($field);
    }

    /**
     * 获取所有服务类型列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 服务类型列表
     * @todo 获取所有服务类型列表
     */
    public function getServiceTypeList($where = null)
    {
        return $this->where($where)->order('serial')->limit()->select();
    }
 
    /**
     * 获取分类信息
     * @author 姜伟
     * @param string $where where子句
     * @param string $fields 要获取的字段名
     * @return array 商品基本信息
     * @todo 根据where查询条件查找商品表中的相关数据并返回
     */
    public function getServiceTypeInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    //是否在积分商城中显示该分类
    public function updateServiceTypeIntegral($service_type_id, $i){
        if($i){
                $this->setServiceType($service_type_id, array('has_all_integral' => 1));
        }else{
                $item_obj = new ItemModel();
                $result = $item_obj->getItemNum('service_type_id ='.$service_type_id .' and is_integral = 1');
                
                if(!$result){
                    $this->setServiceType($service_type_id, array('has_all_integral' => 0));
                }
        }
    }
}
