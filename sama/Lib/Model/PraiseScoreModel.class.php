<?php
/**
 * 赞赏帖鉴定师评分模型类
 */

class PraiseScoreModel extends Model
{
    private $praise_score_id;

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($praise_score_id=0)
    {
        parent::__construct();
        $this->praise_score_id = $praise_score_id;
    }

    //获取某一帖子的平均分
    public function getPostAvgScore($post_id){
        return $this->where('post_id ='.$post_id)->avg('score');
    }

    public function getPraiseScoreList($post_id){
        return $this->where('post_id ='.$post_id)->order('floor desc')->limit()->select();
    }


    public function getListData($score_list){
        foreach ($score_list as $k => $v) {
            $user = M('Users')->where('user_id ='.$v['user_id'])->find();
            $score_list[$k]['nickname'] = $user['nickname'];
            $score_list[$k]['headimgurl'] = $user['headimgurl'];

            $score_list[$k]['addtime'] = get_time($v['addtime']);
        }
        return $score_list;
    }


    //判断是否已评分
    public function getPraiseScoreNum($where){
        return $this->where($where)->count();
    }


    //获取帖子最高楼层
    public function getMaxFloor($post_id){
        $c = $this->where('post_id ='.$post_id)->order('floor desc')->find();

        $floor = $c['floor'];
        return intval($floor);
    }


    //打分
    public function addPraiseScore($arr){
        if(!is_array($arr)) return false;
        $arr['addtime'] = time();
        D('Post')->setPost($arr['post_id'],array( 'updatetime' => $arr['addtime']));
        return $this->add($arr);
    }
}
