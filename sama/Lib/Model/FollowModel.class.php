<?php
/**
 * 关注模型类
 */

class FollowModel extends Model
{
    private $follow_id;

    const CHANNEL = 1;
    const USER = 2;

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($follow_id=0)
    {
        parent::__construct();
        $this->follow_id = $follow_id;
    }


    public function getFollowInfo($where){
        return $this->where($where)->find();
    }

    /**
     * 关注
     * 未关注则关注，已关注则取消关注
     *如果关注的是频道并且是第一次关注升一级
     * @param [type] $user_id     [description]
     * @param [type] $followed_id [description]
     * @param [type] $follow_type 类型，1频道，2用户
     */
    public function setFollow($user_id, $followed_id, $follow_type){
        //是否已关注
        $follow = $this->getFollowInfo('user_id ='.$user_id . ' and followed_id ='.$followed_id . ' and follow_type = '.$follow_type);
        if($follow){
            //取消关注
            $r = $this->where('follow_id ='.$follow['follow_id'])->delete();
        }else{
            $arr = array(
                'user_id' => $user_id,
                'followed_id' => $followed_id,
                'follow_type' => $follow_type,
                'addtime' => time(),
                );
            $r = $this->add($arr);

            if($r && $follow_type == SELF::CHANNEL){
                //若为关注频道
                $user_channel_rank = D('UserChannelRank')->getUserChannelRankInfo('user_id ='.$user_id. ' and channel_id ='.$followed_id);
                if(!$user_channel_rank){
                    //第一次关注
                    $first_rank = D('ChannelRank')->getFirstRank();
                    $data = array(
                        'user_id' => $user_id,
                        'channel_id' => $followed_id,
                        'channel_rank_id' => $first_rank['channel_rank_id'],
                        'exp' => $first_rank['need_exp'],
                        );
                    D('UserChannelRank')->addUserChannelRank($data);
                }
            }
        }

        return $r;
    }


    public function getFollowList($where){
        return $this->where($where)->limit(10000)->select();
    }

    public function getListData($follow_list){

        foreach ($follow_list as $k => $v) {
            
            if($v['follow_type'] == self::CHANNEL){
                $user_channel_rank = '';
                $is_sign = 0;
                //频道信息
                $channel = D('Channel')->getChannelInfo('channel_id ='.$v['followed_id']);
                if($channel['is_del'] || !$channel['isuse']){
                    unset($follow_list[$k]);
                    continue;
                }
                $follow_list[$k]['channel_name'] = $channel['channel_name'];
                $follow_list[$k]['icon'] = $channel['icon'];
                $follow_list[$k]['summary'] = $channel['summary'];
                //用户等级
                $user_channel_rank = D('UserChannelRank')->getUserChannelRankInfo('user_id ='.$v['user_id']. ' and channel_id ='.$v['followed_id']);
                $channel_rank = D('ChannelRank')->getChannelRankInfo('channel_rank_id ='.$user_channel_rank['channel_rank_id']);
                $follow_list[$k]['user_channel_rank'] = $channel_rank['rank_name'];
                //是否签到
                $is_sign = D('Sign')->checkSign($v['user_id'], $v['followed_id']);
                $follow_list[$k]['is_sign'] = $is_sign;

                //该频道今天新发布的帖子数
                $post_obj = new PostModel();
                $follow_list[$k]['tie_num'] = $post_obj->getTodayPostNum($v['followed_id']);

            }elseif($v['follow_type'] == self::USER){
                //关注的人
                $user_obj = new UserModel();
                $concern_info = $user_obj->getUserInfo('user_id , nickname , headimgurl','user_id ='.$v['followed_id']);
                $follow_list[$k]['concern_info'] = $concern_info;
                //是否关注．
                $follow_list[$k]['follow_is'] = self::checkFollowUser(intval(session('user_id')),$v['followed_id']);

                //粉丝
                $fans_obj = new UserModel();
                $fans_info = $fans_obj->getUserInfo('user_id , nickname , headimgurl','user_id ='.$v['user_id']);
                $follow_list[$k]['fans_info'] = $fans_info;
                //是否粉丝．
                $follow_list[$k]['is_follow'] = self::checkFollowUser(intval(session('user_id')),$v['user_id']);
            }
        }

        return $follow_list;
    }

    //获取用户关注的频道列表
    public function getFollowedChannelList($user_id){
        return $this->where('user_id ='.$user_id . ' and follow_type ='. self::CHANNEL)->order('addtime desc')->limit(10000)->select();
    }

    //获取用户关注的频道id
    public function getFollowedChannelids($user_id){
        $r = $this->where('user_id ='.$user_id . ' and follow_type ='. self::CHANNEL)->limit(10000)->getField('followed_id', true);
        $r[] = 0;
        return $r;
    }

    //获取用户关注的频道列表
    public function getFollowChannelList($user_id){
        return $this->where('user_id ='.$user_id . ' and follow_type ='. self::CHANNEL)->order('addtime desc')->limit()->select();
    }

    //获取用户关注的用户列表
    public function getFollowedUserList($user_id){
        return $this->where('user_id ='.$user_id . ' and follow_type ='. self::USER)->order('addtime desc')->limit()->select();
    }

    //获取关注该用户(粉丝)的列表
    public function getFansUserList($user_id){
        return $this->where('followed_id ='.$user_id . ' and follow_type ='. self::USER)->order('addtime desc')->limit()->select();
    }

    //获取用户关注的用户id
    public function getFollowedUserlids($user_id){
        $r = $this->where('user_id ='.$user_id . ' and follow_type ='. self::USER)->limit(10000)->getField('followed_id', true);
        // $r[] = 0;
        return $r;
    }

    //判断用户是否关注某一频道
    public function checkFollowChannel($user_id, $channel_id){
        return $this->where('user_id ='.$user_id .' and followed_id ='.$channel_id .' and follow_type = '.self::CHANNEL)->count();
    }

    //判断用户是否关注某一粉丝
    public function checkFollowUser($user_id, $channel_id){
        return $this->where('user_id ='.$user_id .' and followed_id ='.$channel_id .' and follow_type = '.self::USER)->count();
    }

    //获取用户关注的类型数量
    public function getFollowedTypeNum($user_id,$type){
        return $this->where('user_id ='.$user_id . ' and follow_type ='.$type)->count();
    }

    //获取关注该用户(粉丝)数
    public function getFansUserNum($user_id){
        return $this->where('followed_id ='.$user_id . ' and follow_type ='. self::USER)->count();
    }


    public function getFollowNum($where){
        return $this->where($where)->count();
    }
}
