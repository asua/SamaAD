<?php
/**
 * 频道模型类
 */

class ChannelModel extends Model
{
    private $channel_id;

    const REWARD = 1;//悬赏
    const PRAISE = 2;//赞赏
    const CROWDFUNDING = 3;//心愿
    const NORMAL = 4;//普通


    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($channel_id=0)
    {
        parent::__construct();
        $this->channel_id = $channel_id;
    }


    public function channelTypeName(){
        return array(
            self::REWARD => '悬赏',
            self::PRAISE => '赞赏',
            self::CROWDFUNDING => '心愿',
            self::NORMAL => '普通',
            );
    }

    public function channelType(){
        return array(
            self::REWARD => 'reward',
            self::PRAISE => 'praise',
            self::CROWDFUNDING => 'crowdfunding',
            self::NORMAL => 'normal',
            );
    }

    public function getChannelNum($where='') {
      return $this->where($where)->count();
    }

    /**
     * 添加视频分类
     * @author 姜伟
     * @param array $arr_class 视频分类数组
     * @return boolean 操作结果
     * @todo 添加视频分类
     */
    public function addChannel($arr_class)
    {
        $arr_class['addtime'] = time();
        if (!is_array($arr_class)) return false;
        return $this->add($arr_class);
    }

    /**
     * 删除视频分类
     * @author 姜伟
     * @param string $channel_id 视频分类ID
     * @return boolean 操作结果
     * @todo 删除视频分类
     */
    public function delChannel()
    {
        if (!is_numeric($this->channel_id)) return false;
        return $this->where('channel_id = ' . $this->channel_id)->save(array('is_del'=>1));
    }

    /**
     * 更改视频分类
     * @author 姜伟
     * @param int $channel_id 视频分类ID
     * @param array $arr_class 视频分类数组
     * @return boolean 操作结果
     * @todo 更改视频分类
     */
    public function editChannel($channel_id, $arr_class)
    { 
        if (!is_numeric($channel_id) || !is_array($arr_class)) return false;
        return $this->where('channel_id = ' . $channel_id)->save($arr_class);
    }

    /**
     * 获取视频分类某个字段的信息
     * @author 姜伟
     * @param int $channel_id 视频分类ID
     * @param string $field 查询的字段名
     * @return array 视频分类
     * @todo 获取视频分类某个字段的信息
     */
    public function getChannelField($channel_id, $field)
    {
        if (!is_numeric($channel_id))   return false;
        return $this->where('channel_id = ' . $channel_id)->getField($field);
    }

    /**
     * 获取所有视频分类列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 视频分类列表
     * @todo 获取所有视频分类列表
     */
    public function getChannelList($field = '',$where = null)
    {
        return $this->where($where)->order('serial')->limit()->select();
    }

    public function getValidChannelList($where = '')
    {
        if($where){
            $where = 'isuse = 1 AND '.$where;
        }else{
            $where = 'isuse = 1';
        }

        return $this->where($where)->order('serial')->Limit(1000000)->select();
    }
 
    /**
     * 获取分类信息
     * @author 姜伟
     * @param string $where where子句
     * @param string $fields 要获取的字段名
     * @return array 商品基本信息
     * @todo 根据where查询条件查找商品表中的相关数据并返回
     */
    public function getChannelInfo($where, $fields = '')
    {
        return $this->field($fields)->where($where)->find();
    }


    public function getListData($channel_list){

        foreach ($channel_list as $k => $v) {
            
            //栏目
            $sort_name = D('ChannelSort')->getChannelSortField($v['sort_id'], 'channel_sort_name');
            $channel_list[$k]['sort_name'] = $sort_name;

            $type_list = $this->channelTypeName();
            $channel_list[$k]['type_name'] = $type_list[$v['channel_type']];

            //频道今日的新帖数
            $channel_list[$k]['new_tie'] = D('Post')->getTodayPostNum($v['channel_id']);

        }

        return $channel_list;
    }

    //获取推荐的频道
    public function recommendChannel(){
        $channel_list = $this->where('isuse = 1 and is_del = 0')->select();
        // dump($channel_list);
        $random_keys = array_rand($channel_list, 4);
        // dump($random_keys);
        $arr[] = $channel_list[$random_keys[0]];
        $arr[] = $channel_list[$random_keys[1]];
        $arr[] = $channel_list[$random_keys[2]];
        $arr[] = $channel_list[$random_keys[3]];
        // dump($arr);die;
        return $arr;
    }


    //获取用户未签到的频道
    public function getNotSignChannel($user_id){
        //用户关注的频道
        $followed_channel_ids = D('Follow')->getFollowedChannelids($user_id);
        // dump($followed_channel_ids);
        //已签到的频道
        $signed_channel_ids = D('Sign')->getSignedChannel($user_id);
        // dump($signed_channel_ids);
        //未签到的频道
        $not_sign_channel = array_diff($followed_channel_ids, $signed_channel_ids);
        // dump($not_sign_channel);die;
        return $not_sign_channel;
    }

    //根据栏目获取频道
    public function getChannelBySort($hidden = 0,$sql = null){

        $sort_list = D('ChannelSort')->getValidChannelSortList();
        foreach ($sort_list as $k => $v) {
            $where = 'true ';
            if($hidden){
                //wangtao 2017.9.19 -strat
                $where .= $sql .' AND channel_type ='.ChannelModel::NORMAL;
                //wangtao 2017.9.19 -end
            }
            $channel_list = array();
            $channel_list = M('Channel')->where($where.' AND isuse = 1 and is_del = 0 and sort_id = '.$v['channel_sort_id'])->order('serial')->select();
            $channel_list = $this->getListData($channel_list);
            $sort_list[$k]['channel_list'] = $channel_list;
            $sort_list[$k]['channel_num'] = count($channel_list);
        }
        return $sort_list;
    }


    //判断用户是否为频道的鉴定师
    public function checkChannelJd($user_id, $channel_id){

        //该频道下鉴定师头衔
        $jd_titles = D('Title')->where('type ='.TitleModel::APPRAISER .' and channel_id ='.$channel_id)->getField('title_id', true);
        if(!$jd_titles) return false;

        //获取用户拥有的头衔
        $user_titles = D('UserTitle')->getUserTitleFields('user_id ='.$user_id, 'title_id');
        if(!$user_titles) return false;
        
        //是否为鉴定师
        $r = array_intersect($jd_titles, $user_titles);

        if(count($r)){
            return true;
        }
        return false;
    }
}
