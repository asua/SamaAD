<?php
/**
 * 用户支付宝模型类
 */

class UserAlipayModel extends Model
{
    private $alipay_id;
    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($alipay_id=0)
    {
        parent::__construct();
        $this->alipay_id = $alipay_id;
    }

    public function getUserAlipayNum($where='') {
      return $this->where($where)->count();
    }

    /**
     * 添加用户支付宝
     * @author 姜伟
     * @param array $arr_class 用户支付宝数组
     * @return boolean 操作结果
     * @todo 添加用户支付宝
     */
    public function addUserAlipay($arr_class)
    {
        if (!is_array($arr_class)) return false;
        return $this->add($arr_class);
    }

    /**
     * 删除用户支付宝
     * @author 姜伟
     * @param string $alipay_id 用户支付宝ID
     * @return boolean 操作结果
     * @todo 删除用户支付宝
     */
    public function delUserAlipay()
    {
        if (!is_numeric($this->alipay_id)) return false;
        return $this->where('alipay_id = ' . $this->alipay_id)->delete();
    }

    /**
     * 更改用户支付宝
     * @author 姜伟
     * @param int $alipay_id 用户支付宝ID
     * @param array $arr_class 用户支付宝数组
     * @return boolean 操作结果
     * @todo 更改用户支付宝
     */
    public function setUserAlipay($alipay_id, $arr_class)
    {
        if (!is_numeric($alipay_id) || !is_array($arr_class)) return false;
        return $this->where('alipay_id = ' . $alipay_id)->save($arr_class);
    }

    /**
     * 获取用户支付宝
     * @author 姜伟
     * @param int $alipay_id 用户支付宝ID
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 用户支付宝
     * @todo 获取用户支付宝
     */
    public function getUserAlipay($alipay_id, $fields = null)
    {
        if (!is_numeric($alipay_id))   return false;
        return $this->field($fields)->where('alipay_id = ' . $alipay_id)->find();
    }

    /**
     * 获取用户支付宝某个字段的信息
     * @author 姜伟
     * @param int $alipay_id 用户支付宝ID
     * @param string $field 查询的字段名
     * @return array 用户支付宝
     * @todo 获取用户支付宝某个字段的信息
     */
    public function getUserAlipayField($alipay_id, $field)
    {
        if (!is_numeric($alipay_id))   return false;
        return $this->where('alipay_id = ' . $alipay_id)->getField($field);
    }

    public function getUserAlipayFields($user_id, $field)
    {
        if (!is_numeric($user_id))   return false;
        return $this->where('is_del = 0 AND user_id = ' . $user_id)->getField($field,true);
    }

    /**
     * 获取所有用户支付宝列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 用户支付宝列表
     * @todo 获取所有用户支付宝列表
     */
    public function getUserAlipayList($where = null)
    {
        return $this->where($where)->order('serial')->Limit()->select();
    }

    public function getUserAlipayAllList($where = null ,$order='')
    {
        return $this->where($where)->order($order)->Limit(10000000)->select();
    }

    /**
     * 获取信息
     * @author 姜伟
     * @param string $where where子句
     * @param string $fields 要获取的字段名
     * @return array 商品基本信息
     * @todo 根据where查询条件查找商品表中的相关数据并返回
     */
    public function getUserAlipayInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }


    public function getListData($alipay_list){


        foreach($alipay_list as $k => $v){



        }
        return $alipay_list;
    }
}
