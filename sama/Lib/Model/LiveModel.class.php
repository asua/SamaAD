<?php
/**
 * 视频模型类
 */

class LiveModel extends Model
{
    private $live_id;
    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($live_id=0)
    {
        parent::__construct();
        $this->live_id = $live_id;
    }

    public function getLiveNum($where='') {
      return $this->where($where)->count();
    }

    /**
     * 添加视频分类
     * @author 姜伟
     * @param array $arr_class 视频分类数组
     * @return boolean 操作结果
     * @todo 添加视频分类
     */
    public function addLive($arr_class)
    {
        if (!is_array($arr_class)) return false;
        return $this->add($arr_class);
    }

    /**
     * 删除视频分类
     * @author 姜伟
     * @param string $live_id 视频分类ID
     * @return boolean 操作结果
     * @todo 删除视频分类
     */
    public function delLive()
    {
        if (!is_numeric($this->live_id)) return false;
        return $this->where('live_id = ' . $this->live_id)->delete();
    }

    /**
     * 更改视频分类
     * @author 姜伟
     * @param int $live_id 视频分类ID
     * @param array $arr_class 视频分类数组
     * @return boolean 操作结果
     * @todo 更改视频分类
     */
    public function setLive($live_id, $arr_class)
    {
        if (!is_numeric($live_id) || !is_array($arr_class)) return false;
        return $this->where('live_id = ' . $live_id)->save($arr_class);
    }

    /**
     * 获取视频分类
     * @author 姜伟
     * @param int $live_id 视频分类ID
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 视频分类
     * @todo 获取视频分类
     */
    public function getLive($live_id, $fields = null)
    {
        if (!is_numeric($live_id))   return false;
        return $this->field($fields)->where('live_id = ' . $live_id)->find();
    }

    /**
     * 获取视频分类某个字段的信息
     * @author 姜伟
     * @param int $live_id 视频分类ID
     * @param string $field 查询的字段名
     * @return array 视频分类
     * @todo 获取视频分类某个字段的信息
     */
    public function getLiveField($live_id, $field)
    {
        if (!is_numeric($live_id))   return false;
        return $this->where('live_id = ' . $live_id)->getField($field);
    }

    /**
     * 获取所有视频分类列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 视频分类列表
     * @todo 获取所有视频分类列表
     */
    public function getLiveList($where = null)
    {
        return $this->where($where)->order('serial')->Limit()->select();
    }
 
    /**
     * 获取分类信息
     * @author 姜伟
     * @param string $where where子句
     * @param string $fields 要获取的字段名
     * @return array 商品基本信息
     * @todo 根据where查询条件查找商品表中的相关数据并返回
     */
    public function getLiveInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }


    public function getListData($live_list){

        $live_class_obj = new LiveClassModel();
        foreach($live_list as $k => $v){
            $live_list[$k]['class_name'] = $live_class_obj->getLiveClassField($v['live_class_id'],'live_class_name');
        }
        return $live_list;
    }
}
