<?php
/**
 * 租金模型类
 */

class RentModel extends Model
{

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getRentNum($where = '') {
        return $this->where($where)->count();
    }




    /**
     * 添加租金
     * @author 姜伟
     * @param array $arr_rent 租金数组
     * @return boolean 操作结果
     * @todo 添加租金
     */
    public function addRent($arr_rent)
    {
        if (!is_array($arr_rent)) return false;
        return $this->add($arr_rent);
    }

    /**
     * 删除租金
     * @author 姜伟
     * @param string $rent_id 租金ID
     * @return boolean 操作结果
     * @todo 删除租金
     */
    public function delRent($rent_id)
    {
        if (!is_numeric($rent_id)) return false;
        return $this->where('rent_id = ' . $rent_id)->delete();
    }

    /**
     * 更改租金
     * @author 姜伟
     * @param int $rent_id 租金ID
     * @param array $arr_rent 租金数组
     * @return boolean 操作结果
     * @todo 更改租金
     */
    public function setRent($rent_id, $arr_rent)
    {
        if (!is_numeric($rent_id) || !is_array($arr_rent)) return false;
        return $this->where('rent_id = ' . $rent_id)->save($arr_rent);
    }

    /**
     * 更改租金状态
     * @author 姜伟
     * @param int $rent_id 租金ID
     * @param array $service_providers_status 租金数组
     * @return boolean 操作结果
     * @todo 更改租金
     */
    public function setRentStatus($rent_id,$service_providers_status)
    {
        if (!is_numeric($rent_id)) return false;
        return $this->where('rent_id = ' . $rent_id)->save($service_providers_status);
    }

    /**
     * 获取租金
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 租金
     * @todo 获取租金
     */
    public function getRentInfo($where, $fields = null)
    {
        return $this->field($fields)->where($where)->find();
    }

    /**
     * 获取租金某个字段的信息
     * @author 姜伟
     * @param int $rent_id 租金ID
     * @param string $field 查询的字段名
     * @return array 租金
     * @todo 获取租金某个字段的信息
     */
    public function getRentField($rent_id, $field)
    {
        if (!is_numeric($rent_id))   return false;
        return $this->where('rent_id = ' . $rent_id)->getField($field);
    }

    /**
     * 获取租金某个字段的信息
     * @author 姜伟
     * @param int $where 条件
     * @param array $fields 查询的字段名
     * @return array 租金
     * @todo 获取租金某个字段的信息
     */
    public function getRentWhereFields($where, $fields)
    {
        return $this->where($where)->getField($fields,true);
    }

    /**
     * 获取所有租金列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 租金列表
     * @todo 获取所有租金列表
     */
    public function getRentList($where = null,$order = null)
    {
        return $this->where($where)->order($order)->limit()->select();
    }


    public function getListData($rent_list){

        foreach ($rent_list as $k => $v){
            $state = '';

            if(($v['payable_money']+$v['overdue_money']) == $v['actual_money']){
                $state='是';
            }elseif(($v['actual_money'] == 0)){
                $state='未交';
            }elseif(($v['payable_money']+$v['overdue_money']) > $v['actual_money']) {
                $state = '逾期';
            }
            $rent_list[$k]['state'] = $state;
        }
        return $rent_list;
    }

    //根据合同id计算已收租金
    public function getSumMoney($where,$field){
        return $this->where($where)->sum($field);
    }

}
