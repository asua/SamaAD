<?php
/**
 * 悬赏答案模型类
 */

class RewardAnswerModel extends Model
{
    private $reward_answer_id;



    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($reward_answer_id=0)
    {
        parent::__construct();
        $this->reward_answer_id = $reward_answer_id;
    }  


    public function getRewardAnswerNumByReward($reward_id){
        return $this->where('reward_id ='.$reward_id)->count();
    }


    public function addRewardAnswer($arr){
        if(!is_array($arr)) return false;
        $arr['addtime'] = time();
        $imgs = $arr['imgs'];
        unset($arr['imgs']);
        $r = $this->add($arr);
        
        if($r){
            if($imgs){
                //添加图片
                $imgs = explode(',', $imgs);
                $img_arr = array();
                foreach ($imgs as $k => $v) {
                    $img_arr[$k]['reward_answer_id'] = $r;
                    $img_arr[$k]['img_url'] = $v;
                }
                D('RewardAnswerImg')->addRewardAnswerImg($img_arr);
            }
            
            // $post_exp = $GLOBALS['config_info']['POST_EXP_NUM'];
            // D('UserChannelRank')->addExp($arr['user_id'], $arr['channel_id'], $post_exp);
            D('Post')->setPost($arr['post_id'],array( 'updatetime' => $arr['addtime']));
        }

        return $r;
    }

    public function getRewardAnswerInfo($where){
        $r = $this->where($where)->find();
        if($r){
            $imgs = D('RewardAnswerImg')->getRewardAnswerImg($r['reward_answer_id']);
            $r['imgs'] = $imgs;
        }

        return $r;
    }

    public function getRewardAnswerList($field = '', $where = '', $order='addtime asc'){
        return $this->field($field)->where($where)->order($order)->limit()->select();
    }

    public function getRewardAnswerAllList($field = '', $where = '', $order='addtime asc'){
        return $this->field($field)->where($where)->order($order)->limit(10000000)->select();
    }

    public function getListData($answer_list){
        foreach ($answer_list as $k => $v) {
            if($v){
                $user = M('Users')->where('user_id ='.$v['user_id'])->find();
                $answer_list[$k]['headimgurl'] = $user['headimgurl'];
                $answer_list[$k]['nickname'] = $user['nickname'];
                $answer_list[$k]['addtime'] = get_time($v['addtime']);
                $answer_list[$k]['acp_time'] = date('Y-m-d H:i:s',$v['addtime']);
                $imgs = D('RewardAnswerImg')->getRewardAnswerImg($v['reward_answer_id']);
                $answer_list[$k]['imgs'] = $imgs;

            }else{
                unset($answer_list[$k]);
            }

        }

        return $answer_list;
    }

    //查询某个字段
    public function getRewardAnswerField($where,$field){
        return $this->where($where)->getField($field);
    }

    public function getRewardAnswerFields($where,$field,$group=''){
        return $this->where($where)->group($group)->getField($field,true);
    }

    public function setRewardAnswer($reward_answer_id, $arr)
    {
        if (!is_numeric($reward_answer_id) || !is_array($arr)) return false;
        return $this->where('reward_answer_id = ' . $reward_answer_id)->save($arr);
    }

    public function delRewardAnswer($reward_answer_id)
    {
        if (!is_numeric($reward_answer_id)) return false;
        return $this->where('reward_answer_id = ' . $reward_answer_id)->delete();
    }

    
}
