<?php
/**
 * 资讯模型类
 */

class InformationArticleModel extends Model
{


    public function InformationArticleModel()
    {
        parent::__construct('article');

    }

    //单条信息
    public function getInformationInfo($where, $fields = '')
    {
        return $this->field($fields)->where($where)->find();
    }
    //获取列表
    public function getInformationList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        return $this->field($fields)->where($where)->group()->order($orderby)->limit()->select();
    }

    //获取数量
    public function getInformationNum($where='')
    {
        $count = $this->where($where)->count();
        return $count;
    }
    //增加文章
    public function addInformation($arr){
        return $this->add($arr);
    }


    //删除信息
    public function delInformation($information_id){
        return $this->where('article_id = '.$information_id)->delete();
    }
    //修改信息
    public function editInformation($arr,$information_id){
        return $this->where('article_id = ' . $information_id)->save($arr);
    }

}
