<?php
/**
 * 头衔模型类
 */

class TitleModel extends Model
{
    private $title_id;

    //头衔类型
    const ORDINARY = 1; //普通头衔
    const SPECIAL = 2;  //特殊头衔
    const APPRAISER = 3; //鉴定师

    //普通头衔标签
    const REPLY = 1; //回帖
    const ADMIRE = 2;  //赞赏
    const SHARE = 3; //分享

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($title_id=0)
    {
        parent::__construct();
        $this->title_id = $title_id;
    }

    public function getTitleNum($where='') {
      return $this->where($where)->count();
    }

    /**
     * 添加头衔
     * @author 姜伟
     * @param array $arr_class 头衔数组
     * @return boolean 操作结果
     * @todo 添加头衔
     */
    public function addTitle($arr_class)
    {
        if (!is_array($arr_class)) return false;
        return $this->add($arr_class);
    }

    /**
     * 删除头衔
     * @author 姜伟
     * @param string $title_id 头衔ID
     * @return boolean 操作结果
     * @todo 删除头衔
     */
    public function delTitle()
    {
        if (!is_numeric($this->title_id)) return false;
        return $this->where('title_id = ' . $this->title_id)->delete();
    }

    /**
     * 更改头衔
     * @author 姜伟
     * @param int $title_id 头衔ID
     * @param array $arr_class 头衔数组
     * @return boolean 操作结果
     * @todo 更改头衔
     */
    public function setTitle($title_id, $arr_class)
    {
        if (!is_numeric($title_id) || !is_array($arr_class)) return false;
        return $this->where('title_id = ' . $title_id)->save($arr_class);
    }

    /**
     * 获取头衔
     * @author 姜伟
     * @param int $title_id 头衔ID
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 头衔
     * @todo 获取头衔
     */
    public function getTitle($title_id, $fields = null)
    {
        if (!is_numeric($title_id))   return false;
        return $this->field($fields)->where('title_id = ' . $title_id)->find();
    }

    /**
     * 获取头衔某个字段的信息
     * @author 姜伟
     * @param int $title_id 头衔ID
     * @param string $field 查询的字段名
     * @return array 头衔
     * @todo 获取头衔某个字段的信息
     */
    public function getTitleField($title_id, $field)
    {
        if (!is_numeric($title_id))   return false;
        return $this->where('title_id = ' . $title_id)->getField($field);
    }

    /**
     * 获取所有头衔列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 头衔列表
     * @todo 获取所有头衔列表
     */
    public function getTitleList($where = null,$order = null)
    {
        return $this->where($where)->order($order)->limit()->select();
    }
 
    /**
     * 获取分类信息
     * @author 姜伟
     * @param string $where where子句
     * @param string $fields 要获取的字段名
     * @return array 商品基本信息
     * @todo 根据where查询条件查找商品表中的相关数据并返回
     */
    public function getTitleInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    //头衔的类型
    public function getAddTitleType()
    {
        return array(
//            self::ORDINARY => '普通头衔',
            self::SPECIAL => '特殊头衔',
            self::APPRAISER => '鉴定师',
        );
    }

    //头衔的类型
    public function getEditTitleType()
    {
        return array(
            self::ORDINARY => '普通头衔',
            self::SPECIAL => '特殊头衔',
            self::APPRAISER => '鉴定师',
        );
    }

    //普通头衔的标签
    public function getOrdinaryTitleTag()
    {
        return array(
            self::REPLY => '回帖',
            self::ADMIRE => '赞赏',
            self::SHARE => '分享',
        );
    }


    public function getListData($title_list){

        foreach($title_list as $k => $v){
            $tag_arr = self::getOrdinaryTitleTag();
            $title_list[$k]['tag_name'] = $tag_arr[$v['tag_id']];

            if($v['type'] == self::APPRAISER){
                $title_list[$k]['channel_name'] = M('Channel')->where('channel_id ='.$v['channel_id'])->getField('channel_name');
            }
        }
        return $title_list;
    }

    /**
     * @param int $type 头衔类型
     * @author ccy
     * @param null $where
     * @param null $order
     * @param null $field
     * @return 获取所有普通,特殊 ,鉴定师 头衔列表
     */
    public function getTotalTypeTitleList($type,$where = null,$order = null,$field = null)
    {
        return $this->field($field)->where('type ='.$type.' AND isuse = 1 '.$where)->order($order)->limit(1000000)->select();
    }

    //查询三种普通头衔．
    public function getOrdinaryTitleFields($tag_id,$field){

        return $this->where('type ='.self::ORDINARY.' AND isuse = 1 AND tag_id ='.$tag_id)->getField($field,true);
    }


    public function getTitleIdsByType($type){
        return $this->where('type ='.$type .' and isuse = 1')->getField('title_id', true);
    }


    //判断头衔级别．
    public function getJudgeTitleLevel($title_id){

        $title_info = $this->getTitleInfo('title_id ='.$title_id);
        $where = 'type ='.TitleModel::ORDINARY.' AND isuse = 1 ';
        //上级．
        $superior_info =  $this->getTitleInfo($where.' AND tag_id ='.$title_info['tag_id'].' and rank_up_num >'.$title_info['rank_up_num']);
        //下级．
        $lower_info =  $this->getTitleInfo($where.' AND tag_id ='.$title_info['tag_id'].' and rank_up_num <'.$title_info['rank_up_num']);

        if($superior_info && $lower_info){
            $title_info['level'] = 2;
            $title_info['info'] = $superior_info;
        }elseif($superior_info){
            $title_info['level'] = 1;
            $title_info['info'] = $superior_info;
        }elseif($lower_info){
            $title_info['level'] = 3;
        }

        //我已经有的所有头衔．
        $user_title_obj = new UserTitleModel();
        $my_titles = $user_title_obj->getUserTitleFields('user_id ='.intval(session('user_id')),'title_id');
        //判断该头衔是否在我已有的头衔里面
        $title_info['is_have'] = in_array($title_id,$my_titles) ? true : false;

        return $title_info;
    }

}
