<?php
/**
 * 心愿回帖图片模型类
 */

class CfReplyImgModel extends Model
{
    private $cf_reply_img_id;

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($cf_reply_img_id=0)
    {
        parent::__construct();
        $this->cf_reply_img_id = $cf_reply_img_id;
    }


    public function addCfReplyImg($img_arr){
        return $this->addAll($img_arr);
    }

    public function getCfReplyImg($cf_reply_id){
        return $this->where('cf_reply_id ='.$cf_reply_id)->getField('img_url', true);
    }

    public function getCfReplyImgList($cf_reply_id){
        return $this->where('cf_reply_id ='.$cf_reply_id)->select();
    }


    //删除照片．
    public function delImg($cf_reply_img_id){
        return $this->where('cf_reply_img_id ='.$cf_reply_img_id)->delete();
    }


}
