<?php
/**
 * 工位类型模型类
 */

class StationClassModel extends Model
{
    // 工位类型id
    public $station_class_id;
    
    /**
     * 构造函数
     * @author 姜伟
     * @param $station_class_id 工位类型ID
     * @return void
     * @todo 初始化工位类型id
     */
    public function StationClassModel($station_class_id)
    {
        parent::__construct('station_class');

        if ($station_class_id = intval($station_class_id))
		{
            $this->station_class_id = $station_class_id;
		}
    }

    /**
     * 获取工位类型信息
     * @author 姜伟
     * @param int $station_class_id 工位类型id
     * @param string $fields 要获取的字段名
     * @return array 工位类型基本信息
     * @todo 根据where查询条件查找工位类型表中的相关数据并返回
     */
    public function getStationClassInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改工位类型信息
     * @author 姜伟
     * @param array $arr 工位类型信息数组
     * @return boolean 操作结果
     * @todo 修改工位类型信息
     */
    public function editStationClass($where, $arr)
    {
        return $this->where($where)->save($arr);
    }

    /**
     * 添加工位类型
     * @author 姜伟
     * @param array $arr 工位类型信息数组
     * @return boolean 操作结果
     * @todo 添加工位类型
     */
    public function addStationClass($arr)
    {
        if (!is_array($arr)) return false;

		// $arr['addtime'] = time();

        return $this->add($arr);
    }

    /**
     * 删除工位类型
     * @author 姜伟
     * @param int $station_class_id 工位类型ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delStationClass($where)
    {
        if (!$where) return false;
        $station_class = $this->where($where)->find();
        if(self::TOPIC == $station_class['station_class_type']){
            $model = 'Topic';
            $obj = D($model);
            $obj->where('topic_id ='.$station_class['type_id'])->setDec('station_class_num');
        }elseif($station_class['station_class_type'] == self::PROJECT){
            $model = 'Project';
            $obj = D($model);
            $obj->where('project_id ='.$station_class['type_id'])->setDec('station_class_num');
        }
        return $this->where($where)->delete();
    }

    /**
     * 根据where子句获取工位类型数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的工位类型数量
     * @todo 根据where子句获取工位类型数量
     */
    public function getStationClassNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询工位类型信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 工位类型基本信息
     * @todo 根据SQL查询字句查询工位类型信息
     */
    public function getStationClassList($fields = '', $where = '', $orderby = 'serial asc', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 获取工位类型列表页数据信息列表
     * @author 姜伟
     * @param array $station_class_list
     * @return array $station_class_list
     * @todo 根据传入的$station_class_list获取更详细的工位类型列表页数据信息列表
     */
    public function getListData($station_class_list)
    {
		foreach ($station_class_list AS $k => $v)
		{
		}

		return $station_class_list;
    }

    public function getTypeField($where = '',$field){
        return $this->where($where)->getField($field, true);
    }


    
}
