<?php
/**
 * 项目申报模型类
 */

class ProjectReportModel extends Model
{

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getProjectReportNum($where = '') {
        return $this->where($where)->count();
    }




    /**
     * 添加项目申报
     * @author 姜伟
     * @param array $arr_project_report 项目申报数组
     * @return boolean 操作结果
     * @todo 添加项目申报
     */
    public function addProjectReport($arr_project_report)
    {
        if (!is_array($arr_project_report)) return false;
        return $this->add($arr_project_report);
    }

    /**
     * 删除项目申报
     * @author 姜伟
     * @param string $project_report_id 项目申报ID
     * @return boolean 操作结果
     * @todo 删除项目申报
     */
    public function delProjectReport($project_report_id)
    {
        if (!is_numeric($project_report_id)) return false;
        return $this->where('project_report_id = ' . $project_report_id)->delete();
    }

//    /**
//     * 删除一级分类下的所有项目申报
//     * @author 姜伟
//     * @param string $class_id 一级分类ID
//     * @return boolean 操作结果
//     * @todo 删除项目申报
//     */
//    public function delClassProjectReport($class_id)
//    {
//        if (!is_numeric($class_id)) return false;
//        return $this->where('class_id = ' . $class_id)->delete();
//    }

    /**
     * 更改项目申报
     * @author 姜伟
     * @param int $project_report_id 项目申报ID
     * @param array $arr_project_report 项目申报数组
     * @return boolean 操作结果
     * @todo 更改项目申报
     */
    public function setProjectReport($project_report_id, $arr_project_report)
    {
        if (!is_numeric($project_report_id) || !is_array($arr_project_report)) return false;
        return $this->where('project_report_id = ' . $project_report_id)->save($arr_project_report);
    }

    /**
     * 更改项目申报状态
     * @author 姜伟
     * @param int $project_report_id 项目申报ID
     * @param array $service_providers_status 项目申报数组
     * @return boolean 操作结果
     * @todo 更改项目申报
     */
    public function setProjectReportStatus($project_report_id,$service_providers_status)
    {
        if (!is_numeric($project_report_id)) return false;
        return $this->where('project_report_id = ' . $project_report_id)->save($service_providers_status);
    }

    /**
     * 获取项目申报
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 项目申报
     * @todo 获取项目申报
     */
    public function getProjectReportInfo($where, $fields = null)
    {
        return $this->field($fields)->where($where)->find();
    }

    /**
     * 获取项目申报的孵化器ID
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 项目申报
     * @todo 获取项目申报
     */
    public function getGroupIdFields($fields,$where=null,$group=null){
        return $this->where($where)->group($group)->getField($fields, true);
    }

    /**
     * 获取项目申报某个字段的信息
     * @author 姜伟
     * @param int $project_report_id 项目申报ID
     * @param string $field 查询的字段名
     * @return array 项目申报
     * @todo 获取项目申报某个字段的信息
     */
    public function getProjectReportField($project_report_id, $field)
    {
        if (!is_numeric($project_report_id))   return false;
        return $this->where('project_report_id = ' . $project_report_id)->getField($field);
    }

    /**
     * 获取所有项目申报列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 项目申报列表
     * @todo 获取所有项目申报列表
     */
    public function getProjectReportList($where = null,$order = null,$group =null)
    {
        return $this->where($where)->order($order)->group($group)->limit()->select();
    }


    //数字转文字
    public function numDigitalText($status){

        return $this->arr[$status];
    }



    public function getListData($project_report_list){


//        $service_type_obj = new ServiceTypeModel();
        $province_obj = new AddressProvinceModel();
        $city_obj = new AddressCityModel();
        $project_report_type_obj = new TypeModel();
        foreach ($project_report_list as $k => $v){

            //区域
            $province_name =$province_obj->getProvinceName($v['province_id']);
            $city_name =$city_obj->getCityName($v['city_id']);
            $area_name =M('AddressArea')->where('area_id ='.$v['area_id'])->getField('area_name');
            $area_string = $province_name.'-'.$city_name.'-'.$area_name;
            $project_report_list[$k]['area_string'] = $area_string;

            //类型
            $type_info=$project_report_type_obj->getTypeInfo('type_id ='.$v['project_report_type_id']);
            $project_report_list[$k]['project_report_type']=$type_info['type_name'];

        }

        return $project_report_list;
    }


}
