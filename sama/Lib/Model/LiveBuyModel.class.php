<?php
/**
 * 视频购买模型类
 */

class LiveBuyModel extends Model
{
    private $live_buy_id;



    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($live_buy_id=0)
    {
        parent::__construct();
        $this->live_buy_id = $live_buy_id;
    }  



    //获取视频购买人数
    public function getLiveBuyUserNum($cf_id){
        return $this->where('live_id ='.$cf_id)->group('user_id')->count();
    }

    public function getLiveBuyNum($where = ''){
        return $this->where($where)->count();
    }

    //获取所有该视频购买过的用户．
    public function getLiveBuyAllList($cf_id){
        return $this->where('crowdfunding_id ='.$cf_id)->limit(1000000)->select();
    }

    public function addLiveBuy($arr){
        if(!is_array($arr)) return false;
        $arr['addtime'] = time();
        $r = $this->add($arr);

        return $r;
    }

    public function getLiveBuyList($field =null,$where, $order='addtime desc'){

        return $this->field($field)->where($where)->order($order)->limit()->select();
    }

    public function getListData($support_list){

        foreach ($support_list as $k => $v) {
            $user_obj = new UserModel();
            $support_list[$k]['nick_name'] = $user_obj ->getUserField('nickname','user_id ='.$v['user_id']);

            $support_list[$k]['add_time'] =date('Y-m-d H:i:s',$v['addtime']);
        }
        return $support_list;
    }


}
