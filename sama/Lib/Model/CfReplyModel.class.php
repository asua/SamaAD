<?php
/**
 * 心愿回帖模型类
 */

class CfReplyModel extends Model
{
    private $cf_reply_id;

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($cf_reply_id=0)
    {
        parent::__construct();
        $this->cf_reply_id = $cf_reply_id;
    }



    public function addCfReply($arr){
        if(!is_array($arr)) return false;
        $arr['addtime'] = time();

        $imgs = $arr['imgs'];
        unset($arr['imgs']);
        $r = $this->add($arr);
        
        if($r){
            if($imgs){
                //添加图片
                $imgs = explode(',', $imgs);
                $img_arr = array();
                foreach ($imgs as $k => $v) {
                    $img_arr[$k]['cf_reply_id'] = $r;
                    $img_arr[$k]['img_url'] = $v;
                }
                D('CfReplyImg')->addCfReplyImg($img_arr);
            }
            
            // $post_exp = $GLOBALS['config_info']['POST_EXP_NUM'];
            // D('UserChannelRank')->addExp($arr['user_id'], $arr['channel_id'], $post_exp);
            D('Post')->setPost($arr['post_id'],array( 'updatetime' => $arr['addtime']));
        }

        return $r;
    }


    public function getCfReplyInfo($where){
        $r = $this->where($where)->find();
        $imgs = D('CfReplyImg')->getCfReplyImg($r['cf_reply_id']);
        $r['imgs'] = $imgs;
        return $r;
    }

    public function setCfReply($cf_reply_id, $arr)
    {
        if (!is_numeric($cf_reply_id) || !is_array($arr)) return false;
        return $this->where('cf_reply_id = ' . $cf_reply_id)->save($arr);
    }
}
