<?php
/**
 * 支付宝退款日志模型类
 */

class AlipayRefundLogModel extends Model
{
    // 支付宝退款日志id
    public $alipay_refund_log_id;
   
    /**
     * 构造函数
     * @author 姜伟
     * @param $alipay_refund_log_id 支付宝退款日志ID
     * @return void
     * @todo 初始化支付宝退款日志id
     */
    public function AlipayRefundLogModel($alipay_refund_log_id)
    {
        parent::__construct('alipay_refund_log');

        if ($alipay_refund_log_id = intval($alipay_refund_log_id))
		{
            $this->alipay_refund_log_id = $alipay_refund_log_id;
		}
    }

    /**
     * 获取支付宝退款日志信息
     * @author 姜伟
     * @param int $alipay_refund_log_id 支付宝退款日志id
     * @param string $fields 要获取的字段名
     * @return array 支付宝退款日志基本信息
     * @todo 根据where查询条件查找支付宝退款日志表中的相关数据并返回
     */
    public function getAlipayRefundLogInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改支付宝退款日志信息
     * @author 姜伟
     * @param array $arr 支付宝退款日志信息数组
     * @return boolean 操作结果
     * @todo 修改支付宝退款日志信息
     */
    public function editAlipayRefundLog($arr)
    {
        return $this->where('alipay_refund_log_id = ' . $this->alipay_refund_log_id)->save($arr);
    }

    /**
     * 添加支付宝退款日志
     * @author 姜伟
     * @param array $arr 支付宝退款日志信息数组
     * @return boolean 操作结果
     * @todo 添加支付宝退款日志
     */
    public function addAlipayRefundLog($arr)
    {
        if (!is_array($arr)) return false;

		$arr['addtime'] = time();

        return $this->add($arr);
    }

    /**
     * 删除支付宝退款日志
     * @author 姜伟
     * @param int $alipay_refund_log_id 支付宝退款日志ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delAlipayRefundLog($alipay_refund_log_id)
    {
        if (!is_numeric($alipay_refund_log_id)) return false;
        return $this->where('alipay_refund_log_id = ' . $alipay_refund_log_id)->save(array('isuse' => 2));
    }

    /**
     * 根据where子句获取支付宝退款日志数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的支付宝退款日志数量
     * @todo 根据where子句获取支付宝退款日志数量
     */
    public function getAlipayRefundLogNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询支付宝退款日志信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 支付宝退款日志基本信息
     * @todo 根据SQL查询字句查询支付宝退款日志信息
     */
    public function getAlipayRefundLogList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 获取支付宝退款日志列表页数据信息列表
     * @author 姜伟
     * @param array $alipay_refund_log_list
     * @return array $alipay_refund_log_list
     * @todo 根据传入的$alipay_refund_log_list获取更详细的支付宝退款日志列表页数据信息列表
     */
    public function getListData($alipay_refund_log_list)
    {

		foreach ($alipay_refund_log_list AS $k => $v)
		{
		}

		return $alipay_refund_log_list;
    }

}
