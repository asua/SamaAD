<?php
/**
 * 收藏模型类
 */

class ExpressWillModel extends Model
{
    // 收藏id
    public $express_will_id;
   
    /**
     * 构造函数
     * @author 姜伟
     * @param $express_will_id 收藏ID
     * @return void
     * @todo 初始化收藏id
     */
    public function ExpressWillModel($express_will_id)
    {
        parent::__construct('express_will');

        if ($express_will_id = intval($express_will_id))
		{
            $this->express_will_id = $express_will_id;
		}
    }

    /**
     * 获取收藏信息
     * @author 姜伟
     * @param int $express_will_id 收藏id
     * @param string $fields 要获取的字段名
     * @return array 收藏基本信息
     * @todo 根据where查询条件查找收藏表中的相关数据并返回
     */
    public function getExpressWillInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改收藏信息
     * @author 姜伟
     * @param array $arr 收藏信息数组
     * @return boolean 操作结果
     * @todo 修改收藏信息
     */
    public function editExpressWill($arr)
    {
        return $this->where('express_will_id = ' . $this->express_will_id)->save($arr);
    }

    /**
     * 添加收藏
     * @author 姜伟
     * @param array $arr 收藏信息数组
     * @return boolean 操作结果
     * @todo 添加收藏
     */
    public function addExpressWill($arr)
    {
        if (!is_array($arr)) return false;

		$arr['addtime'] = time();

        return $this->add($arr);
    }

    /**
     * 删除收藏
     * @author 姜伟
     * @param int $express_will_id 收藏ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delExpressWill($express_will_id)
    {
        if (!is_numeric($express_will_id)) return false;
        return $this->where('express_will_id = ' . $express_will_id)->save(array('isuse' => 2));
    }

    /**
     * 根据where子句获取收藏数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的收藏数量
     * @todo 根据where子句获取收藏数量
     */
    public function getExpressWillNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询收藏信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 收藏基本信息
     * @todo 根据SQL查询字句查询收藏信息
     */
    public function getExpressWillList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 获取收藏列表页数据信息列表
     * @author 姜伟
     * @param array $express_will_list
     * @return array $express_will_list
     * @todo 根据传入的$express_will_list获取更详细的收藏列表页数据信息列表
     */
    public function getListData($express_will_list)
    {
        require_cache('Common/func_item.php');

		foreach ($express_will_list AS $k => $v)
		{
			//产品名称
			$item_obj = new ItemModel($v['item_id']);
			$item_info = $item_obj->getItemInfo('item_id = ' . $v['item_id'], 'item_name, isuse, stock, mall_price, base_pic, is_gift');
			$express_will_list[$k]['item_name']  = $item_info['item_name'];
            $express_will_list[$k]['mall_price'] = $item_info['mall_price'];
			$express_will_list[$k]['is_gift']    = $item_info['is_gift'];
			$express_will_list[$k]['small_pic']  = small_img($item_info['base_pic']);

			$status = '';
			if ($item_info['isuse'] == 0)
			{
				$status = '已下架';
			}
			elseif ($item_info['isuse'] == 1)
			{
				$status = $item_info['stock'] ? '上架中' : '缺货';
			}
			$express_will_list[$k]['status'] = $status;
		}

		return $express_will_list;
    }
}
