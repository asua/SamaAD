<?php
/**
 * 投资人认证模型类
 */

class AuthInvestmentModel extends Model
{
    // 投资人认证id
    public $auth_investment_id;
   
    /**
     * 构造函数
     * @author 姜伟
     * @param $auth_investment_id 投资人认证ID
     * @return void
     * @todo 初始化投资人认证id
     */
    public function AuthInvestmentModel($auth_investment_id)
    {
        parent::__construct('auth_investment');

        if ($auth_investment_id = intval($auth_investment_id))
		{
            $this->auth_investment_id = $auth_investment_id;
		}
    }

    /**
     * 获取投资人认证信息
     * @author 姜伟
     * @param int $auth_investment_id 投资人认证id
     * @param string $fields 要获取的字段名
     * @return array 投资人认证基本信息
     * @todo 根据where查询条件查找投资人认证表中的相关数据并返回
     */
    public function getAuthInvestmentInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改投资人认证信息
     * @author 姜伟
     * @param array $arr 投资人认证信息数组
     * @return boolean 操作结果
     * @todo 修改投资人认证信息
     */
    public function editAuthInvestment($user_id, $arr)
    {
        if($arr['status'] == 1){
            M('Users')->where('user_id ='.$user_id)->save(array('is_investment'=> 1));
        }
        return $this->where('user_id ='.$user_id)->save($arr);
    }

    /**
     * 添加投资人认证
     * @author 姜伟
     * @param array $arr 投资人认证信息数组
     * @return boolean 操作结果
     * @todo 添加投资人认证
     */
    public function addAuthInvestment($arr)
    {
        if (!is_array($arr)) return false;

		$arr['addtime'] = time();

        return $this->add($arr);
    }

    /**
     * 删除投资人认证
     * @author 姜伟
     * @param int $auth_investment_id 投资人认证ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delAuthInvestment($auth_investment_id)
    {
        if (!is_numeric($auth_investment_id)) return false;
        return $this->where('auth_investment_id = ' . $auth_investment_id)->save(array('isuse' => 2));
    }

    /**
     * 根据where子句获取投资人认证数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的投资人认证数量
     * @todo 根据where子句获取投资人认证数量
     */
    public function getAuthInvestmentNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询投资人认证信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 投资人认证基本信息
     * @todo 根据SQL查询字句查询投资人认证信息
     */
    public function getAuthInvestmentList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 获取投资人认证列表页数据信息列表
     * @author 姜伟
     * @param array $auth_investment_list
     * @return array $auth_investment_list
     * @todo 根据传入的$auth_investment_list获取更详细的投资人认证列表页数据信息列表
     */
    public function getListData($auth_investment_list)
    {
		foreach ($auth_investment_list AS $k => $v)
		{
		}

		return $auth_investment_list;
    }

    public function getAuthInvestmentField($field, $where = ''){
        return $this->where($where)->getField($field, true);
    }
}
