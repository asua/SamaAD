<?php
/**
 * 帖子评论模型类
 */

class PostCommentModel extends Model
{
    private $post_comment_id;

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($post_comment_id=0)
    {
        parent::__construct();
        $this->post_comment_id = $post_comment_id;
    }


    public function getPostCommentNum($where = ''){
        return $this->where($where)->count();
    }

    public function getPostCommentList($field = '', $where = '', $order='floor asc'){
        return $this->field($field)->where($where)->order($order)->limit()->select();
    }


    public function getListData($comment_list){
        foreach ($comment_list as $k => $v) {
            //用户信息
            $user = M('Users')->field('nickname, headimgurl')->where('user_id ='.$v['user_id'])->select();
            $user = $user[0];
            // echo M('Users')->getLastSql();dump($user);die;
            $comment_list[$k]['comment_user'] = $user;
            $comment_list[$k]['title_list'] = D('UserTitle')->getUserAdornTitle($v['user_id']);
            
            $p_comment_obj = D('PostComment');
            $p_comment_obj->setLimit(10000);
            $reply_comment_list = $p_comment_obj->getPostCommentList('', 'post_id = '.$v['post_id'].' and reply_comment_id ='.$v['post_comment_id']);
            foreach ($reply_comment_list as $key => $value) {
                $reply_user = M('Users')->where('user_id ='.$value['user_id'])->getField('nickname');
                $reply_comment_list[$key]['nickname'] = $reply_user;
            }
            $comment_list[$k]['reply_comment_list'] = $reply_comment_list;

            $comment_list[$k]['addtime'] = get_time($v['addtime']);
            $comment_list[$k]['acp_time'] = date('Y-m-d H:i:s',$v['addtime']);

            $comment_list[$k]['imgs'] = D('CommentImg')->getCommentImg($v['post_comment_id']);
            //隐藏内容
            // $comment_list[$k]['comment_hide'] = D('PraiseHide')->getCommentHide($v['post_comment_id']);

            //是否是隐藏内容
            $praise_hide_obj = new PraiseHideModel();
            $comment_list[$k]['is_hide'] = $praise_hide_obj->getPraiseHideNum('hide_type ='.PraiseHideModel::COMMENT . ' and id ='.$v['post_comment_id']);

//            echo $_SERVER['REQUEST_URI'];die;

        }
        return $comment_list;
    }


    //增加
    public function fieldInc($post_comment_id,$field){
        return $this->where('post_comment_id ='.$post_comment_id)->setInc($field);
    }

    //减少
    public function fieldDec($post_comment_id,$field){
        return $this->where('post_comment_id ='.$post_comment_id)->setDec($field);
    }


    //判断是否点赞
    public function checkPraise($comment_list){
        $user_id = intval(session('user_id'));
        foreach ($comment_list as $k => $v) {
            $praise_num = D('Praise')->getPraiseNum('user_id ='.$user_id .' and praise_type ='.PraiseModel::COMMENT .' and id ='.$v['post_comment_id']);
            $comment_list[$k]['is_praise'] = $praise_num ? 1 : 0;
        }
        return $comment_list;
    }

    //获取评论中的隐藏内容，并判断是否有查看权限
    public function checkCommentHide($user_id, $comment_list){
        foreach ($comment_list as $k => $v) {
            // $hide = array();
            $hide = D('PraiseHide')->getCommentHide($v['post_comment_id']);
            if($hide){
                $see_hide = D('PraiseHide')->checkSeeHidePriv($user_id, $hide['praise_hide_id']);
                if($see_hide){
                    $comment_list[$k]['is_show'] = 1;//有权限
                    $comment_list[$k]['comment_hide'] = $hide;
                }else{
                    $comment_list[$k]['is_show'] = 0;//没有权限
                    $comment_hide['img_num'] = count($hide['hide_imgs']);
                    $comment_hide['str_len'] = $hide['str_len'];
                    $comment_hide['price'] = $hide['price'];
                    $comment_hide['praise_hide_id'] = $hide['praise_hide_id'];
                    $comment_hide['buy_num'] = D('PraiseHideBuy')->getPraiseHideBuyNum('praise_hide_id ='.$hide['praise_hide_id']);
                    $comment_list[$k]['comment_hide'] = $comment_hide;
                }
                $comment_list[$k]['has_hide'] = 1;
            }
            
        }

        return $comment_list;
    }


    //获取帖子最高楼层
    public function getMaxFloor($post_id){
        $c = $this->where('post_id ='.$post_id)->order('floor desc')->find();

        $floor = $c['floor'];
        return intval($floor);
    }


    public function addPostComment($arr){
        if(!is_array($arr)) return false;
        $arr['addtime'] = time();
        $updatetime = $arr['addtime'];
        log_file('Emoji_111='.$arr['content'], 'Emoji');
        // $arr['content'] = removeEmoji($arr['content']);
        log_file('Emoji_222='.$arr['content'], 'Emoji');
        $imgs = $arr['imgs'];
        unset($arr['imgs']);
        $at_arr = array();
        //是否有@
            //$content = '棒棒@续一秒+1s @超宇 蛤蛤蛤';
            preg_match_all('/@(\S)+/',$arr['content'],$at_users);
            foreach ($at_users[0] as $k => $v) {
                $at_user = '';
                $nickname = mb_substr($v,1);
                $user = M('Users')->field('user_id')->where('nickname ="'.$nickname.'"')->find();
                if($user){
                    $at_arr[] = $user['user_id'];
                    // $at_user = '<a class="at native_go" href="/FrontUserCenter/others_space/user_id/'.$user['user_id'].'">'.$v.'</a>';
                    
                    // $arr['content'] = str_replace($v, $at_user, $arr['content']);
                }
            }
            // echo $content;
            // dump($user);
            // dump($arr);die;



        $r = $this->add($arr);
        //发帖成功加经验
        if($r){
            if($imgs && !$arr['reply_user_id']){
                //添加图片
                $imgs = explode(',', $imgs);
                $img_arr = array();
                foreach ($imgs as $k => $v) {
                    $img_arr[$k]['post_comment_id'] = $r;
                    $img_arr[$k]['img_url'] = $v;
                }
                D('CommentImg')->addCommentImg($img_arr);
            }
            
            M('Users')->where('user_id ='.$arr['user_id'])->setInc('reply_num');

            //推消息
            $post_obj = new PostModel();
            $post = $post_obj->getPostInfo('post_id ='.$arr['post_id']);
            //帖子评论数加1
            $post_obj->where('post_id ='.$arr['post_id'])->setInc('comment_num');
            $comment_user = M('Users')->where('user_id ='.$arr['user_id'])->getField('nickname');
            $post_type = $post_obj->postTypeName($post['post_type']);
            $link = '/FrontChannel/post_detail/post_id/'.$arr['post_id'];
            if($post['user_id'] != $arr['user_id']){
                $title = '在'.$post_type[$post['post_type']].'帖中回复了你';
                
                if($arr['reply_user_id']){
                    //回复评论
                    D('Message')->addMessage($r, MessageModel::REPLY, $arr['user_id'],  $arr['reply_user_id'], $comment_user.$title, $title, $link);
                }else{
                    //回复帖子
                    D('Message')->addMessage($r, MessageModel::REPLY, $arr['user_id'],  $post['user_id'], $comment_user.$title, $title, $link);
                }

                
            }
            
            //是否有@
            if($at_arr){
                $at_title = '在'.$post_type[$post['post_type']].'帖中@了你';
                D('Message')->addMessage($r, MessageModel::AT, $arr['user_id'],  $at_arr, $comment_user.$at_title, $at_title, $link);
            }



            // $post_exp = $GLOBALS['config_info']['POST_EXP_NUM'];
            // D('UserChannelRank')->addExp($arr['user_id'], $arr['channel_id'], $post_exp);
            D('Post')->setPost($arr['post_id'],array( 'updatetime' => $updatetime));
        }

        return $r;
    }


    public function getPostCommentInfo($where){
        return $this->where($where)->find();
    }

    public function delPostComment($comment_id){
        if(!$comment_id) return false;
        $reply_comment_ids = self::getPostCommentFields('reply_comment_id ='.$comment_id,'post_comment_id');
        #dump($reply_comment_ids);
        //如果有回复则先删除回复
        if($reply_comment_ids){
            #dump(11111);die;
            foreach($reply_comment_ids as $v){
                $this->where('post_comment_id ='.$v)->delete();
            }
        }
        return $this->where('post_comment_id ='.$comment_id)->delete();
    }


    //查询某个字段
    public function getPostCommentField($where,$field){
        return $this->where($where)->getField($field);
    }

    public function getPostCommentFields($where,$field,$group=''){
        return $this->where($where)->group($group)->getField($field,true);
    }

    public function setPostComment($post_comment_id, $arr)
    {
        if (!is_numeric($post_comment_id) || !is_array($arr)) return false;
        return $this->where('post_comment_id = ' . $post_comment_id)->save($arr);
    }
}
