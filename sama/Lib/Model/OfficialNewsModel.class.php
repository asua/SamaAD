<?php
/**
 * 官方模型类
 */

class OfficialNewsModel extends Model
{
    private $article_id;
    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($article_id=0)
    {
        parent::__construct('article');
        $this->article_id = $article_id;
    }

    public function getOfficialNewsNum($where='') {
      return $this->where($where)->count();
    }

    /**
     * 添加官方资讯
     * @author 姜伟
     * @param array $arr_class 官方资讯数组
     * @return boolean 操作结果
     * @todo 添加官方资讯
     */
    public function addOfficialNews($arr_class)
    {
        if (!is_array($arr_class)) return false;
        return $this->add($arr_class);
    }

    /**
     * 删除官方资讯
     * @author 姜伟
     * @param string $article_id 官方资讯ID
     * @return boolean 操作结果
     * @todo 删除官方资讯
     */
    public function delOfficialNews()
    {
        if (!is_numeric($this->article_id)) return false;
        return $this->where('article_id = ' . $this->article_id)->delete();
    }

    /**
     * 更改官方资讯
     * @author 姜伟
     * @param int $article_id 官方资讯ID
     * @param array $arr_class 官方资讯数组
     * @return boolean 操作结果
     * @todo 更改官方资讯
     */
    public function setOfficialNews($article_id, $arr_class)
    {
        if (!is_numeric($article_id) || !is_array($arr_class)) return false;
        return $this->where('article_id = ' . $article_id)->save($arr_class);
    }

    /**
     * 获取官方资讯
     * @author 姜伟
     * @param int $article_id 官方资讯ID
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 官方资讯
     * @todo 获取官方资讯
     */
    public function getOfficialNews($article_id, $fields = null)
    {
        if (!is_numeric($article_id))   return false;
        return $this->field($fields)->where('article_id = ' . $article_id)->find();
    }

    /**
     * 获取官方资讯某个字段的信息
     * @author 姜伟
     * @param int $article_id 官方资讯ID
     * @param string $field 查询的字段名
     * @return array 官方资讯
     * @todo 获取官方资讯某个字段的信息
     */
    public function getOfficialNewsField($article_id, $field)
    {
        if (!is_numeric($article_id))   return false;
        return $this->where('article_id = ' . $article_id)->getField($field);
    }

    /**
     * 获取所有官方资讯列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 官方资讯列表
     * @todo 获取所有官方资讯列表
     */
    public function getOfficialNewsList($where = null)
    {
        return $this->where($where)->order('serial asc, addtime desc')->Limit()->select();
    }
 
    /**
     * 获取分类信息
     * @author 姜伟
     * @param string $where where子句
     * @param string $fields 要获取的字段名
     * @return array 商品基本信息
     * @todo 根据where查询条件查找商品表中的相关数据并返回
     */
    public function getOfficialNewsInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }


    public function getListData($list){

        foreach($list as $k => $v){
            $txt_obj= new ArticleTxtModel();
            $list[$k]['contents'] = $txt_obj->where('article_id ='.$v['article_id'])->getField('contents');
            $list[$k]['add_time'] = get_time($v['addtime']);
            $list[$k]['acp_time'] = date('Y-m-d H:i:s',$v['addtime']);
        }
        return $list;
    }
}
