<?php
/**
 * 视频类模型类
 */

class LiveClassModel extends Model
{
    private $live_class_id;
    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($live_class_id=0)
    {
        parent::__construct();
        $this->live_class_id = $live_class_id;
    }

    public function getLiveClassNum($where='') {
      return $this->where($where)->count();
    }

    /**
     * 添加视频分类
     * @author 姜伟
     * @param array $arr_class 视频分类数组
     * @return boolean 操作结果
     * @todo 添加视频分类
     */
    public function addLiveClass($arr_class)
    {
        if (!is_array($arr_class)) return false;
        return $this->add($arr_class);
    }

    /**
     * 删除视频分类
     * @author 姜伟
     * @param string $live_class_id 视频分类ID
     * @return boolean 操作结果
     * @todo 删除视频分类
     */
    public function delLiveClass()
    {
        if (!is_numeric($this->live_class_id)) return false;
        return $this->where('live_class_id = ' . $this->live_class_id)->delete();
    }

    /**
     * 更改视频分类
     * @author 姜伟
     * @param int $live_class_id 视频分类ID
     * @param array $arr_class 视频分类数组
     * @return boolean 操作结果
     * @todo 更改视频分类
     */
    public function setLiveClass($live_class_id, $arr_class)
    {
        if (!is_numeric($live_class_id) || !is_array($arr_class)) return false;
        return $this->where('live_class_id = ' . $live_class_id)->save($arr_class);
    }

    /**
     * 获取视频分类
     * @author 姜伟
     * @param int $live_class_id 视频分类ID
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 视频分类
     * @todo 获取视频分类
     */
    public function getLiveClass($live_class_id, $fields = null)
    {
        if (!is_numeric($live_class_id))   return false;
        return $this->field($fields)->where('live_class_id = ' . $live_class_id)->find();
    }

    /**
     * 获取视频分类某个字段的信息
     * @author 姜伟
     * @param int $live_class_id 视频分类ID
     * @param string $field 查询的字段名
     * @return array 视频分类
     * @todo 获取视频分类某个字段的信息
     */
    public function getLiveClassField($live_class_id, $field)
    {
        if (!is_numeric($live_class_id))   return false;
        return $this->where('live_class_id = ' . $live_class_id)->getField($field);
    }

    /**
     * 获取所有视频分类列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 视频分类列表
     * @todo 获取所有视频分类列表
     */
    public function getLiveClassList($where = null)
    {
        return $this->where($where)->order('serial')->Limit()->select();
    }

    public function getValidLiveClassList()
    {
        return $this->where('isuse = 1')->order('serial')->Limit(1000000)->select();
    }
 
    /**
     * 获取分类信息
     * @author 姜伟
     * @param string $where where子句
     * @param string $fields 要获取的字段名
     * @return array 商品基本信息
     * @todo 根据where查询条件查找商品表中的相关数据并返回
     */
    public function getLiveClassInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    public function getListData($live_class_list){

        $live_obj = new LiveModel();
        foreach ($live_class_list as $k => $v){
            $live_class_list[$k]['live_num'] =$live_obj->getLiveNum('live_class_id ='.$v['live_class_id']);
        }
        return $live_class_list;
    }
}
