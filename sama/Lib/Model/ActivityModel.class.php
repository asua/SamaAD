<?php

class ActivityModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    //获取数量
    public function getActivityNum($where='')
    {
        $count = $this->where($where)->count();
        return $count;
    }
    //单条信息
    public function getActivityInfo($where, $fields = '')
    {
        return $this->field($fields)->where($where)->find();
    }
    //获取列表
    public function getActivityList($fields = '', $where = '', $orderby = 'activity_id desc', $groupby = '')
    {
        return $this->field($fields)->where($where)->group($groupby)->order($orderby)->limit()->select();
    }
    //增加活动
    public function addActivity($arr){
        return $this->add($arr);
    }
    //编辑活动
    public function editActivity($activity_id,$arr){
        return $this->where('activity_id = '.$activity_id)->save($arr);
    }
    //删除活动
    public function delActivity($activity_id){
        return $this->where('activity_id = '.$activity_id)->delete();
    }

    public function setFieldInc($field, $where = ''){
        return $this->where($where)->setInc($field);
    }

    public function setFieldDEC($field, $where = ''){
        return $this->where($where)->setDec($field);
    }
}