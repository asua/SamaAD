<?php
/**
 * 活动设施类型模型类
 */

class SiteDeployModel extends Model
{

    const FACILITY  = 1; //配套设施
    const ACTIVITY  = 2; //支持的活动

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getSiteDeployNum($where = '') {
        return $this->where($where)->count();
    }




    /**
     * 添加活动设施
     * @author 姜伟
     * @param array $arr_site_deploy 活动设施数组
     * @return boolean 操作结果
     * @todo 添加活动设施
     */
    public function addSiteDeploy($arr_site_deploy)
    {
        if (!is_array($arr_site_deploy)) return false;
        return $this->add($arr_site_deploy);
    }

    /**
     * 删除活动设施
     * @author 姜伟
     * @param string $site_deploy_id 活动设施ID
     * @return boolean 操作结果
     * @todo 删除活动设施
     */
    public function delSiteDeploy($site_deploy_id)
    {
        if (!is_numeric($site_deploy_id)) return false;
        return $this->where('site_deploy_id = ' . $site_deploy_id)->delete();
    }

    /**
     * 更改活动设施
     * @author 姜伟
     * @param int $site_deploy_id 活动设施ID
     * @param array $arr_site_deploy 活动设施数组
     * @return boolean 操作结果
     * @todo 更改活动设施
     */
    public function setSiteDeploy($site_deploy_id, $arr_site_deploy)
    {
        if (!is_numeric($site_deploy_id) || !is_array($arr_site_deploy)) return false;
        return $this->where('site_deploy_id = ' . $site_deploy_id)->save($arr_site_deploy);
    }

    /**
     * 更改活动设施状态
     * @author 姜伟
     * @param int $site_deploy_id 活动设施ID
     * @param array $site_deploy_status 活动设施数组
     * @return boolean 操作结果
     * @todo 更改活动设施
     */
    public function setSiteDeployStatus($site_deploy_id,$site_deploy_status)
    {
        if (!is_numeric($site_deploy_id)) return false;
        return $this->where('site_deploy_id = ' . $site_deploy_id)->save($site_deploy_status);
    }

    /**
     * 获取活动设施
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 活动设施
     * @todo 获取活动设施
     */
    public function getSiteDeployInfo($where, $fields = null)
    {
        return $this->field($fields)->where($where)->find();
    }

    /**
     * 获取活动设施某个字段的信息
     * @author 姜伟
     * @param int $site_deploy_id 活动设施ID
     * @param string $field 查询的字段名
     * @return array 活动设施
     * @todo 获取活动设施某个字段的信息
     */
    public function getSiteDeployField($site_deploy_id, $field)
    {
        if (!is_numeric($site_deploy_id))   return false;
        return $this->where('site_deploy_id = ' . $site_deploy_id)->getField($field);
    }

    /**
     * 获取所有活动设施列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 活动设施列表
     * @todo 获取所有活动设施列表
     */
    public function getSiteDeployList($where = null,$order = null,$field=null)
    {
        return $this->field($field)->where($where)->order($order)->limit()->select();
    }


    //数字转文字
    public function numDigitalText($status){

        return $this->arr[$status];
    }



    public function getListData($site_deploy_list){


        $service_type_obj = new ServiceTypeModel();
        $province_obj = new AddressProvinceModel();
        $city_obj = new AddressCityModel();
        foreach ($site_deploy_list as $k => $v){

            //区域
            $province_name =$province_obj->getProvinceName($v['province_id']);
            $city_name =$city_obj->getCityName($v['city_id']);
            $area_name =M('AddressArea')->where('area_id ='.$v['area_id'])->getField('area_name');
            $area_string = $province_name.'-'.$city_name.'-'.$area_name;
            $site_deploy_list[$k]['area_string'] = $area_string;

            //服务类型
            $site_deploy_list[$k]['service_type_name'] = $service_type_obj->getServiceTypeField($v['service_type_id'],'service_type_name');

            //活动设施状态
            $site_deploy_list[$k]['status_name'] = self::numDigitalText($v['site_deploy_status']);
        }



        return $site_deploy_list;
    }


}
