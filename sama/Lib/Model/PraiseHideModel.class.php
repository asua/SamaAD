<?php
/**
 * 赞赏帖隐藏内容模型类
 */

class PraiseHideModel extends Model
{

    private $praise_hide_id;

    const POST = 1;
    const COMMENT = 2;

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($praise_hide_id=0)
    {
        parent::__construct();
        $this->praise_hide_id = $praise_hide_id;
    }

    public function getPraiseHideNum($where = ''){
        return $this->where($where)->count();
    }


    public function addPraiseHide($arr){
        if(!is_array($arr)) return false;
        $imgs = $arr['imgs'];
        unset($arr['imgs']);
        $r = $this->add($arr);
        if($r){
            if($imgs){
                //添加图片
                $imgs = explode(',', $imgs);
                $img_arr = array();
                foreach ($imgs as $k => $v) {
                    $img_arr[$k]['praise_hide_id'] = $r;
                    $img_arr[$k]['img_url'] = $v;
                }
                D('PraiseHideImg')->addPraiseHideImg($img_arr);
            }
            
        }
        return $r;
    }


    //获取帖子的赞赏价
    public function getPostPraisePrice($post_id){
        return $this->where('hide_type ='.self::POST .' and id ='.$post_id)->getField('price');
    }


    //获取帖子的隐藏内容
    public function getPostHide($post_id){
        $r = $this->where('hide_type ='.self::POST . ' and id ='.$post_id)->find();
        $hide_imgs = D('PraiseHideImg')->getPraiseHideImg($r['praise_hide_id']);
        $r['hide_imgs'] = $hide_imgs;

        //隐藏字数
        $str_len = mb_strlen($r['content'], 'utf-8');
        $r['str_len'] = $str_len;
        return $r;
    }

    //获取评论的隐藏内容
    public function getCommentHide($comment_id){
        $r = $this->where('hide_type ='.self::COMMENT . ' and id ='.$comment_id)->find();
        if($r){
            $hide_imgs = D('PraiseHideImg')->getPraiseHideImg($r['praise_hide_id']);
            $r['hide_imgs'] = $hide_imgs;
            //后台用
            $hide_imgs_list = D('PraiseHideImg')->getPraiseHideImgList($r['praise_hide_id']);
            $r['hide_imgs_list'] = $hide_imgs_list;

            //隐藏字数
            $str_len = mb_strlen($r['content'], 'utf-8');
            $r['str_len'] = $str_len;
            
        }
        
        return $r;
    }


    public function getHide($hide_id){
        $r = $this->where('praise_hide_id ='.$hide_id)->find();
        if($r){
            $hide_imgs = D('PraiseHideImg')->getPraiseHideImg($r['praise_hide_id']);
            $r['hide_imgs'] = $hide_imgs;

            //隐藏字数
            $str_len = mb_strlen($r['content'], 'utf-8');
            $r['str_len'] = $str_len;
            
        }
        
        return $r;
    }

    /**
     * 判断用户是否有查看隐藏内容的权限
     * @param  [type] $user_id   [description]
     * @param  [type] $hide_type 类型，1帖子，2评论
     * @param  [type] $id        类型对应的id
     * @return [type]            [description]
     */
    public function checkSeeHidePriv($user_id, $hide_id){
        $hide = $this->where('praise_hide_id ='.$hide_id)->find();
        if($user_id == $hide['user_id']) return true;
        
        $buy = D('PraiseHideBuy')->checkSeeHidePriv($user_id, $hide_id);
        if($buy) return true;

        return false;
    }


    public function getPraiseHideInfo($where){
        return $this->where($where)->find();
    }


    //查询某个字段
    public function getPraiseHideField($where,$field){
        return $this->where($where)->getField($field);
    }

    public function getPraiseHideFields($where,$field,$group=''){
        return $this->where($where)->group($group)->getField($field,true);
    }

    public function settPraiseHide($praise_hide_id, $arr)
    {
        if (!is_numeric($praise_hide_id) || !is_array($arr)) return false;
        return $this->where('praise_hide_id = ' . $praise_hide_id)->save($arr);
    }
}
