<?php
/**
 * 合同类模型
 */

Class ContractModel extends Model
{
    private $contract_id;
    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($contract_id=0)
    {
        parent::__construct();
        $this->contract_id = $contract_id;
    }

    public function getContractNum($where='') {
      return $this->where($where)->count();
    }

    /**
     * 添加合同
     * @author 姜伟
     * @param array $arr_Contract 合同数组
     * @return boolean 操作结果
     * @todo 添加合同
     */
    public function addContract($arr_Contract)
    {
        if (!is_array($arr_Contract)) return false;
        return $this->add($arr_Contract);
    }

    /**
     * 删除合同
     * @author 姜伟
     * @param string $contract_id 合同ID
     * @return boolean 操作结果
     * @todo 删除合同
     */
    public function delContract()
    {
        if (!is_numeric($this->contract_id)) return false;
        return $this->where('contract_id = ' . $this->contract_id)->delete();
    }

    /**
     * 更改合同
     * @author 姜伟
     * @param int $contract_id 合同ID
     * @param array $arr_Contract 合同数组
     * @return boolean 操作结果
     * @todo 更改合同
     */
    public function setContract($contract_id, $arr_Contract)
    {
        if (!is_numeric($contract_id) || !is_array($arr_Contract)) return false;
        return $this->where('contract_id = ' . $contract_id)->save($arr_Contract);
    }

    /**
     * 获取合同
     * @author 姜伟
     * @param int $contract_id 合同ID
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 合同
     * @todo 获取合同
     */
    public function getContract($contract_id, $fields = null)
    {
        if (!is_numeric($contract_id))   return false;
        return $this->field($fields)->where('contract_id = ' . $contract_id)->find();
    }

    /**
     * 获取合同
     * @author 姜伟
     * @param string $where 查询条件
     * @param string $field 查询的字段名
     * @return array 合同
     * @todo 获取合同
     */
    public function getContractWhereField($where, $field)
    {
        return $this->where($where)->getField($field);
    }


    /**
     * 获取合同
     * @author 姜伟
     * @param string $where 查询条件
     * @param array $fields 查询的字段名
     * @return array 合同
     * @todo 获取合同
     */
    public function getContractWhereFields($where, $fields)
    {
        return $this->where($where)->getField($fields,true);
    }

    /**
     * 获取合同某个字段的信息
     * @author 姜伟
     * @param int $contract_id 合同ID
     * @param string $field 查询的字段名
     * @return array 合同
     * @todo 获取合同某个字段的信息
     */
    public function getContractField($contract_id, $field)
    {
        if (!is_numeric($contract_id))   return false;
        return $this->where('contract_id = ' . $contract_id)->getField($field);
    }

    /**
     * 获取所有合同列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 合同列表
     * @todo 获取所有合同列表
     */
    public function getContractList($where = null,$order='addtime desc')
    {
        return $this->where($where)->order($order)->limit()->select();
    }
 
    /**
     * 获取合同信息
     * @author 姜伟
     * @param string $where where子句
     * @param string $fields 要获取的字段名
     * @return array 商品基本信息
     * @todo 根据where查询条件查找商品表中的相关数据并返回
     */
    public function getContractInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }



    public function getListData($contract_list){

        foreach($contract_list as $k => $v){
            $type_obj = new TypeModel();
            $deposit_obj = new DepositModel();
            $deposit_info = $deposit_obj->getDepositInfo('contract_id ='.$v['contract_id']);
            /* 押金 start*/
            $contract_list[$k]['start_pay_time']=$deposit_info['start_time'];//押金到账时间
            $contract_list[$k]['end_pay_time']=$deposit_info['end_time'];//押金退还时间
            $contract_list[$k]['is_receipt']=$deposit_info['is_receipt'];//是否开据
            $contract_list[$k]['pay_deposit']=$deposit_info['pay_deposit'];//押金
            $contract_list[$k]['deposit_id']=$deposit_info['deposit_id'];//押金id,退押金时使用
            $contract_list[$k]['surrender_name']=$type_obj->getTypeSiteField('type_name','type_id ='.$deposit_info['surrender']);//退租原因
            /* 押金 end*/

            /* 租金管理 start*/
            //租金总金额 (租赁合同总金额+技术咨询服务费金额)
            $total_money = sprintf("%.2f",($v['lease_total_money'] +$v['consult_total_money']));
            $contract_list[$k]['total_money'] =$total_money;

            $rent_obj = new RentModel();
            //应交总租金
            $total_payable_money = $rent_obj->getSumMoney('contract_id ='.$v['contract_id'],'payable_money');
            $contract_list[$k]['total_payable_money'] = $total_payable_money;
            //已收总租金
            $total_actual_money = $rent_obj->getSumMoney('contract_id ='.$v['contract_id'],'actual_money');
            $contract_list[$k]['total_actual_money'] = sprintf("%.2f",$total_actual_money);
            //未收租金 (租金总金额 - 已收租金)
            $contract_list[$k]['total_goat_money'] =  sprintf("%.2f",$total_money - $total_actual_money);
            //逾期总租金 (应交总租金 - 已收总租金)
            $contract_list[$k]['total_overdue_money'] = sprintf("%.2f",$total_payable_money - $total_actual_money);
            //状态
            $state = '';
            if($total_actual_money == $total_money){
                $state ='结清';
            }elseif ($total_payable_money == $total_actual_money){
                $state ='正常';
            }elseif ($total_payable_money > $total_actual_money){
                $state ='逾期';
            }
            $contract_list[$k]['state'] = $state;
            /* 租金管理 end*/

        }

        return $contract_list;
    }


    /**
     * 统计的方法．
     */
    public function get_statistical_list($where = '',$group = '',$field= ''){

        return $this->field($field)->where($where)->group($group)->select();
    }

}
