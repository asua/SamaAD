<?php
/**
 * 帖子图片模型类
 */

class PostImgModel extends Model
{
    private $post_img_id;

    const REWARD = 1;//悬赏
    const PRAISE = 2;//赞赏
    const CROWDFUNDING = 3;//心愿
    const NORMAL = 4;//普通

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($post_img_id=0)
    {
        parent::__construct();
        $this->post_img_id = $post_img_id;
    }


    public function addPostImg($img_arr){
        return $this->addAll($img_arr);
    }

    public function getPostImg($post_id){
        return $this->where('post_id ='.$post_id)->getField('img_url', true);
    }

    public function getPostImgList($post_id){
        return $this->where('post_id ='.$post_id)->limit(100000)->select();
    }

    //删除照片．
    public function delImg($post_img_id){
        return $this->where('post_img_id ='.$post_img_id)->delete();
    }

}


