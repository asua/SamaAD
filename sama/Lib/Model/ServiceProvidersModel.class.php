<?php

/**
 * 服务商模型类
 */
class ServiceProvidersModel extends Model
{


    const GENERAL = 0;//普通
    const MENTOR = 1;//导师

    const  TOAUDIT = 0; //待审核
    const  APPROVED = 1; //审核通过
    const  NOTPASS = 2; //审核未通过
    const  SOLDOUT = 3; //下架


    public $arr = array(
        '0' => '待审核',
        '1' => '通过/上架',
        '2' => '未通过',
        '3' => '下架',
    );

    protected $_validate = array(
        array('service_providers_status', 'require', '请选择服务商状态！'),
        array('service_type_id', 'require', '请选择服务类型！'),
        array('service_providers_logo', 'require', '请添加服务商LOGO/导师照片！'),
        array('service_providers_name', 'require', '请输入服务商名称！'),
        array('province_id', 'require', '请选择省份！'),
        array('city_id', 'require', '请选择城市！'),
        array('area_id', 'require', '请选择区域！'),
        array('address', 'require', '请输入地址！'),
        array('mobile', 'require', '请输入联系方式！'),
        array('mobile',  '/^((400[0-9]{7})|(800[0-9]{7})|(1[34578]{1}\d{9})|((0[0-9]{2,3}[\-]?)?([0-9]{6,8})+(\-[0-9]{1,4})?))$/','请输入正确联系人手机号'),
        array('url', 'require', '请输入url！'),
        array('url', 'url', '请输入正确的url！'),
        array('score', 'require', '请输入评分！'),
        array('enterprise_name', 'require', '请输入企业简称！'),
        array('charter', 'require', '请添加三证合一或营业执照！'),
        array('poster', 'require', '请添加海报图片！'),
        array('enterprise_user_name', 'require', '请输入企业负责人名称！'),
        array('enterprise_user_mobile', 'require', '请输入企业负责人联系方式！'),
        array('enterprise_user_mobile', '/^((400[0-9]{7})|(800[0-9]{7})|(1[34578]{1}\d{9})|((0[0-9]{2,3}[\-]?)?([0-9]{6,8})+(\-[0-9]{1,4})?))$/','请输入正确企业负责人联系方式！'),
        array('email', 'require', '请输入邮箱！'),
        array('email', 'email', '请输入正确邮箱！'),
        array('accounts', 'require', '请输入公众号！'),
        array('accounts_code', 'require', '请添加公众号二维码！'),
        array('business', 'require', '请输入主营业务！'),
        array('service_providers_describe', 'require', '请输入服务商简介！'),
        array('business', 'require', '请输入主营业务！'),
        array('service_content', 'require', '请输入服务内容！'),
        array('service_describe', 'require', '请输入服务简介！'),
//        array('name','','帐号名称已经存在！',0,'unique',1), //在新增的时候验证name字段是否唯一
//        array('value',array(1,2,3),'值的范围不正确！',2,'in'),// 当值不为空的时候判断是否在一个范围内
//        array('repassword','password','确认密码不正确',0,'confirm'),// 验证确认密码是否和密码一致
//        array('password','checkPwd','密码格式不正确',0,'function'),// 自定义函数验证密码格式);
    );

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getServiceProvidersNum($where = '')
    {
        return $this->where($where)->count();
    }


    /**
     * 添加服务商
     * @author 姜伟
     * @param array $arr_ServiceProviders 服务商数组
     * @return boolean 操作结果
     * @todo 添加服务商
     */
    public function addServiceProviders($arr_ServiceProviders)
    {
        if (!is_array($arr_ServiceProviders)) return false;
        return $this->add($arr_ServiceProviders);
    }

    /**
     * 删除服务商
     * @author 姜伟
     * @param string $service_providers_id 服务商ID
     * @return boolean 操作结果
     * @todo 删除服务商
     */
    public function delServiceProviders($service_providers_id)
    {
        if (!is_numeric($service_providers_id)) return false;
        return $this->where('service_providers_id = ' . $service_providers_id)->delete();
    }

//    /**
//     * 删除一级分类下的所有服务商
//     * @author 姜伟
//     * @param string $class_id 一级分类ID
//     * @return boolean 操作结果
//     * @todo 删除服务商
//     */
//    public function delClassServiceProviders($class_id)
//    {
//        if (!is_numeric($class_id)) return false;
//        return $this->where('class_id = ' . $class_id)->delete();
//    }

    /**
     * 更改服务商
     * @author 姜伟
     * @param int $service_providers_id 服务商ID
     * @param array $arr_ServiceProviders 服务商数组
     * @return boolean 操作结果
     * @todo 更改服务商
     */
    public function setServiceProviders($service_providers_id, $arr_ServiceProviders)
    {
        if (!is_numeric($service_providers_id) || !is_array($arr_ServiceProviders)) return false;
        return $this->where('service_providers_id = ' . $service_providers_id)->save($arr_ServiceProviders);
    }

    /**
     * 更改服务商状态
     * @author 姜伟
     * @param int $service_providers_id 服务商ID
     * @param array $service_providers_status 服务商数组
     * @return boolean 操作结果
     * @todo 更改服务商
     */
    public function setServiceProvidersStatus($service_providers_id, $service_providers_status)
    {
        if (!is_numeric($service_providers_id)) return false;
        return $this->where('service_providers_id = ' . $service_providers_id)->save($service_providers_status);
    }

    /**
     * 获取服务商
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 服务商
     * @todo 获取服务商
     */
    public function getServiceProvidersInfo($where, $fields = null)
    {
        return $this->field($fields)->where($where)->find();
    }

    /**
     * 获取服务商某个字段的信息
     * @author 姜伟
     * @param int $service_providers_id 服务商ID
     * @param string $field 查询的字段名
     * @return array 服务商
     * @todo 获取服务商某个字段的信息
     */
    public function getServiceProvidersField($service_providers_id, $field)
    {
        if (!is_numeric($service_providers_id)) return false;
        return $this->where('service_providers_id = ' . $service_providers_id)->getField($field);
    }

    /**
     * 获取服务商某个字段的信息
     * @author 姜伟
     * @param string $field 查询的字段名
     * @return array 服务商
     * @todo 获取服务商某个字段的信息
     */
    public function getServiceProvidersGroupFields($field, $where = '', $group = '')
    {
        return $this->where($where)->group($group)->getField($field, true);
    }


    /**
     * 获取所有服务商列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 服务商列表
     * @todo 获取所有服务商列表
     */
    public function getServiceProvidersList($where = null, $order = null)
    {
        return $this->where($where)->order($order)->limit()->select();
    }

    /**
     * 获取一级分类下所有启用的服务商列表
     * @author 姜伟
     * @param int $class_id 一级分类ID
     * @return array 服务商列表
     * @todo 获取一级分类下所有isuse=1的服务商列表
     */
    public function getClassServiceProvidersList($class_id)
    {
        if (!is_numeric($class_id)) return false;
        return $this->where('class_id = ' . $class_id . ' AND isuse = 1')->order('serial')->limit()->select();
    }


    public function getCategoryList()
    {
        $Class = D('Class');
        $ServiceProviders = D('ServiceProviders');
        $Item = D('Item');
        $arr_category = array();

        // 获取所有一级分类
        $arr_class = $Class->getClassList('isuse=1');
        foreach ($arr_class as $k1 => $class) {
            $arr_category[$k1] = $class;
            // 获取一级分类下的服务商
            $arr_ServiceProviders = $ServiceProviders->getClassServiceProvidersList($class['class_id']);
            $arr_category[$k1]['ServiceProviders_list'] = $arr_ServiceProviders;
        }

        return $arr_category;
    }


    //数字转文字
    public function numDigitalText($status)
    {

        return $this->arr[$status];
    }


    public function getListData($service_providers_list)
    {
        $service_type_obj = new ServiceTypeModel();
        $province_obj = new AddressProvinceModel();
        $city_obj = new AddressCityModel();
        $domain_obj = new TypeModel();
        $user_obj = new UserModel();
        foreach ($service_providers_list as $k => $v) {

            //区域
            $province_name = $province_obj->getProvinceName($v['province_id']);
            $city_name = $city_obj->getCityName($v['city_id']);
            $area_name = M('AddressArea')->where('area_id =' . $v['area_id'])->getField('area_name');
            $area_string = $province_name . '-' . $city_name . '-' . $area_name;
            $service_providers_list[$k]['area_string'] = $area_string;
            $service_providers_list[$k]['city_name'] = $city_name;

            //服务类型
            $service_providers_list[$k]['service_type_name'] = $service_type_obj->getServiceTypeField($v['service_type_id'], 'service_type_name');

            //服务商状态
            $service_providers_list[$k]['status_name'] = self::numDigitalText($v['service_providers_status']);

            //导师领域
            $domain_arr = $domain_obj->getTypeNameData($v['domain']);
            $service_providers_list[$k]['domain_name'] = implode(',', $domain_arr);

            //职位
            $service_providers_list[$k]['position_name'] = $user_obj->where('user_id=' . $v['user_id'])->getField('position');

            $service_providers_list[$k]['service_providers_describe'] = strip_tags($v['service_providers_describe']);
        }


        return $service_providers_list;
    }


}
