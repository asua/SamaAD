<?php
class ApplePayApiModel extends ApiModel
{

	function iosRecharge($params){
		$user_id = intval(session('user_id'));
		if(!$user_id) ApiModel::returnResult(42001, null, '请先登录');
		$item_list = D('ApplePay')->itemList();
		return array(
				'user_id' => $user_id,
	            'item_list' => $item_list,
            );
	}


	public function applePayRecharge($params){
		$apple_receipt = $params['apple_receipt'];
		log_file('apple_receipt='.$apple_receipt, 'apple_pay');
		$pay_obj = D('ApplePay');
		$result = $pay_obj->recharge($apple_receipt);

		return $result;
	}

	/**
	 * 获取参数列表
	 * @author 姜伟
	 * @param 
	 * @return 参数列表 
	 * @todo 获取参数列表
	 */
	function getParams($func_name)
	{
		$params = array(
			'getAccountList'	=> array(
				array(
					'field'		=> 'firstRow', 
				),
				array(
					'field'		=> 'fetch_num', 
				),
			),
			'iosRecharge'	=> array(
			),
			'applePayRecharge'	=> array(
				array(
					'field'		=> 'apple_receipt', 
					'type'		=> 'string', 
					'required'	=> true, 
				),
				
			),
		);

		return $params[$func_name];
	}
}
