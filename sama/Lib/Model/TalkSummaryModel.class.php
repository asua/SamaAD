<?php
/**
 * 沟通纪要模型类
 */

class TalkSummaryModel extends Model
{
    // 沟通纪要id
    public $talk_summary_id;
   
    /**
     * 构造函数
     * @author 姜伟
     * @param $talk_summary_id 沟通纪要ID
     * @return void
     * @todo 初始化沟通纪要id
     */
    public function TalkSummaryModel($talk_summary_id)
    {
        parent::__construct();

        if ($talk_summary_id = intval($talk_summary_id))
		{
            $this->talk_summary_id = $talk_summary_id;
		}
    }

    /**
     * 获取沟通纪要信息
     * @author 姜伟
     * @param int $talk_summary_id 沟通纪要id
     * @param string $fields 要获取的字段名
     * @return array 沟通纪要基本信息
     * @todo 根据where查询条件查找沟通纪要表中的相关数据并返回
     */
    public function getTalkSummaryInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改沟通纪要信息
     * @author 姜伟
     * @param array $arr 沟通纪要信息数组
     * @return boolean 操作结果
     * @todo 修改沟通纪要信息
     */
    public function editTalkSummary($arr)
    {
        return $this->where('talk_summary_id = ' . $this->talk_summary_id)->save($arr);
    }

    /**
     * 添加沟通纪要
     * @author 姜伟
     * @param array $arr 沟通纪要信息数组
     * @return boolean 操作结果
     * @todo 添加沟通纪要
     */
    public function addTalkSummary($arr)
    {
        if (!is_array($arr)) return false;

        return $this->add($arr);
    }

    /**
     * 删除沟通纪要
     * @author 姜伟
     * @param int $talk_summary_id 沟通纪要ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delTalkSummary($where)
    {
        if (!$where) return false;
        return $this->where($where)->delete();
    }

    /**
     * 根据where子句获取沟通纪要数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的沟通纪要数量
     * @todo 根据where子句获取沟通纪要数量
     */
    public function getTalkSummaryNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询沟通纪要信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 沟通纪要基本信息
     * @todo 根据SQL查询字句查询沟通纪要信息
     */
    public function getTalkSummaryList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit($limit)->select();
    }

    /**
     * 连表查询
     */
    public function getTalkSummaryJoinList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        $data = M('incubator as a')->join('tp_incubator_order as b on b.incubator_id = a.incubator_id')->where($where)->order($orderby)->select();
        return $data;
    }


    /**
     * 获取沟通纪要列表页数据信息列表
     * @author 姜伟
     * @param array $talk_summary_list
     * @return array $talk_summary_list
     * @todo 根据传入的$talk_summary_list获取更详细的沟通纪要列表页数据信息列表
     */
    public function getListData($talk_summary_list)
    {
		foreach ($talk_summary_list AS $k => $v)
		{
            $customer_obj = new CustomerModel();
            $follow_state_list =$customer_obj->followState();
            $talk_summary_list[$k]['follow_state_name'] = $follow_state_list[$v['follow_state']];

		}

		return $talk_summary_list;
    }

    /**
     * 统计客户流失原因
     */
    public function get_statistical_list($where = '',$group = '',$field= ''){

        return $this->field($field)->where($where)->group($group)->select();
    }

}
