<?php
/**
 * 场地模型类
 */

class SiteModel extends Model
{

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getSiteNum($where = '') {
        return $this->where($where)->count();
    }




    /**
     * 添加场地
     * @author 姜伟
     * @param array $arr_site 场地数组
     * @return boolean 操作结果
     * @todo 添加场地
     */
    public function addSite($arr_site)
    {
        if (!is_array($arr_site)) return false;
        return $this->add($arr_site);
    }

    /**
     * 删除场地
     * @author 姜伟
     * @param string $site_id 场地ID
     * @return boolean 操作结果
     * @todo 删除场地
     */
    public function delSite($site_id)
    {
        if (!is_numeric($site_id)) return false;
        return $this->where('site_id = ' . $site_id)->delete();
    }

//    /**
//     * 删除一级分类下的所有场地
//     * @author 姜伟
//     * @param string $class_id 一级分类ID
//     * @return boolean 操作结果
//     * @todo 删除场地
//     */
//    public function delClassSite($class_id)
//    {
//        if (!is_numeric($class_id)) return false;
//        return $this->where('class_id = ' . $class_id)->delete();
//    }

    /**
     * 更改场地
     * @author 姜伟
     * @param int $site_id 场地ID
     * @param array $arr_site 场地数组
     * @return boolean 操作结果
     * @todo 更改场地
     */
    public function setSite($site_id, $arr_site)
    {
        if (!is_numeric($site_id) || !is_array($arr_site)) return false;
        return $this->where('site_id = ' . $site_id)->save($arr_site);
    }

    /**
     * 更改场地状态
     * @author 姜伟
     * @param int $site_id 场地ID
     * @param array $service_providers_status 场地数组
     * @return boolean 操作结果
     * @todo 更改场地
     */
    public function setSiteStatus($site_id,$service_providers_status)
    {
        if (!is_numeric($site_id)) return false;
        return $this->where('site_id = ' . $site_id)->save($service_providers_status);
    }

    /**
     * 获取场地
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 场地
     * @todo 获取场地
     */
    public function getSiteInfo($where, $fields = null)
    {
        return $this->field($fields)->where($where)->find();
    }

    /**
     * 获取场地的孵化器ID
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 场地
     * @todo 获取场地
     */
    public function getGroupIdFields($fields,$where=null,$group=null){
        return $this->where($where)->group($group)->getField($fields, true);
    }

    /**
     * 获取场地某个字段的信息
     * @author 姜伟
     * @param int $site_id 场地ID
     * @param string $field 查询的字段名
     * @return array 场地
     * @todo 获取场地某个字段的信息
     */
    public function getSiteField($site_id, $field)
    {
        if (!is_numeric($site_id))   return false;
        return $this->where('site_id = ' . $site_id)->getField($field);
    }

    /**
     * 获取所有场地列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 场地列表
     * @todo 获取所有场地列表
     */
    public function getSiteList($where = null,$order = null,$group =null)
    {
        return $this->where($where)->order($order)->group($group)->limit()->select();
    }


    //数字转文字
    public function numDigitalText($status){

        return $this->arr[$status];
    }



    public function getListData($site_list){


//        $service_type_obj = new ServiceTypeModel();
//        $province_obj = new AddressProvinceModel();
        $city_obj = new AddressCityModel();

        $incubator_obj = new IncubatorModel();
        $site_type_obj = new TypeModel();
        foreach ($site_list as $k => $v){

            //区域
//            $province_name =$province_obj->getProvinceName($v['province_id']);
//            $city_name =$city_obj->getCityName($v['city_id']);
//            $area_name =M('AddressArea')->where('area_id ='.$v['area_id'])->getField('area_name');
//            $area_string = $province_name.'-'.$city_name.'-'.$area_name;
//            $service_providers_list[$k]['area_string'] = $area_string;

            //所属孵化器的名字
            $site_list[$k]['incubator_name'] = $incubator_obj->where('incubator_id ='.$v['incubator_id'])->getField('incubator_name');

            //场地类型
            $site_list[$k]['site_type_name']=$site_type_obj->getTypeSiteField('type_name','type_id='.$v['site_type_id']);

            //所属孵化器的城市
            $city_id= $incubator_obj->where('incubator_id ='.$v['incubator_id'])->getField('city_id');
            $site_list[$k]['city_id']=$city_id;
            $site_list[$k]['city_name'] =$city_obj->getCityName($city_id);

            //图片
            $site_list[$k]['pic_arr'] = explode(',',$v['pic']);

//            //时间转换
            $site_list[$k]['start_hour'] = $v['start_time']-8*3600;
            $site_list[$k]['end_hour'] = $v['end_time']-8*3600;
        }

        return $site_list;
    }


}
