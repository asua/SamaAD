<?php
/**
 * 客户档案模型类
 */

class CustomerModel extends Model
{
    // 客户档案id
    public $customer_id;
   
    /**
     * 构造函数
     * @author 姜伟
     * @param $customer_id 客户档案ID
     * @return void
     * @todo 初始化客户档案id
     */
    public function CustomerModel($customer_id)
    {
        parent::__construct();

        if ($customer_id = intval($customer_id))
		{
            $this->customer_id = $customer_id;
		}
    }

    /**
     * 获取客户档案信息
     * @author 姜伟
     * @param int $customer_id 客户档案id
     * @param string $fields 要获取的字段名
     * @return array 客户档案基本信息
     * @todo 根据where查询条件查找客户档案表中的相关数据并返回
     */
    public function getCustomerInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改客户档案信息
     * @author 姜伟
     * @param array $arr 客户档案信息数组
     * @return boolean 操作结果
     * @todo 修改客户档案信息
     */
    public function editCustomer($arr)
    {
        return $this->where('customer_id = ' . $this->customer_id)->save($arr);
    }

    /**
     * 添加客户档案
     * @author 姜伟
     * @param array $arr 客户档案信息数组
     * @return boolean 操作结果
     * @todo 添加客户档案
     */
    public function addCustomer($arr)
    {
        if (!is_array($arr)) return false;

        return $this->add($arr);
    }

    /**
     * 删除客户档案
     * @author 姜伟
     * @param int $customer_id 客户档案ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delCustomer($where)
    {
        if (!$where) return false;
        return $this->where($where)->delete();
    }

    /**
     * 根据where子句获取客户档案数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的客户档案数量
     * @todo 根据where子句获取客户档案数量
     */
    public function getCustomerNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询客户档案信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 客户档案基本信息
     * @todo 根据SQL查询字句查询客户档案信息
     */
    public function getCustomerList($fields = '', $where = '', $orderby = 'addtime desc', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 连表查询
     */
    public function getCustomerJoinList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        $data = M('incubator as a')->join('tp_incubator_order as b on b.incubator_id = a.incubator_id')->where($where)->order($orderby)->select();
        return $data;
    }

    /**
     * 客户性质
     */
    public function customerType(){
        return array(
          '1'=> '项目客户',
          '2'=> '合作客户',
        );
    }

    /**
     * 项目性质
     */
    public function projectType(){
        return array(
            '1'=> '众创项目',
            '2'=> '孵化项目',
            '3'=> '加速项目',
            '4'=> '招商项目',
        );
    }

    /**
     * 其他联系方式
     */
    public function otherLink(){
        return array(
            '1'=> '电话',
            '2'=> 'QQ',
            '3'=> '微信',
            '4'=> '邮箱',
            '5'=> '其他',
        );
    }

    /**
     * 客户等级
     */
    public function level(){
        return array(
            '1'=> '高',
            '2'=> '中',
            '3'=> '低',
        );
    }

    /**
     * 跟进状态
     */
    public function followState(){
        return array(
            '0'=> '未跟进',
            '1'=> '初步洽谈',
            '2'=> '需求确认',
            '3'=> '意向确认',
            '4'=> '入孵签订',
            '5'=> '客户流失',
        );
    }

    /**
     * 获取客户档案列表页数据信息列表
     * @author 姜伟
     * @param array $customer_list
     * @return array $customer_list
     * @todo 根据传入的$customer_list获取更详细的客户档案列表页数据信息列表
     */
    public function getListData($customer_list)
    {
		foreach ($customer_list AS $k => $v)
		{
		    $customer_type_list = self::customerType();
            $customer_list[$k]['customer_type_name'] =$customer_type_list[$v['customer_type']];

            $level_list = self::level();
            $customer_list[$k]['level_name'] =$level_list[$v['level']];

            $follow_state_list = self::followState();
            $customer_list[$k]['follow_state_name'] =$follow_state_list[$v['follow_state']];

            $user_obj = new UserModel();
            $user_info =$user_obj->getUserInfo('realname','user_id ='.$v['salesman']);

            $customer_list[$k]['user_name'] =$user_info['realname'];


		}

		return $customer_list;
    }

    /**
     * 统计的方法
     */
    public function get_statistical_list($where = '',$group = '',$field= ''){

        return $this->field($field)->where($where)->group($group)->select();
    }

}
