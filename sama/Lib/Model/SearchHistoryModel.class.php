<?php
/**
 * 搜索模型类
 */

class SearchHistoryModel extends Model
{

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct()
    {
        parent::__construct('');
    }

    public function getSearchHistoryNum($where = '') {
        return $this->where($where)->count();
    }




    /**
     * 添加搜索
     * @author 姜伟
     * @param array $search_arr 搜索数组
     * @return boolean 操作结果
     * @todo 添加搜索
     */
    public function addSearchHistory($search_arr)
    {
        if (!is_array($search_arr)) return false;
        return $this->add($search_arr);
    }

    /**
     * 删除搜索
     * @author 姜伟
     * @param string $search_history_id 搜索ID
     * @return boolean 操作结果
     * @todo 删除搜索
     */
    public function delSearchHistory($search_history_id)
    {
        if (!is_numeric($search_history_id)) return false;
        return $this->where('search_history_id = ' . $search_history_id)->delete();
    }

//    /**
//     * 删除一级分类下的所有搜索
//     * @author 姜伟
//     * @param string $class_id 一级分类ID
//     * @return boolean 操作结果
//     * @todo 删除搜索
//     */
//    public function delClassSearchHistory($class_id)
//    {
//        if (!is_numeric($class_id)) return false;
//        return $this->where('class_id = ' . $class_id)->delete();
//    }

    /**
     * 更改搜索
     * @author 姜伟
     * @param int $search_history_id 搜索ID
     * @param array $search_arr 搜索数组
     * @return boolean 操作结果
     * @todo 更改搜索
     */
    public function setSearchHistory($search_history_id, $search_arr)
    {
        if (!is_numeric($search_history_id) || !is_array($search_arr)) return false;
        return $this->where('search_history_id = ' . $search_history_id)->save($search_arr);
    }

    /**
     * 更改搜索状态
     * @author 姜伟
     * @param int $search_history_id 搜索ID
     * @param array $service_providers_status 搜索数组
     * @return boolean 操作结果
     * @todo 更改搜索
     */
    public function setSearchHistoryStatus($where,$service_providers_status)
    {
        return $this->where($where)->save($service_providers_status);
    }

    /**
     * 获取搜索
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 搜索
     * @todo 获取搜索
     */
    public function getSearchHistoryInfo($where, $fields = null)
    {
        return $this->field($fields)->where($where)->find();
    }

    /**
     * 获取搜索某个字段的信息
     * @author 姜伟
     * @param int $search_history_id 搜索ID
     * @param string $field 查询的字段名
     * @return array 搜索
     * @todo 获取搜索某个字段的信息
     */
    public function getSearchHistoryField($search_history_id, $field)
    {
        if (!is_numeric($search_history_id))   return false;
        return $this->where('search_history_id = ' . $search_history_id)->getField($field);
    }

    /**
     * 获取所有搜索列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 搜索列表
     * @todo 获取所有搜索列表
     */
    public function getSearchHistoryList($where = null,$order = null,$group =null)
    {
        return $this->where($where)->order($order)->group($group)->limit()->select();
    }


    //数字转文字
    public function numDigitalText($status){

        return $this->arr[$status];
    }



    public function getListData($site_list){


//        $service_type_obj = new ServiceTypeModel();
//        $province_obj = new AddressProvinceModel();
        $city_obj = new AddressCityModel();

        $incubator_obj = new IncubatorModel();
        $site_type_obj = new TypeModel();
        foreach ($site_list as $k => $v){

            //区域
//            $province_name =$province_obj->getProvinceName($v['province_id']);
//            $city_name =$city_obj->getCityName($v['city_id']);
//            $area_name =M('AddressArea')->where('area_id ='.$v['area_id'])->getField('area_name');
//            $area_string = $province_name.'-'.$city_name.'-'.$area_name;
//            $service_providers_list[$k]['area_string'] = $area_string;

            //所属孵化器的名字
            $site_list[$k]['incubator_name'] = $incubator_obj->where('incubator_id ='.$v['incubator_id'])->getField('incubator_name');

            //搜索类型
            $site_list[$k]['site_type_name']=$site_type_obj->getTypeSearchHistoryField('type_name','type_id='.$v['site_type_id']);

            //所属孵化器的城市
            $city_id= $incubator_obj->where('incubator_id ='.$v['incubator_id'])->getField('city_id');
            $site_list[$k]['city_id']=$city_id;
            $site_list[$k]['city_name'] =$city_obj->getCityName($city_id);

            //图片
            $site_list[$k]['pic_arr'] = explode(',',$v['pic']);

            //时间转换
            $site_list[$k]['start_hour'] = $v['start_time']-8*3600;
            $site_list[$k]['end_hour'] = $v['end_time']-8*3600;
        }

        return $site_list;
    }


}
