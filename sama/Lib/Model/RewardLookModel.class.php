<?php
/**
 * 悬赏围观模型类
 */

class RewardLookModel extends Model
{
    private $reward_look_id;



    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($reward_look_id=0)
    {
        parent::__construct();
        $this->reward_look_id = $reward_look_id;
    }  



    //获取悬赏围观人数
    public function getRewardLookUserNum($r_id){
        return $this->where('reward_id ='.$r_id)->group('user_id')->count();
    }


    public function getRewardLookNum($where = ''){
        return $this->where($where)->count();
    }

    //获取围观列表
    public function getRewardLookist($field =null,$where, $order='addtime desc'){

        return $this->field($field)->where($where)->order($order)->limit()->select();
    }


    public function getListData($reward_look_list){
        foreach ($reward_look_list as $k => $v) {
            $user_obj = new UserModel();
            $reward_look_list[$k]['nick_name'] = $user_obj->getUserField('nickname','user_id ='.$v['user_id']);
            $reward_look_list[$k]['acp_time'] =date('Y-m-d H:i:s',$v['addtime']);

        }
        return $reward_look_list;
    }

    public function addRewardLook($arr){
        if(!is_array($arr)) return false;
        $arr['addtime'] = time();
        $r = $this->add($arr);

        if($r){
            D('Crowdfunding')->support($arr['crowdfunding_id'], $arr['support_money']);
        }

        return $r;
    }

    //查询某个字段
    public function getRewardLookField($where,$field){
        return $this->where($where)->getField($field);
    }

    public function getRewardLookFields($where,$field,$group=''){
        return $this->where($where)->group($group)->getField($field,true);
    }
}
