<?php
/**
 * 频道等级模型类
 */

class ChannelRankModel extends Model
{
    private $channel_rank_id;
    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($channel_rank_id=0)
    {
        parent::__construct();
        $this->channel_rank_id = $channel_rank_id;
    }

    public function getChannelRankNum($where='') {
      return $this->where($where)->count();
    }


    public function addChannelRank($arr){
        return $this->add($arr);
    }

    public function editChannelRank($channel_rank_id, $arr){
        return $this->where('channel_rank_id ='.$channel_rank_id)->save($arr);
    }


    public function getChannelRankList($field = '', $where = ''){
        return $this->field($field)->where($where)->limit()->select();
    }


    public function getChannelRankInfo($where){
        return $this->where($where)->find();
    }


    public function getFirstRank(){
        return $this->order('need_exp asc')->find();
    }
}
