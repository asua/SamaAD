<?php
/**
 * 悬赏退款原因模型类
 */

class RewardRefundReasonModel extends Model
{
    private $reward_refund_reason_id;


    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($reward_refund_reason_id=0)
    {
        parent::__construct();
        $this->reward_refund_reason_id = $reward_refund_reason_id;
    }
    
    public function addRewardRefundReason($arr)
    {
        if (!is_array($arr)) return false;
        return $this->add($arr);
    }

    public function getRewardRefundReasonNum($where='') {
        return $this->where($where)->count();
    }

    public function setRewardRefundReason($reward_refund_reason_id, $arr)
    {
        if (!is_numeric($reward_refund_reason_id) || !is_array($arr)) return false;
        return $this->where('reward_refund_reason_id = ' . $reward_refund_reason_id)->save($arr);
    }
    
    public function getRewardRefundReasonInfo($where = ''){
        return $this->where($where)->find();
    }

    public function getRewardRefundReasonList($where = ''){
        return $this->where($where)->limit()->select();
    }
}
