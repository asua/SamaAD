<?php
/**
 * 资讯模型类
 */

class EnterTeamModel extends Model
{


    public function EnterTeamModel()
    {
        parent::__construct('enter_team');

    }

    //单条信息
    public function getEnterTeamInfo($where, $fields = '')
    {
        return $this->field($fields)->where($where)->find();
    }
    //获取列表
    public function getEnterTeamList($fields = '', $where = '', $orderby = '', $groupby = '')
    {
        return $this->field($fields)->where($where)->group($groupby)->order($orderby)->limit()->select();
    }
    //获取数量
    public function getEnterTeamNum($where='')
    {
        $count = $this->where($where)->count();
        return $count;
    }
    //增加入驻
    public function addEnterTeam($arr){
        return $this->add($arr);
    }


    //删除入驻
    public function delEnterTeam($enter_team_id){
        return $this->where('enter_team_id = '.$enter_team_id)->delete();
    }
    //修改信息
    public function editEnterTeam($arr,$enter_team_id){
        return $this->where('enter_team_id = ' . $enter_team_id)->save($arr);
    }
}
