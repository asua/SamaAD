<?php
class PostApiModel extends ApiModel
{

	//帖子评论
	function comment($params){
		$user_id = intval(session('user_id'));

		if(!$user_id) ApiModel::returnResult(-1, null, '请先登录');
		$post_id = $params['post_id'];
		if(!$post_id) ApiModel::returnResult(-1, null, '评论失败，请重试'); 
            $post = D('Post')->getPostInfo('post_id ='.$post_id);

            //是否为楼主
            $is_lz = $user_id != $post['user_id'] ? 0 : 1;
            $content = htmlspecialchars($params['content']);
            $imgs = $params['imgs'];
            $imgs = trim($imgs, ',');
            if(!$content && !$imgs){
            	ApiModel::returnResult(42001, null, '评论内容、图片必须填写一项');
            }


            $post_comment_obj = new PostCommentModel();
            
            $arr = array(
                'content' => $content,
                'user_id' => $user_id,
                'post_id' => $post_id,
                'imgs' => $imgs,
                'is_lz' => $is_lz,
                );
            //是否为评论他人评论
            $reply_comment_id = isset($params['reply_comment_id']) ? $params['reply_comment_id'] : 0;
            $reply_user_id = 0;
            if($reply_comment_id){
            	$reply_comment = D('PostComment')->getPostCommentInfo('post_comment_id ='.$reply_comment_id);
            	$reply_user_id = $reply_comment['user_id'];
            	$arr['reply_user_id'] = $reply_user_id;
            	$arr['reply_comment_id'] = $reply_comment_id;
            }else{
            	$floor = $post_comment_obj->getMaxFloor($post_id) + 1;
            	$arr['floor'] = $floor;
            }

            if($c_id = $post_comment_obj->addPostComment($arr)){
            	return '评论成功';
                
            }
            
            return '评论失败';

	}

	



	/**
	 * 获取参数列表
	 * @author 姜伟
	 * @param 
	 * @return 参数列表 
	 * @todo 获取参数列表
	 */
	function getParams($func_name)
	{
		$params = array(
			'comment'	=> array(
				array(
					'field'		=> 'post_id',
					'type'		=> 'int', 
					'required'	=> true, 
					#'len_code'	=> 40004, 
					'miss_code'	=> 41004, 
					'empty_code'=> 44004, 
					'type_code'	=> 45004, 
				),
				array(
					'field'		=> 'content', 
					// 'min_len'	=> 32, 
					// 'max_len'	=> 32, 
					'type'		=> 'string', 
					// 'required'	=> true, 
					// 'len_code'	=> 40005, 
					// 'miss_code'	=> 41005, 
					// 'empty_code'=> 44005, 
					// 'type_code'	=> 45005, 
				),
				array(
					'field'		=> 'imgs',  
					'type'		=> 'string', 
				),
				array(
					'field'		=> 'reply_comment_id',  
					'type'		=> 'int', 
				),
			),
		);

		return $params[$func_name];
	}
}
