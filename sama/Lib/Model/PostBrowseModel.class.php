<?php
/**
 * 浏览模型类
 */

class PostBrowseModel extends Model
{
    // 浏览id
    public $post_browse_id;


    /**
     * 构造函数
     * @author 姜伟
     * @param $post_browse_id 浏览ID
     * @return void
     * @todo 初始化浏览id
     */
    public function PostBrowseModel($post_browse_id)
    {
        parent::__construct('post_browse');

        if ($post_browse_id = intval($post_browse_id))
		{
            $this->post_browse_id = $post_browse_id;
		}
    }

    /**
     * 获取浏览信息
     * @author 姜伟
     * @param int $post_browse_id 浏览id
     * @param string $fields 要获取的字段名
     * @return array 浏览基本信息
     * @todo 根据where查询条件查找浏览表中的相关数据并返回
     */
    public function getPostBrowseInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改浏览信息
     * @author 姜伟
     * @param array $arr 浏览信息数组
     * @return boolean 操作结果
     * @todo 修改浏览信息
     */
    public function editPostBrowse($arr)
    {
        return $this->where('post_browse_id = ' . $this->post_browse_id)->save($arr);
    }

    //修改浏览信息
    public function setPostBrowse($user_id,$post_id)
    {
        return $this->where('user_id ='.$user_id.' and post_id ='.$post_id)->save(array('addtime' => time()));
    }

    /**
     * 添加浏览
     * @author 姜伟
     * @param array $arr 浏览信息数组
     * @return boolean 操作结果
     * @todo 添加浏览
     */
    public function addPostBrowse($arr)
    {
        if (!is_array($arr)) return false;
		$arr['addtime'] = time();
        return $this->add($arr);
    }

    /**
     * 删除浏览
     * @author 姜伟
     * @param int $post_browse_id 浏览ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delPostBrowse($user_id)
    {
        if (!$user_id) return false;
        return $this->where('user_id ='.$user_id)->delete();
    }

    /**
     * 根据where子句获取浏览数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的浏览数量
     * @todo 根据where子句获取浏览数量
     */
    public function getPostBrowseNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询浏览信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 浏览基本信息
     * @todo 根据SQL查询字句查询浏览信息
     */
    public function getPostBrowseList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 获取浏览列表页数据信息列表
     * @author 姜伟
     * @param array $post_browse_list
     * @return array $post_browse_list
     * @todo 根据传入的$post_browse_list获取更详细的浏览列表页数据信息列表
     */
    public function getListData($post_browse_list)
    {
		foreach ($post_browse_list AS $k => $v)
		{

            //浏览时间
            $post_browse_list[$k]['addtime'] = get_time($v['addtime']);

            //帖子信息
            $post_obj = new PostModel();
            $post_info = $post_obj->getPostInfo('post_id ='.$v['post_id']);
            $post_browse_list[$k]['title'] = $post_info['title'];//标题

//            $post_browse_list[$k]['content'] = $post_info['content'];//内容
//            $post_browse_list[$k]['post_type'] = $post_info['post_type'];//帖子类型暂时没用到
//            $post_browse_list[$k]['comment_num'] = $post_info['comment_num'];//评论
//            $post_browse_list[$k]['clickdot'] = $post_info['clickdot'];//点击量

            //频道．
            $channel_obj = new ChannelModel();
            $post_browse_list[$k]['channel_name'] = $channel_obj->getChannelField($post_info['channel_id'] ,'channel_name');

		}

		return $post_browse_list;
    }

    public function getTypeField($where = '',$field){
        return $this->where($where)->getField($field, true);
    }
}
