<?php
class EquipmentModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    //获取总数量
    public function getEquipmentNum($where){
        return $this->where($where)->count();
    }
    //获取配套设备
    public function getEquipmentlist($where){
    return $this->where($where)->select();
    }
    //增加配套设备
    public function addEquipment($arr){
    return $this->add($arr);
    }
    //删除配套设备
    public function delEquipment($equipment_id){
    return $this->where('equipment_id = '.$equipment_id)->delete();
    }
    //查询设备信息
    public function getEquipmentInfo($equipment_id){
        return $this->where('equipment_id = '.$equipment_id)->find();
    }
    //修改设备信息
    public function editEquipment($arr,$equipment_id){
        return $this->where('equipment_id = '.$equipment_id)->save($arr);
    }
}
