<?php
class ArticleSortModel extends Model
{
    public function ArticleTxtModel()
    {
        parent::__construct('article_sort');

    }

    //单条信息
    public function getArticleSortInfo($where, $fields = '')
    {
        return $this->field($fields)->where($where)->find();
    }
    //获取列表
    public function getArticleSortList($fields = '', $where = '', $orderby = '', $groupby = '')
    {
        return $this->field($fields)->where($where)->group($groupby)->order($orderby)->limit()->select();
    }
    //获取数量
    public function getArticleSortNum($where='')
    {
        $count = $this->where($where)->count();
        return $count;
    }
    //增加资讯分类内容
    public function addArticleSort($arr){
        return $this->add($arr);
    }


    //删除信息
    public function delArticleSort($information_id){
        return $this->where('article_sort_id = '.$information_id)->delete();
    }
    //修改信息
    public function editArticleSort($arr,$information_id){
        return $this->where('article_sort_id = ' . $information_id)->save($arr);
    }
}