<?php
class ActivityApiModel extends ApiModel
{
	/**
	 * 登录接口
	 * @author wsq
	 * @param array $params 参数列表
	 * @return 成功返回'登录成功'，失败退出返回错误码
	 * @todo 查看用户名和密码是否合法，不合法退出，否则设置session
	 */
	function activitySign($params)
	{
		$activity_id = $params['activity_id'];
		$user_id = intval(session('user_id'));
		//判断账号密码合法性
		if(!$user_id){
            ApiModel::returnResult(42001, null, '请先登录');
        }

        $activity = M('Activity')->where('activity_id = '.$activity_id)->find();
        if(!$activity){
        	$return_data=array(
                    'code'=>-1,
                    'msg'=>'活动不存在!'
                );
                return $return_data;
        }

		
		$activity_sign_obj = new ActivitySignModel();
        $activity_sign_info=$activity_sign_obj->getActivitySignInfo('activity_id = '.$activity_id.' AND user_id = '.$user_id);
        if($activity_sign_info){
            if($activity_sign_info['is_sign']==1){
                $return_data=array(
                    'code'=>-1,
                    'msg'=>'你已经签到了!'
                );
                return $return_data;
            }
            $result=$activity_sign_obj->editActivitySign($activity_sign_info['activity_sign_id'],array('is_sign'=>1));
            if($result){
                $return_data=array(
                    'code'=>1,
                    'msg'=>'签到成功!'
                );
            }else{
                $return_data=array(
                    'code'=>-1,
                    'msg'=>'签到失败!'
                );
            }

        }else{
            $return_data=array(
                'code'=>-1,
                'msg'=>'签到失败!您没有报名哦!'
            );
        }
		//返回的数组
		return $return_data;
	}

	

	/**
	 * 获取参数列表
	 * @author 姜伟
	 * @param 
	 * @return 参数列表 
	 * @todo 获取参数列表
	 */
	function getParams($func_name)
	{
		$params = array(
			'activitySign'	=> array(
				array(
					'field'		=> 'activity_id',
					//'type'		=> 'int', 
					'required'	=> true, 
					#'len_code'	=> 40004, 
					'miss_code'	=> 41004, 
					'empty_code'=> 44004, 
					'type_code'	=> 45004, 
				),
			),
		);

		return $params[$func_name];
	}
}
