<?php
/**
 * 评论图片模型类
 */

class CommentImgModel extends Model
{
    private $comment_img_id;

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($comment_img_id=0)
    {
        parent::__construct();
        $this->comment_img_id = $comment_img_id;
    }


    public function addCommentImg($img_arr){
        return $this->addAll($img_arr);
    }

    public function getCommentImg($post_comment_id){
        return $this->where('post_comment_id ='.$post_comment_id)->getField('img_url', true);
    }

    public function getCommentList($post_comment_id){
        return $this->where('post_comment_id ='.$post_comment_id)->limit(100000)->select();
    }

    //删除照片．
    public function delImg($comment_img_id){
        return $this->where('comment_img_id ='.$comment_img_id)->delete();
    }


}
