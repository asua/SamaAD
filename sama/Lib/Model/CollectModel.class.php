<?php
/**
 * 收藏模型类
 */

class CollectModel extends Model
{
    // 收藏id
    public $collect_id;


    /**
     * 构造函数
     * @author 姜伟
     * @param $collect_id 收藏ID
     * @return void
     * @todo 初始化收藏id
     */
    public function CollectModel($collect_id)
    {
        parent::__construct('collect');

        if ($collect_id = intval($collect_id))
		{
            $this->collect_id = $collect_id;
		}
    }

    /**
     * 获取收藏信息
     * @author 姜伟
     * @param int $collect_id 收藏id
     * @param string $fields 要获取的字段名
     * @return array 收藏基本信息
     * @todo 根据where查询条件查找收藏表中的相关数据并返回
     */
    public function getCollectInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改收藏信息
     * @author 姜伟
     * @param array $arr 收藏信息数组
     * @return boolean 操作结果
     * @todo 修改收藏信息
     */
    public function editCollect($arr)
    {
        return $this->where('collect_id = ' . $this->collect_id)->save($arr);
    }

    /**
     * 添加收藏
     * @author 姜伟
     * @param array $arr 收藏信息数组
     * @return boolean 操作结果
     * @todo 添加收藏
     */
    public function addCollect($arr)
    {
        if (!is_array($arr)) return false;
		$arr['addtime'] = time();
        $post_obj = new PostModel();
        $post_obj ->fieldInc($arr['post_id'],'collect_num');
        return $this->add($arr);
    }

    /**
     * 删除收藏
     * @author 姜伟
     * @param int $collect_id 收藏ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delCollect($user_id,$post_id)
    {
        if (!$user_id && !$post_id) return false;
        $post_obj = new PostModel();
        $post_obj ->fieldDec($post_id,'collect_num');
        return $this->where('user_id ='.$user_id.' AND post_id ='.$post_id)->delete();
    }

    /**
     * 根据where子句获取收藏数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的收藏数量
     * @todo 根据where子句获取收藏数量
     */
    public function getCollectNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询收藏信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 收藏基本信息
     * @todo 根据SQL查询字句查询收藏信息
     */
    public function getCollectList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 获取收藏列表页数据信息列表
     * @author 姜伟
     * @param array $collect_list
     * @return array $collect_list
     * @todo 根据传入的$collect_list获取更详细的收藏列表页数据信息列表
     */
    public function getListData($collect_list)
    {
		foreach ($collect_list AS $k => $v)
		{
            
            
            $collect_list[$k]['imgs'] = array();
            $imgs = array();
            $imgs = D('PostImg')->getPostImg($v['post_id']);
            if(count($imgs)){
                // $imgs = explode(',', $v['imgs']);
                // $collect_list[$k]['imgs'] = $imgs;

                $collect_list[$k]['img_count'] = count($imgs);
                if(count($imgs) > 6){
                    $new_imgs[0] = $imgs[0];
                    $new_imgs[1] = $imgs[1];
                    $new_imgs[2] = $imgs[2];
                    $new_imgs[3] = $imgs[3];
                    $new_imgs[4] = $imgs[4];
                    $new_imgs[5] = $imgs[5];
                    // $collect_list[$k]['imgs'] = $new_imgs;
                    $imgs = $new_imgs;
                }else{
                    // $collect_list[$k]['imgs'] = $imgs;

                }
                foreach ($imgs as $key => $value) {
                    $imgs[$key] = array('img'=>$value, 'is_show'=>1);
                }
                $collect_list[$k]['imgs'] = $imgs;
            }

            
            //帖子隐藏内容购买人数
            $hide = D('PraiseHide')->getPostHide($v['post_id']);
            $collect_list[$k]['buy_num'] = D('PraiseHideBuy')->getPraiseHideBuyNum($hide['praise_hide_id']);
            //如果赞赏帖主贴显示图片少于6张，则少于部分取隐藏的图片
            $total_img_num = count($collect_list[$k]['imgs']);
            $hide_img_num = 6 - $total_img_num;
            $new_hide_imgs = array();
            if($hide_img_num > 0){
                $hide_imgs = D('PraiseHideImg')->getPraiseHideImg($hide['praise_hide_id']);
                if(count($hide_imgs)){
                    foreach ($hide_imgs as $key => $value) {
                        $new_hide_imgs[] = array('img'=>$value, 'is_show'=>0);
                    }
                }

                $collect_list[$k]['img_count'] += count($hide_imgs);
            }
            $collect_list[$k]['imgs'] = array_merge($collect_list[$k]['imgs'], $new_hide_imgs);

            //帖子信息
            $post_obj = new PostModel();
            $post_info = $post_obj->getPostInfo('post_id ='.$v['post_id']);
            $collect_list[$k]['post_type'] = $post_info['post_type'];//帖子类型暂时没用到
            $collect_list[$k]['addtime'] = get_time($post_info['addtime']);//发帖时间
            $collect_list[$k]['comment_num'] = $post_info['comment_num'];//评论
            $collect_list[$k]['clickdot'] = $post_info['clickdot'];//点击量
            $collect_list[$k]['title'] = $post_info['title'];//标题
            $collect_list[$k]['content'] = $post_info['content'];//内容
            
            $user = M('Users')->where('user_id ='.$post_info['user_id'])->find();
            $user_info['nickname'] = $user['nickname'];
            $user_info['user_id'] = $user['user_id'];
            $user_info['headimgurl'] = $user['headimgurl'];
            $collect_list[$k]['post_user'] = $user_info;
            //用户佩戴中的头衔
            $title_list = D('UserTitle')->getUserTitleList('user_id ='.$post_info['user_id'] .' and is_adorn = 1','adorn_time asc');
            $title_list = D('UserTitle')->getListData($title_list);
            $collect_list[$k]['post_user_title'] = $title_list;
            /* 前台页面暂时没用到．
            if($post_info['post_type'] == PostModel::PRAISE){
                //赞赏价
                $price = D('PraiseHide')->getPostPraisePrice($v['post_id']);
                $collect_list[$k]['praise_price'] = $price;

                //鉴定师评分
                $collect_list[$k]['avg_score'] = round(D('PraiseScore')->getPostAvgScore($v['post_id']), 1);
            }*/

            //频道．
            $channel_obj = new ChannelModel();
            $collect_list[$k]['channel_name'] = $channel_obj->getChannelField($post_info['channel_id'] ,'channel_name');

		}

		return $collect_list;
    }

    public function getTypeField($where = '',$field){
        return $this->where($where)->getField($field, true);
    }


    public function checkCollect($post_id, $user_id){
        $num = $this->where('post_id ='.$post_id. ' and user_id ='.$user_id)->count();
        $r = $num ? true : false;
        return $r;
    }
}
