<?php

class AddressProvinceModel extends Model
{
    public function __construct()
    {
        parent::__construct('address_province');
    }
    public function getProvinceList(){

        return $this->select();
    }
    public function getProvinceName($province_id){

        return $this->where('province_id = '. $province_id)->getField('province_name');
    }

}