<?php
/**
 * 点赞模型类
 */

class PraiseModel extends Model
{
    // 点赞id
    public $praise_id;
    
    const POST = 1;//帖子
    const COMMENT = 2;//评论
    
    /**
     * 构造函数
     * @author 姜伟
     * @param $praise_id 点赞ID
     * @return void
     * @todo 初始化点赞id
     */
    public function PraiseModel($praise_id)
    {
        parent::__construct('praise');

        if ($praise_id = intval($praise_id))
		{
            $this->praise_id = $praise_id;
		}
    }

    /**
     * 获取点赞信息
     * @author 姜伟
     * @param int $praise_id 点赞id
     * @param string $fields 要获取的字段名
     * @return array 点赞基本信息
     * @todo 根据where查询条件查找点赞表中的相关数据并返回
     */
    public function getPraiseInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改点赞信息
     * @author 姜伟
     * @param array $arr 点赞信息数组
     * @return boolean 操作结果
     * @todo 修改点赞信息
     */
    public function editPraise($arr)
    {
        return $this->where('praise_id = ' . $this->praise_id)->save($arr);
    }

    /**
     * 添加点赞
     * @author 姜伟
     * @param array $arr 点赞信息数组
     * @return boolean 操作结果
     * @todo 添加点赞
     */
    public function addPraise($arr)
    {
        if (!is_array($arr)) return false;
        //帖子加1
        if($arr['praise_type'] == PraiseModel::POST){
            $post_obj = new PostModel();
            $post_obj ->fieldInc($arr['id'],'praise_num');
        }
        //评论加1
        if($arr['praise_type'] == PraiseModel::COMMENT){
            $post_comment_obj = new PostCommentModel();
            $post_comment_obj ->fieldInc($arr['id'],'praise_num');
        }

		$arr['addtime'] = time();

        return $this->add($arr);
    }

    /**
     * 删除点赞
     * @author 姜伟
     * @param int $praise_id 点赞ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delPraise($user_id,$id)
    {
        if (!$user_id && $id) return false;
        $praise = $this->where('user_id ='.$user_id.' and id ='.$id)->find();
        //帖子减1
        if($praise['praise_type'] == PraiseModel::POST){
            $post_obj = new PostModel();
            $post_obj ->fieldDec($praise['id'],'praise_num');
        }
        //评论减1
        if($praise['praise_type'] == PraiseModel::COMMENT){
            $post_comment_obj = new PostCommentModel();
            $post_comment_obj ->fieldDec($praise['id'],'praise_num');
        }
        return $this->where( 'user_id ='.$user_id.' and id ='.$id)->delete();
    }

    /**
     * 根据where子句获取点赞数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的点赞数量
     * @todo 根据where子句获取点赞数量
     */
    public function getPraiseNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询点赞信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 点赞基本信息
     * @todo 根据SQL查询字句查询点赞信息
     */
    public function getPraiseList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 获取点赞列表页数据信息列表
     * @author 姜伟
     * @param array $praise_list
     * @return array $praise_list
     * @todo 根据传入的$praise_list获取更详细的点赞列表页数据信息列表
     */
    public function getListData($praise_list)
    {
		foreach ($praise_list AS $k => $v)
		{
		}

		return $praise_list;
    }

    public function getTypeField($where = '',$field){
        return $this->where($where)->getField($field, true);
    }
}
