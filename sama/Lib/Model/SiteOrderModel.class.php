<?php
/**
 * 场地预约预约模型类
 */

class SiteOrderModel extends Model
{

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getSiteOrderNum($where = '') {
        return $this->where($where)->count();
    }




    /**
     * 添加场地预约
     * @author 姜伟
     * @param array $arr_site_order 场地预约数组
     * @return boolean 操作结果
     * @todo 添加场地预约
     */
    public function addSiteOrder($arr_site_order)
    {
        if (!is_array($arr_site_order)) return false;
        return $this->add($arr_site_order);
    }

    /**
     * 删除场地预约
     * @author 姜伟
     * @param string $site_order_id 场地预约ID
     * @return boolean 操作结果
     * @todo 删除场地预约
     */
    public function delSiteOrder($site_order_id)
    {
        if (!is_numeric($site_order_id)) return false;
        return $this->where('site_order_id = ' . $site_order_id)->delete();
    }

//    /**
//     * 删除一级分类下的所有场地预约
//     * @author 姜伟
//     * @param string $class_id 一级分类ID
//     * @return boolean 操作结果
//     * @todo 删除场地预约
//     */
//    public function delClassSiteOrder($class_id)
//    {
//        if (!is_numeric($class_id)) return false;
//        return $this->where('class_id = ' . $class_id)->delete();
//    }

    /**
     * 更改场地预约
     * @author 姜伟
     * @param int $site_order_id 场地预约ID
     * @param array $arr_site_order 场地预约数组
     * @return boolean 操作结果
     * @todo 更改场地预约
     */
    public function setSiteOrder($site_order_id, $arr_site_order)
    {
        if (!is_numeric($site_order_id) || !is_array($arr_site_order)) return false;
        return $this->where('site_order_id = ' . $site_order_id)->save($arr_site_order);
    }

    /**
     * 更改场地预约状态
     * @author 姜伟
     * @param int $site_order_id 场地预约ID
     * @param array $service_providers_status 场地预约数组
     * @return boolean 操作结果
     * @todo 更改场地预约
     */
    public function setSiteOrderStatus($site_order_id,$service_providers_status)
    {
        if (!is_numeric($site_order_id)) return false;
        return $this->where('site_order_id = ' . $site_order_id)->save($service_providers_status);
    }

    /**
     * 获取场地预约
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 场地预约
     * @todo 获取场地预约
     */
    public function getSiteOrderInfo($where, $fields = null)
    {
        return $this->field($fields)->where($where)->find();
    }

    /**
     * 获取场地预约某个字段的信息
     * @author 姜伟
     * @param int $site_order_id 场地预约ID
     * @param string $field 查询的字段名
     * @return array 场地预约
     * @todo 获取场地预约某个字段的信息
     */
    public function getSiteOrderField($site_order_id, $field)
    {
        if (!is_numeric($site_order_id))   return false;
        return $this->where('site_order_id = ' . $site_order_id)->getField($field);
    }

    /**
     * 获取所有场地预约列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 场地预约列表
     * @todo 获取所有场地预约列表
     */
    public function getSiteOrderList($where = null,$order = null)
    {
        return $this->where($where)->order($order)->limit()->select();
    }


    //数字转文字
    public function numDigitalText($status){

        return $this->arr[$status];
    }

    public function payStatus(){
        return array(
          '0'=>'待付款',
          '1'=>'已完成',
          '2'=>'已取消',
          '3'=>'已退款',
        );
    }

    public function getListData($site_order_list){
        $site_obj  = new SiteModel();

        foreach ($site_order_list as $k => $v){
            //所属场地
            $site_info = $site_obj->getSiteInfo('site_id ='.$v['site_id']);
            $site_order_list[$k]['site_name'] =$site_info['site_name'];

            //范围时间
            $range_time_arr = explode(',',$v['range_time']);
            $range_arr= array();
            foreach($range_time_arr as $k1 => $v1){
                $range_arr[] = $v1.':00-'.($v1+1).':00  ';
            }
            $site_order_list[$k]['time_arr'] = $range_arr;

            //支付状态
            $pay_status = self::payStatus();
            $site_order_list[$k]['state'] = $pay_status[$v['pay_status']];

            //封面图
            $pic_arr = explode(',',$site_info['pic']);
            $site_order_list[$k]['pic'] =$pic_arr[0];

            //地址
            $site_order_list[$k]['address'] = $site_info['address'];

            //数量
            $site_order_list[$k]['num'] = count(explode(',',$v['range_time']));
        }
        return $site_order_list;
    }
    /**
     * 生成订单号
     * @author 姜伟
     * @param void
     * @return string $order_sn
     * @todo 查询当天数据库中的订单量$count，订单号即为日期 . ($count + 1)，如201403010099，2014年3月1日的第99个订单
     */
    public function generateOrderSn()
    {
        //当天0点的时间戳
        $timestamp = strtotime(date('Y-m-d', time()));
        //当天订单总量
        $count = $this->where('addtime >=' . $timestamp)->count();
        //后缀位数
        $len = strlen((string) $count);
        //少于4位取4位，否则不变
        $len = $len < 4 ? 4 : $len;
        $suffix = sprintf("%0" . $len . "d", $count);
        $order_sn = date('Ymd') . $suffix;

        return $order_sn;
    }

}
