<?php
/**
 * 文章模型类
 */

class ArticleModel extends Model
{
    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getArticleNum($where = '') {
            return $this->where($where)->count();
    }

    /**
     * 添加文章
     * @author 姜伟
     * @param array $arr_article 官方文章数组
     * @return boolean 操作结果
     * @todo 添加文章
     */
    public function addArticle($arr_article)
    {
        if (!is_array($arr_article)) return false;
        return $this->add($arr_article);
    }

    /**
     * 删除官方文章
     * @author 姜伟
     * @param string $article_id 文章ID
     * @return boolean 操作结果
     * @todo 删除官方文章
     */
    public function delArticle($article_id)
    {
        if (!is_numeric($article_id)) return false;
        return $this->where('article_id = ' . $article_id)->delete();
    }

    /**
     * 更改官方文章
     * @author 姜伟
     * @param int $article_id 文章ID
     * @param array $arr_article 官方文章数组
     * @return boolean 操作结果
     * @todo 更改官方文章
     */
    public function setArticle($article_id, $arr_article)
    {
        if (!is_numeric($article_id) || !is_array($arr_article)) return false;
        return $this->where('article_id = ' . $article_id)->save($arr_article);
    }


    //获取文章
    public function getArticleInfo($where){
        
        return $this->where($where)->find();
    }


    public function editArticle($where, $arr)
    {
        return $this->where($where)->save($arr);
    }

    /**
     * 增加文章收藏字段
     * @author 姜伟
     * @param int $article_id 文章ID
     * @return boolean 操作结果
     * @todo 更改官方文章
     */
    public function addArticleFieldLikeNum($article_id)
    {
        if (!is_numeric($article_id)) return false;
        return $this->where('article_id = ' . $article_id)->setInc('like_num');
    }

    /**
     * 删除文章收藏字段
     * @author 姜伟
     * @param int $article_id 文章ID
     * @return boolean 操作结果
     * @todo 更改官方文章
     */
    public function delArticleFieldLikeNum($article_id)
    {
        if (!is_numeric($article_id)) return false;
        return $this->where('article_id = ' . $article_id)->setDec('like_num');
    }

    /**
     * 增加文章评论字段
     * @author 姜伟
     * @param int $article_id 文章ID
     * @return boolean 操作结果
     * @todo 更改官方文章
     */
    public function addArticleFieldCommentNum($article_id)
    {
        if (!is_numeric($article_id)) return false;
        return $this->where('article_id = ' . $article_id)->setInc('comment_num');
    }

    /**
     * 删除文章评论字段
     * @author 姜伟
     * @param int $article_id 文章ID
     * @return boolean 操作结果
     * @todo 更改官方文章
     */
    public function delArticleFieldCommentNum($article_id)
    {
        if (!is_numeric($article_id)) return false;
        return $this->where('article_id = ' . $article_id)->setDec('comment_num');
    }



    /**
     * 获取官方文章
     * @author 姜伟
     * @param int $article_id 文章ID
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 官方文章
     * @todo 获取官方文章
     */
    public function getArticle($article_id, $fields = null)
    {
        if (!is_numeric($article_id))   return false;
        return $this->field($fields)->where('article_id = ' . $article_id)->find();
    }

    /**
     * 获取官方文章某个字段的信息
     * @author 姜伟
     * @param int $article_id 文章ID
     * @param string $field 查询的字段名
     * @return array 官方文章
     * @todo 获取官方文章某个字段的信息
     */
    public function getArticleField($article_id, $field)
    {
        if (!is_numeric($article_id))   return false;
        return $this->where('article_id = ' . $article_id)->getField($field);
    }

    /**
     * 获取所有官方文章列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 官方文章列表
     * @todo 获取所有官方文章列表
     */
    public function getArticleList($where = null, $limit = null,$order = 'addtime desc')
    {
    	if ($limit) {
    		$_this = $this->limit($limit);
    	}

     return $this->where($where)->order($order)->limit()->select();
    }


    /**
     * 获取文章列表页数据信息列表
     * @author 姜伟
     * @param array $article_list
     * @return array $article_list
     * @todo 根据传入的$article_list获取更详细的商品列表页数据信息列表
     */
    public function getListData($article_list,$user_id=null)
    {

        foreach ($article_list AS $k => $v)
        {
            //栏目名称
            $article_sort_obj = new ArticleSortModel();
            $article_list[$k]['article_sort_name']=$article_sort_obj->getArticleSortField($v['article_sort_id'],'article_sort_name');

            //标签名称
            $topic_sort_obj  = new TopicModel();
            $article_list[$k]['topic_sort_name']= $topic_sort_obj->getOfficialTopicSortField($v['article_sort_id'],'topic_name');

            //创意点子分类
            if($v['article_type'] == ArticleModel::POINT){
                $class = D('CrowdFundingClass')->getCrowdFundingClassInfo('crowd_funding_class_id ='.$v['article_sort_id'], 'class_name');
                $article_list[$k]['article_sort_name'] = $class['class_name'];
            }

            //发布人
            if($v['user_id']){
                $user_obj = D('User');
                $user = $user_obj->getUserInfo('nickname','user_id ='.$v['user_id']);
                $article_list[$k]['nickname'] = $user['nickname']; 
            }
            //查询文章内容
            $article_txt_obj= new ArticleTxtModel();
            $article_list[$k]['contents']=$article_txt_obj->getArticleTxt($v['article_id']);

            $user_obj2 = new UserModel();
            $user_info = $user_obj2->getUserRankInfo($v['user_id']);
            $article_list[$k]['user_info'] = $user_info;

            //查询文章收藏次数
            $like_obj = new LikeModel();
            $article_list[$k]['like_num']=$like_obj->getLikeNum('type_id='.$v['article_id']);

            //查询文章评论次数
            $comment_obj = new CommentModel();
            $article_list[$k]['comment_num'] = $comment_obj->getCommentNum('type_id='.$v['article_id']);


            $article_list[$k]['post_time'] = date('Y-m-d', $v['addtime']);

            //查看是否关注
            $is_focus = $like_obj->getLikeNum('like_type = ' . LikeModel::USER . ' and type_id = ' . $v['user_id'] . ' and user_id =' . $user_id);
            if ($is_focus) {
                $article_list[$k]['is_focus'] = 1;
                if ($user_id == $v['user_id']) {
                    $article_list[$k]['is_focus'] = 2;//关注人与被关注人是同一个不显示关注按钮
                }
            } else {
                $article_list[$k]['is_focus'] = 0;
            }

        }

        return $article_list;
    }
    /**
     * 增加文章点击量
     * @author 徐永
     * @param int $article_id 文章ID
     * @return boolean 操作结果
     * @todo 更改官方文章
     */
    public function addArticleFieldClickdotNum($article_id)
    {
        if (!is_numeric($article_id)) return false;
        return $this->where('article_id = ' . $article_id)->setInc('clickdot');
    }

}
