<?php
/**
 * 场地类型模型类
 */

class SiteTypeModel extends Model
{

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getSiteTypeNum($where = '') {
        return $this->where($where)->count();
    }




    /**
     * 添加场地
     * @author 姜伟
     * @param array $arr_site_type 场地数组
     * @return boolean 操作结果
     * @todo 添加场地
     */
    public function addSiteType($arr_site_type)
    {
        if (!is_array($arr_site_type)) return false;
        return $this->add($arr_site_type);
    }

    /**
     * 删除场地
     * @author 姜伟
     * @param string $site_type_id 场地ID
     * @return boolean 操作结果
     * @todo 删除场地
     */
    public function delSiteType($site_type_id)
    {
        if (!is_numeric($site_type_id)) return false;
        return $this->where('site_type_id = ' . $site_type_id)->delete();
    }

//    /**
//     * 删除一级分类下的所有场地
//     * @author 姜伟
//     * @param string $class_id 一级分类ID
//     * @return boolean 操作结果
//     * @todo 删除场地
//     */
//    public function delClassSiteType($class_id)
//    {
//        if (!is_numeric($class_id)) return false;
//        return $this->where('class_id = ' . $class_id)->delete();
//    }

    /**
     * 更改场地
     * @author 姜伟
     * @param int $site_type_id 场地ID
     * @param array $arr_site_type 场地数组
     * @return boolean 操作结果
     * @todo 更改场地
     */
    public function setSiteType($site_type_id, $arr_site_type)
    {
        if (!is_numeric($site_type_id) || !is_array($arr_site_type)) return false;
        return $this->where('site_type_id = ' . $site_type_id)->save($arr_site_type);
    }

    /**
     * 更改场地状态
     * @author 姜伟
     * @param int $site_type_id 场地ID
     * @param array $service_providers_status 场地数组
     * @return boolean 操作结果
     * @todo 更改场地
     */
    public function setSiteTypeStatus($site_type_id,$service_providers_status)
    {
        if (!is_numeric($site_type_id)) return false;
        return $this->where('site_type_id = ' . $site_type_id)->save($service_providers_status);
    }

    /**
     * 获取场地
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 场地
     * @todo 获取场地
     */
    public function getSiteTypeInfo($where, $fields = null)
    {
        return $this->field($fields)->where($where)->find();
    }

    /**
     * 获取场地某个字段的信息
     * @author 姜伟
     * @param int $site_type_id 场地ID
     * @param string $field 查询的字段名
     * @return array 场地
     * @todo 获取场地某个字段的信息
     */
    public function getSiteTypeField($site_type_id, $field)
    {
        if (!is_numeric($site_type_id))   return false;
        return $this->where('site_type_id = ' . $site_type_id)->getField($field);
    }

    /**
     * 获取所有场地列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 场地列表
     * @todo 获取所有场地列表
     */
    public function getSiteTypeList($where = null,$order = null)
    {
        return $this->where($where)->order($order)->limit()->select();
    }


    //数字转文字
    public function numDigitalText($status){

        return $this->arr[$status];
    }



    public function getListData($service_providers_list){


        $service_type_obj = new ServiceTypeModel();
        $province_obj = new AddressProvinceModel();
        $city_obj = new AddressCityModel();
        foreach ($service_providers_list as $k => $v){

            //区域
            $province_name =$province_obj->getProvinceName($v['province_id']);
            $city_name =$city_obj->getCityName($v['city_id']);
            $area_name =M('AddressArea')->where('area_id ='.$v['area_id'])->getField('area_name');
            $area_string = $province_name.'-'.$city_name.'-'.$area_name;
            $service_providers_list[$k]['area_string'] = $area_string;

            //服务类型
            $service_providers_list[$k]['service_type_name'] = $service_type_obj->getServiceTypeField($v['service_type_id'],'service_type_name');

            //场地状态
            $service_providers_list[$k]['status_name'] = self::numDigitalText($v['service_providers_status']);
        }



        return $service_providers_list;
    }


}
