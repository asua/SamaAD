<?php
/**
 * 频道栏目模型类
 */

class ChannelSortModel extends Model
{
    private $channel_sort_id;
    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($channel_sort_id=0)
    {
        parent::__construct();
        $this->channel_sort_id = $channel_sort_id;
    }

    public function getChannelSortNum($where='') {
      return $this->where($where)->count();
    }

    /**
     * 添加视频分类
     * @author 姜伟
     * @param array $arr_class 视频分类数组
     * @return boolean 操作结果
     * @todo 添加视频分类
     */
    public function addChannelSort($arr_class)
    {
        if (!is_array($arr_class)) return false;
        return $this->add($arr_class);
    }

    /**
     * 删除视频分类
     * @author 姜伟
     * @param string $channel_sort_id 视频分类ID
     * @return boolean 操作结果
     * @todo 删除视频分类
     */
    public function delChannelSort()
    {
        if (!is_numeric($this->channel_sort_id)) return false;
        return $this->where('channel_sort_id = ' . $this->channel_sort_id)->delete();
    }

    /**
     * 更改视频分类
     * @author 姜伟
     * @param int $channel_sort_id 视频分类ID
     * @param array $arr_class 视频分类数组
     * @return boolean 操作结果
     * @todo 更改视频分类
     */
    public function editChannelSort($channel_sort_id, $arr_class)
    {
        if (!is_numeric($channel_sort_id) || !is_array($arr_class)) return false;
        return $this->where('channel_sort_id = ' . $channel_sort_id)->save($arr_class);
    }

    /**
     * 获取视频分类
     * @author 姜伟
     * @param int $channel_sort_id 视频分类ID
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 视频分类
     * @todo 获取视频分类
     */
    public function getChannelSort($channel_sort_id, $fields = null)
    {
        if (!is_numeric($channel_sort_id))   return false;
        return $this->field($fields)->where('channel_sort_id = ' . $channel_sort_id)->find();
    }

    /**
     * 获取视频分类某个字段的信息
     * @author 姜伟
     * @param int $channel_sort_id 视频分类ID
     * @param string $field 查询的字段名
     * @return array 视频分类
     * @todo 获取视频分类某个字段的信息
     */
    public function getChannelSortField($channel_sort_id, $field)
    {
        if (!is_numeric($channel_sort_id))   return false;
        return $this->where('channel_sort_id = ' . $channel_sort_id)->getField($field);
    }

    /**
     *
     */
    public function getChannelPostCash(){

    }

    /**
     * 获取所有视频分类列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 视频分类列表
     * @todo 获取所有视频分类列表
     */
    public function getChannelSortList($where = null)
    {
        return $this->where($where)->order('serial')->limit()->select();
    }

    public function getValidChannelSortList()
    {
        return $this->where('isuse = 1')->order('serial')->limit(1000000)->select();
    }
 
    /**
     * 获取分类信息
     * @author 姜伟
     * @param string $where where子句
     * @param string $fields 要获取的字段名
     * @return array 商品基本信息
     * @todo 根据where查询条件查找商品表中的相关数据并返回
     */
    public function getChannelSortInfo($where, $fields = '')
    {
        return $this->field($fields)->where($where)->find();
    }

}
