<?php
/**
 * 话题模型类
 */

class TopicModel extends Model
{
    // 话题id
    public $topic_id;
   
    /**
     * 构造函数
     * @author 姜伟
     * @param $topic_id 话题ID
     * @return void
     * @todo 初始化话题id
     */
    public function TopicModel($topic_id)
    {
        parent::__construct('topic');

        if ($topic_id = intval($topic_id))
		{
            $this->topic_id = $topic_id;
		}
    }

    /**
     * 获取话题信息
     * @author 姜伟
     * @param int $topic_id 话题id
     * @param string $fields 要获取的字段名
     * @return array 话题基本信息
     * @todo 根据where查询条件查找话题表中的相关数据并返回
     */
    public function getTopicInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改话题信息
     * @author 姜伟
     * @param array $arr 话题信息数组
     * @return boolean 操作结果
     * @todo 修改话题信息
     */
    public function editTopic($arr)
    {
        return $this->where('topic_id = ' . $this->topic_id)->save($arr);
    }

    /**
     * 添加话题
     * @author 姜伟
     * @param array $arr 话题信息数组
     * @return boolean 操作结果
     * @todo 添加话题
     */
    public function addTopic($arr)
    {
        if (!is_array($arr)) return false;

		$arr['addtime'] = time();

        return $this->add($arr);
    }

    /**
     * 删除话题
     * @author 姜伟
     * @param int $topic_id 话题ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delTopic($where)
    {
        return $this->where($where)->save(array('is_del' => 1));
    }

    /**
     * 根据where子句获取话题数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的话题数量
     * @todo 根据where子句获取话题数量
     */
    public function getTopicNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询话题信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 话题基本信息
     * @todo 根据SQL查询字句查询话题信息
     */
    public function getTopicList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        if($orderby){
            $orderby = 'serial asc, '.$orderby;
        }
        return $this->field($fields)->where($where)->join($join)->order($orderby)->limit()->select();
    }

    /**
     * 获取话题列表页数据信息列表
     * @author 姜伟
     * @param array $topic_list
     * @return array $topic_list
     * @todo 根据传入的$topic_list获取更详细的话题列表页数据信息列表
     */
    public function getListData($topic_list)
    {
        foreach ($topic_list as $k => $v) {
            $user_obj = new UserModel($v['user_id']);
            $user = $user_obj->getUserInfo('nickname');
            $topic_list[$k]['nickname'] = $user['nickname'];

            $topic_list[$k]['addtime'] = date('Y-m-d H:i:s', $v['addtime']);

            //是否收藏
            $collect_obj = D('Collect');
            $is_collect = $collect_obj->getCollectNum('type_id ='.$v['topic_id'].' and user_id ='.session('user_id'));
            if($is_collect){
                $topic_list[$k]['is_collect'] = 1;
            }else{
                $topic_list[$k]['is_collect'] = 0;
            }
        }

		return $topic_list;
    }

    //增加观点数
    public function addViewNum($topic_id){
        return $this->where('topic_id ='.$topic_id)->setInc('view_num');
    }

    //增加收藏数
    public function addCollectNum($topic_id){
        return $this->where('topic_id ='.$topic_id)->setInc('collect_num');
    }

    public function setFieldInc($field, $where = ''){
        return $this->where($where)->setDec($field);
    }
    public function setFieldDec($field, $where = ''){
        return $this->where($where)->setDec($field);
    }
}
