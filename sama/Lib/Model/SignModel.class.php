<?php
/**
 * 签到模型类
 */

class SignModel extends Model
{
    private $sign_id;
    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($sign_id=0)
    {
        parent::__construct();
        $this->sign_id = $sign_id;
    }

    /**
     * 签到
     * @param [type] $arr [description]
     */
    public function addSign($user_id, $channel_id){
        if(!$user_id || !$channel_id) return false;
        $exp_num = $GLOBALS['config_info']['CHANNEL_SIGN_EXP_NUM'];
        $arr = array(
            'user_id' => $user_id,
            'channel_id' => $channel_id,
            'exp_num' => $exp_num,
            'addtime' => date('Ymd'),
            );
        
        $r = $this->add($arr);
        if($r){
            //加经验
            D('UserChannelRank')->addExp($arr['user_id'], $arr['channel_id'], $arr['exp_num']);
        }

        return $r;
    }

    //一键签到
    public function allChannelSign($user_id, $channel_ids){
        if(!$user_id || !$channel_ids) return false;
        $success = 0;
        foreach ($channel_ids as $k => $v) {
            $r = $this->addSign($user_id, $v);
            if($r) $success++;
        }

        return $success ? true : false;
    }

    public function getSignNum($where){
        return $this->where($where)->count();
    }


    //判断用户是否已签到
    public function checkSign($user_id, $channel_id){
        $r = $this->where('user_id ='.$user_id. ' and channel_id ='.$channel_id. ' and addtime ='.date('Ymd'))->count();
        return $r;
    }

    //判断是否可一键签到
    public function checkAllSign($user_id){
        $r = D('Channel')->getNotSignChannel($user_id);
        return $r ? true : false;
    }


    //获取用户已签到的频道id
    public function getSignedChannel($user_id){
        $r = $this->where('user_id ='.$user_id . ' and addtime ='.date('Ymd'))->limit(10000)->getField('channel_id', true);
        $r[] = 0;
        return $r;
    }
}
