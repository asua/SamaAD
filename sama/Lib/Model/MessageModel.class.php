<?php
/**
 * 消息模型类
 */

class MessageModel extends Model
{
    // 消息id
    public $message_id; 


    //类型
    const REPLY = 1;//用户回复-帖子
    const AT = 2;//@
    const PRIVATE_LETTER = 3;//私信
    const POST = 4;//帖子相关
    const TITLE = 5;//头衔相关
    const REWARD = 6;//悬赏相关
    const CF = 7;//心愿相关
    const PRAISE = 8;//赞赏相关
    //wangtao 2017.9.15 -strat
    const SELECTED = 9;//推荐相关
    //wangtao 2017.9.15 -end
    /**
     * 构造函数
     * @author 姜伟
     * @param $message_id 消息ID
     * @return void
     * @todo 初始化消息id
     */
    public function MessageModel($message_id)
    {
        parent::__construct('message');

        if ($message_id = intval($message_id))
		{
            $this->message_id = $message_id;
		}
    }

    /**
     * 获取消息信息
     * @author 姜伟
     * @param int $message_id 消息id
     * @param string $fields 要获取的字段名
     * @return array 消息基本信息
     * @todo 根据where查询条件查找消息表中的相关数据并返回
     */
    public function getMessageInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 添加消息
     * @author 姜伟
     * @param array $arr 消息信息数组
     * @return boolean 操作结果
     * @todo 添加消息
     */
    public function addMessage($id = 0, $message_type = 0, $send_user_id = 0, $reply_user, $title, $content, $message_link = '')
    {   
        // if( ($message_type == 4 || $message_type == 5 || $message_type == 6) && $reply_user != 62146){
        //     return;
        // }
       $data = array(
            'id' => $id,
            'message_type' => $message_type,
            'send_user_id' => $send_user_id,
            'message_contents' => $content,
            'message_link' => $message_link,
            'addtime' => time(),
            'message_title' => $title
            );

        $type_list = $this->messageTypeList();
        $m_type = $type_list[$message_type];

        if($message_type == 0){
            if($this->add($data)){
                $push_obj = new PushModel();
                $title = filterAndSubstr(htmlspecialchars_decode($title));
                $push_obj->jpush_all($title, $m_type, $message_link);
                #dump($r);
                return true;
            }
        }else{
            $user_obj = new UserModel($send_user_id);
            $user_info = $user_obj->where('user_id ='.$send_user_id)->find();
            if($user_info){
                $data['send_username'] = $user_info['nickname'];
            }else{
                $data['send_username'] = '';
            }
            
            if(!is_array($reply_user)){
                $reply_user = explode(',', $reply_user);
            }
            

            $count = 0;
            foreach ($reply_user as $v) {
                $data['reply_user_id'] = $v;
                $user_obj2 = new UserModel($v);
                $user_info2 = $user_obj2->where('user_id ='.$v)->find();
                $data['reply_username'] = $user_info2['nickname'];
                if($this->add($data)){
                    $count++;
                }
            }

            if($count){
                $push_obj = new PushModel();
                $title = filterAndSubstr(htmlspecialchars_decode($title));
                $push_obj->jpush_users($title, $reply_user, $m_type, $message_link);
                return true;
            }
        }
    }


    //消息类型
    private function messageTypeList(){
        return array(
            1=> 'reply',
            2=> 'at', 
            3=> 'private_letter' ,
            4=> 'post',
            5=> 'title',
            6=> 'reward',
            7=> 'cf',
            );
    }

    /**
     * 删除消息
     * @author 姜伟
     * @param int $message_id 消息ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delMessage($message_id)
    {
        if (!is_numeric($message_id)) return false;
        return $this->where('message_id = ' . $message_id)->save(array('isuse' => 2));
    }

    /**
     * 根据where子句获取消息数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的消息数量
     * @todo 根据where子句获取消息数量
     */
    public function getMessageNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询消息信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 消息基本信息
     * @todo 根据SQL查询字句查询消息信息
     */
    public function getMessageList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }


    //获取系统消息列表
    public function getSystemMessageList($where){
        return $this->where($where .' and (message_type ='.self::POST . ' OR message_type = '.self::TITLE . ' OR message_type ='.self::REWARD . ' OR message_type ='.self::CF . ' OR message_type ='.self::PRAISE .')')->order('addtime desc')->limit()->select();
    }

    public function getSystemMessageNum($where){
        return $this->where($where .' and (message_type ='.self::POST . ' OR message_type = '.self::TITLE . ' OR message_type ='.self::REWARD . ' OR message_type ='.self::CF . ' OR message_type ='.self::PRAISE .')')->count();
    }

    /**
     * 修改消息
     * @author 姜伟
     * @param array $arr 消息信息数组
     * @return boolean 操作结果
     * @todo 修改消息信息
     */
    public function editMessage($arr)
    {
        return $this->where('message_id = ' . $this->message_id)->save($arr);
    }

    public function getListData($message_list){

        foreach ($message_list as $k => $v){


        }
        return $message_list;
    }


    public function setRead($user_id, $message_type){
        return $this->where('reply_user_id ='.$user_id .' and is_read = 0 and message_type ='.$message_type)->save(array('is_read'=>1));
    }

    public function setReadSystem($user_id){
        return $this->where('reply_user_id ='.$user_id .' and is_read = 0 and (message_type ='.self::POST . ' OR message_type = '.self::TITLE . ' OR message_type ='.self::REWARD. ' OR message_type ='.self::CF. ' OR message_type ='.self::PRAISE .')')->save(array('is_read'=>1));
    }
}
