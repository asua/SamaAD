<?php
/**
 * 工位模型类
 */

class StationModel extends Model
{
    // 工位id
    public $station_id;
   
    /**
     * 构造函数
     * @author 姜伟
     * @param $station_id 工位ID
     * @return void
     * @todo 初始化工位id
     */
    public function StationModel($station_id)
    {
        parent::__construct('station_type');

        if ($station_id = intval($station_id))
		{
            $this->station_id = $station_id;
		}
    }

    /**
     * 获取工位信息
     * @author 姜伟
     * @param int $station_id 工位id
     * @param string $fields 要获取的字段名
     * @return array 工位基本信息
     * @todo 根据where查询条件查找工位表中的相关数据并返回
     */
    public function getStationInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改工位信息
     * @author 姜伟
     * @param array $arr 工位信息数组
     * @return boolean 操作结果
     * @todo 修改工位信息
     */
    public function editStation($arr)
    {
        return $this->where('station_type_id = ' . $this->station_id)->save($arr);
    }

    /**
     * 添加工位
     * @author 姜伟
     * @param array $arr 工位信息数组
     * @return boolean 操作结果
     * @todo 添加工位
     */
    public function addStation($arr)
    {
        if (!is_array($arr)) return false;

        return $this->add($arr);
    }

    /**
     * 删除工位
     * @author 姜伟
     * @param int $station_id 工位ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delStation($where)
    {
        if (!$where) return false;
        return $this->where($where)->delete();
    }

    /**
     * 根据where子句获取工位数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的工位数量
     * @todo 根据where子句获取工位数量
     */
    public function getStationNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句获取工位的某一字段
     * @author 姜伟
     * @param string $where where子句
     * @todo 根据where子句获取某一字段
     */
    public function getStationField($where = '',$field='')
    {
        return $this->where($where)->getField($field);
    }

    /**
     * 根据where子句获取工位的某一字段
     * @author 姜伟
     * @param string $where where子句
     * @todo 根据where子句获取某一字段
     */
    public function getStationFields($where = '',$fields='')
    {
        return $this->where($where)->getField($fields,true);
    }

    /**
     * 根据where子句查询工位信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 工位基本信息
     * @todo 根据SQL查询字句查询工位信息
     */
    public function getStationList($fields = '', $where = '', $orderby = 'sort asc, station_class_id asc', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 连表查询
     */
    public function getStationJoinList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        $data = M('incubator as a')->join('tp_incubator_order as b on b.incubator_id = a.incubator_id')->where($where)->order($orderby)->select();
        return $data;
    }

    /**
     * 获取工位列表页数据信息列表
     * @author 姜伟
     * @param array $station_list
     * @return array $station_list
     * @todo 根据传入的$station_list获取更详细的工位列表页数据信息列表
     */
    public function getListData($station_list)
    {
		foreach ($station_list AS $k => $v)
		{
            $incubator_obj = D('Incubator');
            $incubator = $incubator_obj->getIncubatorInfo('incubator_id ='.$v['incubator_id']);
            $station_list[$k]['incubator_name'] = $incubator['incubator_name'];

            //工位类型
            if($v['station_class_id']){
                $class = M('StationClass')->where('station_class_id ='.$v['station_class_id'])->find();
                $station_list[$k]['class_name'] = $class['class_name'];

                //工位价格
                $station_list[$k]['price'] = $class['price'];
            }
		}

		return $station_list;
    }



    //根据id计算总价
    public function sumPrice($station_ids){
        $sum_price = 0.0;
        $arr =$this->field('count(*) as class_num, station_class_id')->where('station_type_id in('.implode(',',$station_ids).')')->select();
        foreach ($arr as $k => $v) {
            $price = M('StationClass')->where('station_class_id ='.$v['station_class_id'])->getField('price');
            $sum_price += $price * $v['class_num'];
        }
        return $sum_price;
    }


    public function sumYqPrice($station_ids){
        return $this->where('station_type_id in('.implode(',',$station_ids).')')->sum('price');
    }

    /**
     * 统计的方法．
     */
    public function get_statistical_list($where = '',$group = '',$field= ''){

        return $this->field($field)->where($where)->group($group)->select();
    }



    //获取工位号
    public function get_station_no($storey_id, $incubator_id, $station_class){
        return $this->field('station_type_id, station_num, price')->where('incubator_id ='.$incubator_id . ' and storey_id ='.$storey_id.' and station_class_id ='.$station_class)->select();
    }
}
