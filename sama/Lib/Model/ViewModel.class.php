<?php
/**
 * 观点/动态模型类
 */

class ViewModel extends Model
{
    // 观点/动态id
    public $view_id;
   
    const TOPIC = 1;
    const DYNAMIC = 2;

    /**
     * 构造函数
     * @author 姜伟
     * @param $view_id 观点/动态ID
     * @return void
     * @todo 初始化观点/动态id
     */
    public function ViewModel($view_id)
    {
        parent::__construct('view');

        if ($view_id = intval($view_id))
		{
            $this->view_id = $view_id;
		}
    }

    /**
     * 获取观点/动态信息
     * @author 姜伟
     * @param int $view_id 观点/动态id
     * @param string $fields 要获取的字段名
     * @return array 观点/动态基本信息
     * @todo 根据where查询条件查找观点/动态表中的相关数据并返回
     */
    public function getViewInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改观点/动态信息
     * @author 姜伟
     * @param array $arr 观点/动态信息数组
     * @return boolean 操作结果
     * @todo 修改观点/动态信息
     */
    public function editView($arr)
    {
        return $this->where('view_id = ' . $this->view_id)->save($arr);
    }

    /**
     * 添加观点/动态
     * @author 姜伟
     * @param array $arr 观点/动态信息数组
     * @return boolean 操作结果
     * @todo 添加观点/动态
     */
    public function addView($arr)
    {
        if (!is_array($arr)) return false;

		$arr['addtime'] = time();

        //如果是话题观点，则增加话题观点数
        if($arr['view_type'] == self::TOPIC){
            $topic_obj = D('Topic');
            $topic_obj->addViewNum($arr['topic_id']);
        }

        return $this->add($arr);
    }

    /**
     * 删除观点/动态
     * @author 姜伟
     * @param int $view_id 观点/动态ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delView($where)
    {
        return $this->where($where)->save(array('is_del' => 1));
    }

    /**
     * 根据where子句获取观点/动态数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的观点/动态数量
     * @todo 根据where子句获取观点/动态数量
     */
    public function getViewNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询观点/动态信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 观点/动态基本信息
     * @todo 根据SQL查询字句查询观点/动态信息
     */
    public function getViewList($fields = '', $where = '', $orderby = '', $limit = '')
    {
        if($orderby){
            $orderby = 'serial asc, '.$orderby;
        }
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 获取观点/动态列表页数据信息列表
     * @author 姜伟
     * @param array $view_list
     * @return array $view_list
     * @todo 根据传入的$view_list获取更详细的观点/动态列表页数据信息列表
     */
    public function getListData($view_list)
    {
        foreach ($view_list as $k => $v) {
            $user_obj = new UserModel($v['user_id']);
            $user = $user_obj->getUserInfo('nickname,realname, headimgurl,position');
            $view_list[$k]['realname'] = $user['realname'];
            $view_list[$k]['headimgurl'] = $user['headimgurl'];
            $view_list[$k]['position'] = $user['position'];

            $view_list[$k]['addtime'] = date('Y-m-d H:i:s', $v['addtime']);

            //评论
            if($v['view_type'] == self::TOPIC){
                $comment_type = CommentModel::TOPIC;
            }elseif($v['view_type'] == self::DYNAMIC){
                $comment_type = CommentModel::DYNAMIC;
            }
            $comment_obj = D('Comment');
            $comment_list = $comment_obj->getCommentList('', 'comment_type = 2 and type_id ='.$v['view_id'], '');
            $comment_list = $comment_obj->getListData($comment_list);
            $view_list[$k]['comment_list'] = $comment_list;
            $imgs = array();
            if($v['imgs']){
                $imgs = explode(',', $v['imgs']);
            }
            $view_list[$k]['imgs'] = count($imgs) ? $imgs : '';

            //是否点赞
            $praise_obj = D('Praise');
            $is_praise = $praise_obj->getPraiseNum('id ='.$v['view_id'].' and user_id ='.session('user_id'));
            if($is_praise){
                $view_list[$k]['is_praise'] = 1;
            }else{
                $view_list[$k]['is_praise'] = 0;
            }

        }
		return $view_list;
    }

        //方法将数字字段值增加。
    public function setFieldInc($field, $where = ''){
        return $this->where($where)->setInc($field);
    }

    //方法将数字字段值减少。
    public function setFieldDEC($field, $where = ''){
        return $this->where($where)->setDec($field);
    }
}
