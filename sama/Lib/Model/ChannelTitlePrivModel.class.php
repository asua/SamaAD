<?php

class ChannelTitlePrivModel extends Model{

	public function __construct()
    {
        parent::__construct();
    }

    public function setChannelTitlePriv($channel_id, $arr){
    	$priv = $this->getChannelTitlePriv($channel_id);
        if($priv){
            $this->where('channel_id ='.$channel_id)->save($arr);
        }else{
            $arr['channel_id'] = $channel_id;
            $this->add($arr);
        }
    }


    public function getChannelTitlePriv($channel_id){
    	return $this->where('channel_id ='.$channel_id)->find();
    }


    public function getVisitPriv($channel_id){
        $priv = $this->where('channel_id ='.$channel_id)->getField('visit_title_ids');
        $priv = $priv ? $priv : '';
        $priv = explode(',', $priv);
        return $priv;
    }

    public function getPostPriv($channel_id){
        $priv = $this->where('channel_id ='.$channel_id)->getField('post_title_ids');
        $priv = $priv ? $priv : '';
        $priv = explode(',', $priv);
        return $priv;
    }


    public function checkVisitPriv($user_id, $channel_id){

        $channel = D('Channel')->getChannelInfo('channel_id = '.$channel_id);

        if($channel['all_visit']){
            return true;
        }

        //用户的头衔
        $titls_ids = D('UserTitle')->getUserTitleFields('user_id ='.$user_id, 'title_id');

        //频道访问权限
        $visit_priv = $this->getVisitPriv($channel_id);

        $r = array_intersect($titls_ids, $visit_priv);
        if(count($r) > 0){
            return true;
        }else{
            return false;
        }
        // dump($titls_ids);
        // dump($visit_priv);
        // dump($r);die;
    }

    public function checkPostPriv($user_id, $channel_id){

        $channel = D('Channel')->getChannelInfo('channel_id = '.$channel_id);
        if($channel['all_post']){
            return true;
        }

        //用户的头衔
        $titls_ids = D('UserTitle')->getUserTitleFields('user_id ='.$user_id, 'title_id');

        //频道访问权限
        $visit_priv = $this->getPostPriv($channel_id);

        $r = array_intersect($titls_ids, $visit_priv);
        if(count($r) > 0){
            return true;
        }else{
            return false;
        }
        // dump($titls_ids);
        // dump($visit_priv);
        // dump($r);die;
    }
}
