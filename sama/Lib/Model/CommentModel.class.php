<?php
/**
 * 评论模型类
 */

class CommentModel extends Model
{
    // 评论id
    public $comment_id;
    const TOPIC = 1;//话题
    const DYNAMIC = 2;//动态、观点
    const ACTIVITY = 3; //活动
    const PROJECT = 4;//项目
    /**
     * 构造函数
     * @author 姜伟
     * @param $comment_id 评论ID
     * @return void
     * @todo 初始化评论id
     */
    public function CommentModel($comment_id)
    {
        parent::__construct('comment');

        if ($comment_id = intval($comment_id))
		{
            $this->comment_id = $comment_id;
		}
    }

    /**
     * 获取评论信息
     * @author 姜伟
     * @param int $comment_id 评论id
     * @param string $fields 要获取的字段名
     * @return array 评论基本信息
     * @todo 根据where查询条件查找评论表中的相关数据并返回
     */
    public function getCommentInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }

    /**
     * 修改评论信息
     * @author 姜伟
     * @param array $arr 评论信息数组
     * @return boolean 操作结果
     * @todo 修改评论信息
     */
    public function editComment($arr)
    {
        return $this->where('comment_id = ' . $this->comment_id)->save($arr);
    }

    /**
     * 添加评论
     * @author 姜伟
     * @param array $arr 评论信息数组
     * @return boolean 操作结果
     * @todo 添加评论
     */
    public function addComment($arr)
    {
        if (!is_array($arr)) return false;

		$arr['addtime'] = time();

        return $this->add($arr);
    }

    /**
     * 删除评论
     * @author 姜伟
     * @param int $comment_id 评论ID
     * @return boolean 操作结果
     * @todo is_del设为1
     */
    public function delComment($where)
    {
        if (!$where) return false;
        return $this->where($where)->delete();
    }

    /**
     * 根据where子句获取评论数量
     * @author 姜伟
     * @param string|array $where where子句
     * @return int 满足条件的评论数量
     * @todo 根据where子句获取评论数量
     */
    public function getCommentNum($where = '')
    {
        return $this->where($where)->count();
    }

    /**
     * 根据where子句查询评论信息
     * @author 姜伟
     * @param string $fields
     * @param string $where
     * @param string $orderby
     * @param string $limit
     * @return array 评论基本信息
     * @todo 根据SQL查询字句查询评论信息
     */
    public function getCommentList($fields = '', $where = '', $orderby = 'addtime desc', $limit = '')
    {
        return $this->field($fields)->where($where)->order($orderby)->limit()->select();
    }

    /**
     * 获取评论列表页数据信息列表
     * @author 姜伟
     * @param array $comment_list
     * @return array $comment_list
     * @todo 根据传入的$comment_list获取更详细的评论列表页数据信息列表
     */
    public function getListData($comment_list)
    {
		foreach ($comment_list AS $k => $v)
		{
			
            $user = M('Users')->where('user_id ='.$v['user_id'])->find();
            $comment_list[$k]['realname'] = $user['realname'] ? $user['realname'] : $user['nickname'];
            $comment_list[$k]['headimgurl'] = $user['headimgurl'];
            $comment_list[$k]['addtime'] = date('Y-m-d H:i:s', $v['addtime']);

            //被回复人
            

		}

		return $comment_list;
    }
}
