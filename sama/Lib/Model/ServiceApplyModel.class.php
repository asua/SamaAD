<?php
/**
 * 服务预约预约模型类
 */

class ServiceApplyModel extends Model
{

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getServiceApplyNum($where = '') {
        return $this->where($where)->count();
    }




    /**
     * 添加服务预约
     * @author 姜伟
     * @param array $arr_service_apply 服务预约数组
     * @return boolean 操作结果
     * @todo 添加服务预约
     */
    public function addServiceApply($arr_service_apply)
    {
        if (!is_array($arr_service_apply)) return false;
        return $this->add($arr_service_apply);
    }

    /**
     * 删除服务预约
     * @author 姜伟
     * @param string $service_apply_id 服务预约ID
     * @return boolean 操作结果
     * @todo 删除服务预约
     */
    public function delServiceApply($service_apply_id)
    {
        if (!is_numeric($service_apply_id)) return false;
        return $this->where('service_apply_id = ' . $service_apply_id)->delete();
    }

//    /**
//     * 删除一级分类下的所有服务预约
//     * @author 姜伟
//     * @param string $class_id 一级分类ID
//     * @return boolean 操作结果
//     * @todo 删除服务预约
//     */
//    public function delClassServiceApply($class_id)
//    {
//        if (!is_numeric($class_id)) return false;
//        return $this->where('class_id = ' . $class_id)->delete();
//    }

    /**
     * 更改服务预约
     * @author 姜伟
     * @param int $service_apply_id 服务预约ID
     * @param array $arr_service_apply 服务预约数组
     * @return boolean 操作结果
     * @todo 更改服务预约
     */
    public function setServiceApply($service_apply_id, $arr_service_apply)
    {
        if (!is_numeric($service_apply_id) || !is_array($arr_service_apply)) return false;
        return $this->where('service_apply_id = ' . $service_apply_id)->save($arr_service_apply);
    }

    /**
     * 更改服务预约状态
     * @author 姜伟
     * @param int $service_apply_id 服务预约ID
     * @param array $service_providers_status 服务预约数组
     * @return boolean 操作结果
     * @todo 更改服务预约
     */
    public function setServiceApplyStatus($service_apply_id,$service_providers_status)
    {
        if (!is_numeric($service_apply_id)) return false;
        return $this->where('service_apply_id = ' . $service_apply_id)->save($service_providers_status);
    }

    /**
     * 获取服务预约
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 服务预约
     * @todo 获取服务预约
     */
    public function getServiceApplyInfo($where, $fields = null)
    {
        return $this->field($fields)->where($where)->find();
    }

    /**
     * 获取服务预约某个字段的信息
     * @author 姜伟
     * @param int $service_apply_id 服务预约ID
     * @param string $field 查询的字段名
     * @return array 服务预约
     * @todo 获取服务预约某个字段的信息
     */
    public function getServiceApplyField($service_apply_id, $field)
    {
        if (!is_numeric($service_apply_id))   return false;
        return $this->where('service_apply_id = ' . $service_apply_id)->getField($field);
    }

    /**
     * 获取所有服务预约列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 服务预约列表
     * @todo 获取所有服务预约列表
     */
    public function getServiceApplyList($where = null,$order = null)
    {
        return $this->where($where)->order($order)->limit()->select();
    }


    //数字转文字
    public function numDigitalText($status){

        return $this->arr[$status];
    }



    public function getListData($ServiceApply_list){


//        $service_type_obj = new ServiceTypeModel();
//        $province_obj = new AddressProvinceModel();
//        $city_obj = new AddressCityModel();
        $service_providers_obj = new ServiceProvidersModel();
        $project_report_obj = new ProjectReportModel();
        $service_type_obj = new ServiceTypeModel();
        $service_type_id = $service_type_obj -> getServiceTypeFieldId('service_label="xiangmushenbao"','service_type_id');
        foreach ($ServiceApply_list as $k => $v){
            //区域
//            $province_name =$province_obj->getProvinceName($v['province_id']);
//            $city_name =$city_obj->getCityName($v['city_id']);
//            $area_name =M('AddressArea')->where('area_id ='.$v['area_id'])->getField('area_name');
//            $area_string = $province_name.'-'.$city_name.'-'.$area_name;
//            $service_providers_list[$k]['area_string'] = $area_string;
            if($v['service_type_id']==$service_type_id){
                //项目申报
                $project_report_name = $project_report_obj -> getProjectReportField($v['service_providers_id'],'project_report_name');
                $their_name  = '项目申报'.'-'.$project_report_name;
            }else{
                //所属服务商
                $providers_name = $service_providers_obj->getServiceProvidersField($v['service_providers_id'],'service_providers_name');
                //所属导师
                $mentor_name = $service_providers_obj->getServiceProvidersField($v['service_providers_id'],'user_name');
                $their_name  = $providers_name ? $providers_name : $mentor_name;
            }

            //如果有服务商显示服务商名称,不然显示导师名称
            $ServiceApply_list[$k]['their_name'] = $their_name;

            //服务状态
            $apply_status_arr = array(
                '0'=>'待服务',
                '1'=>'服务中',
                '2'=>'已服务',
                '3'=>'已取消',
            );
            $ServiceApply_list[$k]['status'] = $apply_status_arr[$v['apply_status']];

            //取出服务商类型  0,服务商 1,导师
            $ServiceApply_list[$k]['service_type'] = $service_providers_obj->getServiceProvidersField($v['service_providers_id'],'service_providers_type');
        }
        return $ServiceApply_list;
    }


    public function getApplyField($field, $where){
        return $this->where($where)->getField($field, true);
    }
}
