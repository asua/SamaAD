<?php
/**
 * 孵化器模型类
 */

class IncubatorModel extends Model
{


    public function IncubatorModel()
    {
        parent::__construct('incubator');

    }

    //单条信息
    public function getIncubatorInfo($where, $fields = '')
    {
        return $this->field($fields)->where($where)->find();
    }
    //获取列表
    public function getIncubatorList($fields = '', $where = '', $orderby = '', $groupby = '')
    {
        return $this->field($fields)->where($where)->group($groupby)->order($orderby)->limit()->select();
    }
    //获取数量
    public function getIncubatorNum($where='')
    {
        $count = $this->where($where)->count();
        return $count;
    }
    //增加孵化器
    public function addIncubator($arr){
        return $this->add($arr);
    }

    //获取列表的信息
    public function getIncubatorData($incubator_list){
        if(!is_array($incubator_list))
        {
            return false;
        }
        $province_obj = new AddressProvinceModel();
        $city_obj = new AddressCityModel();
        foreach($incubator_list as $k=>$v){
            $incubator_list[$k]['province']=$province_obj->getProvinceName($v['province_id']);
            $incubator_list[$k]['city']=$city_obj->getCityName($v['city_id']);
            $picarr=explode(',',$v['pic_url']);
            $incubator_list[$k]['first_pic']= $picarr[0];
        }
        return $incubator_list;
    }
    //删除信息
    public function delIncubator($incubator_id){
        return $this->where('incubator_id = '.$incubator_id)->delete();
    }
    //修改信息
    public function editIncubator($arr,$incubator_id){
        return $this->where('incubator_id = ' . $incubator_id)->save($arr);
    }
    //查询园区（空间）最多的城市
    public function getAppearMostCity($where){
        /*select cityid from 表 group by cityid  order by sum(cityid) desc;*/
        return $this->where($where)->group('city_id')->order('sum(city_id) DESC')->select();
    }

    //查询孵化器的ID
    public function getIdFields($field,$where=null){
        return $this->where($where)->getField($field, true);
    }

    //查询孵化器单个条件
    public function getIncubatorField($field,$where=null){
        return $this->where($where)->getField($field);
    }

    //查询孵化器单个条件数组
    public function getIncubatorFields($field,$where=null){
        return $this->where($where)->getField($field,true);
    }

    public function getGroupIdFields($field,$where=null,$group=null){
        return $this->where($where)->group($group)->getField($field, true);
    }

    //获取所有园区列表
    public function getIncubatorParkList($fields = '', $orderby = '', $groupby = '')
    {
        return $this->field($fields)->where('incubator_type = 1')->group($groupby)->order($orderby)->limit(100000)->select();
    }

    //获取所有园区列表
    public function getIncubatorZhongcList($fields = '', $orderby = '', $groupby = '')
    {
        return $this->field($fields)->where('incubator_type = 2')->group($groupby)->order($orderby)->limit(100000)->select();
    }



}
