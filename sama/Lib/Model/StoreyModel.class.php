<?php
/**
 * 楼层类模型
 */

Class StoreyModel extends Model
{
    private $storey_id;
    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct($storey_id=0)
    {
        parent::__construct();
        $this->storey_id = $storey_id;
    }

    public function getStoreyNum($where='') {
      return $this->where($where)->count();
    }

    /**
     * 添加楼层
     * @author 姜伟
     * @param array $arr_Storey 楼层数组
     * @return boolean 操作结果
     * @todo 添加楼层
     */
    public function addStorey($arr_Storey)
    {
        if (!is_array($arr_Storey)) return false;
        return $this->add($arr_Storey);
    }

    /**
     * 删除楼层
     * @author 姜伟
     * @param string $storey_id 楼层ID
     * @return boolean 操作结果
     * @todo 删除楼层
     */
    public function delStorey()
    {
        if (!is_numeric($this->storey_id)) return false;
        return $this->where('storey_id = ' . $this->storey_id)->delete();
    }

    /**
     * 更改楼层
     * @author 姜伟
     * @param int $storey_id 楼层ID
     * @param array $arr_Storey 楼层数组
     * @return boolean 操作结果
     * @todo 更改楼层
     */
    public function setStorey($storey_id, $arr_Storey)
    {
        if (!is_numeric($storey_id) || !is_array($arr_Storey)) return false;
        return $this->where('storey_id = ' . $storey_id)->save($arr_Storey);
    }

    /**
     * 获取楼层
     * @author 姜伟
     * @param int $storey_id 楼层ID
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 楼层
     * @todo 获取楼层
     */
    public function getStorey($storey_id, $fields = null)
    {
        if (!is_numeric($storey_id))   return false;
        return $this->field($fields)->where('storey_id = ' . $storey_id)->find();
    }

    /**
     * 获取楼层
     * @author 姜伟
     * @param string $where 查询条件
     * @param string $field 查询的字段名
     * @return array 楼层
     * @todo 获取楼层
     */
    public function getStoreyWhereField($where, $field)
    {
        return $this->where($where)->getField($field);
    }

    /**
     * 获取楼层某个字段的信息
     * @author 姜伟
     * @param int $storey_id 楼层ID
     * @param string $field 查询的字段名
     * @return array 楼层
     * @todo 获取楼层某个字段的信息
     */
    public function getStoreyField($storey_id, $field)
    {
        if (!is_numeric($storey_id))   return false;
        return $this->where('storey_id = ' . $storey_id)->getField($field);
    }

    /**
     * 获取所有楼层列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 楼层列表
     * @todo 获取所有楼层列表
     */
    public function getStoreyList($where = null)
    {
        return $this->where($where)->order()->limit()->select();
    }

    /**
     * 获取所有楼层列表
     * @author 姜伟
     * @param string $where where子句
     * @param string $orderby 顺序
     * @return array 楼层列表
     * @todo 获取所有楼层列表
     */
    public function getStoreyJoinList($where = null, $orderby= '')
    {
        $data = M('incubator as a')->join('tp_storey as b on b.incubator_id = a.incubator_id')->where($where)->order($orderby)->select();

        return $data;
    }
 
    /**
     * 获取楼层信息
     * @author 姜伟
     * @param string $where where子句
     * @param string $fields 要获取的字段名
     * @return array 商品基本信息
     * @todo 根据where查询条件查找商品表中的相关数据并返回
     */
    public function getStoreyInfo($where, $fields = '')
    {
		return $this->field($fields)->where($where)->find();
    }


}
