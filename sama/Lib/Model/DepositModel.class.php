<?php
/**
 * 押金模型类
 */

class DepositModel extends Model
{

    /**
     * 构造函数
     * @author 姜伟
     * @todo 构造函数
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getDepositNum($where = '') {
        return $this->where($where)->count();
    }




    /**
     * 添加押金
     * @author 姜伟
     * @param array $arr_deposit 押金数组
     * @return boolean 操作结果
     * @todo 添加押金
     */
    public function addDeposit($arr_deposit)
    {
        if (!is_array($arr_deposit)) return false;
        return $this->add($arr_deposit);
    }

    /**
     * 删除押金
     * @author 姜伟
     * @param string $deposit_id 押金ID
     * @return boolean 操作结果
     * @todo 删除押金
     */
    public function delDeposit($deposit_id)
    {
        if (!is_numeric($deposit_id)) return false;
        return $this->where('deposit_id = ' . $deposit_id)->delete();
    }

    /**
     * 更改押金
     * @author 姜伟
     * @param int $deposit_id 押金ID
     * @param array $arr_deposit 押金数组
     * @return boolean 操作结果
     * @todo 更改押金
     */
    public function setDeposit($deposit_id, $arr_deposit)
    {
        if (!is_numeric($deposit_id) || !is_array($arr_deposit)) return false;
        return $this->where('deposit_id = ' . $deposit_id)->save($arr_deposit);
    }

    /**
     * 更改押金状态
     * @author 姜伟
     * @param int $deposit_id 押金ID
     * @param array $service_providers_status 押金数组
     * @return boolean 操作结果
     * @todo 更改押金
     */
    public function setDepositStatus($deposit_id,$service_providers_status)
    {
        if (!is_numeric($deposit_id)) return false;
        return $this->where('deposit_id = ' . $deposit_id)->save($service_providers_status);
    }

    /**
     * 获取押金
     * @author 姜伟
     * @param string $fields 查询的字段名，默认为空，取全部
     * @return array 押金
     * @todo 获取押金
     */
    public function getDepositInfo($where, $fields = null)
    {
        return $this->field($fields)->where($where)->find();
    }

    /**
     * 获取押金某个字段的信息
     * @author 姜伟
     * @param int $deposit_id 押金ID
     * @param string $field 查询的字段名
     * @return array 押金
     * @todo 获取押金某个字段的信息
     */
    public function getDepositField($deposit_id, $field)
    {
        if (!is_numeric($deposit_id))   return false;
        return $this->where('deposit_id = ' . $deposit_id)->getField($field);
    }

    /**
     * 获取押金某个字段的信息
     * @author 姜伟
     * @param int $where 条件
     * @param array $fields 查询的字段名
     * @return array 押金
     * @todo 获取押金某个字段的信息
     */
    public function getDepositWhereFields($where, $fields)
    {
        return $this->where($where)->getField($fields,true);
    }

    /**
     * 获取所有押金列表
     * @author 姜伟
     * @param string $where where子句
     * @return array 押金列表
     * @todo 获取所有押金列表
     */
    public function getDepositList($where = null,$order = null)
    {
        return $this->where($where)->order($order)->limit()->select();
    }


    public function getListData($deposit_list){

        return $deposit_list;
    }

    /**
     * 统计的方法．
     */
    public function getStatisticalList($where = '',$group = '',$field= ''){

        return $this->field($field)->where($where)->group($group)->select();
    }
}
